﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace _11_A03_KreiszahlPi
{
    class Program
    {
        static double pi = 0;
        static object piLock = new object();
        static void Main(string[] args)
        {
            Console.WriteLine(Math.PI);
            
            //double pi = 0;
            int anzahlAufrufe = 4;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            //for (int i = 1; i < anzahlAufrufe + 1; i++)
            //{
            //    pi += PI_Berechnung(i, anzahlAufrufe);
            //}

            List<Thread> threadListe = new List<Thread>();
            for (int i = 1; i < anzahlAufrufe + 1; i++)
            {
                threadListe.Add(new Thread(() => PI_Berechnung(i, anzahlAufrufe)));
                threadListe[threadListe.Count - 1].Start();
            }

            foreach (var item in threadListe)
            {
                item.Join();
            }

            sw.Stop();

            Console.WriteLine(pi);
            
            Console.WriteLine("Dauer {0:N0} Millisekunden", sw.ElapsedMilliseconds);

            Console.ReadKey(true);

            pi = 0;
            Main(args);
        }

        // Nach John Machin (http://de.wikipedia.org/wiki/John_Machin)
        //static double PI_Berechnung(int startwert, int schrittweite)
        static void PI_Berechnung(int startwert, int schrittweite)
        {
            double durchlaeufe = 1000000000;
            double x, y = 1 / durchlaeufe;
            double summe = 0;
            //double pi;

            for (double i = startwert; i <= durchlaeufe; i += schrittweite)
            {
                x = y * (i - 0.5);
                summe += 4.0/ (1 + x * x);
            }

            //pi = y * summe;
            //return pi;

            summe = y * summe;
            lock (piLock)
            {
                pi += summe;
            }

        }
    }
}
