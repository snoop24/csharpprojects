﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Drawing;

namespace _11_A07_ThreadPool_Task_Graustufen
{
    class Program
    {
        static bool cursorWait = true;
        static object outputLock = new object();
        static object counterLock = new object();
        static int runningGrayConversion = 0;

        static List<Task> usedTasks = new List<Task>();

        static void Main(string[] args)
        {
            FileSystemWatcher noirJpg = new FileSystemWatcher(@"d:\lookup\bild");
            noirJpg.Created += new FileSystemEventHandler(OnChanged);
            noirJpg.Filter = "*.jpg";
            noirJpg.EnableRaisingEvents = true;

            FileSystemWatcher noirJpeg = new FileSystemWatcher(@"d:\lookup\bild");
            noirJpeg.Created += new FileSystemEventHandler(OnChanged);
            noirJpeg.Filter = "*.jpeg";
            noirJpeg.EnableRaisingEvents = true;

            Console.WriteLine("watcher started (Esc to quit)");
            Task.Factory.StartNew(WaitCursor);
            while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;

            Console.WriteLine("completing unfinished task");
            Task.WaitAll(usedTasks.ToArray());
            cursorWait = false;

        }

        static void OnChanged(object source, FileSystemEventArgs e)
        {

            lock (outputLock)
            {
                Console.WriteLine("File: " + e.FullPath + " " + e.ChangeType); 
            }
            usedTasks.Add(Task.Factory.StartNew(() =>
            {
                try
                {
                    Bitmap picture = new Bitmap(e.FullPath);

                    ColorToGray(picture, e.FullPath.Replace(@"bild\", @"blackandwhite\BaW_"));
                }
                catch
                {
                    Thread.Sleep(500);
                    OnChanged(source, e);
                }
            }));

        }

        static void ColorToGray(Bitmap image, string grayFilename)
        {
            lock (counterLock)
            {
                runningGrayConversion++;
            }
            
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    Color color = image.GetPixel(x, y);
                    double gray = 0.2126 * color.R + 0.7152 * color.G + 0.0722 * color.B;
                    image.SetPixel(x, y, Color.FromArgb(Convert.ToInt32(gray), Convert.ToInt32(gray), Convert.ToInt32(gray)));
                }
            }
            image.Save(grayFilename); // Save gray image
            image.Dispose(); // Release image
            lock (counterLock)
            {
                runningGrayConversion--;
            }
            lock (outputLock)
            {
                Console.WriteLine("File: " + grayFilename + " saved");
            }

        }

        static void WaitCursor()
        {
            string cursorString = @"-\|/" ;
            int oldCursorLeft;
            int oldCursorTop;
            cursorWait = true;
            int index = 0;

            while (cursorWait)
            {
               // while (runningGrayConversion>0)
                //{
                    lock (outputLock)
                    {
                        //save old position
                        oldCursorLeft = Console.CursorLeft;
                        oldCursorTop = Console.CursorTop;

                        //Console.SetCursorPosition(20, 5);
                        Console.SetCursorPosition(oldCursorLeft, oldCursorTop);
                        Console.ForegroundColor = ConsoleColor.Cyan;//(ConsoleColor) ((index+1)*2);
                        Console.Write($"  {cursorString[index]} {runningGrayConversion,2} Conversion(s) running");
                        index = (index + 1) % 4;
                        Thread.Sleep(100);

                        //reset to old position and color;
                        Console.SetCursorPosition(oldCursorLeft, oldCursorTop);
                        Console.ResetColor();
                    } 
               // }
            }
        }

    }
}
