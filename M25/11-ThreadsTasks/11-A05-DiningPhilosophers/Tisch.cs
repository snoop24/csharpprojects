﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _11_A05_DiningPhilosophers
{
    class Tisch
    {
        public object tischLocked = new object();

        List<Philosoph> Gaeste = new List<Philosoph>();
        public List<bool> PlatzHatGabel { get; } = new List<bool>();

        public int Seats { get => PlatzHatGabel.Count; }

        public Tisch(int sitzPlaetze)
        {
            NeuerGast(sitzPlaetze);
        }

        public bool NimmGabeln(int seatIndex)
        {
            bool nimm = false;
                if (seatIndex < Seats)
                {
                    int left = seatIndex == 0 ? Seats - 1 : seatIndex-1;
                    int right = seatIndex == Seats - 1 ? 0 : seatIndex+1;

                    if (!PlatzHatGabel[left] && !PlatzHatGabel[right])
                    {
                        PlatzHatGabel[seatIndex] = true;
                        nimm = true;
                    }
                }
            return nimm;
        }

        public bool LegGabelnHin(int seatIndex)
        {
            bool legHin = false;
                if (seatIndex < Seats)
                {
                    if (PlatzHatGabel[seatIndex])
                    {
                        PlatzHatGabel[seatIndex] = false;
                        legHin = true;
                    }
                }
            return legHin;
        }

        private void NeuerGast(int count)
        {
                for (int i = 0; i < count; i++)
                {
                    Gaeste.Add(new Philosoph(this, Seats));
                    PlatzHatGabel.Add(false);
                }
        }

        public void DineInHell(int eternity)
        {
            List<Thread> philosoffen = new List<Thread>();

            foreach (var item in Gaeste)
            {
                Thread thread = new Thread(() => item.Philosophieren(eternity));
                thread.Start();
            }
            foreach (var item in philosoffen)
            {
                item.Join();
            }
        }

        public void Ausagbe()
        {
            Console.WriteLine(string.Join( " | ",PlatzHatGabel.Select(x => x ? 1:0).ToArray()));
        }
    }
}