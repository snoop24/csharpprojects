﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _11_A05_DiningPhilosophers
{
    class Philosoph
    {
        static Random rnd = new Random();
        static object ausgabeLocked = new object();

        public int Seat { get; }
        public Tisch Table { get; }

        public int denkPause;
        public int essPause;

        public Philosoph(Tisch table, int seat )
        {
            Seat = seat;
            Table = table;
            denkPause = rnd.Next(5000, 20001);
            essPause = rnd.Next(2500, 10001);
        }
        
        public void Philosophieren(int seconds = 0)
        {
            DateTime start = DateTime.Now;
            while (seconds == 0 || (DateTime.Now - start).TotalSeconds < seconds)
            {
                Nachdenken();
                Essen();
            }
        }

        private void Nachdenken()
        {
            // ausgabe und in ruhe nachdenken
            Ausgabe($"P{Seat} denkt für {denkPause}ms nach.", Seat);
            Thread.Sleep(denkPause);
        }

        private void Essen()
        {
            // gabeln aufnehmen
            lock (Table.tischLocked)
            {
                Ausgabe($"P{Seat} möchte essen!", Seat, false);
                Table.Ausagbe();
                while (!Table.NimmGabeln(Seat))
                {
                    Ausgabe($"P{Seat} wartet auf Gabeln!", Seat);
                    Monitor.Wait(Table.tischLocked);
                }
                Ausgabe($"P{Seat} isst{essPause}ms lang.", Seat,false);
                Table.Ausagbe();
            }

            // in ruhe essen
            Thread.Sleep(essPause);
            
            // gabeln zuruecklegen
            lock (Table.tischLocked)
            {
                Table.LegGabelnHin(Seat);
                Ausgabe($"P{Seat} hat aufgegessen.", Seat,false);
                Table.Ausagbe();
                Monitor.PulseAll(Table.tischLocked);
            }

            
        }

        private void Ausgabe(string message, int color, bool newLine = true)
        {
            lock (ausgabeLocked)
            {
                Console.ForegroundColor = (ConsoleColor)(color + 2);
                Console.Write($"{message,40}      {(newLine ? "\n" : string.Empty)}");
                Console.ResetColor();
            }
        }
    }
}
