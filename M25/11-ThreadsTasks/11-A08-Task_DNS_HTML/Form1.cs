﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Text.RegularExpressions;
using System.Diagnostics;


namespace _11_A08_Task_DNS_HTML
{
    public partial class Form1 : Form
    {
        Stopwatch sw = new Stopwatch();
        public Dictionary<string, string> addressIPsTags = new Dictionary<string, string>();
        private object addressIPsTagsLock = new object();

        public Form1()
        {
            InitializeComponent();
            labelAusgabe.Text = string.Empty;
            labelStopWatch.Text = string.Empty;
        }

        public void DisplayFromFile(string path)
        {
            sw.Start();
            this.Cursor = Cursors.WaitCursor;

            // get addresses
            string[] addressList = { "www.drheuer.de", "www.google.de", "www.computerbase.de", "www.bild.de", "www.gamestar.de", "www.zeit.de" }; //GetAddressesFromFile(path)

            // get tags
            string[] tags = { "html", "script", "table" }; // from field

            // do your stuff for addresses in new task
            Task getIt = Task.Factory.StartNew(() => HostIPTagsToList(addressList, tags));

            Task.Factory.StartNew(() =>
            {
                getIt.Wait();
                string message = string.Empty;
                addressIPsTags.Select(x => $"\t\t{x.Key.Trim(),-30}\n{x.Value}\n\n").ToList().ForEach(x => message += x);

                // display results on form
                BeginInvoke((MethodInvoker)delegate
                {
                    labelAusgabe.Text = message;
                    sw.Stop();
                    labelStopWatch.Text = $"elapse time: {sw.ElapsedMilliseconds:N0}ms";
                    sw.Reset();
                    this.Cursor = Cursors.Default;
                });
            });

        }

        public List<string> GetAddressesFromFile(string path) { return null; }

        public void HostIPTagsToList(string[] addressList, string[] tags)
        {
            // clear result list
            addressIPsTags.Clear();

            // do for all in addresslist
            foreach (var address in addressList)
            {
                string ipString = GetIPsString(address);
                string tagString = TagCountSourceCode(address, tags);
                //// get ips
                //Task<string> getIPstring = Task.Factory.StartNew(() => GetIPsString(address), TaskCreationOptions.AttachedToParent);

                //// get source code and count tags
                //Task<string> getTagString = Task.Factory.StartNew(() => TagCountSourceCode(address, tags), TaskCreationOptions.AttachedToParent);

                // address and new string(ips and tagcounts) to dict
                //lock (addressIPsTagsLock)
                {
                    //addressIPsTags.Add(address, $"{getIPstring.Result,-20}\n{getTagString.Result}");
                    addressIPsTags.Add(address, $"{ipString,-20}\n{tagString}");
                }
            }

        }

        public string TagCountSourceCode(string addressWithOutHTTP, string[] tags)
        {
            int[] tagsCount = new int[tags.Length];
            string[] tempTags = new string[tags.Length];
            Array.Copy(tags,tempTags,tags.Length);

            string html = (new WebClient()).DownloadString("http://" + addressWithOutHTTP);

            for (int i = 0; i < html.Length-1; i++)
            {
                if (html[i] == '<')
                {
                    for (int j = 0; j< tempTags.Length;j++)
                    {
                        if (i+ tempTags[j].Length < html.Length && html.Substring(i + 1, tempTags[j].Length) == tempTags[j])
                        {
                            tagsCount[j]++;
                        }
                    }
                }
            }

            for (int i = 0; i < tempTags.Length; i++)
            {
                tempTags[i] = tempTags[i] + ": " + tagsCount[i];
            }
            return string.Join(" | ", tempTags);
        }

        public string GetIPsString(string address)
        {

            string ipString = string.Empty;
            List<string> ipStrings = new List<string>();
            foreach (var item in Dns.GetHostEntry(address).AddressList)
            {
                ipStrings.Add(item.ToString().Trim());
            }

            return string.Join(" | ", ipStrings.ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DisplayFromFile("");
        }
    }
}
