﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _11_A04_ConsumerProducerProblem
    {
    class Program
        {


        static void Main(string[] args)
            {
            bool end = false;
            bool prodEnd = false;
            bool konsEnd = false;

            Console.SetBufferSize(80, 720);
            Console.SetWindowSize(80, 80);

            int prodSleep = 128;
            int konsSleep = 128;
            Lager lager1 = new Lager(5000, 500, 10000, 500);

            Thread produzent = new Thread(() =>
                    {
                        while (!prodEnd)
                            {
                            lager1.Produzieren();
                            System.Threading.Thread.Sleep(prodSleep);
                            }
                    }
                );

            Thread konsument = new Thread(() =>
                    {
                        while (!konsEnd)
                            {
                            lager1.Entnehmen();
                            System.Threading.Thread.Sleep(konsSleep);
                            }
                    }
                );

            produzent.Start();
            konsument.Start();

            ConsoleKey input;
            bool tempProd;
            bool tempKons;
            while (!end)
                {
                input = Console.ReadKey(true).Key;
                lock (lager1.lagerLock)
                    {
                    switch (input)
                        {
                        case ConsoleKey.Escape:
                            prodEnd = true;
                            konsEnd = true;
                            end = true;
                            break;
                        case ConsoleKey.Add:
                            if (prodEnd)
                                {
                                prodEnd = false;
                                produzent = new Thread(() =>
                                     {
                                         while (!prodEnd)
                                             {
                                             lager1.Produzieren();
                                             System.Threading.Thread.Sleep(prodSleep);
                                             }
                                     }
                                     );
                                produzent.Start();
                                }
                            else
                                {
                                prodEnd = true;
                                }
                            break;
                        case ConsoleKey.Subtract:
                            if (konsEnd)
                                {
                                konsEnd = false;
                                konsument = new Thread(() =>
                                    {
                                        while (!konsEnd)
                                            {
                                            lager1.Entnehmen();
                                            System.Threading.Thread.Sleep(konsSleep);
                                            }
                                    }
                                    );
                                konsument.Start();
                                }
                            else
                                {
                                konsEnd = true;
                                }
                            break;
                        case ConsoleKey.PageUp:
                            if (konsSleep / 2 > 0 && prodSleep / 2 > 0)
                                {
                                konsSleep /= 2;
                                prodSleep /= 2;
                                }
                            break;
                        case ConsoleKey.PageDown:
                            if (konsSleep * 2 < 2049 && prodSleep * 2 < 2049)
                                {
                                konsSleep *= 2;
                                prodSleep *= 2;
                                }
                            break;
                        default:
                            break;
                        }
                    }
                }

            konsument.Join();
            produzent.Join();
            Console.WriteLine("\n\tstopped! press any key to end program");
            Console.ReadKey(true);
            }
        }
    }
