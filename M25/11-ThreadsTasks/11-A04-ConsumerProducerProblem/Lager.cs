﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _11_A04_ConsumerProducerProblem
    {
    class Lager
        {
        static Random rnd = new Random();
        internal object lagerLock = new object();

        private int maxBestand = 10000;
        private int minBestand = 500;
        private int bestandPuffer = 100;
        private int bestand = 0;
        public int Bestand { get=> bestand; }
        public object  LagerLock { get => lagerLock; }
        private bool LagerVoll { get => bestand > maxBestand-bestandPuffer; }
        private bool LagerLeer { get => bestand == 0; }

        public Lager(int bestand, int minB, int maxB, int pufB)
            {
            this.bestand = bestand;
            minBestand = minB;
            maxBestand = maxB;
            bestandPuffer = pufB;
            }

        public void Produzieren()
            {
            lock (lagerLock)
                {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                if (LagerVoll)
                    {
                    Console.WriteLine( "Keine Produktion. Das Lager ist voll!");
                    Monitor.Wait(lagerLock);
                    }
                else
                    {
                    int amount = rnd.Next(1, Math.Min(101, maxBestand - bestand));
                    bestand += amount;
                    Console.WriteLine($"Produziert: {amount,4}, Neuer Bestand: {bestand,6}");
                    
                    if (bestand > minBestand && bestand - amount <=minBestand)
                        {
                        Monitor.PulseAll(lagerLock);
                        }
                    }
                Console.ResetColor();
                }
            }

        public void Entnehmen()
            {
           
            lock (lagerLock)
                {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                if (LagerLeer)
                    {
                    Console.WriteLine("Keine Entnahme. Das Lager hat zu wenig Bestand!");
                    Monitor.Wait(lagerLock);
                    }
                else
                    {
                    int amount = rnd.Next(1, Math.Min(101, bestand));
                    bestand -= amount;
                    Console.WriteLine($"Entnommen:  {amount,4}, Neuer Bestand: {bestand,6}");

                    if (bestand < maxBestand-bestandPuffer && bestand + amount >= maxBestand - bestandPuffer)
                        {
                        Monitor.PulseAll(lagerLock);
                        }
                    }
                Console.ResetColor();
                }
            
            }
        }
    }
