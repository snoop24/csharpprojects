﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace _11_A02_Dateivergleich
{
    class Program
    { 
        static void Main(string[] args)
        {
            string[] files = {
                @"Bocholt.txt"
                ,@"Bocholt - Kopie.txt"
            };

            List<string> fileContents = new List<string>();

            List<Thread> threadList = new List<Thread>();

            foreach (var path in files)
            {
                threadList.Add(new Thread(() => ReadFileToList(path,fileContents)));
                threadList[threadList.Count - 1].Start();
            }

            foreach (var thread in threadList)
            {
                thread.Join();
            }

            for (int i = 0; i <fileContents.Count-1; i++)
            {
                Console.WriteLine($"Contents of {files[i]} and {files[i+1]} are equal?\n\t{fileContents[i]==fileContents[i+1]}\n");
            }

            Console.WriteLine("press any key to continue ...");
            Console.ReadKey(true);
        }

        static void ReadFileToList(string filePath, List<string> fileContents)
        {
            try
            {
                fileContents.Add(new StreamReader(filePath).ReadToEnd());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
