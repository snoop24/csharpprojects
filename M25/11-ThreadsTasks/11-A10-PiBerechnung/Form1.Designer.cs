﻿namespace _11_A10_PiBerechnung
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCalcPI = new System.Windows.Forms.Button();
            this.buttonCancelCalc = new System.Windows.Forms.Button();
            this.labelOutPI = new System.Windows.Forms.Label();
            this.progressBarCalcPi = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // buttonCalcPI
            // 
            this.buttonCalcPI.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCalcPI.Location = new System.Drawing.Point(13, 13);
            this.buttonCalcPI.Name = "buttonCalcPI";
            this.buttonCalcPI.Size = new System.Drawing.Size(75, 23);
            this.buttonCalcPI.TabIndex = 0;
            this.buttonCalcPI.Text = "Calc Pi";
            this.buttonCalcPI.UseVisualStyleBackColor = true;
            this.buttonCalcPI.Click += new System.EventHandler(this.buttonCalcPI_Click);
            // 
            // buttonCancelCalc
            // 
            this.buttonCancelCalc.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancelCalc.Location = new System.Drawing.Point(95, 13);
            this.buttonCancelCalc.Name = "buttonCancelCalc";
            this.buttonCancelCalc.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelCalc.TabIndex = 1;
            this.buttonCancelCalc.Text = "Cancel";
            this.buttonCancelCalc.UseVisualStyleBackColor = true;
            this.buttonCancelCalc.Click += new System.EventHandler(this.buttonCancelCalc_Click);
            // 
            // labelOutPI
            // 
            this.labelOutPI.AutoSize = true;
            this.labelOutPI.Location = new System.Drawing.Point(13, 43);
            this.labelOutPI.Name = "labelOutPI";
            this.labelOutPI.Size = new System.Drawing.Size(34, 13);
            this.labelOutPI.TabIndex = 2;
            this.labelOutPI.Text = "Pi = ?";
            // 
            // progressBarCalcPi
            // 
            this.progressBarCalcPi.Location = new System.Drawing.Point(13, 74);
            this.progressBarCalcPi.Name = "progressBarCalcPi";
            this.progressBarCalcPi.Size = new System.Drawing.Size(157, 10);
            this.progressBarCalcPi.Step = 1;
            this.progressBarCalcPi.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancelCalc;
            this.ClientSize = new System.Drawing.Size(182, 96);
            this.Controls.Add(this.progressBarCalcPi);
            this.Controls.Add(this.labelOutPI);
            this.Controls.Add(this.buttonCancelCalc);
            this.Controls.Add(this.buttonCalcPI);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCalcPI;
        private System.Windows.Forms.Button buttonCancelCalc;
        private System.Windows.Forms.Label labelOutPI;
        private System.Windows.Forms.ProgressBar progressBarCalcPi;
    }
}

