﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace _11_A10_PiBerechnung
{
    public partial class Form1 : Form
    {

        CancellationTokenSource cts;


        public Form1()
        {
            InitializeComponent();
            buttonCancelCalc.Enabled = false;
            Text = " Pi";
        }

        private void buttonCalcPI_Click(object sender, EventArgs e)
        {
            CalcPi();
        }

        private async void CalcPi()
        {
            // display on forms
            buttonCalcPI.Enabled = false;
            buttonCancelCalc.Enabled = true;
            labelOutPI.Text = "working ...";
            Cursor = Cursors.WaitCursor;
            progressBarCalcPi.Value = 0;
            Progress<int> barHandler = new Progress<int>(n => { progressBarCalcPi.Value = n; });

            cts = new CancellationTokenSource();


            //labelOutPI.Text = "Pi ≈ " + (await Task.Run(() =>
            // {
            //     string piMessage;
            //     try
            //     {
            //         double sum = 0.0;
            //         double step = 1e-9;
            //         for(int i=0; i< 1000000000;i++)
            //         {
            //             cts.Token.ThrowIfCancellationRequested();

            //             if ((i + 1) % 10000000 == 0)
            //             {
            //                 (barHandler as IProgress<int>).Report((int)((i + 1) / 10000000));
            //             }

            //             double x = (i + 0.5) * step;
            //             sum = sum + 4.0 / (1.0 + x * x);
            //         }
            //         piMessage = (sum * step).ToString();
            //     }
            //     catch (OperationCanceledException e)
            //     {
            //         piMessage = "Pi calculation was cancelled.";
            //         (barHandler as IProgress<int>).Report(0);
            //     }

            //     return piMessage;
            // }));

            //Cursor = Cursors.Default;
            //buttonCalcPI.Enabled = true;
            //buttonCancelCalc.Enabled = false;

            try
            {
                labelOutPI.Text = "Pi ≈ " + (await Task.Run(() =>
                 {
                     double sum = 0.0;
                     double step = 1e-9;
                     for (int i = 0; i < 1000000000; i++)
                     {
                         //cts.Token.ThrowIfCancellationRequested();

                         if ((i + 1) % 10000000 == 0)
                         {
                             (barHandler as IProgress<int>).Report((i + 1) / 10000000);
                         }

                         double x = (i + 0.5) * step;
                         sum = sum + 4.0 / (1.0 + x * x);
                     }
                     return sum * step;
                 })).ToString();
        }
            catch (OperationCanceledException e)
            {
                labelOutPI.Text = "Pi calculation was cancelled.";
                progressBarCalcPi.Value = 0;
            }
            finally
            {
                Cursor = Cursors.Default;
                buttonCalcPI.Enabled = true;
                buttonCancelCalc.Enabled = false;
            }
        }

        private void buttonCancelCalc_Click(object sender, EventArgs e)
        {
            cts.Cancel();
        }

    }
}
