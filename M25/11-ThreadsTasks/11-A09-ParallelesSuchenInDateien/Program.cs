﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_A09_ParallelesSuchenInDateien
{
    class Program
    {
        static ConcurrentStack<string> filesThatMatch = new ConcurrentStack<string>();

        static void Main(string[] args)
        {

            Console.Clear();

            Console.Write("\tenter search directory (default d:\\lookup): ");
            string dir = Console.ReadLine();
            dir = dir == string.Empty ? @"d:\lookup" : dir;

            Console.Write("\tenter search string (default \"test\"): ");
            string search = Console.ReadLine();
            search = search == string.Empty ? "test" : search;


            Parallel.ForEach(GetFilesInDir(dir), file =>
            {
                if (FileContainsString(file, search))
                {
                    filesThatMatch.Push($"{DateTime.Now.Ticks.ToString()}: {file}");
                }
            });

            Console.WriteLine($"\n\tfiles in \"{dir}\" that contain \"{search}\"");
            while (!(filesThatMatch.Count == 0))
            {
                string result;
                while (!filesThatMatch.TryPop(out result)) ;
                Console.WriteLine(result);
            }


            Console.WriteLine("\n\n\t (esc) to exit, (any key) to continue ...");
            if (Console.ReadKey(true).Key == ConsoleKey.Escape)
            {
                Main(args);
            }
        }

        static string[] GetFilesInDir(string path)
        {
            string[] getFilesInDir = { string.Empty };

            try
            {
                getFilesInDir = Directory.GetFiles(path);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return getFilesInDir;
        }

        static bool FileContainsString(string path, string search)
        {
            bool contains = false;

            try
            {
                contains = File.ReadAllText(path).Contains(search);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return contains;
        }
    }
}
