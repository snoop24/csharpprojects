﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace _11_A01_ThreadingGrundlagen
{
    class Program
    {
        const string vergleich = "abcdefghijklmnopqrstuvwxyzäöüß0123456789";

        static Dictionary<string, List<string>> outPutList = new Dictionary<string,List<string>>();

        static void Main(string[] args)
        {

            foreach (var item in (new DirectoryInfo(Directory.GetCurrentDirectory())).GetFiles())
            {
                if (item.Extension == ".txt")
                {
                    Console.WriteLine(item.Name+": wird analysiert\n");
                    Thread thread = new Thread(new ParameterizedThreadStart(ReadDatei));
                    thread.Start(item);
                }
            }
            Console.ReadKey(true);
        }

        static void ReadDatei(object o)
        {
            FileInfo file = o as FileInfo;
            string path = file.FullName;

            using (StreamReader readFile = new StreamReader(path))
            {
                using (StreamWriter writeFile = new StreamWriter(path.Replace(".txt", ".freq")))
                {
                    foreach (var item in FrequenzAnalyse(readFile.ReadToEnd()))
                    {
                        writeFile.WriteLine(item);
                    }
                    Console.WriteLine(file.Name + ": Analyse abgeschlossen\n");
                }
            }
        }

        static List<string> FrequenzAnalyse(string test)
        {
            // clean
            test = CleanToLower(test);

            // declare
            List<string> analyse = new List<string>();
            int[] letterCount = new int[vergleich.Length];
            int letters = 0;
            int count = 0;

            // count 
            foreach (char letter in test)
            {
                int i = 0;
                i = vergleich.IndexOf(letter);
                if (i > -1)
                {
                    letterCount[i]++;
                }
            }

            // create output
            for (int i = 0; i < letterCount.Length; i++)
            {
                if (letterCount[i] > 0)
                {
                    analyse.Add(string.Format("{0}={1}", vergleich[i], letterCount[i]));
                    letters++;
                    count += letterCount[i];
                }
            }
            analyse.Add(string.Format("{1} insgesamt : {0} unterschiedliche", letters, count));

            // return
            return analyse;
        }

        static string CleanToLower(string test)
        {
            StringBuilder temp = new StringBuilder();

            foreach (char letter in test.ToLower())
            {
                if (vergleich.Contains(letter))
                {
                    temp.Append(letter);
                }
            }
            return temp.ToString();
        }

    }


}
