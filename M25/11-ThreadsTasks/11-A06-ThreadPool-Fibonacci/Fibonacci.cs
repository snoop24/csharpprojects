﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading;

namespace _11_A06_ThreadPool_Fibonacci
{
    class Fibonacci
    {
        static object fibLock = new object();

        const ulong a0 = 0;
        const ulong a1 = 1;

        public ulong Value { get; }
        public int Stelle { get; }

        public Fibonacci(int stelle,object o)
        {
            Stopwatch sw = new Stopwatch();
            
            Stelle = stelle;

            sw.Start();
            //Value = FibonacciFast(Stelle, a1, a0);
            Value = FibonacciSlow(Stelle);
            sw.Stop();
            string output = $"{Value,15} : {sw.ElapsedMilliseconds,8:#,###}ms";
            //Console.WriteLine($"{ Stelle,2}:{output}");

            lock (fibLock)
            {
                Program.fibonacciNumbers.Add(Stelle, output);
            }
            ((ManualResetEvent)o).Set();

            
        }

        private static ulong FibonacciFast(int count, ulong b, ulong a)
        {
            return count == 1 ? a : FibonacciFast(count - 1, b + a, b);
        }

        private static ulong FibonacciSlow(int count)
        {
            return count > 2 ? (FibonacciSlow(count - 1) + FibonacciSlow(count - 2)) : count == 2 ? a1 : a0;
        }
    }
}
