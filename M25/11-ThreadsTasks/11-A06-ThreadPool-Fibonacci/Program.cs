﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _11_A06_ThreadPool_Fibonacci
{
    class Program
    {
        static public Dictionary<int,string> fibonacciNumbers = new Dictionary<int,string>();

        static bool cursorWait = false;
        static bool ready = false;

        static void Main(string[] args)
        {
            List<ManualResetEvent> manualResetEvents = new List<ManualResetEvent>();
            for (int i = 30; i < 40; i++)
            {
                int temp = i+1;
                manualResetEvents.Add(new ManualResetEvent(false));
                ThreadPool.QueueUserWorkItem(new WaitCallback((object o)=> new Fibonacci(temp,o)),manualResetEvents[manualResetEvents.Count-1]);
            }

            (new Thread(WaitCursor)).Start();

            WaitHandle.WaitAll(manualResetEvents.ToArray());
            cursorWait = false;

            while (!ready) ;
            Console.WriteLine("threads finished work");

            fibonacciNumbers.OrderBy(item => item.Key).Select(item => ($"{item.Key,2} : {item.Value} ")).ToList().ForEach(item => Console.WriteLine(item));

            Console.WriteLine("finished");
            Console.ReadKey(true);
        }

        static void WaitCursor()
        {
            string cursorString = @"-\|/";
            int oldCursorLeft = Console.CursorLeft;
            int oldCursorTop = Console.CursorTop;
            cursorWait = true;
            int index = 0;

            while (cursorWait)
            {
                //Console.SetCursorPosition(20, 5);
                Console.SetCursorPosition(oldCursorLeft, oldCursorTop);
                Console.ForegroundColor = ConsoleColor.Cyan;//(ConsoleColor) ((index+1)*2);
                Console.Write(cursorString[index]);
                index = (index + 1) % 4;
                Thread.Sleep(200);
            }
            Console.SetCursorPosition(oldCursorLeft, oldCursorTop);
            Console.ResetColor();
            ready = true;
        }
        
    }
}
