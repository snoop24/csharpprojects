﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace _12_A01_SQLClientLesenAendernLoeschen
{
    class Program
    {
        static string sqlConnectionString;
        static SqlConnection connection = new SqlConnection();
        static SqlCommand sqlCom = connection.CreateCommand();
        static SqlDataReader reader;
        static bool connectionOpen = false;

        static string[] fieldNames;

        static bool validMenuSelection = true;

        static void Main(string[] args)
        {
            Console.SetBufferSize(120, 720);
            Console.SetWindowSize(120, 50);

            bool end = false;
            // create standard connection string
            BuildSqlConnectionString(
                "Data Source", @".\SQLEXPRESS"
                , "Initial Catalog", "NETDB"
                , "Integrated Security", "True"
            );
            string tableName = "[dbo].[tblKontakt]";
            Status();
            PrintAllFromTable(tableName);
            Wait();
            Console.Clear();


            // set menu string[]
            string[,] menuString = {
                { "0"," Type a valid number!!!   " } // warning if input was invalid
                ,{ "1"," Alle Kontakte anzeigen" }  // first point
                ,{ "2"," Kontakt hinzufügen" }
                ,{ "3"," Kontakt löschen" }
                ,{ "4"," Kontakt editieren" }

                ,{ "\n 99"," EXIT" } // exit
            };
            // run until end is selected
            
            while (!end)
            {

                Console.WriteLine();
                switch (PrintMenu(menuString))
                {
                    case 1:
                        PrintAllFromTable(tableName);
                        break;
                    case 2:
                        AddToTableWithFirstIsIDENTITY(tableName);
                        break;
                    case 3:
                        DeleteFromTable(tableName);
                        break;
                    case 4:
                        EditFromTable(tableName);
                        break;
                    case 99:
                        end = true;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\n good bye!");
                        Console.ResetColor();
                        break;
                    default:
                        validMenuSelection = false;
                        break;
                }
                if (validMenuSelection) { Wait(); }
            }

            End();
        }

        static void EditFromTable(string tableName)
        {
            PrintAllFromTable(tableName);

            Console.WriteLine();
            Console.Write("Welcher Kontakt soll bearbeitet werden? ID = ");

            int editID = 0;
            int.TryParse(Console.ReadLine(), out editID);
            
            string[] editEntry = new string[fieldNames.Length - 1];

            Console.WriteLine("NeuerEintrag\n");
            Console.WriteLine($"{fieldNames[0],15} = {editEntry}");
            for (int i = 0; i < editEntry.Length; i++)
            {
                Console.Write($"{fieldNames[i + 1],15} = ");
                editEntry[i] = Console.ReadLine();
            }


            OpenSqlConnection();
            sqlCom = connection.CreateCommand();
            sqlCom.CommandText = $"UPDATE {tableName} SET {fieldNames[1]}='{editEntry[0]}' WHERE KontaktID = '{editID}'";

            Console.WriteLine($" {sqlCom.ExecuteNonQuery()} Eintrag/Einträge geändert ");

            CloseSqlConnection();
            sqlCom.CommandText = string.Empty;

            PrintAllFromTable(tableName);
        }

        static void AddToTableWithFirstIsIDENTITY(string tableName)
        {
            Console.Clear();
            Status();
            
            string[] newEntry = new string[fieldNames.Length-1];
            
            Console.WriteLine("NeuerEintrag\n");
            Console.WriteLine($"{fieldNames[0],15} = (automatisch)");
            for (int i = 0; i < newEntry.Length; i++)
            {
                Console.Write($"{fieldNames[i+1], 15} = ");
                newEntry[i] = Console.ReadLine();
            }

            OpenSqlConnection();
            sqlCom = connection.CreateCommand();
            sqlCom.CommandText = $"INSERT INTO {tableName} VALUES ('{string.Join ("','",newEntry)}')";

            Console.WriteLine($" {sqlCom.ExecuteNonQuery()} Eintrag/Einträge hinzugefügt ");

            CloseSqlConnection();
            sqlCom.CommandText = string.Empty;

            PrintAllFromTable(tableName);
        }

        static void DeleteFromTable(string tableName)
        {
            PrintAllFromTable(tableName);

            Console.WriteLine();
            Console.Write("Welcher Kontakt soll gelöscht werden? ID = ");

            int deleteID = 0;
            int.TryParse(Console.ReadLine(), out deleteID);
            
            OpenSqlConnection();
            sqlCom = connection.CreateCommand();
            sqlCom.CommandText = $"DELETE FROM {tableName} WHERE KontaktID = '{deleteID}'";

            Console.WriteLine($" {sqlCom.ExecuteNonQuery()} Eintrag/Einträge gelöscht ");

            CloseSqlConnection();
            sqlCom.CommandText = string.Empty;

            PrintAllFromTable(tableName);
        }

        static void PrintAllFromTable(string tableName)
        {


            OpenSqlConnection();

            sqlCom = connection.CreateCommand();

            sqlCom.CommandText = $"SELECT * FROM {tableName}";

            reader = sqlCom.ExecuteReader();

            Status();
            fieldNames = new string[reader.FieldCount];
            for (int i = 0; i < reader.FieldCount; i++)
            {
                fieldNames[i] = reader.GetName(i);
                Console.Write($"{reader.GetName(i),-20}");
            }
            Console.WriteLine();

            Console.WriteLine(new string('─', reader.FieldCount * 20));
            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    Console.Write($"  {reader[reader.GetName(i)],-20}");
                }
                Console.WriteLine();

            }
            reader.Close();

            CloseSqlConnection();
            sqlCom.CommandText = string.Empty;
        }

        static void BuildSqlConnectionString(params string[] parameter)
        {
            SqlConnectionStringBuilder connSB = new SqlConnectionStringBuilder();
            for (int i = 0; i < parameter.Length; i += 2)
            {
                connSB[parameter[i]] = parameter[i + 1];
            }
            sqlConnectionString = connSB.ToString();
        }

        static void OpenSqlConnection()
        {
            try
            {
                connection = new SqlConnection(sqlConnectionString);
                connection.Open();
                connectionOpen = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void CloseSqlConnection()
        {
            if (connectionOpen)
            {
                connection.Close();
                connectionOpen = false;
            }
        }

        static void Status()
        {
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.DarkCyan;

            Console.WriteLine($" ConnectionString: \n    {sqlConnectionString}\n Status: {(connectionOpen ? "Open" : "Closed")}");
            Console.WriteLine($" Command Text: \n    {sqlCom.CommandText}");
            Console.WriteLine(new string('─', Console.WindowWidth));
            Console.WriteLine();
            Console.ResetColor();
        }

        static int PrintMenu(string[,] menuString)
        {
            //show status
            Status();

            int selection = 0;

            if (!validMenuSelection)
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(menuString[0, 1]);
                Console.ResetColor();
                validMenuSelection = true;

                Console.WriteLine();
            }

            for (int i = 1; i < menuString.GetLength(0); i++)
            {
                Console.WriteLine($" {menuString[i, 0],2}: {menuString[i, 1]}");
            }
            Console.Write("\nYour choice: ");
            int.TryParse(Console.ReadLine(), out selection);
            return selection;
        }

        static void End()
        {
            Console.CursorVisible = false;
            Console.Write("\n\n\nthe end is near (any key)...");
            Console.ReadKey(true);
            Console.CursorVisible = true;
        }

        static void Wait()
        {
            Console.WriteLine("\n\npress any key ...");
            Console.ReadKey(true);
        }

    }
}
