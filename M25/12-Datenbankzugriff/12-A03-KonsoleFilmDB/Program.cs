﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace _12_A03_KonsoleFilmDB
{
	class Program
	{
		static SqlConnection connection;
		static string connString = string.Empty;
		static SqlDataReader sqlReader;
		static SqlCommand sqlCom;
		static SqlCommand sqlComStoredProcedure;


		static string[] fieldNames;

		static void Main(string[] args)
		{
			// console
			Console.SetBufferSize(120, 720);
			Console.SetWindowSize(120, 50);

			// connection string
			connString = ConfigurationManager.ConnectionStrings["SQLEXPRESS"]?.ConnectionString;


			using (connection = new SqlConnection(connString))
			{
				try
				{
					connection.Open();

				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}
				sqlCom = connection.CreateCommand();
				sqlComStoredProcedure = connection.CreateCommand();
				sqlComStoredProcedure.CommandType = CommandType.StoredProcedure;

				string tableName = "[dbo].[film]";


				// initial display
				PrintAllFromTableWithFirstIsID(tableName);
				Wait();

				// invoke menu
				MainMenu(tableName);

			}


			End();
		}

		static void MainMenu(string tableName)
		{
			bool end = false;




			// set menu string[]
			string[,] menuStrings = {
				{"\n Ihre Wahl: "," Bitte korrekte Wahl treffen!   " } // warning if input was invalid
				,{ "1",": Alle Filme anzeigen" }  // first point
				,{ "2",": Film editieren" }
				,{ "3",": Film hinzufügen" }
				,{ "4",": Film löschen" }
				,{ "","" }
				,{ "0",": EXIT" } // exit
			};
			string menuTitle = "DB Filme";
			string result = "0";
			while (!end)
			{
				try
				{
					result = Menu(menuTitle, menuStrings, result);
					// do according to input
					switch (result)
					{
						case "0":
							end = true;
							break;
						case "1":
							PrintAllFromTableWithFirstIsID(tableName);
							break;
						case "2":
							Console.WriteLine(2);
							break;
						case "3":
							AddObjectToTable(tableName);
							break;
						case "4":
							DeleteObjectByID(tableName);
							break;
						case "5":
							Console.WriteLine(5);
							break;
						// invalid input
						default:
							result = string.Empty;
							break;
					}

					// wait until next menu is shown if valid input and not end
					if (result != string.Empty && !end) { Wait(); }
				}
				catch (Exception e)
				{
					Console.BackgroundColor = ConsoleColor.Red;
					Console.WriteLine("\n  "+e.Message);
					Console.ResetColor();
					Wait();
				}
			}
		}



		#region: tools
		// menu print
		static string Menu(string title, string[,] menuStrings, string result)
		{
			string message = menuStrings[0, 1];

			// clear and print menu
			Console.Clear();
			Console.BackgroundColor = ConsoleColor.DarkGreen;
			Console.Write($"\n   {title}: ");
			Console.BackgroundColor = (result != string.Empty ? ConsoleColor.Blue : ConsoleColor.Red);
			Console.WriteLine((result != string.Empty ? string.Empty : message));
			Console.ResetColor();
			Console.WriteLine();
			for (int i = 1; i < menuStrings.GetLength(0); i++)
			{
				Console.WriteLine($" {menuStrings[i, 0],2} {menuStrings[i, 1]}");
			}

			// get user input
			Console.Write(menuStrings[0, 0]);
			result = Console.ReadLine();
			Console.WriteLine();

			return result;
		}

		// end wait and wait
		static void End()
		{
			Console.CursorVisible = false;
			Console.Write("\n\n\nthe end is near (any key)...");
			Console.ReadKey(true);
			Console.CursorVisible = true;
		}

		static void Wait()
		{
			Console.WriteLine("\n\npress any key ...");
			Console.ReadKey(true);
		}
		#endregion

		#region: database methods
		static void PrintAllFromTableWithFirstIsID(string tableName, bool orderedByName = false)
		{
			try
			{
				//sqlCom.CommandText = $"SELECT * FROM {tableName} " + (orderedByName ? "ORDER BY Titel" : string.Empty + ";");
				sqlCom.CommandText = "usp_select_all_films";
				using (sqlReader = sqlCom.ExecuteReader())
				{
					fieldNames = new string[sqlReader.FieldCount];

					Console.ForegroundColor = ConsoleColor.Yellow;
					for (int i = 0; i < sqlReader.FieldCount; i++)
					{
						fieldNames[i] = sqlReader.GetName(i);

						if (i == 0)
						{
							Console.Write($"  {fieldNames[i],-4}");
						}
						else
						{
							Console.Write($"  {fieldNames[i],-20}");
						}

					}

					Console.WriteLine();
					Console.WriteLine(new string('─', (sqlReader.FieldCount - 1) * 22 + 4));
					Console.ResetColor();

					while (sqlReader.Read())
					{
						for (int i = 0; i < sqlReader.FieldCount; i++)
						{
							if (i == 0)
							{
								Console.Write($"  {sqlReader[sqlReader.GetName(i)],-4}");
							}
							else
							{
								Console.Write($"  {sqlReader[sqlReader.GetName(i)],-20}");
							}
						}
						Console.WriteLine();

					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				Console.WriteLine();
			}
			finally
			{
				sqlCom.CommandText = string.Empty;
			}
		}

		private static void AddObjectToTable(string tableName)
		{
			string[,] fields = { { "Titel", "" }, { "Jahr", "" }, { "Regisseur", "" } };

			sqlComStoredProcedure.Parameters.Clear();
			sqlComStoredProcedure.CommandText = "usp_insert_film";

			Console.WriteLine("Bitte Daten des Films eingeben.");

			for (int i = 0; i < fields.GetLength(0); i++)
			{
				Console.Write(fields[i, 0] + ": ");
				fields[i, 1] = Console.ReadLine();
				sqlComStoredProcedure.Parameters.AddWithValue(fields[i, 0], fields[i, 1]);
			}

			if (int.TryParse(fields[1, 1], out int result))
			{

				sqlComStoredProcedure.ExecuteNonQuery();

				Console.WriteLine();
				PrintAllFromTableWithFirstIsID(tableName);
			}
			else
			{
				Console.BackgroundColor = ConsoleColor.Red;
				Console.WriteLine("\n   Fehler bei der Eingabe des Jahres!   ");
				Console.ResetColor();
			}
			
		}


		private static void DeleteObjectByID(string tableName)
		{
			sqlComStoredProcedure.Parameters.Clear();
			sqlComStoredProcedure.CommandText = "usp_delete_film";

			PrintAllFromTableWithFirstIsID(tableName);

			Console.WriteLine("Bitte ID des zu löschenden Eintrags angeben: ");
			if (int.TryParse(Console.ReadLine(), out int result))
			{
				sqlComStoredProcedure.Parameters.AddWithValue("ID", result);

				sqlComStoredProcedure.ExecuteNonQuery();

				Console.WriteLine();
				PrintAllFromTableWithFirstIsID(tableName);
			}
			else
			{
				Console.BackgroundColor = ConsoleColor.Red;
				Console.WriteLine("\n   Fehler bei der Eingabe!   ");
				Console.ResetColor();
			}




		}
		#endregion
	}
}
