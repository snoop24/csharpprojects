﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace _12_A02_SQLClientExportierenImportierenAendern
{
    class Program
    {
        static string sqlConnectionString;
        static SqlConnection connection;
        static SqlCommand sqlCom;
        static SqlDataReader reader;

        static string tblName = "[dbo].[Kontakte]";
        static string[] fieldNames;

        static bool end = false;
        
        // main
        static void Main(string[] args)
        {
            // set up console
            Console.SetBufferSize(120, 720);
            Console.SetWindowSize(120, 50);

            // create standard connection string
            BuildSqlConnectionString(
                "Data Source", @".\SQLEXPRESS"
                , "Initial Catalog", "NETDB"
                , "Integrated Security", "True"
            );
            string importFileName = @"Import.csv";
            string exportFileName = @"Export.csv";
            string changeFileName = @"Changes.txt";

            // open connection to db
            using (connection = new SqlConnection(sqlConnectionString))
            {
                // open connnection
                connection.Open();
                // create command for connection
                sqlCom = connection.CreateCommand();

                Console.Clear();

                PrintAllFromTableWithFirstIsID(tblName, false);
                Wait();

                Console.Clear();

                // invoke menu ( until end is selected)
                string menu = "" +
                    "\n  0: Ende" +
                    "\n" +
                    "\n  1: Ausgabe" +
                    "\n  2: Import" +
                    "\n  3: Export" +
                    "\n  4: Change" +
                    "\n  5: Delete IDs > 99" +
                    "\n  6: Insert Initial Values" +
                    "\n  7: Delete All" +
                    "\n" +
                    "\n Ihre Wahl: ";
                string result = string.Empty;
                while (!end)
                {

                    result = Menu(menu, result);
                    // do according to input
                    switch (result)
                    {
                        case "0":
                            end = true;
                            break;
                        case "1":
                            PrintAllFromTableWithFirstIsID(tblName, true);
                            break;
                        case "2":
                            ImportToTableFromFile(tblName, importFileName);
                            break;
                        case "3":
                            ExportToFileFromTable(tblName, exportFileName);
                            break;
                        case "4":
                            ChangeTableByFile(tblName, changeFileName);
                            break;
                        case "5":
                            DelelteAllIDGreate99(tblName);
                            break;
                        case "6":
                            InsertInitialValuesToTable(tblName);
                            break;
                        case "7":
                            DelelteAllID(tblName);
                            break;
                        // invalid input
                        default:
                            result = string.Empty;
                            break;
                    }

                    // wait until next menu is shown if valid input and not end
                    if (result != string.Empty && !end) { Wait(); }
                }

                // close connection to db
            }

            End();
        }

        // insert initial values
        private static void InsertInitialValuesToTable(string tblName)
        {
            try
            {
                sqlCom.CommandText = 
                    $"INSERT INTO Kontakte VALUES" +
                    $"(1, 'Adenauer', 'Konrad', '0201 32472343', 'konrad@kanzler.de', 'GESCHÄFTLICH')," +
                    $"(2, 'Erhard', 'Ludwig', '0201 3547343', 'ludwig@kanzler.de', 'GESCHÄFTLICH')," +
                    $"(3, 'Kiesinger', 'Kurt-Georg', '0201 46472341', 'kurt@kanzler.de', 'GESCHÄFTLICH')," +
                    $"(4, 'Brandt', 'Willy', '0201 21372343', 'willy@kanzler.de', 'GESCHÄFTLICH')," +
                    $"(5, 'Schmidt', 'Helmut', '0201 3223213', 'smokey@kanzler.de', 'GESCHÄFTLICH'); ";
                sqlCom.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                sqlCom.CommandText = string.Empty;
            }

            PrintAllFromTableWithFirstIsID(tblName, false);
        }

        // delete all id 
        private static void DelelteAllID(string tblName)
        {
            try
            {
                sqlCom.CommandText = $"DELETE FROM{tblName};";
                sqlCom.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                sqlCom.CommandText = string.Empty;
            }

            PrintAllFromTableWithFirstIsID(tblName, false);
        }

        // delete all id greater 99
        private static void DelelteAllIDGreate99(string tblName)
        {
            try
            {
                sqlCom.CommandText = $"DELETE FROM{tblName} WHERE ID > 99;";
                sqlCom.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                sqlCom.CommandText = string.Empty;
            }

            PrintAllFromTableWithFirstIsID(tblName, false);
        }

        // change by commands in given file
        static void ChangeTableByFile(string tblName, string changeFileName)
        {
            //try
            //{
            //    using (StreamReader sr = new StreamReader(changeFileName))
            //    {
            //        string[] entry = new string[fieldNames.Length];
            //        List<string> values = new List<string>();
            //        while (!sr.EndOfStream)
            //        {
            //            string temp = sr.ReadLine();
            //            if (int.TryParse(temp[0].ToString(), out int result))
            //            {
            //                temp = "('" + temp.Replace("|", "','") + "')";
            //                values.Add(temp);
            //            }
            //        }
            //        sqlCom.CommandText = $"INSERT INTO {tblName} VALUES {string.Join(",", values.ToArray())};";
            //        sqlCom.ExecuteNonQuery();
            //    }
            //    File.Move(changeFileName, changeFileName.Replace(".txt", $"-Done_{DateTime.Now.ToString("yyyyMMdd-hhmmss")}.csv"));
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //    Console.WriteLine();
            //}
            //finally
            //{
            //    sqlCom.CommandText = string.Empty;
            //}

            PrintAllFromTableWithFirstIsID(tblName, false);
        }

        //export to given file
        static void ExportToFileFromTable(string tblName, string exportFileName)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(exportFileName))
                {
                    sw.WriteLine(string.Join("|", fieldNames));
                    sqlCom.CommandText = $"SELECT * FROM {tblName};";
                    using (reader = sqlCom.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string[] temp = new string[reader.FieldCount];
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                temp[i] = reader[i].ToString() ;
                            }
                            sw.WriteLine(string.Join("|",temp));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }
            finally
            {
                sqlCom.CommandText = string.Empty;
            }

        }

        //import from given file
        static void ImportToTableFromFile(string tblName, string importFileName)
        {
            try
            {
                using (StreamReader sr = new StreamReader(importFileName))
                {
                    string[] entry = new string[fieldNames.Length];
                    List<string> values = new List<string>();
                    while (!sr.EndOfStream)
                    {
                        string temp = sr.ReadLine();
                        if (int.TryParse(temp[0].ToString(), out int result))
                        {
                            temp = "('" + temp.Replace("|", "','") + "')";
                            values.Add(temp);
                        }
                    }
                    sqlCom.CommandText = $"INSERT INTO {tblName} VALUES {string.Join(",", values.ToArray())};";
                    sqlCom.ExecuteNonQuery();
                }
                File.Move(importFileName, importFileName.Replace(".csv", $"-Done_{DateTime.Now.ToString("yyyyMMdd-hhmmss")}.csv"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }
            finally
            {
                sqlCom.CommandText = string.Empty;
            }

            PrintAllFromTableWithFirstIsID(tblName, false);

        }

        // build connnection string
        static void BuildSqlConnectionString(params string[] parameter)
        {
            SqlConnectionStringBuilder connSB = new SqlConnectionStringBuilder();
            for (int i = 0; i < parameter.Length; i += 2)
            {
                connSB[parameter[i]] = parameter[i + 1];
            }
            sqlConnectionString = connSB.ToString();
        }

        // print out table odered by id or by name
        static void PrintAllFromTableWithFirstIsID(string tableName, bool orderedByName = false)
        {
            try
            {
                sqlCom.CommandText = $"SELECT * FROM {tableName} " + (orderedByName ? "ORDER BY Name" : string.Empty+";");
                using (reader = sqlCom.ExecuteReader())
                {
                    fieldNames = new string[reader.FieldCount];

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        fieldNames[i] = reader.GetName(i);

                        if (i == 0)
                        {
                            Console.Write($"  {fieldNames[i],-4}");
                        }
                        else
                        {
                            Console.Write($"  {fieldNames[i],-20}");
                        }

                    }

                    Console.WriteLine();
                    Console.WriteLine(new string('─', (reader.FieldCount - 1) * 22 + 4));
                    Console.ResetColor();

                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            if (i == 0)
                            {
                                Console.Write($"  {reader[reader.GetName(i)],-4}");
                            }
                            else
                            {
                                Console.Write($"  {reader[reader.GetName(i)],-20}");
                            }
                        }
                        Console.WriteLine();

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }
            finally
            {
                sqlCom.CommandText = string.Empty;
            }
        }

        // menu print
        static string Menu(string menu, string result)
        {
            string message = ": Bitte korrekte Wahl treffen!   ";

            // clear and print menu
            Console.Clear();
            Console.BackgroundColor = (result != string.Empty ? ConsoleColor.Blue : ConsoleColor.Red);
            Console.WriteLine("\n   Menu" + (result != string.Empty ? string.Empty : message));
            Console.ResetColor();
            Console.Write(menu);

            // get user input
            result = Console.ReadLine();
            Console.WriteLine();

            return result;
        }

        // end wait and wait
        static void End()
        {
            Console.CursorVisible = false;
            Console.Write("\n\n\nthe end is near (any key)...");
            Console.ReadKey(true);
            Console.CursorVisible = true;
        }

        static void Wait()
        {
            Console.WriteLine("\n\npress any key ...");
            Console.ReadKey(true);
        }
    }
}
