﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace _08_A04_SerialisierungPerson
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> spinner = new List<Person>();

            spinner.Add(new Person("Zufall", "Rainer", new DateTime(1981, 1, 1)));
            spinner.Add(new Person("Pfahl", "Marta", new DateTime(1982, 2, 2)));

            spinner.Add(new Person("Höhle", "Axel", new DateTime(1954, 4, 4)));
            spinner.Add(new Person("Himmel", "Klara", new DateTime(1953, 3, 3)));
            
            spinner.Add(new Person("Schweiß", "Axel", new DateTime(1955, 5, 5)));
            spinner.Add(new Person("Fall", "Klara", new DateTime(1956, 6, 6)));
            
            spinner.Add(new Person("Bär", "Carmen", new DateTime(2007, 7, 7)));


            spinner[0].mutter = spinner[5];
            spinner[0].vater = spinner[4];
            spinner[4].kind = spinner[0];
            spinner[5].kind = spinner[0];

            spinner[1].mutter = spinner[3];
            spinner[1].vater = spinner[2];
            spinner[2].kind = spinner[1];
            spinner[3].kind = spinner[1];

            spinner[6].mutter = spinner[1];
            spinner[6].vater = spinner[0];
            spinner[0].kind = spinner[6];
            spinner[1].kind = spinner[6];

            FileStream fs = new FileStream("data.sv", FileMode.OpenOrCreate);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, spinner);
            fs.Close();

            fs = new FileStream("data.sv", FileMode.Open);
            foreach (var item in (List<Person>)bf.Deserialize(fs))
            {
                Console.WriteLine(item);
                Console.WriteLine("---");
            }
            fs.Close();
            Console.ReadKey(true);
        }
    }
}
