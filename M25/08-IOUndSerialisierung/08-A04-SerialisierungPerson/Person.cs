﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace _08_A04_SerialisierungPerson
{
    [Serializable]
    class Person
    {
        public string nachname;
        public string vorname;
        public DateTime geburtsDatum;
        public Person mutter;
        public Person vater;
        public Person kind;

        [NonSerialized]
        public int alter;

        [OnDeserialized]
        public void RestoreAlter(StreamingContext a)
        {
            alter = (DateTime.Now.Year - geburtsDatum.Year) - (DateTime.Now < geburtsDatum.AddYears(DateTime.Now.Year - geburtsDatum.Year) ? 1 : 0);
        }

        public Person() { }

        public Person(string nachname, string vorname, DateTime geburtsDatum)
        {
            this.nachname = nachname;
            this.vorname = vorname;
            this.geburtsDatum = geburtsDatum;
            alter = (DateTime.Now.Year - geburtsDatum.Year) - (DateTime.Now < geburtsDatum.AddYears(DateTime.Now.Year - geburtsDatum.Year) ? 1 : 0);
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}: {2} \n\tMutter: {3}\n\tVater: {4}\n\tKind: {5}", nachname, vorname, alter,mutter?.nachname +", "+mutter?.vorname, vater?.nachname + ", " + vater?.vorname, kind?.nachname + ", " + kind?.vorname); 
        }
    }
}
