﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace _08_A02_FileSystemWatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSystemWatcher bello = new FileSystemWatcher(@"d:\\lookup");

            bello.Changed += new FileSystemEventHandler(OnChanged);
            bello.Created += new FileSystemEventHandler(OnChanged);
            bello.Deleted += new FileSystemEventHandler(OnChanged);
            bello.Renamed += new RenamedEventHandler(OnRenamed);

            bello.Changed += new FileSystemEventHandler(OnChangedLookup);
            bello.Created += new FileSystemEventHandler(OnChangedLookup);
            bello.Renamed += new RenamedEventHandler(OnChangedLookup);

            bello.Filter = "*.lookup";

            Console.WriteLine(bello.Filter);

            bello.EnableRaisingEvents = true;

            while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;
        }

        static void OnChanged(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("File: " + e.FullPath + " " + e.ChangeType);
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);
        }
        
        private static void OnChangedLookup(object source, FileSystemEventArgs e)
        {

            StreamReader sr = new StreamReader(e.FullPath);
            StreamWriter sw = new StreamWriter(e.FullPath + @".resolved");

            while (!sr.EndOfStream)
            {
                string fqdn = sr.ReadLine();
                Console.WriteLine("Looking up:{0}", fqdn);
                string[] ips = ResolveFQDN(fqdn);
                Console.WriteLine("writing matches in file...");
                sw.WriteLine(DateTime.Now);
                sw.Write(fqdn);
                foreach (string item in ips)
                {
                    sw.WriteLine(item);
                    Console.Write(".");
                }
                sw.WriteLine();
                Console.WriteLine("\nfinished looking up {0}", fqdn);
                
            }
            sw.Close();
            sr.Close();
        }

        private static string[] ResolveFQDN(string fqdn)
        {
            try
            {
                IPAddress[] addr = Dns.GetHostAddresses(fqdn);
                return addr.Select(a => a.ToString()).ToArray();
            }
            catch
            {
                return null;
            }
        }
    }
}
