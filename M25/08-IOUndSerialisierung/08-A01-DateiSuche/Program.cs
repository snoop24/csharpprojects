﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _08_A01_DateiSuche
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = @"append.txt";
            string dirName = @"d:\";

            Schreiben(DateiSuchen(dirName,fileName),fileName);
            //Console.WriteLine(DateiSuchen(@"d:\", @"append.txt"),fileName);
            Console.WriteLine("press any key ...");
            Console.ReadKey(true);
        }

        static string DateiSuchen(string path,string fileName)
        {
            string here = null;

            try
            {
                string[] files = Directory.GetFiles(path);
                if (files.Where(x => x.Contains(fileName)).Count() > 0)
                {
                    here = path;
                }
                else
                {
                    foreach (string item in Directory.GetDirectories(path))
                    {
                        here = DateiSuchen(item, fileName);
                        if (here != null)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Fehler bei {0} - {1}",path, fileName);
            }

            return here;
        }
        static void Schreiben(string path,string fileName)
        {
            if (path != "" && fileName !=  "")
            {
                string pathFile = path + @"\" + fileName;
                StreamWriter sw = new StreamWriter(pathFile, true);
                bool end = false;
                string input = "";

                Console.WriteLine("Schreiben in {0}",pathFile);
                Console.WriteLine("Eingabe mit einzelnem Punkt \".\" und anschliessendem Return in einer Zeile beenden!");
                do
                {
                    input = Console.ReadLine();
                    if (input == ".")
                    {
                        end = true;
                    }
                    else
                    {
                        sw.WriteLine(DateTime.Now +": "+input);
                    }
                } while (!end);
                sw.Close();
                Console.WriteLine();
            }
        }
    }
}
