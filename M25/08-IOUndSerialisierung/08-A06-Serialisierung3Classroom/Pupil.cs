﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _08_A06_Serialisierung3Classroom
{
    [Serializable]
    public class Pupil : Person
    {
        public string ClassName { get; set; }

        public Pupil()
            :base()
        {
            ClassName = "dummy class";
        }

        public Pupil(string name, string className)
            : base(name)
        {
            ClassName = className;
        }

        public override string ToString()
        {
            return base.ToString() + $"    Klasse: {ClassName}";
        }
    }
}
