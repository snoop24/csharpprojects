﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _08_A06_Serialisierung3Classroom
{
    [Serializable]
    [XmlRoot("LoginDaten")]
    public class ClassRoom
    {
        [XmlAttribute]
        public string ClassName { get; set; }
        
        [XmlArray]
        public List<Person> Members { get;  } = new List<Person>();
        
        private ClassRoom() { }

        public ClassRoom(string className)
        {
            ClassName = className;
        }

        public string Show()
        {
            string show = $"Klassenraum: {ClassName}\n\n";

            foreach (var item in Members)
            {
                show += item.ToString() + "\n\n";
            }

            return show;
        }
    }
}
