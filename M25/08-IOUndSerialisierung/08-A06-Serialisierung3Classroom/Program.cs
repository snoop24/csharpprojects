﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.IO;

namespace _08_A06_Serialisierung3Classroom
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetBufferSize(120, 600);
            Console.SetWindowSize(120, 60);
            Teacher t1 = new Teacher("Gerwin Schnittjer", "C#");

            Pupil p1 = new Pupil() { Name="Patrick Czezor",ClassName= "FIAE" };
            Pupil p2 = new Pupil("Christian Klebsch", "FIAE");
            Pupil p3 = new Pupil("Friedrich Wünnenberg", "FIAE");
            Pupil p4 = new Pupil("Klaus Dieter Schaub", "FIAE");
            Pupil p5 = new Pupil("Alex Meister", "FIAE");
            Pupil p6 = new Pupil("Stephan Grote", "FIAE");
            Pupil p7 = new Pupil("Ejrun Korusi", "FIAE");
            Pupil p8 = new Pupil("David Vidovic", "FIAE");
            Pupil p9 = new Pupil("Daniel Schnoklake", "FIAE");
            Pupil p10 = new Pupil("Jörg Stiller", "FIAE");
            Pupil p11 = new Pupil("Daniel Dittrich", "FIAE");
            Pupil p12 = new Pupil("Thomas Vogler", "FIAE");
            Pupil p13 = new Pupil("Tobias Broens", "FIAE");
            Pupil p14 = new Pupil("Ömer Avci", "FIAE");
            Pupil p15 = new Pupil("El Mahdi El Hadri", "FIAE");
            Pupil p16 = new Pupil("Arno Hagen", "FIAE");
            Pupil p17 = new Pupil("Martin Skerra", "FIAE");
            Pupil p18 = new Pupil("Hanno Alexander von Bodecker", "FIAE");
            Pupil p19 = new Pupil("Sohail Bayzaei", "FIAE");
            Pupil p20 = new Pupil("Michelle Schwake", "FIAE");
            Pupil p21 = new Pupil("Boushra Alhamdan", "FIAE");
            Pupil p22 = new Pupil("Behezi Kabiah", "FIAE");

            ClassRoom cr1 = new ClassRoom("ITA1") { Members = { t1, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22 }};

            Console.WriteLine(cr1.Show());

            Console.ReadKey(true);
            Console.Clear();
            // binary formatter
            FileStream fs1 = new FileStream(@"data.sv", FileMode.OpenOrCreate);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs1, cr1);
            fs1.Close();

            FileStream fs2 = new FileStream(@"data.sv", FileMode.Open);

            Console.WriteLine("Binary Deserialization\n");
            Console.WriteLine(((ClassRoom)bf.Deserialize(fs2)).Show());
            Console.ReadKey(true);
            Console.Clear();

            // xml serializer
            fs1 = new FileStream(@"data.xml", FileMode.OpenOrCreate);
            XmlSerializer xmlSer = new XmlSerializer(typeof(ClassRoom));
            xmlSer.Serialize(fs1, cr1);
            fs1.Close();

            fs2 = new FileStream(@"data.xml", FileMode.Open);
            Console.WriteLine("Xml Deserialization\n");
            
            Console.WriteLine(((ClassRoom)xmlSer.Deserialize(fs2)).Show());

            Console.ReadKey(true);
        }
    }
}
