﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _08_A06_Serialisierung3Classroom
{
    [Serializable]
    [XmlInclude(typeof(Pupil))]
    [XmlInclude(typeof(Teacher))]
    public class Person 
    {
        static int anonym = 0;

        [XmlAttribute]
        public string Name { get; set; }
        
        public Person()
        {
            Name = (anonym  %2 == 0 ? "John Doe" : "Jane Doe");
            anonym++;
        }

        public Person (string name)
        {
            Name = name;
        }
        public override string ToString()
        {
            string toString = "Name";
            if (GetType().Name == "Teacher")
            {
                toString = "Lehrer";
            }
            else if (GetType().Name == "Pupil")
            {
                toString = "Schüler";
            }

            return $"{toString,8}: {Name,30}"; 
        }
    }
}
