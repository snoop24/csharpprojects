﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _08_A06_Serialisierung3Classroom
{
    [Serializable]
    public class Teacher : Person
    {
        public string Subject { get; set; }

        private Teacher()
            : base()
        {
            Subject = "dummy subject";
        }

        public Teacher(string name, string subject)
            : base(name)
        {
            Subject = subject;
        }

        public override string ToString()
        {
            return base.ToString() + $"    Fach: {Subject}";
        }
    }
}
