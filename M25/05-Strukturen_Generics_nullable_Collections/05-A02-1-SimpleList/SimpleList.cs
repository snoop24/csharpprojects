﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_1_SimpleList
{
    class SimpleList<T> where T : IComparable
    {
        SLEntry<T> head;
        SLEntry<T> tail;

        public int Count
        {
            get
            {
                int count = 0;

                SLEntry<T> temp = head;

                while (temp != null)
                {
                    temp = temp.Next;
                    count++;
                }
                return count;
            }
        }

        #region add or remove
        public void Add(T element)
        {
            SLEntry<T> temp = new SLEntry<T>(element);

            if (head == null)
            {
                head = temp;
                tail = temp;
            }
            else
            {
                tail.Next = temp;
                tail = temp;
            }
        }

        public void Remove(T element)
        {
            if (head != null)
            {
                // if head matches
                if (head.Element.Equals(element))
                {
                    head = head.Next;
                }
                else
                {
                    // actual entry
                    SLEntry<T> searchEntry = head;

                    // search until match or end of list
                    while (searchEntry.Next != null)
                    {
                        // if match remove and break;
                        if (searchEntry.Next.Element.Equals(element))
                        {
                            searchEntry.Next = searchEntry.Next.Next;
                            break;
                        }
                        // if no match
                        else
                        {
                            searchEntry = searchEntry.Next;
                        }
                    }
                }
            }

        }

        public void RemoveAt(int index)
        {
            if (head != null)
            {
                if (index == 0)
                {
                    head = head.Next;
                }
                else
                {

                    SLEntry<T> searchEntry = head;
                    int i = 0;

                    // goto 1 before index. proceed only if is the actual is not null
                    while (i + 1 < index && searchEntry != null)
                    {
                        i++;
                        searchEntry = searchEntry.Next;
                    }

                    // if 1 before index has been reached and has .next, set its .next to next.next
                    if (i + 1 == index && searchEntry.Next != null)
                    {
                        searchEntry.Next = searchEntry.Next.Next;
                    }
                }
            }
        }
        #endregion

        #region search or return
        public int IndexOf(T element)
        {
            // actual entry
            SLEntry<T> searchEntry = head;

            int indexOf = -1;
            int index = -1;

            // search until entry is null
            while (searchEntry != null)
            {
                index++;

                // if match set return value and break
                if (searchEntry.Element.Equals(element))
                {
                    indexOf = index;
                    break;
                }
                // goto next entry
                else
                {
                    searchEntry = searchEntry.Next;
                }
            }

            return indexOf;
        }

        public T ReturnAt(int index)
        {
            T returnAt = default(T);

            // if not empty
            if (head != null)
            {
                SLEntry<T> searchEntry = head;
                int i = 0;

                // goto index
                while (i < index && searchEntry != null)
                {
                    i++;
                    searchEntry = searchEntry.Next;
                }

                // if index exits
                returnAt = searchEntry != null ? searchEntry.Element : default(T);

            }

            return returnAt;
        }
        #endregion

        #region format
        public T[] ToArray()
        {
            T[] array = new T[Count];

            SLEntry<T> temp = head;

            int i = 0;

            // loop through elements until element is null
            while (temp != null)
            {
                array[i] = temp.Element;
                temp = temp.Next;
                i++;
            }

            return array;
        }

        public override string ToString()
        {
            return string.Join(" - ", ToArray());
        }
        #endregion
    }
}
