﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A03_Baeume
{
    public interface IBinaryTree<T> where T : IComparable<T>
    {
        void Clear();
        void Insert(T value);
        void Delete(T value);
        bool Contains(T value);
        BinaryTreeNode<T> Search(T value);
        void PrintInorder();
    }

}
