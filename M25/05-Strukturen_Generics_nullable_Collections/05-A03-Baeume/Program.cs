﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A03_Baeume
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Insert(50);
            tree.Insert(100);
            tree.Insert(25);
            tree.Insert(1);
            tree.Insert(10);
            tree.Insert(75);
            tree.Insert(65);
            tree.Insert(85);
            tree.Insert(61);
            tree.Insert(45);
            tree.Insert(35);
            tree.Insert(15);
            tree.Insert(10);
            tree.Insert(46);


            tree.PrintInorder();
            Console.WriteLine();

            tree.Delete(46);

            tree.PrintInorder();
            Console.WriteLine();

            tree.Delete(75);

            tree.PrintInorder();
            Console.WriteLine();




            tree.Delete(50);

            tree.PrintInorder();
            Console.WriteLine();

            Console.WriteLine(tree.Contains(50));
            Console.WriteLine();

            Console.WriteLine(tree.Contains(10));
            Console.WriteLine();

            Console.WriteLine(tree.Search(35));
            Console.WriteLine();

            Console.WriteLine("--- "+ tree.Search(99));
            Console.WriteLine();

            tree.PrintInorder();
            Console.WriteLine();

            tree.Delete(61);
            tree.PrintInorder();
            Console.WriteLine();

            tree.Delete(61);
            tree.Clear();
            tree.PrintInorder();
            Console.WriteLine();

            tree.Delete(61);
            tree.PrintInorder();
            Console.WriteLine();


            Console.ReadKey();
        }
    }
}
