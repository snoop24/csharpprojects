﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A03_Baeume
{
    class BinaryTree<T> : IBinaryTree<T> where T : IComparable<T>
    {
        // tree top/start node
        protected BinaryTreeNode<T> start;

        // help variable saves upper node if search return not start
        protected BinaryTreeNode<T> temp;

        public bool IsEmpty
        {
            get
            {
                return (start == null ? true : false);
            }
        }

        public void Clear()
        {
            start = null;
        }

        #region contains
        public bool Contains(T value)
        {
            return Contains(value, start);
        }

        protected bool Contains(T value, BinaryTreeNode<T> startNode)
        {
            return Search(value, startNode) != null;
        }
        #endregion

        #region delete
        public void Delete(T value)
        {
            // if tree not empty
            if (!IsEmpty)
            {
                // if starting value machtes
                if (value.CompareTo(start.Data) == 0)
                {
                    // add right subtree to left subtree
                    Insert(start.Right, start.Left);

                    // set left subtree as new tree
                    start = start.Left;
                }
                else
                {
                    // delete below start
                    Delete(value, start);
                }
            }
        }

        protected void Delete(T value, BinaryTreeNode<T> deleteBelow)
        {
            // find node to delete below
            BinaryTreeNode<T> delete = Search(value, deleteBelow);

            // if found
            if (delete != null)
            {
                // check if temp(parent from search).left/right points to delete and set to null
                if (delete.Data.CompareTo(temp.Data) < 0)
                {
                    temp.Left = null;
                }
                else
                {
                    temp.Right = null;
                }
                // insert child nodes from deleted node into tree right below temp)
                Insert(delete.Left, temp);
                Insert(delete.Right, temp);
            }
        }
        #endregion

        #region insert
        public void Insert(T value)
        {
            // create node from value
            BinaryTreeNode<T> insertNode = new BinaryTreeNode<T>(value);
            
            // if tree empty set new start
            if (IsEmpty)
            {
                start = insertNode;
            }
            // tree not empty insert node below start
            else
            {
                Insert(insertNode, start);
            }
        }

        protected void Insert(BinaryTreeNode<T> insertNode, BinaryTreeNode<T> insertHere)
        {
            // if node to insert is not null 
            if (insertNode != null)
            {
                // check insert to the left or right below insertHere
                if (insertNode.Data.CompareTo(insertHere.Data) < 0)
                {
                    // if left is not null insert 
                    if (insertHere.Left == null)
                    {
                        insertHere.Left = insertNode;
                    }
                    // if left is set insert below left
                    else
                    {
                        Insert(insertNode, insertHere.Left);
                    }
                }
                else
                {
                    // if right is not null insert
                    if (insertHere.Right == null)
                    {
                        insertHere.Right = insertNode;
                    }
                    else
                    {
                        // if left is set insert below left
                        Insert(insertNode, insertHere.Right);
                    }
                }
            }
        }
        #endregion

        #region print
        public void PrintInorder()
        {
            if (start != null)
            {
                PrintInorder(start);
            }
        }

        public void PrintInorder(BinaryTreeNode<T> printfromHere)
        {
            if (printfromHere != null)
            {
                // print left partial tree
                PrintInorder(printfromHere.Left);

                // print own data (top of tree)
                Console.Write("{0,4} ",printfromHere.Data);

                // print right partial tree
                PrintInorder(printfromHere.Right);
            }

        }
        #endregion

        #region search
        public BinaryTreeNode<T> Search(T value)
        {
            return Search(value, start);
        }

        private BinaryTreeNode<T> Search(T value, BinaryTreeNode<T> searchHere)
        {
            BinaryTreeNode<T> search = null;

            if (searchHere == null)
            {
                search = null;
            }
            else if (value.CompareTo(searchHere.Data) == 0)
            {
                search = searchHere;
            }
            else
            {
                temp = searchHere;
                if (value.CompareTo(searchHere.Data) < 0)
                {
                    search = Search(value, searchHere.Left);
                }
                else
                {
                    search = Search(value, searchHere.Right);
                }
            }
            return search;
        }
        #endregion
    }
}
