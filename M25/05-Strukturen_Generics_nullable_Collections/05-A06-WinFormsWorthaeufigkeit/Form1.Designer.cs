﻿namespace _05_A06_WinFormsWorthaeufigkeit
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tBoxText = new System.Windows.Forms.TextBox();
            this.lBoxNormal = new System.Windows.Forms.ListBox();
            this.lBoxAlpha = new System.Windows.Forms.ListBox();
            this.lBoxNum = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // tBoxText
            // 
            this.tBoxText.Location = new System.Drawing.Point(12, 12);
            this.tBoxText.Multiline = true;
            this.tBoxText.Name = "tBoxText";
            this.tBoxText.Size = new System.Drawing.Size(395, 459);
            this.tBoxText.TabIndex = 0;
            this.tBoxText.Text = "<Shift+Enter> Starten";
            this.tBoxText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBoxText_KeyDown);
            // 
            // lBoxNormal
            // 
            this.lBoxNormal.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lBoxNormal.FormattingEnabled = true;
            this.lBoxNormal.Location = new System.Drawing.Point(413, 12);
            this.lBoxNormal.Name = "lBoxNormal";
            this.lBoxNormal.Size = new System.Drawing.Size(139, 459);
            this.lBoxNormal.TabIndex = 1;
            // 
            // lBoxAlpha
            // 
            this.lBoxAlpha.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lBoxAlpha.FormattingEnabled = true;
            this.lBoxAlpha.Location = new System.Drawing.Point(558, 12);
            this.lBoxAlpha.Name = "lBoxAlpha";
            this.lBoxAlpha.Size = new System.Drawing.Size(139, 459);
            this.lBoxAlpha.TabIndex = 2;
            // 
            // lBoxNum
            // 
            this.lBoxNum.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lBoxNum.FormattingEnabled = true;
            this.lBoxNum.Location = new System.Drawing.Point(703, 12);
            this.lBoxNum.Name = "lBoxNum";
            this.lBoxNum.Size = new System.Drawing.Size(139, 459);
            this.lBoxNum.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 490);
            this.Controls.Add(this.lBoxNum);
            this.Controls.Add(this.lBoxAlpha);
            this.Controls.Add(this.lBoxNormal);
            this.Controls.Add(this.tBoxText);
            this.Name = "Form1";
            this.Text = "WorteZaehlen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBoxText;
        private System.Windows.Forms.ListBox lBoxNormal;
        private System.Windows.Forms.ListBox lBoxAlpha;
        private System.Windows.Forms.ListBox lBoxNum;
    }
}

