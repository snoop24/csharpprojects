﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace _05_A06_WinFormsWorthaeufigkeit
{
    public partial class Form1 : Form
    {

        private Dictionary<string, int> words = new Dictionary<string, int>();

        public Form1()
        {
            InitializeComponent();
        }

        private void tBoxText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.Shift)
            {
                e.SuppressKeyPress = true;
                
                words.Clear();

                string temp ;

                if (tBoxText.Text == "\r\n" || tBoxText.Text == "")
                {

                    OpenFileDialog myFileDialog = new OpenFileDialog
                    {
                        InitialDirectory = Directory.GetCurrentDirectory(),
                        Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*",
                        FilterIndex = 1,
                        RestoreDirectory = true,
                        Title = "Datei auswählen"
                    };

                    if (myFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        StreamReader reader = new StreamReader(myFileDialog.FileName);
                        StringBuilder buildIt = new StringBuilder();
                        while (!reader.EndOfStream)
                        {
                            buildIt.Append(reader.ReadLine()+ "\r\n");
                        }
                        tBoxText.Text = buildIt.ToString();
                    }
                }
                
                temp = tBoxText.Text; 
                temp = Regex.Replace(temp, @"\p{P}", " ");
                temp = Regex.Replace(temp, @"\s{2,}", " ");

                if (temp != "")
                {
                    foreach (string item in temp.ToLower().Split(' '))
                    {
                        if (item != "")
                        {
                            if (words.ContainsKey(item))
                            {
                                words[item]++;
                            }
                            else
                            {
                                words.Add(item, 1);
                            }
                        }
                    }
                }

                //List<string> normal = new List<string>();
                //List<string> alpha = new List<string>();
                //List<string> num = new List<string>();

                //foreach (var item in words)
                //{
                //    normal.Add(string.Format("{0,3}: {1}", item.Value, item.Key));
                //}
                //foreach (var item in words.OrderBy(item => item.Key))
                //{
                //    alpha.Add(string.Format("{0,3}: {1}", item.Value, item.Key));
                //}
                //foreach (var item in words.OrderByDescending(item => item.Value))
                //{
                //    num.Add(string.Format("{0,3}: {1}", item.Value, item.Key));
                //}

                lBoxNormal.DataSource = null;
                lBoxAlpha.DataSource = null;
                lBoxNum.DataSource = null;

                lBoxNormal.DataSource = words.Select(item => (string.Format("{0,3}: {1}", item.Value, item.Key))).ToList();
                lBoxAlpha.DataSource = words.OrderBy(item => item.Key).Select(item => (string.Format("{0,3}: {1}", item.Value, item.Key))).ToList();
                lBoxNum.DataSource = words.OrderByDescending(item => item.Value).Select(item=>(string.Format("{0,3}: {1}", item.Value, item.Key))).ToList();

                tBoxText.Select(0, 0);
                tBoxText.ScrollToCaret();

                e.Handled = true;
            }
        }
    }
}


