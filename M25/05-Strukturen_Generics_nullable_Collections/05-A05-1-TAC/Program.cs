﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _05_A05_1_TAC
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader read = new StreamReader("FroschkoenigUnixZeilenumbrueche.txt");

            Stack<string> tac = new Stack<string>();

            while (!read.EndOfStream)
            {
                tac.Push(read.ReadLine());
            }

            while (tac.Count > 0)
            {
                Console.WriteLine(tac.Pop());
            }

            Console.ReadKey(true);
        }
    }
}
