﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A01_GrossKlein
{
    class Program
    {
        static void Main(string[] args)
        {
            CharacterHandler charHandlerUL = new CharacterHandler(Character.UpperLower);

            CharacterHandler charHandlerL = new CharacterHandler(Character.LowerCase);

            CharacterHandler charHandlerU = new CharacterHandler(Character.UpperCase);

            string s = "Holla die Waldfee!";

            charHandlerUL(s);
            charHandlerU(s);
            charHandlerL(s);

            CharacterHandler charHandlerCombo = new CharacterHandler(Character.UpperLower) + new CharacterHandler(Character.LowerCase) + new CharacterHandler(Character.UpperCase);

            charHandlerCombo(s);

            charHandlerCombo += delegate (string s1)
            {
                foreach (char item in s1)
                {
                    Console.Write("{0,4}", (int)item);
                }
                Console.WriteLine();
            };

            charHandlerCombo(s);

            Delegate[] liste = charHandlerCombo.GetInvocationList();

            foreach (var item in liste)
            {
                Console.WriteLine(item.Method);
            }
        }
    }
}
