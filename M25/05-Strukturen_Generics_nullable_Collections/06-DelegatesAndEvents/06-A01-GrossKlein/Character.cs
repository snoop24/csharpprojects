﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A01_GrossKlein
{

    delegate void CharacterHandler(string s);

    class Character
    {
        public static void UpperCase(string s)
        {
            Console.WriteLine(s.ToUpper());
        }

        public static void LowerCase(string s)
        {
            Console.WriteLine(s.ToLower());
        }

        public static void UpperLower(string s)
        {
            Console.WriteLine(s);
        }
    }
}
