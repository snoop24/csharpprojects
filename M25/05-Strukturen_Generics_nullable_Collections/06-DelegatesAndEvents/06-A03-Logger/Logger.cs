﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _06_A03_Logger
{

    delegate void LogHandler(string data);

    class Log
    {
        private string Path { get; }
        private StreamWriter writer;

        public Log(string path)
        {
            Path = path;
        }

        public void LogData(string data)
        {
            if (writer == null || writer.BaseStream == null)
            {
                writer = new StreamWriter(Path, true);
            }
            writer.WriteLine("{0}: {1}", DateTime.Now, data);
        }
        
        public void CloseLogFile()
        {
            if (writer != null && writer.BaseStream != null)
            {
                writer.Close();
            }
        }

    }
}
