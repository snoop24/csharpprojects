﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A03_Logger
{
    class Program
    {
        static void Main(string[] args)
        {
            Log error = new Log("testFile.txt");

            LogHandler logError = new LogHandler(error.LogData);

            LogHandler errorToConsole = new LogHandler(Console.WriteLine);

            logError += errorToConsole;

            logError += delegate (string data)
            {
                Console.WriteLine(data.ToUpper());
            };

            logError("eieieieiei CRITICAL FAILURE, Attaenchen Please, Weihnachtsbaumkauf in Sachsen");
            error.CloseLogFile();

            logError("eieieieiei CRITICAL FAILURE");
            error.CloseLogFile();

            Console.ReadKey(true);

        }


    }
}
