﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A02_MinMax
{
    class Program
    {
        delegate bool VergleichsHandler(int a, int b);

        static void Main(string[] args)
        {
            VergleichsHandler maximal = IstGroesser;
            VergleichsHandler minimal = IstKleiner;

            int[] test = RandomNumberArrayOfLength(10, 0, 99);

            int min = GetLimit(minimal, test);
            int max = GetLimit(maximal, test);

            Console.WriteLine(string.Join("  ", test));
            Console.WriteLine("Min Index: {0,2}      Max Index: {1,2}", min, max);

            Console.ReadKey(true);
        }

        static int GetLimit(VergleichsHandler vergleich, int[] array)
        {
            int limit = 0;

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (!vergleich(array[limit], array[i + 1]))
                {
                    limit = i + 1;
                }
            }

            return limit;
            // return array[limit];
        }

        static bool IstKleiner(int a, int b)
        {
            return a < b;
        }

        static bool IstGroesser(int a, int b)
        {
            return a > b;
        }

        static int[] RandomNumberArrayOfLength(int c, int min, int max)
        {
            Random rnd = new Random();
            int[] array = new int[c];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(min, max + 1);
            }

            return array;
        }
    }
}
