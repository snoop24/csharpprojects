﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A04_Warpkern
{
    class WarpCoreConsole
    {
        private WarpCore core;
        public WarpCore Core
        {
            get
            {
                return core;
            }
            set
            {
                if (core != null)
                {
                    core.OnCoreTempChanged -= CoreTempChanged;
                    core.OnCoreTempCritical -= CoreTempChanged;
                }
                core = value;

                core.OnCoreTempChanged += CoreTempChanged;
                core.OnCoreTempCritical += CoreTempCritical;
            }
        }

        public void CoreTempChanged(object sender, TemperaturEventArgs e)
        {
            Console.WriteLine("{0}: {1}° changed by {2}° to {3}°", e.EventTime, e.Temp - e.Delta, e.Delta, e.Temp);

        }

        public void CoreTempCritical(object sender, TemperaturEventArgs e)
        {
            Console.WriteLine("{0}: CRITIICAL TEMP CORE {1}° ! CORE FAILURE IMMINENT!", e.EventTime, e.Temp);
        }
    }
}
