﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A04_Warpkern
{
    delegate void TemperaturEventHandler(object sender, TemperaturEventArgs e);

    class WarpCore
    {
        private double coreTemp;

        public double CoreTemp { get; }

        public WarpCoreConsole CoreConsole { get; }

        public event TemperaturEventHandler OnCoreTempChanged;
        public event TemperaturEventHandler OnCoreTempCritical;

        public WarpCore(WarpCoreConsole coreConsole, double coreTemp = 375)
        {
            this.coreTemp = coreTemp;
            CoreConsole = coreConsole;
            CoreConsole.Core = this;
        }

        public void Run(int seconds = 0)
        {
            DateTime timeStart = DateTime.Now;
            Random rnd = new Random();
            double delta = 0;
            while ((DateTime.Now - timeStart).Seconds < seconds)
            {
                delta = rnd.Next(-99, 100);

                if ((delta < 0 && coreTemp < 250) || (delta > 0 && coreTemp > 800))
                {
                    delta = -delta;
                }
                ChangeCoreTemp(delta);
                System.Threading.Thread.Sleep(500);
            }
            Console.WriteLine("{0}: Core Shutting Down!", DateTime.Now);
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("{0}: Core Offline!", DateTime.Now);
        }

        private void ChangeCoreTemp(double delta)
        {
            coreTemp += delta;
            OnCoreTempChanged?.Invoke(this, new TemperaturEventArgs(coreTemp, delta, DateTime.Now));
            if (coreTemp > 500)
            {
                OnCoreTempCritical?.Invoke(this, new TemperaturEventArgs(coreTemp, delta, DateTime.Now));
            }
        }
    }
}
