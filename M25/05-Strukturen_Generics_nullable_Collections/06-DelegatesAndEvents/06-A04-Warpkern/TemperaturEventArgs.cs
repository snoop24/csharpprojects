﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A04_Warpkern
{
    class TemperaturEventArgs : EventArgs
    {
        public double Temp { get; set; }
        public double Delta { get; set; }
        public DateTime EventTime { get; set; }

        public TemperaturEventArgs(double temp, double delta, DateTime eventTime)
        {
            Temp = temp;
            Delta = delta;
            EventTime = eventTime;
        }
    }
}
