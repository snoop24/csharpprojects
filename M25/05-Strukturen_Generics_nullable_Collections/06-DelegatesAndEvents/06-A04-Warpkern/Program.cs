﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A04_Warpkern
{
    class Program
    {
        static void Main(string[] args)
        {
            WarpCoreConsole console1 = new WarpCoreConsole();

            WarpCore core1 = new WarpCore(console1);

            core1.Run(30);

            Console.ReadKey(true);
        }
    }
}
