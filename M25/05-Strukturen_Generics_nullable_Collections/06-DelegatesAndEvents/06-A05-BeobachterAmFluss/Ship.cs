﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A05_BeobachterAmFluss
{
    class Ship
    {

        private bool atStop;
        
        public string Name { get; }
        public bool AtStop { get { return atStop; } }

        public River SailingOn { get; set; }

        public Ship(string name,River sailingOn, bool atStop=false)
        {
            Name = name;
            SailingOn = sailingOn;
            this.atStop = atStop;
            sailingOn.WaterLevelMeasuredEventHandler += OnWaterLevelMeasure;
        }

        public void OnWaterLevelMeasure(object sender, WaterLevelMeasuredEventArgs e)
        {
            if (atStop)
            {
                if (e.Level > 249)
                {
                    atStop = false;
                    Console.WriteLine("Schiff {0} auf dem {1} fährt weiter.",Name, SailingOn);
                }
            }
            else
            {
                if (e.Level < 250)
                {
                    atStop = true;
                    Console.WriteLine("Schiff {0} auf dem {1} hat gestoppt.", Name, SailingOn);
                }
            }
        }

        public override string ToString()
        {
            return Name;
        }


    }
}
