﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A05_BeobachterAmFluss
{
    public class WaterLevelMeasuredEventArgs : EventArgs
    {
        public int Level {get;}
        public int OldLevel { get; }

        public WaterLevelMeasuredEventArgs(int level, int oldLevel = 0)
        {
            Level = level;
            OldLevel = oldLevel;
        }
    }
}