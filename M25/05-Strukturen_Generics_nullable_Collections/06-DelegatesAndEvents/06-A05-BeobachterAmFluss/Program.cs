﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A05_BeobachterAmFluss
{
    class Program
    {
        static void Main(string[] args)
        {
            #region declare
            // rhein
            River river1 = new River("Rhein", 5000, 0);

            Ship ship1 = new Ship("Rheingold", river1);
            Ship ship2 = new Ship("Lorelei", river1);

            City city1 = new City("Köln", river1);
            City city2 = new City("Düsseldorf", river1);

            // donau
            River river2 = new River("Donau", 4000, 0);

            Ship ship3 = new Ship("Xaver", river2);
            Ship ship4 = new Ship("Franz", river2);

            City city3 = new City("Ulm", river2);

            SewageTreatmentPlant stp1 = new SewageTreatmentPlant("Strauß 1", river2);

            // river system
            River[] riverSystem = { river1, river2 };
            #endregion

            // start

            RunRiverSystem(riverSystem, 120);

            Console.ReadKey(true);
        }

        static void RunRiverSystem(River[] rivers, int seconds)
        {
            DateTime startTime = DateTime.Now;
            Random rnd = new Random();
            while ((DateTime.Now - startTime).TotalSeconds < seconds)
            {
                foreach (River item in rivers)
                {
                    RiverLevelChange(item,rnd.Next(-1000,1000));
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        static void RiverLevelChange(River r,int amount)
        {
            r.InFlow(amount);
            r.MeasureLevel();
        }
    }
}
