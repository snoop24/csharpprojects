﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A05_BeobachterAmFluss
{
    class River
    {
        private int level;

        public string Name { get; }
        public int Level { get { return level; } }
        private int Drain { get; }

        public event EventHandler<WaterLevelMeasuredEventArgs> WaterLevelMeasuredEventHandler;

        public River(string name, int level, int drain)
        {
            this.level = level;
            Drain = drain;
            Name = name;
        }

        public void InFlow(int amount)
        {
            int oldLevel = level;
            int newLevel = level + amount - Drain;
            if (100 < newLevel && newLevel < 10000)
            {
                level += amount - Drain;
            }
            else
            {
                level = newLevel < level ? 100 : 10000;
            }
        }

        public void  MeasureLevel()
        {
            Console.WriteLine("{0,10}:{1,5}mm",Name,level);
            WaterLevelMeasuredEventHandler(this, new WaterLevelMeasuredEventArgs(level));
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
