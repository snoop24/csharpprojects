﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A05_BeobachterAmFluss
{
    class City
    {
        private bool hasWaterBulwark;

        public string Name { get; }
        public bool HasWaterBulwark { get { return hasWaterBulwark; } }

        public River NearRiver { get; set; }

        public City(string name, River nearRiver, bool hasWaterBulwark=false)
        {
            Name = name;
            NearRiver = nearRiver;
            this.hasWaterBulwark = hasWaterBulwark;
            nearRiver.WaterLevelMeasuredEventHandler += OnWaterLevelMeasure;
        }

        public void OnWaterLevelMeasure(object sender, WaterLevelMeasuredEventArgs e)
        {
            if (hasWaterBulwark)
            {
                if (e.Level < 8201)
                {
                    hasWaterBulwark = false;
                    Console.WriteLine("Stadt {0} am {1} baut die Wasserschutzwand ab.", Name, NearRiver);
                }
            }
            else
            {
                if (e.Level >8200)
                {
                    hasWaterBulwark = true;
                    Console.WriteLine("Stadt {0} am {1} baut eine Wasserschutzwand auf.", Name, NearRiver);
                }
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
