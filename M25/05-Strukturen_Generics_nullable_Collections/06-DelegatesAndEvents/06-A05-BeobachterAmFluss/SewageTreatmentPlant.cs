﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A05_BeobachterAmFluss
{
    class SewageTreatmentPlant
    {
        private bool hasOutFlow;

        public string Name { get; }
        public bool HasOutFlow { get { return hasOutFlow; } }

        public River NearRiver { get; set; }

        public SewageTreatmentPlant(string name, River nearRiver, bool hasOutFlow = true)
        {
            Name = name;
            NearRiver = nearRiver;
            this.hasOutFlow = hasOutFlow;
            nearRiver.WaterLevelMeasuredEventHandler += OnWaterLevelMeasure;
        }

        public void OnWaterLevelMeasure(object sender, WaterLevelMeasuredEventArgs e)
        {
            if (hasOutFlow)
            {
                if (e.Level > 8000)
                {
                    hasOutFlow = false;
                    Console.WriteLine("Klärwerk {0} am {1} stoppt die Einleitung.", Name, NearRiver);
                }
                else if (e.Level < 3000)
                {
                    Console.WriteLine("Klärwerk {0} am {1} erhöht die Einleitungmenge.", Name, NearRiver);
                }
            }
            else
            {
                if (e.Level < 8001)
                {
                    hasOutFlow = true;
                    Console.WriteLine("Klärwerk {0} am {1} leitet wieder Wasser ein.", Name, NearRiver);
                }
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
