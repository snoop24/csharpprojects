﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace _05_A06_Worthaeufigkeit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetBufferSize(100, 480);
            Console.SetWindowSize(100, 80);

            StreamReader read = new StreamReader("FroschkoenigUnixZeilenumbrueche.txt");
            Dictionary<string, int> words = new Dictionary<string, int>();

            string temp;
            while (!read.EndOfStream)
            {
                temp = read.ReadLine();
                temp = Regex.Replace(temp, @"\p{P}", " ");
                temp = Regex.Replace(temp, @"\s{2,}", " ");
                if (temp != "")
                {
                    foreach (string item in temp.ToLower().Split(' '))
                    {
                        if (item != "")
                        {
                            if (words.ContainsKey(item))
                            {
                                words[item]++;
                            }
                            else
                            {
                                words.Add(item, 1);
                            }
                        }
                    }
                }
            }

            foreach (var item in words)
            {
                Console.Write("{0,3} : {1,-19}", item.Value, item.Key);
            }

            Console.ReadKey(true);
            Console.Clear();

            foreach (var item in words.OrderBy(x => x.Key))
            {
                Console.Write("{0,3} : {1,-19}", item.Value, item.Key);
            }

            Console.ReadKey(true);
            Console.Clear();

            foreach (var item in words.OrderByDescending(x => x.Value))
            {
                Console.Write("{0,3} : {1,-19}", item.Value, item.Key);
            }

            Console.ReadKey(true);
        }
    }
}
