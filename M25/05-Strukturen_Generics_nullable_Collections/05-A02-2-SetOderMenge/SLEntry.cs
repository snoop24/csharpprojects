﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_2_SetOderMenge
{
    class SLEntry<T>
    {
        // entry itself
        public T Element { get; }

        // next entry
        public SLEntry<T> Next { get; set; }

        public SLEntry(T element)
        {
            Element = element;
            Next = null;
        }
    }
}
