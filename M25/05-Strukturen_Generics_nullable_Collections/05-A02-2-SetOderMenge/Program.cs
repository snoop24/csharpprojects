﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_2_SetOderMenge
{
    class Program
    {
        static void Main(string[] args)
        {
            Set<int> testSet = new Set<int>();

            testSet.Add(1);

            Console.WriteLine(testSet);

            testSet.Add(1);

            Console.WriteLine(testSet);

            testSet.Add(2);

            Console.WriteLine(testSet);

            testSet.Add(3);

            Console.WriteLine(testSet);

            testSet.Add(4);

            Console.WriteLine(testSet);

            testSet.Remove(2);

            Console.WriteLine(testSet);

            Console.WriteLine(testSet.Count);

            Console.WriteLine(string.Join("-", testSet.ToArray()));

            Console.ReadKey(true);
            Console.Clear();

            Set<int> testSet2 = new Set<int>();

            testSet2.Add(1);
            testSet2.Add(3);
            testSet2.Add(5);
            testSet2.Add(7);
            testSet2.Add(9);
            testSet2.Add(10);
            testSet2.Add(11);

            Set<int> testSet3 = new Set<int>();

            testSet3.Add(2);
            testSet3.Add(4);
            testSet3.Add(6);
            testSet3.Add(8);
            testSet3.Add(10);
            testSet3.Add(11);
            testSet3.Add(12);

            Console.WriteLine(testSet2);
            Console.WriteLine(testSet3);

            Console.ReadKey(true);

            Console.WriteLine(Set<int>.InnerJoin(testSet2,testSet3));
            Console.WriteLine(Set<int>.Merge(testSet2, testSet3));

            Console.ReadKey(true);
        }
    }
}
