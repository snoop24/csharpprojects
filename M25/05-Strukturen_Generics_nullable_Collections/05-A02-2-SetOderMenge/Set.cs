﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_2_SetOderMenge
{
    class Set<T> : SimpleList<T> where T : IComparable
    {

        public new void Add(T element)
        {
            // if index is -1 add, otherwise it exists in list already
            if (IndexOf(element) == -1)
            {
                base.Add(element);
            }
        }

        #region static methods
        public static Set<T> Merge(Set<T> setA, Set<T> setB)
        {
            Set<T> merge = new Set<T>();
            
            // add each item from setA 
            foreach (var item in setA.ToArray())
            {
                merge.Add(item);
            }

            // add each item from setB 
            foreach (var item in setB.ToArray())
            {
                merge.Add(item);
            }

            return merge;
        }

        public static Set<T> InnerJoin(Set<T> setA, Set<T> setB)
        {
            Set<T> innerJoin = new Set<T>();
            
            // check each item of setA for matching in setB and add if true
            foreach (var itemA in setA.ToArray())
            {
                foreach (var itemB in setB.ToArray())
                {
                    if (itemA.Equals(itemB))
                    {
                        innerJoin.Add(itemA);
                        break;
                    }
                }
            }

            return innerJoin;
        }
        #endregion


    }
}
