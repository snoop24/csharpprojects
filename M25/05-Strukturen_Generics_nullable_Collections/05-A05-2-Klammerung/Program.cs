﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A05_2_Klammerung
{
    class Program
    {
        static void Main(string[] args)
        {
            string test;


            test = "()";
            KlammerTest(test);

            test = "(";
            KlammerTest(test);

            test = ")";
            KlammerTest(test);

            test = "(3+6)*((7-4)*9)";
            KlammerTest(test);

            test = "(3+6*((7-4)*9)";
            KlammerTest(test);

            test = "(((3+6)((*((7-4)*9)";
            KlammerTest(test);

            test = "(3+6)*)((7-4)*9))";
            KlammerTest(test);

            test = "(3+6)*)((7-4)*9)))";
            KlammerTest(test);

            Console.ReadKey(true);
        }

        static void KlammerTest(string test)
        {
            Stack<int> klammerAufPos = new Stack<int>();
            Stack<int> klammerZuPos = new Stack<int>();

            // check each char in string
            for (int i = 0; i < test.Length; i++)
            {
                // if ( add position to stack
                if (test[i] == '(')
                {
                    klammerAufPos.Push(i);
                }
                // if (
                else if (test[i] == ')')
                {
                    // if open )
                    if (klammerAufPos.Count > 0)
                    {
                        // remove last ( position
                        klammerAufPos.Pop();
                    }
                    else
                    {
                        // add ) position
                        klammerZuPos.Push(i);
                    }
                }
            }


            // reverse stack orders into temp then copy
            Stack<int> temp = new Stack<int>();

            // reverse ( stack
            while (klammerAufPos.Count > 0)
            {
                temp.Push(klammerAufPos.Pop());
            }
            klammerAufPos = temp;

            // reverse ) stack
            while (klammerZuPos.Count > 0)
            {
                temp.Push(klammerZuPos.Pop());
            }
            klammerZuPos = temp;

            // print out result
            PrintTest(test, klammerAufPos, klammerZuPos);
        }

        static void PrintTest(string test, Stack<int> klammerAufPos, Stack<int> klammerZuPos)
        {
            for (int i = 0; i < test.Length; i++)
            {
                // if char is (
                if ( test[i] == '(')
                {
                    // if ( pos is on the stack
                    if (klammerAufPos.Count > 0 && i == klammerAufPos.Peek())
                    {
                        // print with bad color
                        Print(test[i], ConsoleColor.Red);
                        // remove pos from stack
                        klammerAufPos.Pop();
                    }
                    else
                    {
                        // print with good color
                        Print(test[i], ConsoleColor.Green);
                    }
                }
                // if char is )
                else if (test[i] == ')')
                {
                    // if ) pos is on the stack
                    if (klammerZuPos.Count > 0 && i == klammerZuPos.Peek())
                    {
                        // print with bad color
                        Print(test[i], ConsoleColor.Red);
                        // remove pos from stack
                        klammerZuPos.Pop();
                    }
                    else
                    {
                        // print with good color
                        Print(test[i], ConsoleColor.Green);
                    }
                }
                else
                {
                    // simply print char
                    Print(test[i], ConsoleColor.White);
                }

            }

            // linefeed
            Console.WriteLine();
            
        }

        static void Print(char item, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(item);
            Console.ResetColor();
        }
    }
}
