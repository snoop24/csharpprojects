﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A07_SpringerTour
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetBufferSize(160, 480);
            Console.SetWindowSize(160, 40);
            Console.Title = "Knight on Board";

            Console.CursorVisible = false;

            int x = 8;
            do
            {
                // best 3x3 to 16x6 (3x5 6x4 etc allowed)
                Board knightOnBoard = new Board(x, x);

                knightOnBoard.StartMoving(0, 0, 1,true);

                #region print ende
                Console.SetCursorPosition(0, 5);
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("                              ");
                Console.WriteLine("      Escape zum Beenden      ");
                Console.WriteLine("                              ");
                Console.ResetColor();
                #endregion
                x++;
            } while (x<31&& Console.ReadKey(true).Key != ConsoleKey.Escape);


            Console.CursorVisible = true;
        }




    }
}
