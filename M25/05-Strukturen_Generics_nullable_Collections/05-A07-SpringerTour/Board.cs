﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A07_SpringerTour
{
    class Board
    {
        #region output format strings
        private string corner = "+";
        private string topBorder = "----";
        private string sideBorder = "|";
        #endregion

        #region knight movement
        public Field KnightPos { get; set; }
        private int moveCount = 0;
        #endregion

        #region board variables
        public int Left { get; }
        public int Top { get; }
        public Field[,] Fields { get; private set; }
        #endregion

        #region constructing
        public Board(int left, int top)
        {
            Left = left;
            Top = top;
            SetFields();
            Console.SetWindowSize(5 * left + 8 + (left + 1) / 2 * 7, (Top + 1) * 2 + 1);
            Console.SetBufferSize(5 * left + 8 + (left + 1) / 2 * 7, (Top + 1) * 2 + 1);
        }

        private void SetFields()
        {
            // set array 
            Fields = new Field[Left, Top];

            // create single fields in array
            for (int i = 0; i < Left; i++)
            {
                for (int j = 0; j < Top; j++)
                {
                    Fields[i, j] = new Field(i, j, this);
                }
            }

            // create fields' knight move neighbours
            foreach (Field item in Fields)
            {
                item.CalcKnightMoveNeighbours();
            }
        }

        public void Clear()
        {
            Fields = new Field[Left, Top];
            SetFields();
            KnightPos = null;
            moveCount = 0;
            PrintBoard();
        }
        #endregion

        #region knight movement methods
        private bool MoveKnight()
        {
            bool knightMoved = true;

            Field nextPos = KnightPos.GetUsableNeighbourWithMaxOpenNexts();

            if (nextPos == null)
            {
                knightMoved = false;
            }
            {
                KnightPos = nextPos;
            }
            return knightMoved;
        }

        public void StartMoving(int left, int top, int timeToRun = 0, bool delay = true)
        {
            PrintBoard();

            // set start pos 
            KnightPos = Fields[left, Top - 1 - top];

            // move until no move possible
            do
            {
                moveCount++;
                KnightPos.UsedAs = moveCount;

                PrintAfterMove();

                #region step in by key or by timedelay
                int delayMS = timeToRun * 1000 / Left / Top;
                if (delay)
                {
                    if (timeToRun > 0)
                    {
                        System.Threading.Thread.Sleep(delayMS);
                    }
                    else
                    {
                        Console.ReadKey();
                    }
                }
                #endregion

            } while (MoveKnight());

            #region print loop finished
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;

            Console.SetCursorPosition(Left * 5 + 6, 0);
            Console.Write(" Ende! ");
            Console.ReadKey();
            #endregion
        }
        #endregion

        #region print
        // print initial board
        private void PrintBoard()
        {
            Console.Clear();

            PrintHLine();
            for (int i = 0; i < Top; i++)
            {
                Console.Write(" {0,2} |", (char)((Top - i) < 10 ? Top - i + 48 : (Top - i + 87)));
                for (int j = 0; j < Left; j++)
                {
                    Console.Write("    " + sideBorder);
                }
                Console.WriteLine();
                PrintHLine();
            }
            Console.Write("   ");
            for (int i = 0; i < Left; i++)
            {
                Console.Write("    " + (char)(i + 65));
            }
            Console.WriteLine();
        }
        private void PrintHLine(ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.Write("    " + corner);
            for (int i = 0; i < Left; i++)
            {
                Console.Write(topBorder + corner);
            }
            Console.WriteLine();
            Console.ResetColor();
        }
        
        // print after each move
        private void PrintAfterMove()
        {
            #region print moves list entry
            Console.SetCursorPosition(Left * 5 + 6 + (moveCount - 1) / (Top * 2) * 7, 1 + (moveCount - 1) % (Top * 2));
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write(" {0,2}.", moveCount);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("{0}", KnightPos);
            #endregion

            #region print movesCount on Field
            Console.SetCursorPosition(6 + KnightPos.Left * 5, 1 + KnightPos.Top * 2);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("{0,2}", moveCount);
            #endregion

            Console.ResetColor();
        }
        #endregion
    }
}
