﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A07_SpringerTour
{
    class Field
    {
        // possible moves for a knight
        static int[,] knightMovesLeftTop = new int[8, 2]
            {
                    { 1, 2 }
                    , { 2, 1 }
                    , { -1, 2 }
                    , { -2, 1 }
                    , { 1, -2 }
                    , { 2, -1 }
                    , { -1, -2 }
                    , { -2, -1 }
            };

        // board fields are aligned to
        private Board board;

        // attributes
        public int Left { get; }
        public int Top { get; }
        public int UsedAs { get; set; }

        public List<Field> KnightMoveNeighbours { get; private set; }
        public int OpenNeighbours
        {
            get
            {
                int open = 0;
                foreach (var item in KnightMoveNeighbours)
                {
                    open += item.UsedAs == 0 ? 1 : 0;
                }
                return open;
            }
        }

        // constructor
        public Field(int left, int top, Board board)
        {
            Left = left;
            Top = top;
            this.board = board;
            UsedAs = 0;
            KnightMoveNeighbours = new List<Field>();
        }

        // add neighbours to list
        public void CalcKnightMoveNeighbours()
        {
            // coords of neighbour field
            int newLeft;
            int newTop;
            // test all possible moves
            for (int i = 0; i < knightMovesLeftTop.GetLength(0); i++)
            {
                newLeft = Left + knightMovesLeftTop[i, 0];
                newTop = Top + knightMovesLeftTop[i, 1];
                // if coords in board range
                if (-1 < newLeft && newLeft < board.Left && -1 < newTop && newTop < board.Top)
                {
                    KnightMoveNeighbours.Add(board.Fields[newLeft, newTop]);
                }
            }
        }

        // get not stepped on neighbour with possible moves left
        public Field GetUsableNeighbourWithMaxOpenNexts()
        {
            Field neighbour = null;

            // loop through neihno
            foreach (Field item in KnightMoveNeighbours)
            {
                if (item.UsedAs == 0)
                {
                    // if no next field is set
                    if (neighbour == null)
                    {
                        neighbour = item;
                    }
                    // if next field is set , has more nexts then actual list item and list item has at least 1 next
                    else if (neighbour != null && neighbour.OpenNeighbours > item.OpenNeighbours && item.OpenNeighbours > 0)
                    {
                        neighbour = item;
                    }
                }
            }
            return neighbour;
        }

        // to string
        public override string ToString()
        {
            return string.Format("{0}{1}", (char)(Left+65), (char)(board.Top - Top<10? board.Top - Top+48:(board.Top - Top+87)));
        }
    }
}
