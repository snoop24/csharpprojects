﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A01_IndexerSimpleList
{
    //class SimpleList<T> where T : IComparable
    class SimpleList<T> : IEnumerable<T> where T : IComparable
    {
        #region attribute
        // attribute
        private SLEntry<T> head;
        private SLEntry<T> tail;
        private int count;
        #endregion

        #region property
        //property
        public bool IsEmpty { get => head == null; }
        public int Count { get => count; }
        #endregion

        #region index enumerable
        public T this[int index] { get => ReturnAt(index); set => SetAt(index, value); }

        public IEnumerable<T> GetENum(int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return ReturnAt(i);
            }
        }
        public IEnumerable<T> GetEntries()
        {
            return GetENum(count);
        }

        #endregion

        #region add remove
        public void Add(T data)
        {
            SLEntry<T> temp = new SLEntry<T>(data);

            if (IsEmpty)
            {
                head = temp;
            }
            else
            {
                tail.Next = temp;
            }
            tail = temp;
            count++;
        }

        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }

        public void RemoveAt(int index)
        {
            // if index in range
            if (-1 < index && index < count)
            {
                // if index is first entry
                if (index == 0)
                {
                    head = head.Next;
                }
                else
                {
                    SLEntry<T> indexParent = head;
                    int i = 0;

                    // goto 1 before index
                    while (i + 1 < index)
                    {
                        i++;
                        indexParent = indexParent.Next;
                    }

                    // delete entry
                    indexParent.Next = indexParent.Next.Next;
                }
                count--;
            }
        }

        public void Remove(T data)
        {
            // get index of an entry with data
            int index = IndexOf(data);
            // if element found remove it
            if (index != -1) { RemoveAt(index); }
        }

        public void RemoveAll(T data)
        {
            // get index of first entry with data
            int index = IndexOf(data);

            // if element found remove it and get next entry and if element found remove it and ...
            while (index != -1)
            {
                RemoveAt(index);
                index = IndexOf(data);
            }
        }
        #endregion

        #region search return
        public int IndexOf(T data)
        {
            // actual entry
            SLEntry<T> searchEntry = head;

            int indexOf = -1;
            int index = -1;

            // search until entry is null
            while (searchEntry != null)
            {
                index++;

                // if match set return value and break
                if (searchEntry.Data.Equals(data))
                {
                    indexOf = index;
                    break;
                }
                // goto next entry
                else
                {
                    searchEntry = searchEntry.Next;
                }
            }

            return indexOf;
        }

        private SLEntry<T> ReturnElementAt(int index)
        {
            SLEntry<T> returnAt = null;

            // if index in range
            if (index < 0 && count - 1 < index)
            {
                throw new IndexOutOfRangeException();
            }
            else
            {
                SLEntry<T> searchEntry = head;
                int i = 0;

                // goto index
                while (i < index && searchEntry != null)
                {
                    i++;
                    searchEntry = searchEntry.Next;
                }
                returnAt = searchEntry;
            }

            return returnAt;
        }

        public T ReturnAt(int index)
        {
            return ReturnElementAt(index).Data;
        }

        public void SetAt(int index, T data)
        {
            ReturnElementAt(index).Data = data;
        }
        #endregion

        #region format
        public T[] ToArray()
        {
            T[] array = new T[Count];

            SLEntry<T> temp = head;

            int i = 0;

            // loop through elements until element is null
            while (temp != null)
            {
                array[i] = temp.Data;
                temp = temp.Next;
                i++;
            }

            return array;
        }

        public override string ToString()
        {
            return string.Join(" | ", ToArray());
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
