﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A01_IndexerSimpleList
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleList<int> testList = new SimpleList<int>();


            Console.WriteLine(testList);

            testList.Remove(5);

            Console.WriteLine(testList);

            testList.Add(5);

            Console.WriteLine(testList);

            testList.Add(6);

            Console.WriteLine(testList);

            testList.Add(7);

            Console.WriteLine(testList);

            testList.Add(8);

            Console.WriteLine(testList);

            testList.Remove(6);

            Console.WriteLine(testList);

            Console.WriteLine(testList[2]);

            Console.WriteLine(testList.IndexOf(8));
            Console.WriteLine(testList);

            testList.RemoveAt(1);
            Console.WriteLine(testList);

            testList[0] = 76;
            Console.WriteLine(testList[0]);
            Console.WriteLine(testList);

            Console.WriteLine("IEnumerable test");

            foreach (var item in testList)
            {
                Console.WriteLine(item);
            }




            Console.ReadKey(true);
        }
    }

   
}
