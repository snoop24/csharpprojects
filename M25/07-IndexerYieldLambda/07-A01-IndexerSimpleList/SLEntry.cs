﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A01_IndexerSimpleList
{
    class SLEntry<T>
    {
        // entry data
        public T Data { get; set; }

        // next entry
        public SLEntry<T> Next { get; set; }

        // constructor
        public SLEntry(T data)
        {
            Data = data;
        }

        // ToString override
        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
