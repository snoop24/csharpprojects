﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A02_LambdaAusdruecke
{
    class Program
    {

        //-	Rückgabe: void, 	Name: AusgabeHandler, 	Parameterliste: string
        //-	Rückgabe: double, 	Name: CalcHandler,	 	Parameterliste: double, double
        //-	Rückgabe: void, 	Name: ChangeHandler, 	Parameterliste: ref double, ref double
        //-	Rückgabe: string Name: ConcatHandler,	Parameterliste: params[] string
        //-	Rückgabe: int, 	Name: GGTHandler,	 	Parameterliste: int, int
        //-	Rückgabe: int, 	Name: KGVHandler, 		Parameterliste: int, int
        //-	Rückgabe: long, 	Name: FakHandler,	 	Parameterliste: byte


        #region delegate 
        delegate void AusgabeHandler(string s);
        delegate double CalcHandler(double d1, double d2);
        delegate void ChangeHandler(ref double d1, ref double d2);
        delegate string ConcatHandler(params string[] array);
        delegate int GGTHandler(int a, int b);
        delegate int KGVHandler(int a, int b);
        delegate long FakHandler(byte b);
        #endregion

        static void Main(string[] args)
        {
            #region Aufgabe 1

            #region declare etc
            AusgabeHandler ausgabe = s => Console.WriteLine(s);

            CalcHandler calcAdd = (d1, d2) => d1 + d2;
            CalcHandler calcSub = (d1, d2) => d1 - d2;
            CalcHandler calcMul = (d1, d2) => d1 * d2;
            CalcHandler calcDiv = (d1, d2) => d2 == 0 ? throw new DivideByZeroException() : d1 / d2;

            ChangeHandler change = (ref double d1, ref double d2) => { if (d1 > d2) d2 = d1 + 0 * (d1 = d2); };


            ConcatHandler concat = array => string.Join("", array);

            GGTHandler ggt = null;
            ggt = (a, b) => b == 0 ? a : ggt(b, a % b);

            KGVHandler kgv = (a, b) => a * b / ggt(a, b);

            FakHandler fak = b =>
            {
                long faculty = 1;
                for (int i = 2; i < b + 1; i++)
                {
                    faculty *= i;
                }
                return faculty;
            };
            #endregion

            // test area

            ausgabe("hallo welt");

            double doub1 = 5;
            double doub2 = 3;
            Console.WriteLine(calcAdd(doub1, doub2));
            Console.WriteLine(calcSub(doub1, doub2));
            Console.WriteLine(calcMul(doub1, doub2));
            Console.WriteLine(calcDiv(doub1, doub2));

            double[] array2 = new double[] { 5, 3 };
            Console.WriteLine(string.Join("  ", array2));
            change(ref array2[0], ref array2[1]);
            Console.WriteLine(string.Join("  ", array2));

            Console.WriteLine(concat("Rainer" + " " + "Zufall"));

            Console.WriteLine(ggt(48, 30));
            Console.WriteLine(ggt(30, 48));

            Console.WriteLine(kgv(15, 16));

            Console.WriteLine(fak(5));
            Console.ReadKey(true);
            #endregion


            #region Aufgabe 2
            Console.Clear();

            Person[] leute = {
                    new Person ("Zufall","Rainer",35)
                    ,new Person ("Schweiß","Axel",94)
                    ,new Person ("Musterfrau","Erika",29)
                    ,new Person ("Mustermann","Max",49)
                    ,new Person ("Rambo","John",59)
            };

            for (int i = 0; i < leute.Length; i++)
            {
                Console.WriteLine(leute[i]);
            }

            Array.Sort(leute, (x, y) => x.Alter.CompareTo(y.Alter));
            Console.WriteLine("\n Sortiert\n");

            for (int i = 0; i < leute.Length; i++)
            {
                Console.WriteLine(leute[i]);
            }

            Console.ReadKey(true);
            #endregion

            #region Aufgabe 3
            Console.Clear();

            List<Person> leuteListe = new List<Person>();

            leuteListe.Add(new Person("Zufall", "Rainer", 35));
            leuteListe.Add(new Person("Schweiß", "Axel", 94));
            leuteListe.Add(new Person("Musterfrau", "Erika", 29));
            leuteListe.Add(new Person("Mustermann", "Max", 49));
            leuteListe.Add(new Person("Rambo", "John", 59));
            leuteListe.Add(new Person("Mall", "Paul", 17));

            Console.WriteLine(string.Join("\n", leuteListe));
            Console.WriteLine("\nerster Name mit M");
            Console.WriteLine(leuteListe.First<Person>(x => x.Name[0] == 'M'));
            Console.WriteLine(leuteListe.FindIndex(x => x.Name[0] == 'M'));
            Console.WriteLine("\nletzter Name mit M");
            Console.WriteLine(leuteListe.Last<Person>(x => x.Name[0] == 'M'));
            Console.WriteLine(leuteListe.FindLastIndex(x => x.Name[0] == 'M'));


            Console.WriteLine("\n\n erster und letzter Name mit M");
            char moep = 'M';
            Console.WriteLine(string.Join("\n", leuteListe.Where<Person>(x => (x == leuteListe.First(y => y.Name[0] == moep) || (x == leuteListe.Last(y => y.Name[0] == moep))))));

            Console.WriteLine();
            Console.ReadKey(true);
            #endregion

        }
    }
}
