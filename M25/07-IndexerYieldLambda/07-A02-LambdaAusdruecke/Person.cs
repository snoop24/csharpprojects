﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A02_LambdaAusdruecke
{
    class Person
    {
        public string Name { get; }
        public string Vorname { get; }
        public int Alter { get; }

        public Person(string name, string vorname, int alter)
        {
            Name = name;
            Vorname = vorname;
            Alter = alter;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}: {2} Jahre",Name, Vorname,Alter); 
        }
    }
}
