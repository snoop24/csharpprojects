﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_A01_MeileKilometerYard
{
    class Meile
    {private double value = 0;

        public double Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public Meile(double value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString() + " Meile(n)";
        }

        #region operator calculation
        // addition
        public static Meile operator +(Meile x, Meile y)
        {
            return new Meile(x.value + y.value);
        }
        public static Meile operator +(Meile x, Kilometer y)
        {
            return x+ (Meile)y;
        }
        public static Meile operator +(Meile x, Meter y)
        {
            return x + (Meile)y;
        }
        public static Meile operator +(Meile x, Yard y)
        {
            return x + (Meile)y;
        }

        // subtraction
        public static Meile operator -(Meile x, Meile y)
        {
            return new Meile(x.value + y.value);
        }
        public static Meile operator -(Meile x, Kilometer y)
        {
            return x - (Meile)y;
        }
        public static Meile operator -(Meile x, Meter y)
        {
            return x - (Meile)y;
        }
        public static Meile operator -(Meile x, Yard y)
        {
            return x - (Meile)y;
        }
        #endregion

        #region conversion
        public static explicit operator Meile(Yard x)
        {
            return new Meile(x.Value / 1760);
        }
        public static explicit operator Meile(Meter x)
        {
            return new Meile(x.Value / 0.9144 / 1760 );
        }
        public static explicit operator Meile(Kilometer x)
        {
            return new Meile(x.Value *1000 / 0.9144 / 1760 );
        }
        #endregion
    }
}
