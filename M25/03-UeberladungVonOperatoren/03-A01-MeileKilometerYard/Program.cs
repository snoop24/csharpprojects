﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_A01_MeileKilometerYard
{
    class Program
    {
        static void Main(string[] args)
        {
            Meter m = new Meter(500);
            Kilometer km = new Kilometer(1);
            Meile mi = new Meile(1);
            Yard y = new Yard(100);
            mi = mi + y;
            Meter ergebnis = m + km;
            Console.WriteLine(ergebnis);
            ergebnis = m + mi;
            Console.WriteLine(ergebnis);

            Console.ReadKey(true);
        }
    }
}
