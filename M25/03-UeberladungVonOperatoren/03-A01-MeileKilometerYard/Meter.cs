﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_A01_MeileKilometerYard
{
    class Meter
    {
        private double value = 0;

        public double Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public Meter(double value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString() + " Meter";
        }

        #region operator calculation
        // addition
        public static Meter operator +(Meter x, Meter y)
        {
            return new Meter(x.value + y.value);
        }
        public static Meter operator +(Meter x, Yard y)
        {
            return x + (Meter)y;
        }
        public static Meter operator +(Meter x, Kilometer y)
        {
            return x + (Meter)y;
        }
        public static Meter operator +(Meter x, Meile y)
        {
            return x + (Meter)y;
        }

        // subtraction
        #endregion

        #region conversion
        public static explicit operator Meter(Kilometer x)
        {
            return new Meter(x.Value * 1000);
        }
        public static explicit operator Meter(Yard x)
        {
            return new Meter(x.Value * 0.9144);
        }
        public static explicit operator Meter(Meile x)
        {
            return new Meter(x.Value * 1760 * 0.9144);
        }
        #endregion
    }
}
