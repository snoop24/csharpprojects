﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_A02_BinaerDezimal
{
    class NoBinaryFormatException : Exception
    {
        public NoBinaryFormatException(string message) : base(message)
        {
        }
    }
}
