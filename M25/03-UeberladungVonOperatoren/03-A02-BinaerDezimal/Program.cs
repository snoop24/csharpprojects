﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_A02_BinaerDezimal
{
    class Program
    {
        static void Main(string[] args)
        {
            // Aufgabe 1

            BinaryValue a = new BinaryValue("1011"); // Dezimal 11 
            BinaryValue b = new BinaryValue(11); // Dezimal 11 
            BinaryValue c = new BinaryValue(0); // Dezimal 0

            c.Add(a, b); // Berechnung c = a + b 

            c.PrintBin(); // Soll den Wert 10110 ausgeben 
            c.PrintDec(); // Soll den Wert 22 ausgeben 

            a.BinValue = "11"; // 3 
            b.DecValue = 5;

            c.Add(a, b);

            c.PrintBin(); // Soll den Wert 1000 ausgeben 
            c.PrintDec(); // Soll den Wert 8 ausgeben 



            // Aufgabe 2

            a = new BinaryValue("1011"); // Dezimal 11 
            b = new BinaryValue(11); // Dezimal 11 
            c = new BinaryValue(0); // Dezimal 0 

            c = a + b;

            c.PrintBin(); // Soll den Wert 10110 ausgeben 
            c.PrintDec(); // Soll den Wert 22 ausgeben



            Console.ReadKey(true);
        }
    }
}
