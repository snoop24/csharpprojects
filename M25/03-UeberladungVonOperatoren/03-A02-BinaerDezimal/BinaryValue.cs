﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_A02_BinaerDezimal
{
    struct BinaryValue
    {
        #region attribute
        private int decValue;
        #endregion

        #region property
        public int DecValue
        {
            get
            {
                return decValue;
            }

            set
            {
                decValue = value> 0 ? value : 0;
            }
        }

        public string BinValue
        {
            get
            {
                return DecimalToBinary(decValue);
            }
            set
            {
                decValue = BinaryToDecimal(value);
            }

        }
        #endregion property

        #region constructor
        public BinaryValue(string binary)
        {
            decValue = BinaryToDecimal(binary);
        }

        public BinaryValue(int decValue)
        {
            this.decValue = decValue;
        }

        //public BinaryValue()
        //{
        //    decValue = 0;
        //}
        #endregion

        #region output
        public void PrintBin()
        {
            Console.WriteLine(BinValue);
        }
        public void PrintDec()
        {
            Console.WriteLine(DecValue);
        }
        #endregion

        #region binary decimal methods
        private static int BinaryToDecimal(string binary)
        {
            int dec = 0;

            foreach (char item in binary)
            {
                if ((47<item) && (item < 50))
                {
                    dec = (dec * 2) + item -48;
                }
                else
                {
                    throw new NoBinaryFormatException("string contains invalid characters. only \"0\" and \"1\" are allowed.");
                }
            }

            return dec;
        }

        private static string DecimalToBinary(int dec)
        {
            StringBuilder binary = new StringBuilder();

            do
            {
                binary.Insert(0,dec % 2);
                dec /= 2;
            } while (dec > 0);

            return binary.ToString();
        }
        #endregion

        #region calculation methods
        public void Add(BinaryValue a, BinaryValue b)
        {
            decValue = a.DecValue + b.DecValue;
        }

        public void Subtract(BinaryValue a, BinaryValue b)
        {
            decValue = (a.DecValue - b.DecValue > 0) ? a.DecValue - b.DecValue : 0;
        }

        public void Multiply(BinaryValue a, BinaryValue b)
        {
            decValue = a.DecValue * b.DecValue;
        }

        public void Divide(BinaryValue a, BinaryValue b)
        {
            decValue = b.DecValue > 0 ? a.DecValue / b.DecValue : 0;
        }
        #endregion

        #region operator overload
        public static BinaryValue operator +(BinaryValue a, BinaryValue b)
        {
            BinaryValue temp = new BinaryValue(0);
            temp.Add(a, b);
            return temp;
        }

        public static BinaryValue operator -(BinaryValue a, BinaryValue b)
        {
            BinaryValue temp = new BinaryValue(0);
            temp.Subtract(a, b);
            return temp;
        }

        public static BinaryValue operator *(BinaryValue a, BinaryValue b)
        {
            BinaryValue temp = new BinaryValue(0);
            temp.Multiply(a, b);
            return temp;
        }

        public static BinaryValue operator /(BinaryValue a, BinaryValue b)
        {
            BinaryValue temp = new BinaryValue(0);
            temp.Divide(a, b);
            return temp;
        }
        #endregion
    }
}
