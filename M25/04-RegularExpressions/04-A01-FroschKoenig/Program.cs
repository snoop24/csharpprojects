﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace _04_A01_FroschKoenig
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.SetBufferSize(120, 320);
            Console.SetWindowSize(120, 80);

            string headLine = "";
            string regex = "";

            // 63 Treffer
            headLine = "- Alle Zeilen mit einem Umlaut(große und kleine Umlaute). ";
            regex = @"[äöü]";
            MatchIt(headLine, regex, RegexOptions.IgnoreCase);

            // 35 Treffer
            headLine = "- Alle Zeilen in denen das Wort „der“ alleine steht(d.h.als einzelnes Wort).";
            regex = @"\b[Dd]er\b";
            MatchIt(headLine, regex);

            // 48 Treffer
            headLine = "- Alle Zeilen die mit einem großen Buchstaben beginnen.";
            regex = @"^[A-ZÄÖÜ]";
            MatchIt(headLine, regex);

            // 23 Treffer
            headLine = "- Alle Zeilen, in denen das Wort Frosch oder Froschkönig vorkommt.";
            regex=@"\bFrosch(könig)?\b";
            MatchIt(headLine, regex);

            // 12 Treffer
            headLine = "- Alle Zeilen mit einem. (Punkt) am Ende der Zeile. ";
            regex = @"\.$";
            MatchIt(headLine, regex);

            // 15 Treffer
            headLine = "- Alle Zeilen in denen ein Wort mit ß am Ende des Wortes steht(daß, saß, heiß, usw.). ";
            regex = @"ß\b";
            MatchIt(headLine, regex);

            // 30 Treffer
            headLine = "- Die Anzahl der leeren Zeilen im Dokument. Hinweis: Eine leere Zeile ist eine Zeile, bei der am Zeilenanfang das Zeilenende steht.";
            regex = @"^$";
            MatchIt(headLine, regex);

            // 32 Treffer
            headLine = "- Alle Zeilen bei denen am Anfang der Zeile ein Wort mit genau 3 Buchstaben steht(und, der, sie, usw.). Es sollen große und kleine Wörter ausgeben werden.";
            regex = @"^[a-zäöüß]{3}\b";
            MatchIt(headLine, regex, RegexOptions.IgnoreCase);

            // 56 Treffer
            headLine = "-Alle Zeilen die einen bestimmten Artikel enthalten(der, die, das).";
            regex = @"\b[Dd]((er)|(ie)|(as))\b";
            MatchIt(headLine, regex);
            
        }


        static void MatchIt(string message, string matchIt)
        {
            MatchIt(message, matchIt, RegexOptions.None);
        }

        static void MatchIt(string message, string matchIt, RegexOptions option)
        {

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(message);
            Console.ResetColor();
            Console.WriteLine();

            StreamReader readIt = new StreamReader("FroschkoenigUnixZeilenumbrueche.txt");
            int count = 0;
            int matches = 0;
            string testString = "";

            while (!readIt.EndOfStream)
            {
                count++;

                testString = readIt.ReadLine();

                if (Regex.IsMatch(testString, matchIt, option))
                {
                    matches++;
                    Console.WriteLine("{0,3}:{1,3}: {2}", matches, count, testString);
                }
            }
            readIt.Close();

            Console.WriteLine("Treffer: " + matches);
            Console.WriteLine();


            Console.ReadKey(true);
            Console.Clear();
        }


    }
}
