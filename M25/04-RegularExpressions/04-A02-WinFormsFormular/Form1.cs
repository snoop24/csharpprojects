﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace _04_A02_WinFormsFormular
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btbCheck_Click(object sender, EventArgs e)
        {
            InputCheck();
        }


        private void InputCheck()
        {
            bool anwenderNameOK = false;
            bool emailOK = false;
            bool telefonOK = false;
            bool ipV4AdresseOK = false;

            string ergebnis = "Eingaben OK!";

            string anwenderNamePattern = @"[@#!$&]$";

            string emailPattern = @"^\w+([-.]\w+)*@\w+([-.]\w+)*\.\w+";

            string telefonPattern = @"^\+[1-9][0-9]{0,2}[-\s]?[1-9][0-9]{2,3}[-\s]?[1-9][0-9]{3,7}$";

            string ipV4Pattern = @"^(([0-9])|([1-9][0-9])|(1[0-9][0-9])|(2[0-4][0-9])|(25[0-5]))\.(([0-9])|([1-9][0-9])|(1[0-9][0-9])|(2[0-4][0-9])|(25[0-5]))\.(([0-9])|([1-9][0-9])|(1[0-9][0-9])|(2[0-4][0-9])|(25[0-5]))\.(([0-9])|([1-9][0-9])|(1[0-9][0-9])|(2[0-4][0-9])|(25[0-5]))$";

            anwenderNameOK = !Regex.IsMatch(tBAnwenderName.Text, anwenderNamePattern);
            emailOK = Regex.IsMatch(tBEmail.Text, emailPattern);
            telefonOK = Regex.IsMatch(tBTelefon.Text, telefonPattern);
            ipV4AdresseOK = Regex.IsMatch(tBIPv4.Text, ipV4Pattern);

            if (!anwenderNameOK || !emailOK || !telefonOK || !ipV4AdresseOK)
            {
                ergebnis = string.Format(
                "Fehlerhafte Eingabe(n)!\n" +
                "{0}" +
                "{1}" +
                "{2}" +
                "{3}"
                , anwenderNameOK ? "" : "Anwendername\n"
                , emailOK ? "" : "Email\n"
                , telefonOK ? "" : "Telefon\n"
                , ipV4AdresseOK ? "" : "IPv4 Adresse");
            }
            MessageBox.Show(ergebnis);

        }
    }
}
