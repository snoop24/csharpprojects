﻿namespace _04_A02_WinFormsFormular
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbAnwenderName = new System.Windows.Forms.Label();
            this.tBAnwenderName = new System.Windows.Forms.TextBox();
            this.tBEmail = new System.Windows.Forms.TextBox();
            this.lbEmail = new System.Windows.Forms.Label();
            this.tBTelefon = new System.Windows.Forms.TextBox();
            this.lTelefon = new System.Windows.Forms.Label();
            this.tBIPv4 = new System.Windows.Forms.TextBox();
            this.lbIPv4 = new System.Windows.Forms.Label();
            this.btbCheck = new System.Windows.Forms.Button();
            this.lbErgebnis = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbAnwenderName
            // 
            this.lbAnwenderName.AutoSize = true;
            this.lbAnwenderName.Location = new System.Drawing.Point(12, 9);
            this.lbAnwenderName.Name = "lbAnwenderName";
            this.lbAnwenderName.Size = new System.Drawing.Size(81, 13);
            this.lbAnwenderName.TabIndex = 0;
            this.lbAnwenderName.Text = "Anwendername";
            // 
            // tBAnwenderName
            // 
            this.tBAnwenderName.Location = new System.Drawing.Point(15, 25);
            this.tBAnwenderName.Name = "tBAnwenderName";
            this.tBAnwenderName.Size = new System.Drawing.Size(226, 20);
            this.tBAnwenderName.TabIndex = 1;
            // 
            // tBEmail
            // 
            this.tBEmail.Location = new System.Drawing.Point(15, 64);
            this.tBEmail.Name = "tBEmail";
            this.tBEmail.Size = new System.Drawing.Size(226, 20);
            this.tBEmail.TabIndex = 3;
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Location = new System.Drawing.Point(12, 48);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(32, 13);
            this.lbEmail.TabIndex = 2;
            this.lbEmail.Text = "Email";
            // 
            // tBTelefon
            // 
            this.tBTelefon.Location = new System.Drawing.Point(15, 103);
            this.tBTelefon.Name = "tBTelefon";
            this.tBTelefon.Size = new System.Drawing.Size(226, 20);
            this.tBTelefon.TabIndex = 5;
            // 
            // lTelefon
            // 
            this.lTelefon.AutoSize = true;
            this.lTelefon.Location = new System.Drawing.Point(12, 87);
            this.lTelefon.Name = "lTelefon";
            this.lTelefon.Size = new System.Drawing.Size(43, 13);
            this.lTelefon.TabIndex = 4;
            this.lTelefon.Text = "Telefon";
            // 
            // tBIPv4
            // 
            this.tBIPv4.Location = new System.Drawing.Point(15, 142);
            this.tBIPv4.Name = "tBIPv4";
            this.tBIPv4.Size = new System.Drawing.Size(226, 20);
            this.tBIPv4.TabIndex = 7;
            // 
            // lbIPv4
            // 
            this.lbIPv4.AutoSize = true;
            this.lbIPv4.Location = new System.Drawing.Point(12, 126);
            this.lbIPv4.Name = "lbIPv4";
            this.lbIPv4.Size = new System.Drawing.Size(73, 13);
            this.lbIPv4.TabIndex = 6;
            this.lbIPv4.Text = "IPv4-Adresse:";
            // 
            // btbCheck
            // 
            this.btbCheck.Location = new System.Drawing.Point(15, 168);
            this.btbCheck.Name = "btbCheck";
            this.btbCheck.Size = new System.Drawing.Size(226, 30);
            this.btbCheck.TabIndex = 8;
            this.btbCheck.Text = "Prüfen";
            this.btbCheck.UseVisualStyleBackColor = true;
            this.btbCheck.Click += new System.EventHandler(this.btbCheck_Click);
            // 
            // lbErgebnis
            // 
            this.lbErgebnis.AutoSize = true;
            this.lbErgebnis.Location = new System.Drawing.Point(152, 28);
            this.lbErgebnis.Name = "lbErgebnis";
            this.lbErgebnis.Size = new System.Drawing.Size(0, 13);
            this.lbErgebnis.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 210);
            this.Controls.Add(this.lbErgebnis);
            this.Controls.Add(this.btbCheck);
            this.Controls.Add(this.tBIPv4);
            this.Controls.Add(this.lbIPv4);
            this.Controls.Add(this.tBTelefon);
            this.Controls.Add(this.lTelefon);
            this.Controls.Add(this.tBEmail);
            this.Controls.Add(this.lbEmail);
            this.Controls.Add(this.tBAnwenderName);
            this.Controls.Add(this.lbAnwenderName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbAnwenderName;
        private System.Windows.Forms.TextBox tBAnwenderName;
        private System.Windows.Forms.TextBox tBEmail;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.TextBox tBTelefon;
        private System.Windows.Forms.Label lTelefon;
        private System.Windows.Forms.TextBox tBIPv4;
        private System.Windows.Forms.Label lbIPv4;
        private System.Windows.Forms.Button btbCheck;
        private System.Windows.Forms.Label lbErgebnis;
    }
}

