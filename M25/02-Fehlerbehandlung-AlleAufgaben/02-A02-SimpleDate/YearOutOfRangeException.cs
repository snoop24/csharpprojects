﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A02_SimpleDate
{
    class YearOutOfRangeException:Exception
    {
        public YearOutOfRangeException(string message) 
            : base(message)
        { }
    }
}
