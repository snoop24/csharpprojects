﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A02_SimpleDate
{
    class SimpleDate
    {
        enum MonateDE { Januar, Februar, März, April, Mai, Juni, Juli, August, September, Oktober, November, Dezember }

        private int year = 1900;
        private int month = 1;
        private int day = 1;

        #region property
        public int Day
        {
            get{ return day; }
            set
            {

                if (DayExistsInMonthInYear(value))
                //if (0<day && day < DateTime.DaysInMonth(year,month))
                {
                    day = value;
                }
                else
                {
                    throw new DayOfMonthException(string.Format("Der Tag kommt im gewählten Monat({0}) des Jahres {1} nicht vor!", (MonateDE)month, year));
                }
            }
        }

        public int Month
        {
            get { return month; }
            set
            {
                if (0 < value && value < 13)
                {
                    month = value;

                }
                else
                {
                    throw new MonthOutOfRangeException("Es gibt nur Monatsnummern von 1 - 12!");
                }
            }
        }

        public int Year
        {
            get { return year; }
            set
            {
                if (0 < value && value < 10000)
                {
                    year = value;

                }
                else
                {
                    throw new YearOutOfRangeException("Jahreszahlen sind nur von 1 - 9999 zugelassen!");
                }
            }
        }
        #endregion

        #region Constructor
        public SimpleDate() { }

        public SimpleDate(int year, int month, int day)
        {
            Year = year;
            Month = month;
            Day = day;
        }
        #endregion

        public bool IsLeapYear()
        {
            bool isLeapYear = false;
            if (year < 1582)
            {
                if (year % 4 == 0)
                {
                    isLeapYear = true;
                }
            }
            else if ((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0))
            {
                isLeapYear = true;
            }

            return isLeapYear;
        }

        private bool DayExistsInMonthInYear(int day)
        {
            bool dayExists = false;
            int[] monthDaysOfYear = new int[12] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

            //  (day greater than zero) and ( (lower than max monthDays + 1) or ((leap year) and (february) and (day < 29+1))
            if (0 < day && (day < monthDaysOfYear[month - 1] + 1) || (IsLeapYear() && month == 2 && day < 29 + 1))
            {
                dayExists = true;
            }

            return dayExists;
        }

    }
}