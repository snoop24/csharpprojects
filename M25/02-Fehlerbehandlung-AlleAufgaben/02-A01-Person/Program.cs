﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A01_Person
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person("Müller", "Peter", 20);

            p1.Alter = 25;
            Console.WriteLine(p1.Alter);
            Console.ReadKey();

            p1.Alter = -25;
            Console.WriteLine(p1.Alter);
            Console.ReadKey();
        }
    }
}
