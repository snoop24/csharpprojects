﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A01_Person
{
    class Person
    {
        public string Name { get; set; }
        public string Vorname { get;  set; }

        private int alter;
        public int Alter
        {
            get
            {
                return alter;
            }
            set
            {
                if (value < 0)
                {
                    alter = 0;
                    throw new Exception("Kein negatives Alter erlaubt!");
                }
                else
                {
                    alter = value;
                }
            }

        }



        public Person(string name, string vorname, int alter)
        {
            Name = name;
            Vorname = vorname;
            Alter = alter;
        }
    }
}
