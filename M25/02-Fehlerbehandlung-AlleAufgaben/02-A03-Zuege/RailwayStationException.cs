﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A03_Zuege
{
    class RailwayStationException : Exception
    {
        public RailwayStationException()
        {
        }

        public RailwayStationException(string message) 
            : base(message)
        {
        }
    }
}
