﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A03_Zuege
{
    class RailwayStation
    {
        private List<Train> trainsInStation = new List<Train>();
        private int maxCarriagesInStation;
        private int carriagesInStation;

        public RailwayStation(int maxCarriagesInStation)
        {
            this.maxCarriagesInStation = maxCarriagesInStation;
        }

        public void AddTrain(Train item)
        {
            if (maxCarriagesInStation < carriagesInStation + item.CarriagesCount)
            {
                throw new RailwayStationException(item.Number + " : train can't be added");
            }
            else
            {
                trainsInStation.Add(item);
                carriagesInStation += item.CarriagesCount;
            }
        }

        public Train TrainLeave()
        {
            Train temp = null;

            if (trainsInStation.Count < 1)
            {
                throw new RailwayStationException("no train in railwaystation");
            }
            else
            {
                carriagesInStation -= trainsInStation[0].CarriagesCount;
                trainsInStation.RemoveAt(0);
            }


            return temp;
        }

        public List<Train> TrainsInStation()
        {
            return trainsInStation;
        }

        public void PrintTrains()
        {
            foreach (var item in trainsInStation)
            {
                Console.WriteLine(item);
            }
        }
    }
}
