﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A03_Zuege
{
    class Program
    {
        static void Main(string[] args)
        {
            Train re01 = new Train("RE01", 11);
            Train re02 = new Train("RE02", 12);
            Train re03 = new Train("RE03", 13);
            Train re04 = new Train("RE04", 14);
            Train re05 = new Train("RE05", 15);
            Train re06 = new Train("RE06", 10);
            Train re07 = new Train("RE07", 09);
            Train re08 = new Train("RE08", 08);
            Train re09 = new Train("RE09", 07);
            Train re10 = new Train("RE10", 06);

            RailwayStation herne = new RailwayStation(50);

            //herne.TrainLeave();
            herne.AddTrain(re01);
            herne.AddTrain(re02);
            herne.AddTrain(re03);
            herne.AddTrain(re04);

            herne.PrintTrains();
            Console.WriteLine();

            List<Train> temp = herne.TrainsInStation();

            temp.RemoveRange(3, 1);
            temp.Add(re05);

            herne.PrintTrains();

            Console.ReadKey(true);
        }
    }
}
