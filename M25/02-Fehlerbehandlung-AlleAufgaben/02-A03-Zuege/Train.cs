﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A03_Zuege
{
    class Train
    {
        public string Number { get; }
        public int CarriagesCount { get; }

        public Train(string number, int carriagesCount)
        {
            Number = number;
            CarriagesCount = carriagesCount;
        }

        public override string ToString()
        {
            return Number + "(" + CarriagesCount + ")";
        }
    }
}
