﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace _09_A01_Erweiterungsmethoden
{
    static class StringHelper
    {
        static public Int32 ToInt32(this string s)
        {
            return Int32.Parse(s);
        }

        static public string ReverseString(this string s)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char item in s)
            {
                sb.Insert(0,item);
            }
            
            return sb.ToString();
        }

        static public bool IsPalindrom(this string s)
        {
            bool isPalindrom = true;

            // unifying to lower case and removing punctuation and spaces
            s = s.ToLower();
            s = Regex.Replace(s, @"\p{P}", "");
            s = Regex.Replace(s, @"\s", "");

            // actual palindrom check
            for (int i = 0; i < s.Length / 2; i++)
            {
                if (s[i] != s[s.Length - 1 - i])
                {
                    return false;
                }
            }

            return isPalindrom;
        }

    }
}
