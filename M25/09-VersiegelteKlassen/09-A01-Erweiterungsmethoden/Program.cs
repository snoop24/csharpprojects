﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A01_Erweiterungsmethoden
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Console.WriteLine("Dreh Dich nicht um, denn der Schnittjer geht herum.".ReverseString());

            Console.WriteLine(  );

            string pal1 = "O Genie, der Herr ehre dein Ego!";
            string pal2 = "Oh Genie, der Herr ehre dein Ego!";
            Console.WriteLine("{0}\n  Ist ein Palindrom:  {1}",pal1,pal1.IsPalindrom());
            Console.WriteLine();
            Console.WriteLine("{0}\n  Ist ein Palindrom:  {1}", pal2, pal2.IsPalindrom());

            Console.WriteLine();

            string aInt32 = "12";

            Console.WriteLine(aInt32.ToInt32());
            Console.WriteLine();
            Console.ReadKey(true);

            string bInt32 = "12a";

            Console.WriteLine(bInt32.ToInt32());

            Console.ReadKey(true);
        }
    }
}
