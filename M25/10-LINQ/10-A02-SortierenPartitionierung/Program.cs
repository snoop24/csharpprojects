﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _10_A02_SortierenPartitionierung
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetBufferSize(80, 640);
            Console.SetWindowSize(80, 80);

            //            Aufgabe – Sortieren
            // part 1
            #region 
            //Es sei folgendes Array gegeben: 
            string message = "";
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0, 22, 12, 16, 18, 11, 19, 13 };
            Console.WriteLine();
            Console.WriteLine(string.Join(" | ", numbers));
            Console.WriteLine();

            message = "1.Geben Sie das obige Array aufsteigend sortiert aus";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numbers.OrderBy(x=>x)));
            Console.WriteLine(string.Join(" | ", from x in numbers orderby x select x));
            Console.WriteLine();
            
            message = "2.Geben Sie das obige Array absteigend sortiert aus";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numbers.OrderByDescending(x => x)));
            Console.WriteLine(string.Join(" | ", from x in numbers orderby x descending select x));
            Console.WriteLine();
            
            message="1.Geben Sie das obige Array aufsteigend sortiert aus";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numbers.Where(x => x %2==0).OrderBy(x => x)));
            Console.WriteLine(string.Join(" | ", from x in numbers where x %2==0 orderby x select x));
            Console.WriteLine();
            Console.ReadKey(true);
            Console.Clear();
            #endregion

            // part 1
            #region 
            //Es sei folgendes Array gegeben: 

            string[] numStrings = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen" };
            Console.WriteLine();
            Console.WriteLine(string.Join(" | ", numStrings));
            Console.WriteLine();
            
            message ="1.Geben Sie das obige Array nach der Länge der Worte aufsteigend sortiert aus";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numStrings.OrderBy(x => x.Length)));
            Console.WriteLine(string.Join(" | ", from x in numStrings orderby x.Length select x));
            Console.WriteLine();
            
            message = "2.Geben Sie das obige Array nach der Länge der Worte aufsteigend sortiert aus, bei gleicher Länge soll alphabetisch absteigend sortiert werden";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numStrings.OrderBy(x => x.Length).ThenBy(x=> x)));
            Console.WriteLine(string.Join(" | ", from x in numStrings orderby x.Length,x select x));
            Console.WriteLine();

            message = "3.Drehen Sie die Reihenfolge der Elemente im Array um";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numStrings.Reverse()));
            Console.WriteLine(string.Join(" | ", (from x in numStrings orderby x.Length, x select x).Reverse()));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();


            //Erstellen Sie ein DirectoryInfo-Objekt für ein Verzeichnis Ihrer Wahl.

            var files = new DirectoryInfo(@"D:\Microsoft Visual Studio 14.0\Common7\IDE\NewFileItems").GetFiles();


            message = "4.Listen Sie alle Dateien in dem Verzeichnis, absteigend nach Namen sortiert auf";
            Console.WriteLine(message);
            Console.WriteLine(string.Join("\n", 
                files
                .OrderByDescending(x => x.Name)
                ));
            Console.WriteLine("\t\t\t--- --- ---");
            Console.WriteLine(string.Join("\n", 
                from x in files
                orderby x.Name 
                descending select x
                ));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();

            message = "5.Listen Sie alle Dateien in dem Verzeichnis, nach Größe aufsteigend sortiert auf";
            Console.WriteLine(message);
            Console.WriteLine(string.Join("\n", 
                files
                .OrderBy(x => x.Length)
                .Select(x => $"{ x.Length,20} : {x.Name}")
                ));
            Console.WriteLine("\t\t\t--- --- ---");
            Console.WriteLine(string.Join("\n", 
                from x in files
                orderby x.Length
                select $"{ x.Length,20} : {x.Name}"
                ));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();

            message = "6.Listen Sie alle Dateien in dem Verzeichnis, nach dem Datum des letzten Zugriffs auf, jüngste Dateien zuerst";
            Console.WriteLine(message);
            Console.WriteLine(string.Join("\n", 
                files
                .OrderByDescending(x => x.LastAccessTime)
                .Select(x => $"{ x.LastAccessTime,20} : {x.Name}")
                ));
            Console.WriteLine("\t\t\t--- --- ---");
            Console.WriteLine(string.Join("\n", 
                from x in files
                orderby x.LastAccessTime descending
                select $"{ x.LastAccessTime,20} : {x.Name}"
                ));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();
            #endregion

            //Aufgabe – Partitionierung
            #region
            //Es sei folgendes Array gegeben: 

            //int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0, 22, 12, 16, 18, 11, 19, 13 };
            Console.WriteLine();
            Console.WriteLine(string.Join(" | ", numbers));
            Console.WriteLine();

            //Ermitteln Sie mittels LINQ-Ausdrücken die folgenden Informationen:
            
            message = "1.Ermitteln Sie die ersten 5 Elemente im Array";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numbers.Take(5)));
            Console.WriteLine(string.Join(" | ", (from x in numbers select x).Take(5)));
            Console.WriteLine();
            
            message = "2.Ermitteln Sie die letzten 5 Elemente im Array";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numbers.Skip(numbers.Length-5)));
            Console.WriteLine(string.Join(" | ", (from x in numbers select x).Skip(numbers.Length-5)));
            Console.WriteLine();
            
            message = "3.Ermitteln Sie alle Elemente, bis auf die ersten und letzten drei Elemente";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numbers.Take(numbers.Length - 3).Skip(3)));
            Console.WriteLine(string.Join(" | ", (from x in numbers select x).Take(numbers.Length - 3).Skip(3)));
            Console.WriteLine();

            
            message = "4.Ermitteln Sie alle Elemente, beginnend vom ersten Element, die größer als 0 sind";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numbers.Select(x => x > 0)));
            Console.WriteLine(string.Join(" | ", (from x in numbers where x > 0 select x)));
            Console.WriteLine();
            
            message = "5.Ermitteln Sie alle Elemente, beginnend vom ersten Element, die nach der 12 im Array stehen";
            Console.WriteLine(message);
            Console.WriteLine(string.Join(" | ", numbers.SkipWhile(x=> x!=12).Skip(1)));
            Console.WriteLine(string.Join(" | ", (from x in numbers select x).SkipWhile(x => x != 12).Skip(1)));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();

            //Erstellen Sie ein DirectoryInfo - Objekt für ein Verzeichnis Ihrer Wahl.

            message = "7.Listen Sie die fünf neuesten Dateien in dem Verzeichnis auf";
            Console.WriteLine(message);
            Console.WriteLine(string.Join("\n",
                files
                .OrderByDescending(x => x.CreationTime)
                .Select(x => $"{ x.CreationTime,20} : {x.Name}")
                .Skip(5)
                ));
            Console.WriteLine("\t\t\t--- --- ---");
            Console.WriteLine(string.Join("\n",
                from x in files
                orderby x.CreationTime descending
                select $"{ x.CreationTime,20} : {x.Name}"
                ));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();

            ////8.Listen Sie alle Dateien in dem Verzeichnis in „Seiten“ zu je 5 Dateien auf 
            //message = "8.Listen Sie alle Dateien in dem Verzeichnis in „Seiten“ zu je 5 Dateien auf";
            //Console.WriteLine(message);
            //int i = 0;
            //Console.WriteLine(string.Join("\n",
            //    files
            //    .

            //    ));
            //Console.WriteLine("\t\t\t--- --- ---");
            //Console.WriteLine(string.Join("\n",
            //    from x in files
            //    select new {element=x , page=(i++ / 5)  }
            //    ));
            //Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();
            #endregion

            Console.ReadKey(true);
        }
    }
}
