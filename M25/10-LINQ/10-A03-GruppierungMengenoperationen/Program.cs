﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace _10_A03_GruppierungMengenoperationen
{
    class Program
    {
        static void Main(string[] args)
        {
            string message = "";

            //Aufgabe – Gruppierung

            //Es sei folgendes Array gegeben: 

            string[] numbers = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen" };

            message = "1.Gruppieren Sie die Worte im obigen Array nach dem Anfangsbuchstaben";
            Console.WriteLine(message);

            numbers.OrderBy(x => x)
                .GroupBy(x => x[0])
                .ToList()
                .ForEach(firstLetterGroup =>
                {
                    Console.WriteLine(firstLetterGroup.Key);
                    firstLetterGroup.ToList()
                        .ForEach(item => Console.WriteLine("\t" + item));
                });

            Console.ReadKey(true);
            Console.Clear();


            message = "2.Gruppieren Sie die Worte im obigen Array nach der Länge";
            Console.WriteLine(message);

            numbers.OrderBy(x => x.Length)
                .GroupBy(x => x.Length)
                .ToList()
                .ForEach(lengthGroup =>
                {
                    Console.WriteLine(lengthGroup.Key);
                    lengthGroup.ToList()
                        .ForEach(item => Console.WriteLine("\t" + item));
                });

            Console.ReadKey(true);
            Console.Clear();

            message = "3.Gruppieren Sie die Worte im obigen Array nach dem Anfangsbuchstaben und der Länge";
            Console.WriteLine(message);

            numbers.OrderBy(x => x[0]).ThenBy(x => x.Length)
                .GroupBy(x => x[0])
                .ToList()
                .ForEach(firstLetterGroup =>
                {
                    Console.WriteLine(firstLetterGroup.Key);
                    firstLetterGroup.GroupBy(x => x.Length)
                        .ToList()
                        .ForEach(lengthGroup =>
                        {
                            Console.WriteLine("\t"+lengthGroup.Key);
                            lengthGroup.ToList()
                                .ForEach(item => Console.WriteLine("\t\t" + item));
                        });
                });

            Console.ReadKey(true);
            Console.Clear();

            // Die folgenden Gruppierungen sollen auf der Liste der Prozesse auf Ihrem System stattfinden: 
            var procs = Process.GetProcesses();

            // 1. Geben Sie die Prozesse auf Ihrem System gruppiert nach der Anzahl der Threads aus 
            // 2. Geben Sie die Prozesse auf Ihrem System gruppiert nach der Anzahl der Module aus 
            // 3. Geben Sie die Prozesse auf Ihrem System gruppiert nach der Anzahl der Module aus, in der //Ausgabe sollen die Namen der Prozesse alphabetisch aufsteigend sortiert sein 

            // Hinweis: Das Abfragen der Anzahl der Module eines Prozesses führt ggf. zu einer Exception 


            //Aufgabe – Mengenoperationen

            //Es seien folgende Arrays gegeben: 

            int[] factorsOf300 = { 2, 2, 3, 5, 5 };
            int[] numbersA = { 0, 2, 4, 5, 6, 8, 9 };
            int[] numbersB = { 1, 3, 5, 7, 8 };

            Console.Write("factorsOf300\n");
            factorsOf300.ToList().ForEach(x => Console.Write(x + "  "));
            Console.WriteLine("\n");

            Console.Write("numbersA\n");
            numbersA.ToList().ForEach(x => Console.Write(x + "  "));
            Console.WriteLine("\n");

            Console.Write("numbersB\n");
            numbersB.ToList().ForEach(x => Console.Write(x + "  "));
            Console.WriteLine("\n");


            message = "1.Welche einzelnen Faktoren sind in factorsOf300 vertreten?";
            Console.WriteLine(message);

            factorsOf300.Distinct()
                .ToList().ForEach(x => Console.Write(x + "  "));
            Console.WriteLine("\n");


            message = "2.Wie ist die Vereinigungsmenge der beiden Arrays numbersA und numbersB?";
            Console.WriteLine(message);

            numbersA.Union(numbersB)
                .ToList().ForEach(x => Console.Write(x + "  "));
            Console.WriteLine("\n");
            

            message = "3.Haben die beiden Arrays numbersA und numbersB eine Schnittmenge?";
            Console.WriteLine(message);

            numbersA.Intersect(numbersB)
                .ToList().ForEach(x => Console.Write(x + "  "));
            Console.WriteLine("\n");
            

            message = "4.Welche Elemente kommen nur in numbersB vor, aber nicht in numbersA?";
            Console.WriteLine(message);

            numbersB.Except(numbersA)
                .ToList().ForEach(x => Console.Write(x + "  "));
            Console.WriteLine("\n");

            Console.ReadKey(true);
            Console.Clear();

            Console.ReadKey(true);
        }
    }
}
