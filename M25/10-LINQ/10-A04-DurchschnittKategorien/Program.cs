﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _10_A04_DurchschnittKategorien
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string[]> lines = new List<string[]>();

            StreamReader sr = new StreamReader(@"data.csv");

            while (!sr.EndOfStream) 
            {
                lines.Add(sr.ReadLine().Replace(".",",").Split(';'));
            }

            lines.GroupBy(x => x[2])
                .ToList().ForEach(group =>
                    Console.WriteLine(
                        $"{group.Key,20}: {group.Average(x => double.Parse(x.ToArray()[1])),9:0.00}")
                );

            Console.ReadKey(true);
        }
    }
}
