﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace _10_A01_FilterOperationen_Aggregationen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetBufferSize(80, 640);
            Console.SetWindowSize(80, 40);

            // Filter Operationen
            // part 1
            #region 

            //Es sei folgendes Array gegeben:

            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0, 22, 12, 16, 18, 11, 19, 13 };
            Console.WriteLine();
            Console.WriteLine(string.Join(" | ", numbers));
            Console.WriteLine();

            //Ermitteln Sie mittels LINQ-Ausdrücken die folgenden Informationen:

            //1.Alle Zahlen echt kleiner als 7
            Console.WriteLine("1.Alle Zahlen echt kleiner als 7");
            Console.WriteLine(string.Join(" | ", numbers.Where(x => x < 7)));
            Console.WriteLine(string.Join(" | ", from x in numbers where x < 7 select x));
            Console.WriteLine();

            //2.Alle geraden Zahlen
            Console.WriteLine("2.Alle geraden Zahlen");
            Console.WriteLine(string.Join(" | ", numbers.Where(x => x % 2 == 0)));
            Console.WriteLine(string.Join(" | ", from x in numbers where x% 2 ==0 select x));
            Console.WriteLine();

            //3.Alle einstelligen ungeraden Zahlen
            Console.WriteLine("3.Alle einstelligen ungeraden Zahlen");
            Console.WriteLine(string.Join(" | ", numbers.Where(x => x % 2 == 1 && x < 10 && x > -10) ));
            Console.WriteLine(string.Join(" | ", from x in numbers where (x % 2 == 1 && x < 10 && x > -10) select x));
            Console.WriteLine();

            //4.Alle geraden Zahlen ab dem 6 Element(einschließlich) im Array
            Console.WriteLine("4.Alle geraden Zahlen ab dem 6 Element(einschließlich) im Array");
            Console.WriteLine(string.Join(" | ", numbers.Skip(5).Where(x => x % 2 == 0)));
            Console.WriteLine(string.Join(" | ", from x in numbers.Skip(5) where ( x % 2 == 0) select x));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();
            #endregion

            //part2
            #region
            //Es sei folgendes Array gegeben:

            string[] numStrings = new string[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen" };
            Console.WriteLine();
            Console.WriteLine(string.Join(" | ", numStrings));
            Console.WriteLine();

            //Ermitteln Sie mittels LINQ-Ausdrücken die folgenden Informationen:

            //1.Alle „Zahlen“ die drei Zeichen lang sind
            Console.WriteLine("1.Alle „Zahlen“ die drei Zeichen lang sind");
            Console.WriteLine(string.Join(" | ", numStrings.Where(x => x.Length==3)));
            Console.WriteLine(string.Join(" | ", from x in numStrings where  x.Length == 3 select x));
            Console.WriteLine();

            //2.Alle „Zahlen“ die ein „o“ enthalten
            Console.WriteLine("2.Alle „Zahlen“ die ein „o“ enthalten");
            Console.WriteLine(string.Join(" | ", numStrings.Where(x => x.Contains('o'))));
            Console.WriteLine(string.Join(" | ",from x in numStrings where x.Contains('o')select x));
            Console.WriteLine();

            //3.Alle „Zahlen“ die auf „teen“ enden
            Console.WriteLine("3.Alle „Zahlen“ die auf „teen“ enden");
            Console.WriteLine(string.Join(" | ", numStrings.Where(x=> x.EndsWith("teen"))));
            Console.WriteLine(string.Join(" | ", from x in numStrings where x.EndsWith("teen") select x));
            Console.WriteLine();

            //4.Die Großbuchstabendarstellung aller „Zahlen“ die auf „teen“ enden
            Console.WriteLine("4.Die Großbuchstabendarstellung aller „Zahlen“ die auf „teen“ enden");
            Console.WriteLine(string.Join(" | ", numStrings.Where(x => Regex.IsMatch(x, @"teen$")).Select(x => x.ToUpper())));
            Console.WriteLine(string.Join(" | ", from x in numStrings where Regex.IsMatch(x, @"teen$") select x.ToUpper()));
            Console.WriteLine();

            //5.Alle „Zahlen“ die „four“ enthalten
            Console.WriteLine("5.Alle „Zahlen“ die „four“ enthalten");
            Console.WriteLine(string.Join(" | ", numStrings.Where(x => Regex.IsMatch(x, @"four"))));
            Console.WriteLine(string.Join(" | ", from x in numStrings where Regex.IsMatch(x, @"four")select x));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();
            #endregion

            // Aggregationen
            #region
            //Es sei folgendes Array gegeben:
            
            Console.WriteLine();
            Console.WriteLine(string.Join(" | ", numbers));
            Console.WriteLine();

            //Ermitteln Sie mittels LINQ-Ausdrücken die folgenden Informationen:

            //1.Die Summe aller Werte im Array
            Console.WriteLine("1.Die Summe aller Werte im Array");
            Console.WriteLine(string.Join(" | ", numbers.Sum()));
            Console.WriteLine(string.Join(" | ", (from x in numbers select x).Sum() ));
            Console.WriteLine();

            //2.Die kleinste Zahl
            Console.WriteLine("2.Die kleinste Zahl");
            Console.WriteLine(string.Join(" | ", numbers.Min()));
            Console.WriteLine(string.Join(" | ", (from x in numbers select x).Min()));
            Console.WriteLine();

            //3.Die größte Zahl
            Console.WriteLine("3.Die größte Zahl");
            Console.WriteLine(string.Join(" | ", numbers.Max()));
            Console.WriteLine(string.Join(" | ", (from x in numbers select x).Max()));
            Console.WriteLine();

            //4.Den Durchschnittswert
            Console.WriteLine("4.Den Durchschnittswert");
            Console.WriteLine(string.Join(" | ", numbers.Average()));
            Console.WriteLine(string.Join(" | ", (from x in numbers select x).Average()));
            Console.WriteLine();

            //5.Die kleinste gerade Zahl
            Console.WriteLine("5.Die kleinste gerade Zahl");
            Console.WriteLine(string.Join(" | ", numbers.Where(x=> x % 2 ==0).Min()));
            Console.WriteLine(string.Join(" | ", (from x in numbers where x % 2 == 0 select x).Min()));
            Console.WriteLine();

            //6.Die größte ungerade Zahl
            Console.WriteLine("6.Die größte ungerade Zahl");
            Console.WriteLine(string.Join(" | ", numbers.Where(x => x % 2 == 1).Max()));
            Console.WriteLine(string.Join(" | ", (from x in numbers where x % 2 == 1 select x).Max()));
            Console.WriteLine();

            //7.Die Summe aller geraden Zahlen
            Console.WriteLine("7.Die Summe aller geraden Zahlen");
            Console.WriteLine(string.Join(" | ", numbers.Where(x => x % 2 == 0).Sum()));
            Console.WriteLine(string.Join(" | ", (from x in numbers where x % 2 == 0 select x).Sum()));
            Console.WriteLine();

            //8.Den Durchschnittswert aller ungeraden Zahlen
            Console.WriteLine("8.Den Durchschnittswert aller ungeraden Zahlen");
            Console.WriteLine(string.Join(" | ", numbers.Where(x => x % 2 == 1).Average()));
            Console.WriteLine(string.Join(" | ", (from x in numbers where x % 2 == 1 select x).Average()));
            Console.WriteLine();

            Console.ReadKey(true);
            Console.Clear();
            #endregion

        }
    }
}
