﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _99_A01_WinFormsDraw
{
    public partial class Form1 : Form
    {
        static List<Point> points = new List<Point>()
        {
            new Point(50,50)
            , new Point(50, 154)
            ,new Point(23,-150)
            ,new Point(89,-56)
            ,new Point(-68,-156)
            ,new Point(-100,154)
            ,new Point(-220,10)
            ,new Point(-200,176)
        };
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panel1.Paint += PrintPoints;
        }

        private void PrintPoints(object sender, PaintEventArgs e)
        {

            int offsetLeft = (panel1.Width + 1) / 2;
            int offsetTop = (panel1.Height + 1) / 2;

            int minX = points.Min(item => item.X);
            int maxX = points.Max(item => item.X);
            int minY = points.Min(item => item.Y);
            int maxY = points.Max(item => item.Y);
            int offset = 0;

            e.Graphics.DrawRectangle(new Pen(Color.Green), offsetLeft + (minX - offset), offsetTop - (maxY + offset), (maxX + offset) - (minX - offset)+1, (maxY+ offset) - (minY - offset)+1);



            e.Graphics.DrawRectangle(new Pen(Color.Black), offsetLeft-1, offsetTop -1, 3, 3);



            foreach (var item in points)
            {
                e.Graphics.DrawRectangle(new Pen(Color.Red), offsetLeft + item.X, offsetTop - item.Y, 1, 1);
            }
        }
    }
}
