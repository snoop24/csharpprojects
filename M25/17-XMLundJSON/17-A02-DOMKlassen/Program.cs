﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace _17_A02_DOMKlassen
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string inputTxt = "data.txt";
            string outputXml= "data.xml";
            string temp = string.Empty;

            Console.Write("Datei Pfad und Name\n\t\t INPUT: ");
            temp = Console.ReadLine();
            inputTxt = temp == string.Empty ? inputTxt : temp;

            Console.Write("Datei Pfad und Name\n\t\tOUTPUT: ");
            temp = Console.ReadLine();
            outputXml = temp == string.Empty ? outputXml : temp;

            try
            {
                string[] columnNames;
                string[] columns;
                XmlDocument inventory = new XmlDocument();
                inventory.AppendChild(inventory.CreateXmlDeclaration("1.0", "utf-8", "yes"));
                inventory.AppendChild(inventory.CreateElement("inventory"));
                StreamReader sr = new StreamReader(inputTxt);
                if (!sr.EndOfStream)
                {
                    columnNames = sr.ReadLine().ToLower().Split('|');
                    while (!sr.EndOfStream)
                    {
                        columns = sr.ReadLine().Split('|');
                        XmlNode node = inventory.DocumentElement.AppendChild(inventory.CreateElement("item"));
                        for (int i = 0; i < columns.Length; i++)
                        {
                            if (columnNames[i] == "pcname")
                            {
                                node.Attributes.Append(inventory.CreateAttribute("id")).Value = columns[0];
                            }
                            else if (columnNames[i] == "ip")
                            {
                                XmlNode netAddressNode = node.AppendChild(inventory.CreateElement("network"));
                                netAddressNode.Attributes.Append(inventory.CreateAttribute(columnNames[i+1])).Value = columns[i + 1];
                                netAddressNode.AppendChild(inventory.CreateElement(columnNames[i])).InnerText = columns[i];
                                i++;
                            }
                            else
                            {
                                node.AppendChild(inventory.CreateElement(columnNames[i])).InnerText = columns[i];
                            }
                        }


                    }
                }
                inventory.Save(Console.Out);
                inventory.Save(outputXml);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            Console.ReadKey(true);
        }
    }
}
