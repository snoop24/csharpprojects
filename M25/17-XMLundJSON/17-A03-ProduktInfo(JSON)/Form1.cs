﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Json;

namespace _17_A03_ProduktInfo_JSON_
{
    public partial class Form1 : Form
    {
        Warehouse warenhaus;

        public Form1()
        {
            InitializeComponent();

            warenhaus = WareHouseGetFromJSON("Produkte.json");

            comboBoxKategorie.DataSource = warenhaus.Categories.Select(item => item.CategoryName).ToList();
        }

        Warehouse WareHouseGetFromJSON(string path)
        {
            Warehouse temp = new Warehouse();

            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(Category[]));

            try
            {
                using (FileStream fs = File.OpenRead(path))
                {
                    temp.Categories = (Category[])js.ReadObject(fs);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            //// test
            //DataContractJsonSerializer js2 = new DataContractJsonSerializer(typeof(Warehouse));
            //using (FileStream fs = File.Create("test.json"))
            //{
            //    js2.WriteObject(fs, temp);
            //}
            
            return temp;
        }

        private void comboBoxKategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxProduktListe.DataSource = warenhaus.Categories
                .First(item => item.CategoryName == comboBoxKategorie.Text)
                .Products.ToList();
            listBoxProduktListe.DisplayMember = "Name";
            listBoxProduktListe.SelectedIndex = 0;

        }

        private void listBoxProduktListe_SelectedIndexChanged(object sender, EventArgs e)
        {
            Product selected = warenhaus.Categories
                .First(item => item.CategoryName == comboBoxKategorie.Text)
                .Products[listBoxProduktListe.SelectedIndex];

            textBoxProduktName.Text = selected.Name;

            textBoxPreis.Text = selected.Price.ToString();
        }
    }
}
