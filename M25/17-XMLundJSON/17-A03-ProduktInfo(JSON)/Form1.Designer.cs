﻿namespace _17_A03_ProduktInfo_JSON_
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxKategorie = new System.Windows.Forms.ComboBox();
            this.labelKategorieAuswahl = new System.Windows.Forms.Label();
            this.listBoxProduktListe = new System.Windows.Forms.ListBox();
            this.labelProduktListe = new System.Windows.Forms.Label();
            this.labelProduktName = new System.Windows.Forms.Label();
            this.textBoxProduktName = new System.Windows.Forms.TextBox();
            this.textBoxPreis = new System.Windows.Forms.TextBox();
            this.labelPreis = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxKategorie
            // 
            this.comboBoxKategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxKategorie.FormattingEnabled = true;
            this.comboBoxKategorie.Location = new System.Drawing.Point(12, 21);
            this.comboBoxKategorie.Name = "comboBoxKategorie";
            this.comboBoxKategorie.Size = new System.Drawing.Size(278, 21);
            this.comboBoxKategorie.TabIndex = 0;
            this.comboBoxKategorie.SelectedIndexChanged += new System.EventHandler(this.comboBoxKategorie_SelectedIndexChanged);
            // 
            // labelKategorieAuswahl
            // 
            this.labelKategorieAuswahl.AutoSize = true;
            this.labelKategorieAuswahl.Location = new System.Drawing.Point(9, 5);
            this.labelKategorieAuswahl.Name = "labelKategorieAuswahl";
            this.labelKategorieAuswahl.Size = new System.Drawing.Size(91, 13);
            this.labelKategorieAuswahl.TabIndex = 1;
            this.labelKategorieAuswahl.Text = "Kategorieauswahl";
            // 
            // listBoxProduktListe
            // 
            this.listBoxProduktListe.FormattingEnabled = true;
            this.listBoxProduktListe.Location = new System.Drawing.Point(12, 62);
            this.listBoxProduktListe.Name = "listBoxProduktListe";
            this.listBoxProduktListe.Size = new System.Drawing.Size(277, 121);
            this.listBoxProduktListe.TabIndex = 2;
            this.listBoxProduktListe.SelectedIndexChanged += new System.EventHandler(this.listBoxProduktListe_SelectedIndexChanged);
            // 
            // labelProduktListe
            // 
            this.labelProduktListe.AutoSize = true;
            this.labelProduktListe.Location = new System.Drawing.Point(9, 46);
            this.labelProduktListe.Name = "labelProduktListe";
            this.labelProduktListe.Size = new System.Drawing.Size(62, 13);
            this.labelProduktListe.TabIndex = 3;
            this.labelProduktListe.Text = "Produktliste";
            // 
            // labelProduktName
            // 
            this.labelProduktName.AutoSize = true;
            this.labelProduktName.Location = new System.Drawing.Point(333, 5);
            this.labelProduktName.Name = "labelProduktName";
            this.labelProduktName.Size = new System.Drawing.Size(70, 13);
            this.labelProduktName.TabIndex = 4;
            this.labelProduktName.Text = "Produktname";
            // 
            // textBoxProduktName
            // 
            this.textBoxProduktName.Location = new System.Drawing.Point(336, 21);
            this.textBoxProduktName.Name = "textBoxProduktName";
            this.textBoxProduktName.ReadOnly = true;
            this.textBoxProduktName.Size = new System.Drawing.Size(257, 20);
            this.textBoxProduktName.TabIndex = 5;
            // 
            // textBoxPreis
            // 
            this.textBoxPreis.Location = new System.Drawing.Point(336, 62);
            this.textBoxPreis.Name = "textBoxPreis";
            this.textBoxPreis.ReadOnly = true;
            this.textBoxPreis.Size = new System.Drawing.Size(257, 20);
            this.textBoxPreis.TabIndex = 7;
            // 
            // labelPreis
            // 
            this.labelPreis.AutoSize = true;
            this.labelPreis.Location = new System.Drawing.Point(333, 46);
            this.labelPreis.Name = "labelPreis";
            this.labelPreis.Size = new System.Drawing.Size(33, 13);
            this.labelPreis.TabIndex = 6;
            this.labelPreis.Text = "Preis:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 195);
            this.Controls.Add(this.textBoxPreis);
            this.Controls.Add(this.labelPreis);
            this.Controls.Add(this.textBoxProduktName);
            this.Controls.Add(this.labelProduktName);
            this.Controls.Add(this.labelProduktListe);
            this.Controls.Add(this.listBoxProduktListe);
            this.Controls.Add(this.labelKategorieAuswahl);
            this.Controls.Add(this.comboBoxKategorie);
            this.Name = "Form1";
            this.Text = "Produktinformationen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxKategorie;
        private System.Windows.Forms.Label labelKategorieAuswahl;
        private System.Windows.Forms.ListBox listBoxProduktListe;
        private System.Windows.Forms.Label labelProduktListe;
        private System.Windows.Forms.Label labelProduktName;
        private System.Windows.Forms.TextBox textBoxProduktName;
        private System.Windows.Forms.TextBox textBoxPreis;
        private System.Windows.Forms.Label labelPreis;
    }
}

