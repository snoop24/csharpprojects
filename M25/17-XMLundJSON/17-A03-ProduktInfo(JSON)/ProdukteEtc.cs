﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace _17_A03_ProduktInfo_JSON_
{
    [DataContract]
    class Warehouse
    {
        [DataMember]
        public Category[] Categories { get; set; }
    }

    [DataContract]
    class Category
    {
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public Product[] Products { get; set; }
    }

    [DataContract]
    class Product
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Price { get; set; }
    }

}
