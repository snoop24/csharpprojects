import math

def pentagon(n):
	return n * (3 * n - 1) / 2

def isPentagon(n):
	return (math.sqrt(2 / 3 * n + 1 / 36) + 1 / 6) % 1 == 0


def main():
	end = False
	n_diff = 1
	n_p = 1
	diff = pentagon(n_diff)
	p_n = pentagon(n_p)
	while not end:
		if pentagon(n_p + 1) - p_n > diff:
			n_diff = n_diff + 1
			diff = pentagon(n_diff)
			n_p = 1
			p_n = pentagon(n_p)
		else:
			if not (isPentagon(p_n + diff) and isPentagon(2 * p_n + diff)):
				n_p = n_p + 1
				p_n = pentagon(n_p)
			else:
				end = True
				print(p_n)

main()




