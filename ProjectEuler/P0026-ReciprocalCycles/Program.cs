﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0026_ReciprocalCycles
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetBufferSize(120, 600);
            Console.SetWindowSize(120, 58);
            string temp = string.Empty;
            string tempPeriod = string.Empty;
            string longestPeriod = string.Empty;
            double j = 0;
            for (int i = 2; i < 1000; i++)
            {
                temp = Divide(1, i, 10000);
                tempPeriod = Period(temp);
                //Console.WriteLine("1 / {0,-4} = {1,-72} {2}",i,temp,tempPeriod);
                if (tempPeriod.Length > 1)
                {
                    Console.WriteLine("1 / {0,-4} {1,-10} {2}", i, 1.0 / i, tempPeriod.Length);
                }
                if (tempPeriod.Length > longestPeriod.Length)
                {
                    longestPeriod = tempPeriod;
                    j = i;
                }
            }
            Console.WriteLine($"{j,4}: {1 / j,10} - {longestPeriod} ({longestPeriod.Length})");
            Console.ReadKey(true);
        }

        static string Divide(int a, int b, int count)
        {
            StringBuilder divided = new StringBuilder();


            divided.Append(a / b);
            if (count > 0) { divided.Append(","); }
            a = (a % b) * 10;

            int i = 0;
            while (i < count && a > 0)
            {
                divided.Append(a / b);
                a = (a % b) * 10;
                i++;
            }

            return divided.ToString();
        }

        static string Period(string divided)
        {
            string period = string.Empty;
            string temp = string.Empty;

            divided = divided.Substring(2);
            for (int i = 0; i < ((divided.Length+1) / 2); i++)
            {
                for (int j = 0; j < (divided.Length-i)/2; j++)
                {
                    if (divided.Substring(i, j + 1) == divided.Substring(i + j + 1, j + 1))
                    {
                        temp = divided.Substring(i, j + 1);
                        // check if period....


                        j = divided.Length - 2 * i;
                        i = ((divided.Length + 1) / 2);
                    }
                }
            }

            return period;
        }

    }
}
