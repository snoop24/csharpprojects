﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0009_SpecialPythagoreanTriplet
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            int b;
            int c;
            for ( c = 334; c < 998; c++)
            {
                for ( b = (1000-c)/2; b < 1000-c; b++)
                {
                    a = 1000 - b - c;
                    if (a * a + b * b == c * c)
                    {
                        Console.WriteLine(" a {0} | b {1} | c {2} \n  a*b*c = {3}",a,b,c,a*b*c);
                        c = 1000;
                        b = 1000;
                        break;
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
