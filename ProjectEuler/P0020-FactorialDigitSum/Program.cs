﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0020_FactorialDigitSum
{
    class Program
    {
        static void Main(string[] args)
        {
            string faculty = "1";
            int sum = 0;

            DateTime startTime, endTime;

            startTime = DateTime.Now;

            for (int i = 2; i < 101; i++)
            {
                faculty = MultiplyStringAndInt(faculty, i);
            }

            foreach (char item in faculty.ToString())
            {
                sum += item - 48;
            }

            endTime = DateTime.Now;

            Console.WriteLine(sum);
            Console.WriteLine((endTime-startTime).TotalSeconds+"s");

            Console.ReadKey(true);

        }

        static string MultiplyStringAndInt(string s1, int number)
        {
            int temp = 0;
            StringBuilder sum = new StringBuilder();

            int i = 0;
            while (temp > 0 || i < s1.Length)
            {
                temp += i < s1.Length ? (s1[s1.Length - 1 - i] - 48) * number : 0;
                sum.Insert(0, temp % 10);
                temp = temp / 10;
                i++;
            }

            return sum.ToString();
        }
    }
}
