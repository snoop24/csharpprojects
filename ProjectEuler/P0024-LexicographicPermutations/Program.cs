﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0024_LexicographicPermutations
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime timeStart = DateTime.Now;
            List<int> chars = new List<int>();
            string permutation = "";
            for (int i = 0; i < 10; i++)
            {
                chars.Add(i);
            }

            int limit = 1000000-1;

            int[] facCount = new int[10];
            int fac = 1;
            int count= 1;
            while (limit > 0 )
            {
                for (int i = 1; i < 11; i++)
                {
                    if (fac * i <= limit)
                    {
                        fac *= i;
                        count = i;
                    }

                }
                limit -= fac;
                fac = 1;
                facCount[count]++;
            }
            Console.WriteLine(string.Join(" | ",facCount)+"   "+limit);

            for (int i = 1; i < 10; i++)
            {
                permutation += chars[facCount[10-i]];
                chars.RemoveAt(facCount[10 - i]);
            }
            permutation += chars[0];
            Console.WriteLine(permutation);

            Console.WriteLine(timeStart - DateTime.Now);
            Console.ReadKey(true);
        }
    }
}
