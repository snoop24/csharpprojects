﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0007_10001stPrime
{
    class Program
    {
        static List<int> primes = new List<int>();

        static void Main(string[] args)
        {
            FillPrimeList(110000);

            Console.WriteLine(primes.Count);
            if (primes.Count >= 10000) Console.WriteLine(primes[10000]);

            Console.ReadKey(true);
        }

        private static void PrintPrimeList()
        {
            foreach (var item in primes)
            {
                Console.Write("{0,6}",item);
            }
        }

        private static void FillPrimeList(int limit)
        {
            limit = limit > 2 ? limit : 3;
            bool[] notPrime = new bool[limit];
            notPrime[0]= true;
            notPrime[1] = true;

            for (int i =2; i < Math.Sqrt(notPrime.Length)+1; i++)
            {
                for (int j = i*i; j < notPrime.Length; j++)
                {
                    if (j % i == 0)
                    {
                        notPrime[j] = true;
                    }
                }
            }


            for (int k = 0; k < notPrime.Length; k++)
            {
                if (!notPrime[k])
                {
                    primes.Add(k);
                }
            }
        }
    }
}
