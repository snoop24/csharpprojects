﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0021_AmicableNumbers
{
    class Program
    {
       // static List<int> divisors = new List<int>();

        static void Main(string[] args)
        {
            DateTime timeStart = DateTime.Now;

            int limit = 10000;

            int[] amicableWishPartner = new int[limit];

            int amicableSum = 0;

            for (int i = 2; i < limit; i++)
            {
               // divisors.Clear();
                amicableWishPartner[i]= GetDivisors(i);
            }

            for (int i = 2; i < limit; i++)
            {
                if (i != amicableWishPartner[i] && amicableWishPartner[i] < limit &&i == amicableWishPartner[amicableWishPartner[i]])
                {
                    amicableSum += i;
                    Console.WriteLine("{0} -> {1}",i,amicableSum);
                }
            }

            Console.WriteLine(amicableSum);

            Console.WriteLine(timeStart - DateTime.Now);
            Console.ReadKey(true);
        }

        static int GetDivisors(int number)
        {
            int sum=1;
            int sqrt = (int)Math.Sqrt(number);

            sum += (number == sqrt * sqrt ? sqrt : 0);

            for (int i = 2; i < sqrt; i++)
            {
                if (number % i == 0 )
                {
                   // divisors.Add(i);
                    //divisors.Add(number / i);
                    sum +=  i+number / i;
                }
            }
            return sum;
        }
    }
}
