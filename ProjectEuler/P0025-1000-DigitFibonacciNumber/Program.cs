﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0025_1000_DigitFibonacciNumber
{
    class Program
    {

        static void Main(string[] args)
        {
            DateTime startTime, endTime;
            startTime = DateTime.Now;

            string[] fib = new string[2] { "1", "1" };
            
            int searchIndex = 1000;
            int fibIndex = 2;
            while (fib[1].Length < searchIndex)
            {
                string temp = fib[1];
                fib[1] = AddStrings(fib);
                fib[0] = temp;
                fibIndex++;
            }

            Console.WriteLine(fib[1]+"\n"+fibIndex);

            endTime = DateTime.Now;

            Console.WriteLine((endTime - startTime).TotalSeconds);
            Console.ReadKey(true);
        }


        #region string calculations

        static string AddStrings(string s1, params string[] sArray)
        {
            string[] temp = new string[sArray.Length + 1];
            for (int i = 0; i < sArray.Length; i++)
            {
                temp[i] = sArray[i];
            }
            temp[sArray.Length] = s1;
            return AddStrings(temp);
        }
        static string AddStrings(string[] sArray)
        {
            int temp = 0;
            int numberMaxDigits = sArray.OrderByDescending(item => item.Length).First().Length;
            StringBuilder sum = new StringBuilder();

            int i = 0;
            while (temp > 0 || i < numberMaxDigits)
            {
                foreach (string item in sArray)
                {
                    temp += i < item.Length ? (item[item.Length - 1 - i] - 48) : 0;
                }
                
                sum.Insert(0, temp % 10);
                temp = temp / 10;
                i++;
            }

            return sum.ToString();
        }

        static string MultiplyStringAndInt(string s1, int number)
        {
            ulong temp = 0;
            StringBuilder sum = new StringBuilder();

            int i = 0;
            while (temp > 0 || i < s1.Length)
            {
                temp += i < s1.Length ? (ulong)(s1[s1.Length - 1 - i] - 48) * (ulong)number : 0;
                sum.Insert(0, temp % 10);
                temp = temp / 10;
                i++;
            }

            return sum.ToString();
        }
        #endregion
    }
}
