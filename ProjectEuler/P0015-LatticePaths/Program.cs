﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0015_LatticePaths
{
    class Program
    {
        static void Main(string[] args)
        {
            const int gridSize = 20;
            ulong possibilities = 1;
            DateTime timeStart = DateTime.Now;
            for (int i = 0; i < gridSize; i++)
            {
                possibilities *= (ulong)((2 * gridSize) - i);
                possibilities /= (ulong)( i + 1);
            }
            Console.WriteLine(possibilities);

            Console.Write(timeStart - DateTime.Now);
            Console.ReadKey(true);
        }
    }
}
