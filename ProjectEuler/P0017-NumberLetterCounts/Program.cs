﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0017_NumberLetterCounts
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime startTime = DateTime.Now;

            int letterCount = 0;
            int altLetterCount = 0;
            int testNumberStart = 1;
            int testNumber = 1000;

            DateTime startTime1 = DateTime.Now;

            //for (int j = 0; j < 1000; j++)
            //{
            for (int i = testNumberStart; i < testNumber + 1; i++)
            {
                int temp = LettersInANumberGT0LT10000(i);
                letterCount += temp;
                //Console.WriteLine("{0:0000}:{1,4}", i, temp);
                //Console.ReadKey(true);
                //if (letterCount != altLetterCount) break;
            }

            //}
            DateTime endTime1 = DateTime.Now;

            Console.WriteLine(letterCount);
            Console.WriteLine((endTime1 - startTime1).TotalSeconds);


            DateTime startTime2 = DateTime.Now;
            //for (int j = 0; j < 1000; j++)
            //{
            for (int i = testNumberStart; i < testNumber + 1; i++)
            {
                int temp2 = AltLettersInANumberGT0LT10000(i);
                altLetterCount += temp2;
            }

            //}
            DateTime endTime2 = DateTime.Now;

            Console.WriteLine(altLetterCount);

            Console.WriteLine((endTime2 - startTime2).TotalSeconds);

            Console.WriteLine((DateTime.Now - startTime).TotalSeconds);
            Console.ReadKey(true);
        }

        static int AltLettersInANumberGT0LT10000(int number)
        {
            // return value
            int letterCount = 0;

            // letter count for number word
            int[] singles = { 0, 3, 3, 5, 4, 4, 3, 5, 5, 4, 3, 6, 6, 8, 8, 7, 7, 9, 8, 8 };
            int[] decimals = { 0, 3, 6, 6, 5, 5, 5, 7, 6, 6 };

            // letter count for digit word
            int hundred = 7;
            int thousand = 8;

            // 10^1 digit
            int a = number % 10;
            // 10^2 digit
            int b = number % 100 / 10;
            // 10^3 digit
            int c = number % 1000 / 100;
            // 10^4 digit
            int d = number % 10000 / 1000;

            // 10^4
            if (d != 0)
            {
                letterCount += singles[d] + thousand;
            }

            // 10^3
            if (c != 0)
            {
                letterCount += singles[c] + hundred;
                if (b != 0 || a != 0)
                {
                    letterCount += 3;
                }
            }

            // ..00 to ..19 and ..20 to ..99
            if (b < 2)
            {
                letterCount += singles[b * 10 + a];
            }
            else
            {
                letterCount += decimals[b] + singles[a];
            }

            // return value
            return letterCount;
        }

        static int LettersInANumberGT0LT10000(int number)
        {
            int lettersInIt = 0;

            // get letter count for 1000 digit and thousand and then remove digit
            if (number > 999)
            {
                lettersInIt += LettersInANumberGT0LT10000(number / 1000) + 8;
                number %= 1000;
            }

            // return letter count for 100 digit and hundred and remove digit 
            if (number > 99)
            {
                lettersInIt += LettersInANumberGT0LT10000(number / 100) + 7;
                number %= 100;
                // if remainder add and letterCount
                if (number > 0)
                {
                    lettersInIt += 3;
                }
            }

            // if less then 20 lookup letterCount for word
            if (number < 20)
            {
                switch (number)
                {
                    case 1:
                    case 2:
                    case 6:
                    case 10:
                        lettersInIt += 3;
                        break;
                    case 4:
                    case 5:
                    case 9:
                        lettersInIt += 4;
                        break;
                    case 3:
                    case 7:
                    case 8:
                        lettersInIt += 5;
                        break;
                    case 11:
                    case 12:
                        lettersInIt += 6;
                        break;
                    case 15:
                    case 16:
                        lettersInIt += 7;
                        break;
                    case 13:
                    case 14:
                    case 18:
                    case 19:
                        lettersInIt += 8;
                        break;
                    case 17:
                        lettersInIt += 9;
                        break;

                    case 0:
                    default:
                        break;
                }
            }
            else
            {
                // get 10 digit letterCount
                switch (number / 10)
                {
                    case 2:
                    case 3:
                    case 8:
                    case 9:
                        lettersInIt += 6;
                        break;
                    case 4:
                    case 5:
                    case 6:
                        lettersInIt += 5;
                        break;
                    case 7:
                        lettersInIt += 7;
                        break;
                    default:
                        break;
                }
                // remove 10 digit
                number %= 10;
                // get remainder letter count
                lettersInIt += LettersInANumberGT0LT10000(number);
            }
            return lettersInIt;
        }
    }
}
