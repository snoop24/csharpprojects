﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0001_MultiplesOf3And5
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;
            int sum = 0;
            for (int i = 1; i < 1000; i++)
            {
                if (i % 3 == 0 || i % 5 ==0) sum += i;
            }
            DateTime end = DateTime.Now;
            Console.WriteLine(sum);
            Console.WriteLine((end-start).Milliseconds);
            Console.ReadKey();
        }
    }
}
