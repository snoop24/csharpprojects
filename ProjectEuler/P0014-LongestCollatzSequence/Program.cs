﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0014_LongestCollatzSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime timeStart = DateTime.Now;

            long numberLongestChain=1;
            long maxCount=1;

            for (int i = 1; i < 1000000; i++)
            {
                long number = i;
                int count = 1;
                while (number != 1)
                {
                    number = number % 2 == 0 ? number / 2 : 3 * number + 1;
                    count++;
                }
                numberLongestChain = count > maxCount ? i + 0 * (maxCount = count) : numberLongestChain;
                //Console.WriteLine("Number {0}\nCount  {1}", i, count);
            }

            Console.WriteLine("Number {0}\nCount  {1}", numberLongestChain,maxCount);

            Console.WriteLine(timeStart-DateTime.Now);
            Console.ReadKey(true);
        }
    }
}
