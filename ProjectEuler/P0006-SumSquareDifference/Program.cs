﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0006_SumSquareDifference
{
    class Program
    {
        static void Main(string[] args)
        {
            int squaresSum = 0;
            int sumSquared = 0;

            int rangeMin = 1;
            int rangeMax = 100;

            for (int i = rangeMin; i <= rangeMax; i++)
            {
                squaresSum += i * i;
            }

            sumSquared = (int)Math.Pow((rangeMax*(rangeMax + 1) / 2) - ((rangeMin-1)*(rangeMin ) / 2),2);


            Console.WriteLine("{0}-{1}={2}",sumSquared,squaresSum, sumSquared-squaresSum);

            Console.ReadKey();
        }
    }
}
