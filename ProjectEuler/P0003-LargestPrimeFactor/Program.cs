﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0003_LargestPrimeFactor
{
    class Program
    {
        static void Main(string[] args)
        {
            

            // 600851475143
            Console.WriteLine(ReturnLargestPrimeFactor(600851475143));

            Console.ReadKey();
        }

        static long ReturnLargestPrimeFactor(long limit)
        {
            long factor = 3;

            while (factor < limit)
            {
                while (limit % factor == 0)  limit /= factor;
                factor += 2;
            }

            return factor;
        }

        //static long ReturnLargestPrimeFactor(long limit)
        //{
        //    List<long> primes= new List<long>() ;
        //    long factor = limit;
        //    for (long i = 2; i < limit/2; i++)
        //    {
        //        bool prime =true;
        //        foreach (var item in primes)
        //        {
        //            if (i % item == 0)
        //            {
        //                prime = false;
        //                break;
        //            }
        //        }
        //        if (prime)
        //        {
        //            primes.Add(i);
        //            if (limit % i ==0)
        //            {
        //                factor = ReturnLargestPrimeFactor(limit / i);
        //                break;
        //            }
        //        }
        //    }
        //        return factor;
        //}
    }
}
