﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0012_HighlyDivisibleTriangleNumber
{
    class Program
    {
        static List<ulong> divisors = new List<ulong>();

        static void Main(string[] args)
        {
            DateTime timeStart = DateTime.Now;
            ulong triangle = 0;
            for (ulong i = 1; i < 1000000000; i++)
            {
                triangle += i;
                divisors.Clear();
                GetDivisors(triangle);
                if (divisors.Count > 500)
                {
                    Console.WriteLine("{0}\n{1}\n{2}", i,triangle, divisors.Count);
                    break;
                }
            }
            Console.WriteLine(timeStart-DateTime.Now);
            Console.ReadKey(true);
        }

        static void GetDivisors(ulong number)
        {
            for (ulong i = 1; i < Math.Sqrt(number); i++)
            {
                if (number % i ==0 )
                {
                    divisors.Add(i);
                    divisors.Add(number / i);
                }
            }
        }
    }
}
