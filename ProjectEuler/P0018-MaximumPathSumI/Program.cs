﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0018_MaximumPathSumI
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] array = new int[15][];
            array[0] = new int[1] { 75 };
            array[1] = new int[2] { 95, 64 };
            array[2] = new int[3] { 17, 47, 82 };
            array[3] = new int[4] { 18, 35, 87, 10 };
            array[4] = new int[5] { 20, 04, 82, 47, 65 };
            array[5] = new int[6] { 19, 01, 23, 75, 03, 34 };
            array[6] = new int[7] { 88, 02, 77, 73, 07, 63, 67 };
            array[7] = new int[8] { 99, 65, 04, 28, 06, 16, 70, 92 };
            array[8] = new int[9] { 41, 41, 26, 56, 83, 40, 80, 70, 33 };
            array[9] = new int[10] { 41, 48, 72, 33, 47, 32, 37, 16, 94, 29 };
            array[10] = new int[11] { 53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14 };
            array[11] = new int[12] { 70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57 };
            array[12] = new int[13] { 91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48 };
            array[13] = new int[14] { 63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31 };
            array[14] = new int[15] { 04, 62, 98, 27, 23, 09, 70, 98, 73, 93, 38, 53, 60, 04, 23 };

            for (int i = 0; i < array.Length-1; i++)
            {
                for (int j = 0; j < array[array.Length-2-i].Length; j++)
                {
                    array[array.Length - 2 - i][j] += Math.Max(array[array.Length - 2 - i + 1][j], array[array.Length - 2 - i + 1][j + 1]);
                }
            }

            Console.WriteLine(array[0][0]);

            Console.ReadKey(true);
        }
    }
}