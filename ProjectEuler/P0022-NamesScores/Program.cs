﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace P0022_NamesScores
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime timeStart = DateTime.Now;

            Dictionary<string,int> dictNames = new Dictionary<string,int>();
            StreamReader streamRead = new StreamReader("p022_names.txt");

            string names = streamRead.ReadLine();

            string[] namesArray = names.Replace("\"","").Split(',');
            Array.Sort(namesArray);

            int sum = 0;

            for (int i = 0; i < namesArray.Length; i++)
            {
                int temp = 0;
                foreach (char item in namesArray[i])
                {
                    temp += item - 64;
                }
                sum += temp * (i + 1);
            }

            Console.WriteLine(sum);

            Console.WriteLine(timeStart - DateTime.Now);
            Console.ReadKey(true);
        }
    }
}
