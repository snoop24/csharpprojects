﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0019_CountingSundays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] monthDays = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            int year = 1900;
            int month = 1;
            int dayOfWeek = 1;
            int count = 0;

            int startYear = 1901;
            int endYear = 2000;

            DateTime startTime = DateTime.Now;

            // do until year out of range
            while (year < endYear + 1) 
            {
                // if day is sunday and year in Range
                count += dayOfWeek != 0 ? 0 : year<startYear? 0:1;
                // get next month day of week by adding month days to day of week and doing modulo 7
                dayOfWeek = (dayOfWeek + monthDays[month]+ ((IsLeapYear(year) && month==2)? 1: 0))%7;
                // increase year if necessary
                year += month == 12 ? 1:0 ;
                // increase month if necessary
                month += month < 12 ? 1 : -11;
            }


            DateTime endTime = DateTime.Now;

            Console.WriteLine((endTime-startTime).TotalSeconds);

            Console.WriteLine(count);

            Console.ReadKey(true);
        }

        static bool IsLeapYear(int year)
        {
            return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        }

    }
}
