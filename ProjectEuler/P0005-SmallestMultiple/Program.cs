﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0005_SmallestMultiple
{
    class Program
    {
        static void Main(string[] args)
        {
            int leastCommonMultipleOverRange;

            int rangeMin = 1;
            int rangeMax = 20;
            int[] factors = new int[rangeMax-rangeMin+1];

            for (int i = rangeMin-1; i < rangeMax; i++)
            {
                factors[i] = i + 1;
            }

            leastCommonMultipleOverRange = LeastCommonMultiple(factors);

            Console.WriteLine(leastCommonMultipleOverRange);

            Console.ReadKey();
        }

        static int GreatestCommonDivisor(int a, int b)
        {
            while (a % b != 0)
            {
                a = b + 0 * (b = a % b);
            }
            return b;
        }

        static int LeastCommonMultiple(int[] factors)
        {
            int lcm = 1;
            foreach (var item in factors)
            {
                lcm *= item / GreatestCommonDivisor(lcm, item);
            }
            return lcm;
        }
    }
}
