﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0023_Non_AbundantSums
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime timeStart = DateTime.Now;

            int limit = 28123+1;

            bool[] sumOfTwoAbundant = new bool[limit];
            bool[] abundant = new bool[limit ];

            // is abundant check
            //for (int i = 2; i < limit; i++)
            //{
            //    abundant[i] = i < GetSumOfDivisors(i);
            //}
            Parallel.For(2, limit, (x) =>
              {
                  abundant[x] = x < GetSumOfDivisors(x);
              });

            // set all sums of abundants in bool array to true
            //for (int i = 1; i < limit / 2; i++)
            //{
            //    if (abundant[i])
            //    {
            //        for (int j = i; j < limit - i; j++)
            //        {
            //            if (abundant[j])
            //            {
            //                sumOfTwoAbundant[i + j] = true;
            //            }

            //        }
            //    }
            //}
            Parallel.For(1, limit / 2, (i) =>
            {
                if (abundant[i])
                {
                    for (int j = i; j < limit - i; j++)
                    {
                        if (abundant[j])
                        {
                            sumOfTwoAbundant[i + j] = true;
                        }

                    }
                }
            });


            int SumOfNotSumOfTwoAbundantSum = 0;

            // sum up all possible sums of two abundants
            for (int i = 1; i < limit; i++)
            {
                if (!sumOfTwoAbundant[i])
                {
                    SumOfNotSumOfTwoAbundantSum += i;
                }
            }


            Console.WriteLine(SumOfNotSumOfTwoAbundantSum);

            Console.WriteLine(timeStart - DateTime.Now);
            Console.ReadKey(true);
            Main(args);
        }

        static int GetSumOfDivisors(int number)
        {
            int sum =  1;

            int sqrt = (int)Math.Sqrt(number);

            for (int i = 2; i <= sqrt; i++)
            {
                if (number % i == 0)
                {
                    sum += i + (number == i * i ? 0 : number /i);
                }
            }
            return sum;
        }
    }
}
