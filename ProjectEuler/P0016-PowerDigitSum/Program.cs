﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P0016_PowerDigitSum
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime startTime = DateTime.Now;


            string s1 = "2";
            int factor = 2;

            for (int i = 1; i < 1000; i++)
            {
                s1 = MultiplyStringAndInt(s1,factor);
            }

            int sum = 0;

            foreach (char item in s1)
            {
                sum += item - 48;
            }


            Console.WriteLine(s1);
            Console.WriteLine(sum);

            Console.WriteLine((DateTime.Now - startTime).TotalSeconds + " s");
            Console.ReadKey(true);
        }

        static string MultiplyStringAndInt(string s1,int number)
        {
            int temp = 0;
            StringBuilder sum = new StringBuilder();

            int i = 0;
            while (temp > 0 || i < s1.Length)
            {
                temp += i < s1.Length ? (s1[s1.Length - 1 - i] - 48) * number : 0;
                sum.Insert(0, temp % 10);
                temp = temp / 10;
                i++;
            }

            return sum.ToString();
        }
    }
}
