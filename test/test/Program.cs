﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            
            {
                FileSystemWatcher wächter = new FileSystemWatcher(@"D:\Lookup", "*.lookup");
                //wächter.NotifyFilter = NotifyFilters.FileName;
                wächter.Created += OnChanged;
                wächter.EnableRaisingEvents = true;
                Console.WriteLine(wächter.Filter);
            }
            while (true) ;
        }

        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            StreamReader lies = new StreamReader(e.FullPath);
            StreamWriter schreib = new StreamWriter(e.FullPath + ".resolved");
            Console.WriteLine(  "ich lese");
            while (!lies.EndOfStream)
            {
                schreib.WriteLine(string.Join("\r\n", ResolveFQDN(lies.ReadLine()).ToString()));
            }
            lies.Close();
            schreib.Close();
            Console.WriteLine("fertig");
        }


        private static string[] ResolveFQDN(string fqdn)
        {
            try
            {
                Console.WriteLine("resolving");
                IPAddress[] addr = Dns.GetHostAddresses(fqdn);
                return addr.Select(a => a.ToString()).ToArray();
            }
            catch
            {
                return null;
            }
        }
    }
}
