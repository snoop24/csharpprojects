﻿
function BarChart(canvasName, dataObject) {
    this.canvas = document.getElementById(canvasName);
    this.data = dataObject;

    this.draw = function () {
        var context = this.canvas.getContext("2d");

        // get max bar value
        var max = 0;
        for (var i in this.data) {
            if (max < this.data[i]) {
                max = this.data[i];
            }
        }

        // get data count
        var dataCount = Object.keys(this.data).length;

        // set offsets
        var widthOffset = 25;
        var heightOffset = 25;
        var barMargin = 5;
        var textBarMargin = 5;

        // set bar height 
        var barHeight = (this.canvas.height - 2 * heightOffset - (dataCount - 1) * barMargin)/dataCount;

        // set bar width unit per value
        var barUnit = (this.canvas.width - 2 * widthOffset ) / max;

        var fontSize = barHeight /2;
        context.font = fontSize+"px Arial";

        var count = 0;

        for (var item in this.data) {
            context.fillStyle = "red";
            
            context.fillRect(widthOffset
                , heightOffset + (count * (barHeight + barMargin))
                , this.data[item] * barUnit
                , barHeight
            );

            context.fillStyle = "black";
            context.fillText(item
                , widthOffset + textBarMargin
                , heightOffset + barHeight - textBarMargin + (count * (barHeight + barMargin))
            );
            count++;
        }
    };


}

window.onload = function () {
    // set canvas values
    var canvas = {
        name: "canvas1"
        , width: 500
        , height: 300
    };
    // create canvas element in place
    document.getElementById("canvasHere").innerHTML = "<canvas id='" + canvas.name + "' height='" + canvas.height + "' width='" + canvas.width + "'></canvas>";

    // set diagramm data
    var data = { "C#": 20, "Java": 50, "C++": 30, "JavaScript": 20 };
    // create diagramm
    var diagramm = new BarChart(canvas.name, data);
    // draw diagramm
    diagramm.draw();
};

// draw a line in a given context
function drawLine(x1, x2, y1, y2, context) {
    context.beginPath();
    context.moveTo(x1, x2);
    context.lineTo(y1, y2);
    context.stroke();
}