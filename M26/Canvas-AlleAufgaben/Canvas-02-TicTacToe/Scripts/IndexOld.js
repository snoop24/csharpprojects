﻿window.onload = function () {

    game = new Game(402, 3, "canvasHere");

};


//var game = null;

function Game(size, thickness, canvasDivName) {

    this.drawArea = new DrawArea(size, thickness, canvasDivName, this);
    this.thickness = thickness;
    this.board = makeBoard(size,thickness);

    this.nextplayer = 0;

    this.where = function (coords) {
        var board = this.board;
        var matchIndex = -10;
        for (var item in board) {
            if (board[item].xMin <= coords.x && board[item].xMax >= coords.x
                && board[item].yMin <= coords.y && board[item].yMax >= coords.y
            ) {
                matchIndex = item;
            }
        }
        this.TryMove(matchIndex);
        return matchIndex;
    };

    this.MoveOn = function (index) {
        var moveOk = false;
        if (this.board[index].value < 0) {
            this.board[index].value = this.nextplayer;
            this.nextplayer = (this.nextplayer + 1) % 2;
            moveOk = true;
        }
        return moveOk;
    };

    this.TryMove = function (index) {
        if (this.MoveOn(index)) {
            this.drawArea.drawSign(index, this.board[index].value);
            var end = this.CheckEnd();
            switch (end) {
                case 0:
                    alert("Player X hat gewonnen!");
                    break;
                case 1:
                    alert("Player O hat gewonnen!");
                    break;
                case 2:
                    alert("Keiner hat gewonnen!");
                    break;
                default:
            }
        }
        else {
            //alert("invalid move");
        }
    };

    this.CheckEnd = function () {
        return -1;
    };
}

function makeBoard(size, thickness) {
    var space = (size - 2 * thickness) / 3;
    var fields = [];
    for (let i = 0; i < 9; i++) {
        fields[i] = {
            xMin: i % 3 * (space + thickness)
            , xMax: i % 3 * (space + thickness) + space - 1
            , yMin: Math.floor(i / 3) * (space + thickness)
            , yMax: Math.floor(i / 3) * (space + thickness) + space - 1
            , value: -10
        };
    }
    return fields;
}

function DrawArea(size, thickness, canvasDivName, game) {

    this.game = game;

    // html page
    var canvas = (function (canvasDivName, size) {
        var canvasId = "ticTacToe";

        // create canvas element on page
        document.body.innerHTML += "<article id='"+canvasDivName+"'><canvas id='" + canvasId + "' height='" + size + "' width='" + size + "'></canvas></article>";
        // return canvas element
        return document.getElementById(canvasId);
    })(canvasDivName, 600);

    // get context
    var context = canvas.getContext("2d");
    this.context = context;

    // set on mouse click
    canvas.onclick = function (event) {
        var coords = {
            x: e.clientX 
            , y: e.clientY 
        };
        var fieldIndex = game.where(coords);
    };

    // draw 
    drawField(thickness, context, size);

    // draw sign
    this.drawSign = function (index, value) {
        if (value === 0) {
            drawX(this.context, this.game, index);
        }
        else {
            drawO(this.context, this.game, index);
        }
    };
}


// drawing functions

// draw X
function drawX(context, game, index) {
    var thickness = game.thickness * 3;
    var field = game.board[index];
    drawLine(field.xMin + 20, field.yMin + 20, field.xMax - 20, field.yMax - 20, thickness, context);
    drawLine(field.xMin + 20, field.yMax - 20, field.xMax - 20, field.yMin + 20, thickness, context);
}

// draw O
function drawO(context, game,index){
    var thickness = game.thickness*3;
    var field = game.board[index];
    context.lineWidth = thickness;
    context.beginPath();
    context.arc((field.xMin + field.xMax) / 2, (field.yMin + field.yMax) / 2, (field.xMax - field.xMin) / 2-20, 0, 2 * Math.PI);
    context.stroke();
}

// draw a line
function drawLine(x1, x2, y1, y2, thickNess, context) {
    context.lineWidth = thickNess;
    context.beginPath();
    context.moveTo(x1, x2);
    context.lineTo(y1, y2);
    context.stroke();
}

//draw field
function drawField(borderThickness, context, size) {
    var space = (size - borderThickness * 2) / 3;
    drawLine(space, 0, space, size, borderThickness, context);
    drawLine(space * 2 + borderThickness, 0, space * 2 + borderThickness, size, borderThickness, context);
    drawLine(0, space, size, space, borderThickness, context);
    drawLine(0, space * 2 + borderThickness, size, space * 2 + borderThickness, borderThickness, context);
}