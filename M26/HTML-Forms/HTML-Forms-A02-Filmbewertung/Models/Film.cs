﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTML_Forms_A02_Filmbewertung.Models
{
    public class Film
    {
        public int ID { get; }
        public string Title { get; }

        private int[] noten;

        public int[] Noten
        {
            get { return noten; }
        }

        public Film(int id, string title, int[] noten)
        {
            ID = id;
            Title = title;
            this.noten = noten;
        }

        public void Vote(int note)
        {
            if (note > 1 && note < noten.Length + 1)
            {
                noten[note - 1]++;
            }
        }
    }
}