﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace HTML_Forms_A02_Filmbewertung.Models
{
    public class FilmRepository
    {
        public static string[] NotenNamen { get; } = { "sehr gut", "gut", "befriedigend", "ausreichend", "mangelhaft", "ungenügend" };

        private static string lastErrorMessage;
        public static string LastErrorMessage { get { return lastErrorMessage; } }

        static SqlConnection connection;
        static string connString = @"Data Source=.\SQLEXPRESS; Initial Catalog=TeachSQL;Integrated Security=True;";
        static SqlDataReader sqlReader;
        static SqlCommand sqlCom;


        public static List<Film> GetFilms()
        {
            List<Film> films = new List<Film>();

            using (connection = new SqlConnection(connString))
            {
                try
                {
                    connection.Open();

                }
                catch (Exception e)
                {
                    lastErrorMessage = e.Message;
                }
                sqlCom = connection.CreateCommand();

                sqlCom.CommandText = $"SELECT ID, TITEL FROM [dbo].Film ORDER BY TITEL";

                using (sqlReader = sqlCom.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        films.Add(new Film(int.Parse(sqlReader[0].ToString()), sqlReader[1].ToString(), new int[6]));
                    }
                }


            }

            return films;
        }

        public static Film GetFilmWithVotes(int id)
        {
            Film film = null;


            using (connection = new SqlConnection(connString))
            {
                try
                {
                    connection.Open();

                }
                catch (Exception e)
                {
                    lastErrorMessage = e.Message;
                }
                sqlCom = connection.CreateCommand();

                sqlCom.CommandText = $"SELECT ID, TITEL FROM [dbo].Film WHERE ID={id}";

                using (sqlReader = sqlCom.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        film = new Film(int.Parse(sqlReader[0].ToString()), sqlReader[1].ToString(), new int[6]);
                    }
                }

                sqlCom.CommandText = $"SELECT FilmID, Note, Count(*)" +
                    $" FROM dbo.FilmAbstimmung" +
                    $" WHERE FilmID= {id}" +
                    $" GROUP BY FilmID, Note" +
                    $" ORDER BY Note, FilmID; ";

                using (sqlReader = sqlCom.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {

                        film.Noten[Array.IndexOf(NotenNamen, sqlReader[1].ToString())] = int.Parse(sqlReader[2].ToString());
                    }
                }


            }

            return film;
        }

        public static void VoteForFilm(int id, string note)
        {
            using (connection = new SqlConnection(connString))
            {
                try
                {
                    connection.Open();

                }
                catch (Exception e)
                {
                    lastErrorMessage = e.Message;
                }
                sqlCom = connection.CreateCommand();

                sqlCom.CommandText = $"INSERT INTO [dbo].FilmAbstimmung(FilmID, Note) VALUES ({id},'{note}')";

                sqlCom.ExecuteNonQuery();
            }
        }
    }
}