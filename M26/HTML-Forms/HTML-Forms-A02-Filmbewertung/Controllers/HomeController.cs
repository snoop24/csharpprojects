﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTML_Forms_A02_Filmbewertung.Controllers
{
    public class HomeController : Controller
    {

        // GET: Home
        public ActionResult Index()
        {
            Dictionary<int, string> filme = Models.FilmRepository.GetFilms().ToDictionary(x => x.ID, x => x.Title);

            return View(filme);
        }

        public ActionResult Abstimmen(int filmID, string note)
        {
            //Models.Film film = new Models.Film(111, "Test", new int[] { 45, 87, 0, 1, 2, 3 });

            if (Models.FilmRepository.NotenNamen.Contains(note))
            {
                Models.FilmRepository.VoteForFilm(filmID, note);
            }

            Models.Film film = Models.FilmRepository.GetFilmWithVotes(filmID);

            return View(film);
        }
    }
}