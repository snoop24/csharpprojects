﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTML_Forms_A01_1_Palindrom.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }


        // POST: Home
        [HttpPost]
        public ActionResult Index(string palindrom)
        {

            // palindrom check
            Models.PalindromChecker testSubject = new Models.PalindromChecker(palindrom);

            return View(testSubject);
        }

    }
}