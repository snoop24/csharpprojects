﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;

namespace HTML_Forms_A01_1_Palindrom.Models
{
    public class PalindromChecker
    {
        #region:Palindrom

        private const string vergleich = "abcdefghijklmnopqrstuvwxyzäöüß0123456789";

        private string palindrom;
        public string Palindrom
        {
            get { return palindrom; }
        }

        private string answer;
        public string Answer
        {
            get { return answer; }
        }

        private bool isPalindrom;
        public bool IsPalindrom
        {
            get { return isPalindrom; }
        }


        public PalindromChecker(string palindrom)
        {
            this.palindrom = palindrom;
            isPalindrom = true;
            CheckForPalindrom();
            answer = $"Es liegt {(isPalindrom ? "ein" : "kein")} Palindrom vor!";
        }

        private void CheckForPalindrom()
        {
            isPalindrom = true;

            string test = palindrom;

            test = CleanToLower(test);

            for (int i = 0; i < test.Length / 2; i++)
            {
                if (test[i] != test[test.Length - 1 - i])
                {
                    isPalindrom = false;
                    i = test.Length;
                }
            }
        }

        private string CleanToLower(string test)
        {
            StringBuilder temp = new StringBuilder();

            foreach (char letter in test.ToLower())
            {
                if (vergleich.Contains(letter))
                {
                    temp.Append(letter);
                }
            }
            return temp.ToString();
        }
        #endregion
    }
}