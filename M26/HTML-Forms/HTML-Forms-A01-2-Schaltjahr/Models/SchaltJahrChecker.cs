﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTML_Forms_A01_2_Schaltjahr.Models
{
    public class SchaltJahrChecker
    {
        private string year;

        public string Year
        {
            get { return year; }
            set { year = value; }
        }

        private bool isLeapYear;

        public bool IsLeapYear
        {
            get { return isLeapYear; }
        }

        private string answer;

        public string Answer
        {
            get { return answer; }
            set { answer = value; }
        }


        public SchaltJahrChecker(string year)
        {
            this.year = year;
            if (int.TryParse(year, out int result))
            {
                isLeapYear = DateTime.IsLeapYear(result);
                answer = $"{year} ist {(isLeapYear ? "ein" : "kein")} Schaltjahr!";
            }
            else
            {
                isLeapYear = false;
                answer = "Eingabe fehlerhaft";
            }
        }

    }
}