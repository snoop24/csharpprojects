﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTML_Forms_A01_2_Schaltjahr.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // Post: Home
        [HttpPost]
        public ActionResult Index(string schaltjahr)
        {
            Models.SchaltJahrChecker jahr = new Models.SchaltJahrChecker(schaltjahr);

            return View(jahr);
        }

    }
}