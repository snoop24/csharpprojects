﻿self.addEventListener("message", function (event) {
    //console.log("worker event:" + event.data);
    var startTime = new Date();
    console.log("primSieb: gestartet");
    primSieben(event.data.maximum);
    console.log("primSieb: fertig nach " + (new Date() - startTime) + "ms");
    self.close();
});

function primSieben(maxPrim) {
    var primNein = [];
    primNein[1] = true;
    //self.postMessage({ number: 1, class: 'noPrime' });

    // loop all prime numbers and set primeNein[multiples]=false start with prime 2
    for (let i = 2; i * i <= maxPrim;) {

        // return message with prime number
        //console.log('prim: ' + i);
        self.postMessage({ number: i, class: 'prime' });


        // loop all numbers after prime*prime
        for (let j = i * i; j <= maxPrim; j += i) {
            if (primNein[j] !== true) {
                primNein[j] = true;

                //return message with non prime numberv
                //console.log('nix prim: ' + j);
                self.postMessage({ number: j, class: 'noPrime' });
            }
        }

        // check if next i is prime, else next i
        do { i++; } while (primNein[i] === true);

        if (i * i > maxPrim) {
            for (let k = i; k <= maxPrim; k++) {
                // return message with prime number left not used in sieve
                //console.log('prim: ' + i);
                if (primNein[k] !== true) {
                    self.postMessage({ number: k, class: 'prime' });
                }
            }
        }

    }
}