﻿window.onload = function () {
    document.getElementById('btnStart').onclick = function () {
        start();
    };

    document.getElementById('inpMax').onchange = function () {
        document.getElementById('inpSlider').value = document.getElementById('inpMax').value;
        buildElements();
    };

    document.getElementById('inpSlider').onchange = function () {
        //console.log(document.getElementById('inpSlider').value);
        buildElements();
    };

    document.getElementById('inpSlider').oninput = function () {
        document.getElementById('inpMax').value = document.getElementById('inpSlider').value;
    };

    buildElements();

};

function start() {
    console.log("script start");
    var maxPrim = document.getElementById('inpMax').value;

    var faerbeElemente = [];
    var intervalId = setInterval(jetztFaerben, Math.max(220 - maxPrim / 5, 25));
    disableInput(true);
    console.log("faerben: start");

    function jetztFaerben() {
        var element = faerbeElemente.shift();
        //console.log("faerben: " + element);
        //document.getElementById("primFeld" + element.number).style.backgroundColor = element.prime ? "red" : "blue";
        document.getElementById("primFeld" + element.number).classList.add(element.class);
        if (faerbeElemente.length === 0) {
            clearInterval(intervalId);
            console.log("faerben: ende gelände");
            disableInput(false);
        }
    }

    var primSieb = new Worker("/Scripts/primSieb.js");
    primSieb.addEventListener("message", function (event) {
        //console.log(event.data);
        //setTimeout(function () {
        //    document.getElementById("primFeld" + event.data.number).style.backgroundColor = event.data.prime ? "red" : "blue";
        //}, 1000);
        faerbeElemente.push(event.data);
    });
    primSieb.postMessage({ maximum: maxPrim });
}

function buildElements() {
    var maxPrim = document.getElementById('inpMax').value;
    var oberElement = document.getElementById("primFelder");
    oberElement.innerHTML = '';
    //console.log("elemente bauen");
    for (let i = 1; i <= maxPrim; i++) {
        oberElement.innerHTML += "<div id='primFeld" + i + "'>" + (i === 1 ? "&nbsp;" : i) + "</div>";
        //console.log(html);
    }
}

function disableInput(value) {
    document.getElementById('btnStart').disabled = value;
    document.getElementById('inpSlider').disabled = value;
    document.getElementById('inpMax').disabled = value;
}