﻿window.onload = function () {

    

    document.getElementById('btnStart').onclick = function () {
        start();
    };
    document.getElementById('btnStop').onclick = function () {
        stop();
    };

    disableStart(false);    
};

var piWorker;

function start() {
    console.log("script start");
    disableStart(true);
    piWorker = new Worker("/Scripts/piBerechnen.js");
    piWorker.addEventListener("message", function (event) {
        console.log(event.data);
        document.getElementById('progressBar').value = event.data.progress;
        document.getElementById('lblBar').innerHTML = event.data.progress +"%";
        document.getElementById('inpPI').value = event.data.value;
        if (event.data.progress === 100) {
            disableStart(false);
            //piWorker.terminate();
        }
    });


    piWorker.postMessage("start");
}

function stop() {
    console.log("script stop");
    piWorker.terminate();
    disableStart(false);
    document.getElementById('inpPI').value = 'Abgebrochen (' + document.getElementById('inpPI').value + ')';
}

function disableStart(diss) {
    document.getElementById('btnStart').disabled = diss;
    document.getElementById('btnStop').disabled = !diss;
}