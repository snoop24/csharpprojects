﻿self.addEventListener("message", function (event) {
    console.log("worker event:" + event.data);
    self.postMessage({ progress: 100, value: computePi() });
    self.close();
});

function computePi() {
    var sum = 0.0;
    var step = 1e-9;
    var end = 1000000000;
    for (let i = 0; i < end; i++) {
        var x = (i + 0.5) * step;
        sum = sum + 4.0 / (1.0 + x * x);
        if (i % (end / 100) === 0) {
            self.postMessage({ progress: i / (end / 100), value: sum * step });
        }
    }
    return sum * step;
}