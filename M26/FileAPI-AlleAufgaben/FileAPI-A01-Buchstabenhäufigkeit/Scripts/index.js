﻿/// <reference path="jquery-3.3.1.js" />

window.onload = function () {
    "use strict";
    // target

    $(document).on("dragover", function (event) {
        event.preventDefault();
        return false;
    });

    $(document).on("dragenter", function (event) {
        console.log("document: dragenter");
    });

    $(document).on("drop", function (event) {
        event.preventDefault();
        return false;
    });

    $('.dropTarget').on('dragover', allowDrop);
    $('.dropTarget').on('drop', handleDrop);

    $('#fileInput').on('change', function (event) {
        var file = event.target.files[0];
        $('#h2FileName').html(file.name);
        readFile(file);
    });
};

// source and target
function allowDrop(event) {
    "use strict";
    event.preventDefault();
    //console.log("allowDrop");
    //console.log(event);
}

function handleDrop(event) {
    "use strict";
    console.log("handleDrop");
    //console.log(event);
    event.preventDefault();
    var data = event.originalEvent.dataTransfer;
    if (data.files.length !== 0) {
        var f = data.files[0];
        //console.log("... first file.name = " + f.name);
        //console.log("... first file.type= " + f.type);
        $('#h2FileName').html(f.name);
        $('#fileInput').val("");
    }
    readFile(f);
}

function readFile(file) {

    // Wird die File-API vom Browser unterstützt?
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // FileReader Objekt anlegen
        var reader = new FileReader();

        // Erste Datei lesen
        reader.readAsText(file);

        // Event-Handler wird aufgerufen sobald die Datei gelesen wurde
        reader.onload = function (readerEvent) {
            // Aus dem Event-Objekt den Inhalt lesen
            var dateiInhalt = readerEvent.target.result;

            // Ausgabe auf der Konsole
            //console.log(dateiInhalt);
            buchstaben(dateiInhalt);

            document.getElementById("divFileOutput").innerHTML = dateiInhalt;
        };
    }
}

function buchstaben(text) {
    console.log("buchstaben", $('#h2FileName').html());
    text = text.toLowerCase();
    var buchstaben = "abcdefghijklmnopqrstuvwxyzäöüß";
    var anzahl = {};
    for (let i = -1; i < buchstaben.length; i++) {
        anzahl[i] = 0;
    }
    for (let i = 0; i < text.length; i++) {
        anzahl[buchstaben.indexOf(text[i])]++;
    }
    console.log(anzahl);
    display(anzahl, buchstaben);
}

function display(anzahl, buchstaben) {
    var html = '<table>';
    console.log('display');
    var arrayLength = Object.keys(anzahl).length
    var zeilen = Math.floor((arrayLength + 1) / 2);
    for (let i = 0; i < zeilen; i++) {
        var feld1 = buchstaben[i];
        var feld2 = anzahl[i];
        html += '<tr><td class="buchstabe">' + feld1 + '</td><td class="anzahl">' + feld2 + '</td>';
        console.log('display: links');
        var htmlZusatz = "";
        if (i + zeilen < arrayLength - 1) {
            feld1 = buchstaben[i + zeilen];
            feld2 = anzahl[i + zeilen];

            htmlZusatz = '<td class="buchstabe">' + feld1 + '</td><td class="anzahl">' + feld2 + '</td></tr>';
        }
        else if (i + zeilen < arrayLength) {
            htmlZusatz = '<td class="buchstabe">Rest</td><td class="anzahl">' + anzahl[-1] + '</td></tr>';
        }
        else {
            htmlZusatz = "</tr>";
        }
        console.log('display: rechts');

        html += htmlZusatz;
    }

    html += '</table>';
    $('#divAnzahlOutput').html(html);
}