﻿/// <reference path="jquery-3.3.1.js" />

window.onload = function () {
    "use strict";
    // target

    $(document).on("dragover", function (event) {
        event.preventDefault();
        return false;
    });

    $(document).on("dragenter", function (event) {
        console.log("document: dragenter");
    });

    $(document).on("drop", function (event) {
        event.preventDefault();
        return false;
    });

    $('.dropTarget').on('dragover', allowDrop);
    $('.dropTarget').on('drop', handleDrop);

    $('#fileInput').on('change', function (event) {
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            $('#h2FileName').html(file.name);
            readFile(file);
        }
    });
};

// source and target
function allowDrop(event) {
    "use strict";
    event.preventDefault();
    //console.log("allowDrop");
    //console.log(event);
}

function handleDrop(event) {
    "use strict";
    console.log("handleDrop");
    //console.log(event);
    event.preventDefault();
    var data = event.originalEvent.dataTransfer;
    console.log(data);
    if (data.files.length !== 0) {
        var f = data.files[0];
        //console.log("... first file.name = " + f.name);
        //console.log("... first file.type= " + f.type);
        $('#h2FileName').html(f.name);
        $('#fileInput').val("");

        readFile(f);
    }
}

function readFile(file) {
    // Wird die File-API vom Browser unterstützt?
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // FileReader Objekt anlegen
        var reader = new FileReader();

        // Erste Datei lesen
        reader.readAsDataURL(file);

        // Event-Handler wird aufgerufen sobald die Datei gelesen wurde
        reader.onload = function (readerEvent) {
            // Aus dem Event-Objekt den Inhalt lesen
            var dateiInhalt = readerEvent.target.result;

            // Ausgabe auf der Konsole
            //console.log(dateiInhalt);
            displayResult(dateiInhalt);
        };
    }
}


function displayResult(imgData) {
    var tmpImage = new Image();
    tmpImage.src = imgData;
    $('#divImageOutput').empty();
    $('#divImageOutput').append(tmpImage);

    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    //console.log(tmpImage);
    canvas.width = tmpImage.width;
    canvas.height = tmpImage.height;
    ctx.drawImage(tmpImage, 0, 0);
    var pixelData = ctx.getImageData(0, 0, tmpImage.width, tmpImage.height);
    var asciiImg = "";
    var Step = 400;
    var StepW = 600;
    var StepH = 200;
    var XBlock = Math.floor(tmpImage.width / StepW);
    var YBlock = Math.floor(tmpImage.height / StepH);
    for (var rows = 0; rows < tmpImage.height / YBlock; rows++) {
        for (var cols = 0; cols < tmpImage.width / XBlock; cols++) {
            var i = 0;
            var avg = 0;
            for (xp = 0; xp < XBlock; xp++) {
                for (yp = 0; yp < YBlock; yp++) {
                    i = (rows * YBlock + yp) * 4 * tmpImage.width + (cols * XBlock + xp) * 4;
                    avg += (pixelData.data[i] + pixelData.data[i + 1] + pixelData.data[i + 2]) / 3;
                }
            }

            asciiImg += getChar(avg / (XBlock * YBlock));
            //console.log("AVG: " + avg + " / " + (XBlock * YBlock));
        }
        asciiImg += '\r\n';

        console.log("working");
    }

    $('#divAsciiOutput').html('<pre>' + asciiImg + '</pre>');
}

function getChar(colAvg) {
    if (colAvg > 30 && colAvg <= 60) {
        return '@';
    }
    if (colAvg > 60 && colAvg <= 90) {
        return 'D';
    }
    if (colAvg > 90 && colAvg <= 120) {
        return '$';
    }
    if (colAvg > 120 && colAvg <= 150) {
        return '*';
    }
    if (colAvg > 150 && colAvg <= 180) {
        return '+';
    }
    if (colAvg > 180 && colAvg <= 210) {
        return ';';
    }
    if (colAvg > 210 && colAvg <= 235) {
        return ',';
    }
    if (colAvg > 235 && colAvg <= 250) {
        return '.';
    }
    if (colAvg > 250) {
        return ' ';
    }
    return '#';
}
