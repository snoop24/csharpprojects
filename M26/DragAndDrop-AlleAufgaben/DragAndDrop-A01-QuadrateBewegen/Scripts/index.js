﻿window.onload = function () {

    // source and target
    $('.dragTarget').on('dragover', allowDrop);
    $('.dragTarget').on('drop', handleDrop);
    
    // drag object
    $('.dragAble').attr('draggable', 'true');
    $('.dragAble').on('dragstart', dragStart);
};

// source and target
function allowDrop(event) {
    event.preventDefault();
    console.log("allowDrop");
    console.log(event);
}

// drag object
function dragStart(event) {
    event.originalEvent.dataTransfer.setData("text", event.target.id);
    event.originalEvent.dataTransfer.effectAllowed = "move";
    console.log("dragStart");
    console.log(event);
}

function handleDrop(event) {
    console.log("handleDrop");
    console.log(event);
    event.originalEvent.preventDefault();
    var data = event.originalEvent.dataTransfer.getData("text");
    $(this).append($('#' + data));
}