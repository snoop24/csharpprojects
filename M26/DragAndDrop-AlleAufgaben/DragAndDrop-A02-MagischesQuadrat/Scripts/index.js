﻿window.onload = function () {

    // source and target
    $('#tblTarget .dragTarget').on('dragover', allowDrop);
    $('#tblTarget .dragTarget').on('drop', handleDrop);

    // drag object
    $('.dragAble').attr('draggable', 'true');
    $('.dragAble').on('dragstart', dragStart);
};

// source and target
function allowDrop(event) {
    event.preventDefault();
    console.log("allowDrop");
    console.log(event);
}

// drag object
function dragStart(event) {
    event.originalEvent.dataTransfer.setData("text", event.target.id);
    event.originalEvent.dataTransfer.effectAllowed = "move";
    console.log("dragStart");
    console.log(event);
}

function handleDrop(event) {
    console.log("handleDrop");
    //console.log(event);
    var data = event.originalEvent.dataTransfer.getData("text");


    // enable drop on now empty position
    $('#' + data).parent().on('dragover', allowDrop);
    $('#' + data).parent().on('drop', handleDrop);

    // drop into new
    $(this).append($('#' + data));

    // disable drop on new position
    $(this).off('drop', handleDrop);
    $(this).off('dragover', allowDrop);


    calcSums();
}

function calcSums() {
    for (let i = 1; i < 4; i++) {
        //rows
        var sum = 0;
        var count = 0;
        var target = $('#tblRowSum .tr' + i + ' td');
        $('#tblTarget .tr' + i + ' td div').each(function () {
            sum += parseInt($(this).html());
            count++;
        });
        target.html(sum);
        console.log(sum);
        if (count === 3 && sum === 15) {
            target.addClass("perfect");
        }
        else {
            target.removeClass("perfect");
        }
        console.log(target[0].outerHTML);

        // columns
        sum = 0;
        count = 0;
        target = $('#tblColSum .td' + i);
        $('#tblTarget .td' + i + ' div').each(function () {
            sum += parseInt($(this).html());
            count++;
        });
        target.html(sum);
        if (count === 3 && sum === 15) {
            target.addClass("perfect");
        }
        else {
            target.removeClass("perfect");
        }
    }

}