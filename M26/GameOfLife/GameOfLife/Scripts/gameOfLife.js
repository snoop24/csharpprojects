﻿/// <reference path="jquery-3.3.1.js" />

window.onload = function () {
    
    var game;

    initGame(game);
}

function disableButtons(value) {
    if (value) {
        $('.start').attr('disabled', 'disabled');
        $('.stop').removeAttr('disabled');
    }
    else {
        $('.stop').attr('disabled', 'disabled');
        $('.start').removeAttr('disabled');
    }
}

function initGame(game) {
    // set game options
    var width = 48;
    var height = Math.floor(width / 1.6);
    var dotsPerField = 20;
    var thickness = 0;
    var cycleDelay = 333;


    var gameParams = { fields: { w: width, h: height, dpf: dotsPerField, borderThickness: thickness }, delay: cycleDelay };

    // initialize game
    game = new GameOfLife(gameParams);

    // align button
    document.getElementById('btnStep').onclick = game.step;
    document.getElementById('btnStart').onclick = game.start;
    document.getElementById('btnStop').onclick = game.stop;
    document.getElementById('btnRandom').onclick = game.random;
    
    
}

// game of life constructor
function GameOfLife(gameParams) {
    // test function 
    this.log = function (data) {
        console.log(data);
    };
    
    var colors = ['none', 'deeppink'];

    // initialize drawing element
    var world = new GOlWorld(this, gameParams.fields);

    var board = newBoard(gameParams.fields);
    function newBoard(fields) {
        var array = []
        for (let i = 0; i < fields.w; i++) {
            var innerArray = [];
            for (var j = 0; j < fields.h; j++) {
                innerArray[j] = 0;
            }
            array[i] = innerArray;
        }
        return array;
    };

    // clicked on the game board
    this.worldGotClicked = function (field) {
        
        //console.log("game.worldGotClicked"); console.log(field);
        board[field.x][field.y] = (board[field.x][field.y] + 1) % 2;
        world.paintField(field.x, field.y, colors[board[field.x][field.y]]);
    };

    // prevent input if necessary
    world.clickAble(true);

    // game random
    this.random = function () {
        for (let i = 0; i < gameParams.fields.w; i++) {
            for (var j = 0; j < gameParams.fields.h; j++) {
                var random = Math.floor(Math.random() * 2);
                board[i][j] = random;
                world.paintField(i, j, colors[random]);
            }
        }
    }

    var id;
    // step life cycle
    this.step = cycle;

    // start life cycle
    this.start = function () {
        console.log("start");
        id = setInterval(cycle, gameParams.delay);
        disableButtons(true);
    }

    // stop life cycle
    this.stop = function () {
        console.log("stop");
        clearInterval(id);
        disableButtons(false);
    }

    // cycle
    function cycle() {
        //console.log("cycle");

        var boardNextCycle = newBoard(gameParams.fields);
        
        for (let i = 0; i < gameParams.fields.w; i++) {
            for (var j = 0; j < gameParams.fields.h; j++) {
                field = boardNextCycle[i][j]
                switch (neighbourSum(i, j)) {
                    case 2:
                        boardNextCycle[i][j] = board[i][j];
                        break;
                    case 3:
                        boardNextCycle[i][j] = 1;
                        world.paintField(i, j, colors[1]);
                        //console.log(i+"/"+j+" : 1")
                        break;
                    default:
                        boardNextCycle[i][j] = 0;
                        world.paintField(i, j, colors[0]);
                        //console.log(i + "/" + j + " : 0")
                        break;
                }
            }
        }
        board = boardNextCycle;
    }

    function neighbourSum(x, y) {
        var sum = 0;
        for (let i = 0; i < 3; i++) {
            for (var j = 0; j < 3; j++) {
                //if (x - 1 + i > -1 && x - 1 + i < gameParams.fields.w && y - 1 + j > -1 && y - 1 + j < gameParams.fields.h && !(i == 1 && j == 1)) {
                if (board[x - 1 + i] && board[x - 1 + i][y - 1 + j]) {
                    sum += board[x - 1 + i][y - 1 + j];
                }
            }
        }
        sum += -board[x][y];
        console.log(x + " "+y +" "+ sum);
        return sum;
    }
}

function GOlWorld(game, fields) {

    // prevent inputs if living
    var clickable = true;
    this.clickAble = function (input) {
        clickable = input;
    }

    // set parent for calling on click
    var parent = game;
    var space = (fields.dpf + fields.borderThickness)
    var width = fields.w * space + 1;
    var height = fields.h * space + 1;

    // create canvas for displaying etc.
    var canvas = (function () {
        document.getElementById('divCanvas').innerHTML = '<canvas id="canMyWorld" height="' + height + '" width="' + width + '"></canvas>';
        return document.getElementById('canMyWorld');
    })();

    var context = canvas.getContext("2d");

    // canvas click
    canvas.onclick = function (event) {
        if (clickable) {
            var clickedField = convertPixelToField(event.pageX - this.offsetLeft, event.pageY - this.offsetTop);
           //console.log("canvas.onlick"); console.log(clickedField);
            // do your stuff here
            if (clickedField.ok) {
                parent.worldGotClicked(clickedField);
            }
        }
    };

    // draw a line
    function drawLine(x1, x2, y1, y2, thickness) {
        context.lineWidth = thickness;
        context.strokeStyle = "lightgrey";//"#a0a0a0";//"#fffc6a";
        context.beginPath();
        context.moveTo(x1, x2);
        context.lineTo(y1, y2);
        context.stroke();
    }



    // paint a field
    this.paintField= function (x, y, color) {
        //console.log("paintField:");console.log(x);console.log(y);console.log(color);
        var left = x * (space) + 1;
        var top = y * (space) + 1;
        if (color === 'none') {
            context.clearRect(left, top, fields.dpf, fields.dpf);
        }
        else {
            context.fillStyle = color;
            context.fillRect(left, top, fields.dpf, fields.dpf);
        }
    }

    // draw field borders
    // vertical
    for (let i = 0; i < fields.w + 1; i++) {
        drawLine(i * (space), 0, i * (space), height - 1, fields.borderThickness);
    }
    // horizontal
    for (let i = 0; i < fields.h + 1; i++) {
        drawLine(0, i * (space), width - 1, i * (space), fields.borderThickness);
    }


    // convert clicked position into field coords and field top and left;
    function convertPixelToField(pixelX, pixelY) {
        var spacing = fields.dpf + fields.borderThickness;

        // offset if check function on board array with margin...
        var offset = 0;
        var indX = Math.floor(pixelX / (spacing));
        var indY = Math.floor(pixelY / (spacing));
        var field = { x: indX+offset, y: indY+offset, ok: true }

        if (pixelX === width || pixelY  === height) {
            field = { x: -1, y: -1, ok: false }
        }

        return field;
    }

}