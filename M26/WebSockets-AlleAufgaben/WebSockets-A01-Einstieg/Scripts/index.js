﻿/// <reference path="jquery-3.2.1.js" />

window.onload = function () {
    "use strict";

    var socket = new WebSocket("ws://127.0.0.1:8081");

    socket.onopen = function () {
        console.log( "WebSocket erfolgreich geöffnet");
    };

    socket.onclose = function () {
        console.log("WebSocket geschlossen");
    };

    socket.onmessage = function (event) {

        var data = {};
        data = JSON.parse(event.data);
        switch (data.type) {
            case 'userJoined':
            case 'userLeft':
                $('#lblCount').html(data.message);
                break;
            case 'newId':
                $('#lblUserId').val('user' + data.message);
                break;
            case 'message':
                $('#txarOutput').val(data.from+ ":"+data.message+"\r\n" + $('#txarOutput').val());
                break;
            default:
                break;
        }
        
    };

    socket.onerror = function (event) {
        console.log("Fehler: " + JSON.stringify(event));
    };

    $('#btnInput').click(
        function () {
            socket.send($('#lblUserId').val()+": " + $('#txtInput').val());
        }
    );

    $('#txtInput').keyup(
        function (event) {
            if (event.keyCode === 13) {
                $('#btnInput').click();
            }
        }
    );

};