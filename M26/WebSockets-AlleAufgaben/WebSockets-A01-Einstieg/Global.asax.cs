﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.SessionState;

namespace WebSockets_A01_Einstieg
{
    public class Global : System.Web.HttpApplication
    {
        private List<IWebSocketConnection> allSockets;


        protected void Application_Start(object sender, EventArgs e)
        {
            allSockets = new List<IWebSocketConnection>();

            var server = new WebSocketServer("ws://127.0.0.1:8081");

            int userID = 0;

            server.Start(socket =>
            {
                socket.OnOpen = () =>
                {
                    allSockets.Add(socket);
                    SendToAll(CreateJsonString("userJoined", "server", allSockets.Count.ToString()));
                    socket.Send(CreateJsonString("newId","server",(++userID).ToString()));
                };

                socket.OnClose = () =>
                {
                    allSockets.Remove(socket);
                    foreach (var s in allSockets)
                    {
                        SendToAll(CreateJsonString("userLeft", "server", allSockets.Count.ToString()));
                    }
                };

                socket.OnMessage = message =>
                {
                    var messageSplit = message.Split(':');
                    SendToAll(CreateJsonString("message", messageSplit[0], messageSplit[1]));
                };
            });

        }

        private void SendToAll(string message)
        {
            foreach (var s in allSockets)
            {
                s.Send(message ?? "invalid message");
            }
        }


        private string CreateJsonString(string type, string from, string message)
        {
            return $"{{ \"type\": \"{type}\", \"from\": \"{from}\",\"message\": \"{message}\" }}";
        }

        #region: unnecessary
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
        #endregion 
    }
}