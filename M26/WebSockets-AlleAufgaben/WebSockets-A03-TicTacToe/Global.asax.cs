﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Script.Serialization;
using Fleck;
namespace WebSockets_A03_TicTacToe
{
    public class Global : System.Web.HttpApplication
    {
        static public JavaScriptSerializer json = new JavaScriptSerializer();

        List<IWebSocketConnection> allSockets;

        Models.TicTacToeGame game;

        protected void Application_Start(object sender, EventArgs e)
        {
            allSockets = new List<IWebSocketConnection>();

            var server = new WebSocketServer("ws://127.0.0.1:8081");

            server.Start(socket =>
            {
                socket.OnOpen = () =>
                {
                    if (allSockets.Count == 0) {
                        game = new Models.TicTacToeGame();
                    }
                    allSockets.Add(socket);
                    string message;
                    int player = allSockets.IndexOf(socket);
                    if (player < 2)
                    {
                        message = json.Serialize(new Models.TicTacToeMessage("player", player.ToString()));
                    }
                    else {
                        message = json.Serialize(new Models.TicTacToeMessage("player", (2).ToString()));
                    }
                    socket.Send(message);
                };

                socket.OnClose = () =>
                {
                    allSockets.Remove(socket);
                };

                socket.OnMessage = message =>
                {
                    Models.TicTacToeMessage clientMessage = json.Deserialize<Models.TicTacToeMessage>(message);
                    clientMessage = json.Deserialize<Models.TicTacToeMessage>(message);
                    if (!game.GameOver)
                    {
                        string moveMessage = json.Serialize(game.MoveByPlayer(clientMessage));
                        string winMessage = json.Serialize(game.WonByPlayer());
                        foreach (var s in allSockets)
                        {
                            s.Send(moveMessage);
                            s.Send(winMessage);
                        }
                    }
                    
                };
            });

        }


        #region:hide
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
        #endregion
    }

    
}