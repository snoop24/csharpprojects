﻿/// <reference path="jquery-3.3.1.js" />

var game;
var player;

window.onload = function () {
    "use strict";

    var intervalId;
    var socket = new WebSocket("ws://127.0.0.1:8081");

    socket.onopen = function () {
        console.log("Verbindung hergestellt");
    };

    socket.onclose = function () {
        console.log("Verbindung geschlossen");
        clearInterval(intervalId);
    };

    socket.onmessage = function (event) {
        //console.log(event.data);
        var data = JSON.parse(event.data);
        console.log(data);
        switch (data.type) {
            case '0':
            case '1':
                console.log("player " + data.type + " moves on " + data.value);
                game.move(parseInt(data.value), parseInt(data.type));
                break;
            case 'player':
                player = parseInt(data.value);
                console.log(player);
                $('#divPlayer').html('Player ' + (player === 0 ? 'X' : 'O'));
                break;
            case 'result':
                message = data.value === '2' ? 'Draw' : 'Player ' + (data.value === '0' ? 'X' : 'O');
                alert(message);
                break;
            default:
                //alert(event.data);
                break;
        }

    };

    socket.onerror = function (event) {
        console.log("Fehler: " + JSON.stringify(event));
    };
    
    initGame(socket);
};

function initGame(socket) {
    // create new game
    var data = { size: 402, thickness: 3, canvasDivName: "canvasHere", canvasId: "ticTacToe", connection: socket };
    game = new Game(data);
}

function Game(data) {

    // create draw object
    var drawArea = new DrawArea(data);

    

    // draw on html page
    drawArea.drawBoard();
    
    // move on index
    this.move = moveAtAsPlayer;


    function moveAtAsPlayer (index, player) {
        drawArea.drawSign(index, player);
    }
}

// draw area constructor 
function DrawArea(data) {
    // create board to store field coords 
    var board = makeBoard(data);

    var size = data.size;
    var thickness = data.thickness;
    var game = data.game;
    var connection = data.connection;

    // html canvas element
    var canvas = (function () {
        document.body.innerHTML += "<article id='" + data.canvasDivName + "'><canvas id='" + data.canvasId + "' height='" + size + "' width='" + size + "'></canvas></article>";
        return document.getElementById(data.canvasId);
    })();

    // set canvas onclick event
    canvas.onclick = function (event) {
        var coords = {
            x: event.pageX - this.offsetLeft
            , y: event.pageY - this.offsetTop
        };
        var matchIndex = -1;
        for (var item in board) {
            if (board[item].xMin <= coords.x && board[item].xMax >= coords.x
                && board[item].yMin <= coords.y && board[item].yMax >= coords.y
            ) {
                matchIndex = item;
            }
        }
        if (matchIndex !== -1) {
            var jsonString = JSON.stringify({ type: player.toString(), value: matchIndex.toString() });
            console.log("jsonString: " + jsonString);
            connection.send(jsonString);
        }
    };

    // set context
    var context = canvas.getContext("2d");

    // draw a line
    function drawLine(x1, x2, y1, y2, timesThicker) {
        context.lineWidth = thickness * timesThicker;
        context.beginPath();
        context.moveTo(x1, x2);
        context.lineTo(y1, y2);
        context.stroke();
    }

    // draw board
    this.drawBoard = function () {
        var space = (size - thickness * 2) / 3;
        var timesThicker = 1;
        drawLine(space, 0, space, size, timesThicker);
        drawLine(space * 2 + thickness, 0, space * 2 + thickness, size, timesThicker);
        drawLine(0, space, size, space, timesThicker);
        drawLine(0, space * 2 + thickness, size, space * 2 + thickness, timesThicker);
    };

    // draw sign
    this.drawSign = function (index, value) {
        var field = board[index];
        if (value === 0) {
            drawX(field);
        }
        else {
            drawO(field);
        }
    };

    // draw X
    function drawX(field) {
        context.strokeStyle = "#FF0000";
        drawLine(field.xMin + 20, field.yMin + 20, field.xMax - 20, field.yMax - 20, 3);
        drawLine(field.xMin + 20, field.yMax - 20, field.xMax - 20, field.yMin + 20, 3);
        context.strokeStyle = "#000000";
    }

    // draw O
    function drawO(field) {
        context.lineWidth = thickness * 3;
        context.strokeStyle = "#0000FF";
        context.beginPath();
        context.arc((field.xMin + field.xMax) / 2, (field.yMin + field.yMax) / 2, (field.xMax - field.xMin) / 2 - 20, 0, 2 * Math.PI);
        context.stroke();
        context.strokeStyle = "#000000";
    }
}

// board creation
function makeBoard(data) {
    var thickness = data.thickness;
    var space = (data.size - 2 * thickness) / 3;
    var fields = [];
    for (let i = 0; i < 9; i++) {
        fields[i] = {
            xMin: i % 3 * (space + thickness)
            , xMax: i % 3 * (space + thickness) + space - 1
            , yMin: Math.floor(i / 3) * (space + thickness)
            , yMax: Math.floor(i / 3) * (space + thickness) + space - 1
        };
    }
    return fields;
}

