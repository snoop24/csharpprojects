﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSockets_A03_TicTacToe.Models
{
    public class TicTacToeGame
    {
        private int[] board = new int[] { -9, -9, -9, -9, -9, -9, -9, -9, -9 };

        private int nextPlayer=0;
        public int NextPlayer { get { return nextPlayer; } }

        public bool GameOver
        {
            get
            {
                return GameState()>-1;
            }
        }

        public TicTacToeMessage MoveByPlayer(TicTacToeMessage message)
        {
            return MoveByPlayer(int.Parse(message.value), int.Parse(message.type));
        }

        public TicTacToeMessage MoveByPlayer(int index, int player)
        {
            bool validMove = Move(index, player);
            string messageType = validMove ? player.ToString() : "invalidMove";
            string messageValue = validMove? index.ToString(): "blocked field/wrong player";
            TicTacToeMessage moveByPlayer = new TicTacToeMessage(messageType, messageValue);
            return moveByPlayer;
        }

        public TicTacToeMessage WonByPlayer()
        {
            string messageType = "winner";
            string messageValue = GameState().ToString() ;
            TicTacToeMessage moveByPlayer = new TicTacToeMessage(messageType, messageValue);
            return moveByPlayer;
        }

        private bool Move(int index,int player) {
            bool moved = false;
            if (player == nextPlayer && board[index] == -9)
            {
                board[index] = nextPlayer;
                nextPlayer = (nextPlayer+1)% 2;
                moved = true;
            }
            return moved;
        }

        private int GameState()
        {
            int gameState = -1;

            gameState = Winner();

            if (gameState == -1 && board.Sum()>-1)
            {
                gameState = 2;
            }

            return gameState;
        }

        private int Winner() {
            int winner = -1;
            int i = 0;
            if (board[i] != -10 && (
                board[i] == board[i + 1] && board[i] == board[i + 2]
                ||
                board[i] == board[i + 3] && board[i] == board[i + 6]
                ||
                board[i] == board[i + 4] && board[i] == board[i + 8]
            )
            )
            {
                winner = board[i];
            }
            i = 4;
            if (board[i] != -10 && (
                board[i] == board[i - 1] && board[i] == board[i + 1]
                ||
                board[i] == board[i - 3] && board[i] == board[i + 3]
                ||
                board[i] == board[i - 2] && board[i] == board[i + 2]
            )
            )
            {
                winner = board[i];
            }
            i = 8;
            if (board[i] != -10 && (
                board[i] == board[i - 1] && board[i] == board[i - 2]
                ||
                board[i] == board[i - 3] && board[i] == board[i - 6]
            )
            )
            {
                winner = board[i];
            }

            return winner;
        }

    }

    public class TicTacToeMessage
    {
        public string type = "";

        public string value = "";

        public TicTacToeMessage(string type, string value)
        {
            this.type = type;
            this.value = value;
        }

        public TicTacToeMessage() { }
    }

}