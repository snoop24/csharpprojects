﻿using System;
using System.Diagnostics;
using Fleck;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Script.Serialization;

namespace WebSockets_A02_Leistungsdaten
{
    public class Global : System.Web.HttpApplication
    {
        JavaScriptSerializer json = new JavaScriptSerializer();

        protected void Application_Start(object sender, EventArgs e)
        {

            var server = new WebSocketServer("ws://127.0.0.1:8081");

            server.Start(socket =>
            {
                socket.OnMessage = message =>
                {
                    switch (message) {
                        case "getData":
                            socket.Send(json.Serialize(PerfMon.GetActualData()));
                            break;
                        default:
                            break;
                    }   
                };
            });
        }
        

        #region: unnecessary
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
        #endregion
    }

    public class PerfMon
    {
        public float percentCpu;
         
        public float usedRam;
        public float freeRam;
         
        public float percentFreeLogicalDisk;
        public float freeLogicalDisk;

        public string unit;

        public PerfMon(float percentCpu, float usedRam, float freeRam, float percentFreeLogicalDisk, float freeLogicalDisk, string unit = "memory:KB,disk:MB") {
            this.percentCpu = percentCpu;
            this.usedRam = usedRam;
            this.freeRam = freeRam;
            this.percentFreeLogicalDisk = percentFreeLogicalDisk;
            this.freeLogicalDisk = freeLogicalDisk;
            this.unit = unit;
        }

        static public PerformanceCounter cpuPercent = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        static public PerformanceCounter memUsed = new PerformanceCounter("Memory", "Available Bytes");
        static public PerformanceCounter memFree = new PerformanceCounter("Memory", "Committed Bytes");
        static public PerformanceCounter lDiskFreePercent = new PerformanceCounter("LogicalDisk", "% Free Space", "_Total");
        static public PerformanceCounter lDiskFreeBytes = new PerformanceCounter("LogicalDisk", "Free Megabytes", "_Total");


        static public PerfMon GetActualData()
        {
            return new PerfMon(cpuPercent.NextValue(), memUsed.NextValue(), memFree.NextValue(), lDiskFreePercent.NextValue(), lDiskFreeBytes.NextValue());
        }

    }
}