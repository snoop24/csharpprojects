﻿/// <reference path="jquery-3.2.1.js" />

window.onload = function () {
    "use strict";

    var intervalId;
    var socket = new WebSocket("ws://127.0.0.1:8081");

    socket.onopen = function () {
        console.log("Verbindung hergestellt");
    };

    socket.onclose = function () {
        console.log("Verbindung geschlossen");
        clearInterval(intervalId);
    };

    socket.onmessage = function (event) {
        console.log("raw: " + event.data);
        var data = {};
        data = JSON.parse(event.data);
        console.log(data);
        //console.log(new Date().getSeconds());
        data = shapeData(data);
        displayData(data);
        googleGaugeCharts(data);
    };

    socket.onerror = function (event) {
        console.log("Fehler: " + JSON.stringify(event));
    };

    function startInterval() {
        intervalId = setInterval(function () {
            socket.send('getData');
        }, 1000 * parseFloat($('#txtInterval').val()));
    }

    $('#txtInterval').on('input', function () {
        $('#rngInterval').val(parseFloat($('#txtInterval').val()));
        clearInterval(intervalId);
        startInterval();
        console.log("txt.oninput");
    });

    $('#rngInterval').on('change', function () {
        $('#txtInterval').val(parseFloat($('#rngInterval').val()));
        $('#txtInterval').trigger('input');
        console.log("rng.onchange");
    });

    startInterval();
};

function displayData(data) {
    $('#lblCpuPercent').text(data.percentCpu + " %");
    $('#proCpu').val(data.percentCpu);

    $('#lblRamUsed').text(data.usedRam);
    $('#proRam').val(data.percentRam);

    $('#lblDiskUsed').text(data.usedLogicalDisk);
    $('#proDisk').val(data.percentLogicalDisk);

    console.log(data);
}

function shapeData(data) {

    data.percentCpu = data.percentCpu.toFixed(1);

    data.maxRam = ((data.usedRam + data.freeRam) / 1024 / 1024).toFixed(0);
    data.percentRam = (data.usedRam / (data.freeRam + data.usedRam) * 100).toFixed(1);
    data.usedRam = (data.usedRam / 1024 / 1024).toFixed(0);
    data.freeRam = (data.freeRam / 1024 / 1024).toFixed(0);

    data.maxLogicalDisk = (data.freeLogicalDisk / data.percentFreeLogicalDisk * 100 / 1024).toFixed(0);
    data.percentFreeLogicalDisk = data.percentFreeLogicalDisk.toFixed(1);
    data.freeLogicalDisk = (data.freeLogicalDisk / 1024).toFixed(0);
    data.usedLogicalDisk = data.maxLogicalDisk - data.freeLogicalDisk;
    data.percentLogicalDisk = 100 - data.percentFreeLogicalDisk;

    return data;
}

function googleGaugeCharts(myData) {
    //var myData = inpData;
    google.charts.load('current', { 'packages': ['gauge'] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        console.log("Data set:");
        console.log(myData);

        var data = google.visualization.arrayToDataTable([
            ['Label', 'Value'],
            //['Memory', 5],
            //['CPU', 10],
            //['Disk', 15]
            ['Memory', parseFloat(myData.percentRam)],
            ['CPU', parseFloat(myData.percentCpu)],
            ['Disk', parseFloat(myData.percentLogicalDisk)]
        ]);

        var options = {
            width: 400, height: 120,
            redFrom: 90, redTo: 100,
            yellowFrom: 75, yellowTo: 90,
            minorTicks: 5
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        chart.draw(data, options);
    }
}
