﻿window.onload = function () {

    document.getElementById("inpLoginName").onblur = function () {
        loginValid();
    }

    document.getElementById("inpPassword").onblur = function () {
        passwordValid();
    }

    document.getElementById("inpRepeatPW").onblur = function () {
        passwordValid();
    }

    document.getElementById("btnNewUser").onclick = function () {
        newUser();
    }

};




function newUser() {
    if (loginValid() && passwordValid()) {
        alert("Benutzer angelegt!");
        clear();
    }
};

function clear() {
    document.getElementById("inpLoginName").value = "";
    document.getElementById("inpPassword").value = "";
    document.getElementById("inpRepeatPW").value = "";

    document.getElementById("lblLoginName").value = "";
    document.getElementById("lblPassword").value = "";
    document.getElementById("lblRepeatPW").value = "";
}

function loginValid() {
    var inpLoginName = document.getElementById("inpLoginName");

    var lblLoginName = document.getElementById("lblLoginName");

    var nameOk = false;
    // login valid
    if (inpLoginName.value !== "") {
        lblLoginName.innerHTML = "";
        nameOk = true;
    }
    else {
        lblLoginName.innerHTML = "*required";
    }

    return nameOk;
};

function passwordValid() {
    var inpPassword = document.getElementById("inpPassword");
    var inpRepeatPW = document.getElementById("inpRepeatPW");

    var lblPassword = document.getElementById("lblPassword");
    var lblRepeatPW = document.getElementById("lblRepeatPW");

    var pwOk = false;

    // password power valid and matching with repeated pw
    var pwOkState = passwordPowerCheck(inpPassword.value);
    if (pwOkState.ok) {
        lblPassword.innerHTML = "";

        if (inpPassword.value === inpRepeatPW.value) {

            lblRepeatPW.innerHTML = "";
            pwOk = true;
        }
        else {
            lblRepeatPW.innerHTML = "Passwörter stimmen nicht überein!";
        }
    }
    else {
        lblPassword.innerHTML = "*(min. 8 Zeichen, 1 Gro&szlig;buchstaben, 1 Ziffer, 1 Sonderzeichen aus §$%&/(),.;: ||| state:" + pwOkState.state.join();
        lblRepeatPW.innerHTML = "";
    }

    return pwOk;
};

function passwordPowerCheck(password) {
    var powerOk = true;
    var stateValue = [0,0,0,0];
    // pw length
    if (!(password.length >= 8)) {
        powerOk = false;
        stateValue[0] = 1;
    }
    // at least one digit
    //if (!(new RegExp("\\d").test(password))) {
    if (!/\d/.test(password)) {
        powerOk = false;
        stateValue[1]= 1;
    }
    // at least one UpperCaseLetter
    if (!/[A-Z]/.test(password)) {
        powerOk = false;
        stateValue[2]= 1;
    }
    // one of this §$%&/(),.;: characters
    if (!/[§$%&/(),.;:]/.test(password)) {
        powerOk = false;
        stateValue[3]= 1;
    }
    var test = { ok: powerOk, state: stateValue }
    return test;
}