﻿function diceIt(throws) {
    // array to count dice result
    var countDice = [0, 0, 0, 0, 0, 0];
    // dice
    for (var i = 0; i < throws; i++) {
        countDice[Math.floor(Math.random() * 6)]++;
    }
    return countDice;
}

(function() {
    // get number of throws and go dice
    var throws = prompt("Wie viele Würfe?", "100");
    var result = diceIt(throws);
    // output
    document.write("<h1>W&uuml;rfelstatistik "+throws+" Würfe</h2>");
    document.write("<table><tr><th>Augenzahl</th><th>Anzahl geworfen</th></tr>");
    for (var i = 0; i < 6; i++){
        document.write("<tr><td>"+(i+1)+"</td><td>"+result[i]+"</td></tr>")
    }
    document.write("</table>");
})();

window.onload = function(){
    var item = document.getElementById("test");
    // get number of throws and go dice
    var throws = prompt("Wie viele Würfe?", "100");
    var result = diceIt(throws);
    // output
    var htmlstring="<h1>W&uuml;rfelstatistik " + throws + " Würfe</h2>";
    htmlstring +="<table><tr><th>Augenzahl</th><th>Anzahl geworfen</th></tr>";
    for (var i = 0; i < 6; i++) {
        htmlstring += "<tr><td>" + (i + 1) + "</td><td>" + result[i] + "</td></tr>";
    }
    htmlstring +="</table>";
    item.innerHTML = htmlstring;
};