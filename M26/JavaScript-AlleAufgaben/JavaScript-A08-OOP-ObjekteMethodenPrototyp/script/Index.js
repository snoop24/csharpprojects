﻿
// teil 1
function Punkt(x, y) {
    this.x = x;
    this.y = y;

    this.toString = function () {
        return "(" + this.x + "/" + this.y + ")"
    }

    this.log = function () {
        console.log(this.toString());
    }

    this.getDistanceFrom = function (punkt) {
        return Math.sqrt(Math.pow(this.x - punkt.x, 2) + Math.pow(this.y - punkt.y, 2));
    }

    this.getDistance = function () {
        return this.getDistanceFrom(new Punkt(0, 0));
    }
}

// teil 3
function Linie(startX, startY, endeX, endeY) {
    this.start = new Punkt(startX, startY);
    this.ende = new Punkt(endeX, endeX);

    this.toString = function () {
        return ("L[ S" + this.start.toString() + " / E" + this.ende.toString() + " : "+this.length()+" ]");
    }

    this.log = function () {
        console.log(this.toString());
    }

    this.length = function () {
        return this.start.getDistanceFrom(this.ende);

    }
}

//teil 4
Array.prototype.shuffle = function () {
    for (var i = 0; i < this.length*4;i++){
        index1 = Math.floor(Math.random() * this.length);
        index2 = Math.floor(Math.random() * this.length);
        var temp = this[index1];
        this[index1] = this[index2];
        this[index2] = temp;
    }
    return this;
}


// teil 2 Testen Sie Ihren Code vom vorherigen Punkt anhand des folgenden Codes: 
console.log("\n--------- Teil 2\n")

var p1 = new Punkt(1, 1);
var p2 = new Punkt(10, 10);
console.log(p1.toString());
p2.log();
console.log(p1.getDistance());
console.log(p2.getDistanceFrom(p1));

// teil 3 Erweitern Sie den Code aus der Aufgabe davor so, dass folgendes Testprogramm möglich wird:
console.log("\n--------- Teil 3\n")

var linie = new Linie(1, 1, 10, 10);
//alert(linie.toString());
linie.log();
console.log(linie.length());

//4. Sorgen Sie dafür, dass jedes Array in Ihrem Skript wie folgt behandelt werden kann:
console.log("\n--------- Teil 4\n")
var x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0].shuffle();
console.log(x.join(","));

var y = ["Kirk", "Spock", "Bones", "Scotty", "Chekov", "Uhura"].shuffle();
console.log(y.join(","));