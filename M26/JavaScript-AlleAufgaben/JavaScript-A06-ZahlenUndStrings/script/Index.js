﻿window.onload = function () {
    // aufgabe 1
    document.getElementById("ausgabe1").onchange = ausgabe1;

    //aufgabe 2
    setOnChange(true);

    // aufgabe 3
    document.getElementById("btnISBN").onclick = ausgabe3;
};

// aufgabe 1
function ggt(a, b) {
    var temp = 0;
    while (b !== 0) {
        temp = a % b;
        a = b;
        b = temp;
    }
    return a;
}

function kgV(a, b) {
    return (a * b / ggt(a, b));
}

function ausgabe1() {
    var a = document.getElementById("inpNumber1").value;
    var b = document.getElementById("inpNumber2").value;

    if (a !== "" && b !== "") {
        if (document.getElementById("radioGgt").checked) {
            document.getElementById("inpGgt").value = ggt(a, b);
        }
        else {
            document.getElementById("inpGgt").value = kgV(a, b);
        }

    }
}

// aufgabe 2
function ausgabe2(zahl, system) {

    var binOctDecHex = alleUmwandeln(zahl, system);

    setOnChange(false);
    document.getElementById("inpBin").value = binOctDecHex.bin;
    document.getElementById("inpOct").value = binOctDecHex.oct;
    document.getElementById("inpDec").value = binOctDecHex.dec;
    document.getElementById("inpHex").value = binOctDecHex.hex;
    setOnChange(true);
}

function setOnChange(state) {
    if (state) {
        document.getElementById("inpBin").onchange = function () {
            ausgabe2(document.getElementById("inpBin").value, 2);
        }
        document.getElementById("inpOct").onchange = function () {
            ausgabe2(document.getElementById("inpOct").value, 8);
        }
        document.getElementById("inpDec").onchange = function () {
            ausgabe2(document.getElementById("inpDec").value, 10);
        }
        document.getElementById("inpHex").onchange = function () {
            ausgabe2(document.getElementById("inpHex").value, 16);
        }
    }
    else {
        document.getElementById("inpBin").onchange = function () {
        }
        document.getElementById("inpOct").onchange = function () {
        }
        document.getElementById("inpDec").onchange = function () {
        }
        document.getElementById("inpHex").onchange = function () {
        }
    }
}

function alleUmwandeln(zahl, system) {
    var systems = { bin: 2, oct: 8, hex: 16 };
    var binOctDecHex = [];

    // to decimal
    if (system !== 10) {
        binOctDecHex.dec = parseInt(zahl, system).toString(10);
    }
    else {
        binOctDecHex.dec = zahl;
    }

    // from decimal to binary, octal and hexadecimal if necessary
    for (var x in systems) {
        if (systems[x] !== system) {
            binOctDecHex[x] = parseInt(binOctDecHex.dec, 10).toString(systems[x]).toUpperCase();
        }
        else {
            binOctDecHex[x] = zahl.toUpperCase();
        }
    }

    return binOctDecHex;
}

// aufgabe 3

function ausgabe3() {
    var isbnOk = checkISBN(document.getElementById("inpISBN").value);
    document.getElementById("lblISBN").innerHTML = (isbnOk ? "OK" : "Fehlerhaft");
}

function checkISBN(isbn) {
    var isbnOk = false;
    isbn = isbn.toString();
    var isbnLength = isbn.toString().length;
    if (isbnLength === 10) {
        var temp = 0;
        for (var i = 0; i < isbnLength; i++) {
            temp += (i+1) * isbn[i];
        }
        //alert(temp);
        isbnOk = (temp % 11 === 0);
    }
    else if (isbn.toString().length === 13) { 
        var temp = 0;
        for (var i = 0; i < isbnLength; i++) {
            temp += ((i%2)*2+1) * isbn[i];
        }
        //alert(temp);
        isbnOk = (temp % 10 === 0);
    }
    return isbnOk;
}