﻿var name = prompt("Wie ist Ihr Name?", "IhrName");
if (name === null) {
    name = "Niemand";
}
var anrede = prompt("Wie möchten Sie angeredet werden?", "Du/Sie");
if (anrede !== null && anrede.toLowerCase() === "du") {
    anrede = true;
}
else {
    anrede = false;
}
document.write("<p>Herzlich Willkommen auf dieser Seite, <b>" + name + "</b>, " + (anrede === true ? "Du" : "Sie") + " Pfeifenheini!</p>");
document.write("<br/>");
document.write("<hr/>");