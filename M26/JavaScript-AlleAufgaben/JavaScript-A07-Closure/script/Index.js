﻿window.onload = startUp;


function startUp() {
    var buttons = document.querySelectorAll("input[type=button]");
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].onclick = (function (btnIndex) {
            return function inner(e) {
                alert('Button Nummer: ' + (btnIndex + 1) +' | html ID: '+e.currentTarget.id);
            }
        })(i);
    }
}