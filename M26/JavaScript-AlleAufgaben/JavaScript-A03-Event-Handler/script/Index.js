﻿window.onload = OnLoad;

function OnLoad() {
    // aufgabe 1
    document.getElementById("btnBackColor").onclick = function () {
        ChangeBodyBackground('red');
    }

    // aufgabe 2
    document.getElementById("btnHello").onclick = function () {
        document.getElementById("helloAgain").innerHTML = "Hallo Welt";
    };

    // aufgabe 3
    document.getElementById("javaScriptLogo").onmouseover = function () {
        changePictureSource("javaScriptLogo", "javascript_logo2")
    }
    document.getElementById("javaScriptLogo").onmouseout = function () {
        changePictureSource("javaScriptLogo", "javascript_logo1")
    }
    // aufgabe 4
    document.getElementById("javaScriptLogo").onclick = function () {
        changePictureSource("javaScriptLogo", "javascript_logo3")
    }

    // aufgabe 5
    document.getElementById("txtNetto").oninput = function () {
        berechneMwSt(document.getElementById("txtNetto"));
    };
    document.getElementById("txtMwStSatz").oninput = function () {
        berechneMwSt(document.getElementById("txtMwStSatz"));
    };
    document.getElementById("txtMwStBetrag").oninput = function () {
        berechneMwSt(document.getElementById("txtMwStBetrag"));
    };
    document.getElementById("txtBrutto").oninput = function () {
        berechneMwSt(document.getElementById("txtBrutto"));
    };
}

function ChangeBodyBackground(colorBack) {
    document.body.style.backgroundColor = colorBack;
}

function changePictureSource(id, name) {
    document.getElementById(id).src = "/data/img/" + name + ".png";
}

function berechneMwSt(changedElement) {
    var netto = document.getElementById("txtNetto");
    var satz = document.getElementById("txtMwStSatz");
    var betrag = document.getElementById("txtMwStBetrag");
    var brutto = document.getElementById("txtBrutto");

    var nettoZahl = parseInt(netto.value * 100, 10);
    var satzZahl = parseFloat(satz.value / 100, 10);
    var betragZahl = parseInt(betrag.value * 100, 10);
    var bruttoZahl = parseInt(brutto.value * 100, 10);

    switch (changedElement) {
        case brutto:
            nettoZahl = Math.round(bruttoZahl / (1 + satzZahl));
            betragZahl = bruttoZahl - nettoZahl;
            netto.value = nettoZahl / 100;
            betrag.value = betragZahl / 100;
            break;
        case betrag:
            nettoZahl = Math.round(betragZahl / satzZahl);
            bruttoZahl = betragZahl + nettoZahl;
            netto.value = nettoZahl/100;
            brutto.value = bruttoZahl/100;
            break;
        default:
            betragZahl = Math.round(nettoZahl * satzZahl);
            bruttoZahl = nettoZahl + betragZahl;
            betrag.value = betragZahl / 100;
            brutto.value = bruttoZahl / 100;
            break;
    }
}