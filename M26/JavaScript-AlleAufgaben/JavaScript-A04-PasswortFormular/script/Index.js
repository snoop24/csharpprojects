﻿window.onload = function () {

    document.getElementById("inpLoginName").onblur = function () {
        loginValid();
    }

    document.getElementById("inpPassword").onblur = function () {
        passwordValid();
    }

    document.getElementById("inpRepeatPW").onblur = function () {
        passwordValid();
    }

    document.getElementById("btnNewUser").onclick = function () {
        newUser();
    }

};




function newUser() {
    if (loginValid() && passwordValid()) {
        alert("Benutzer angelegt!");
        clear();
    }
};

function clear() {
    document.getElementById("inpLoginName").value = "";
    document.getElementById("inpPassword").value = "";
    document.getElementById("inpRepeatPW").value = "";

    document.getElementById("lblLoginName").value = "";
    document.getElementById("lblPassword").value = "";
    document.getElementById("lblRepeatPW").value = "";
}

function loginValid() {
    var inpLoginName = document.getElementById("inpLoginName");

    var lblLoginName = document.getElementById("lblLoginName");

    var nameOk = false;
    // login valid
    if (inpLoginName.value !== "") {
        lblLoginName.innerHTML = "";
        nameOk = true;
    }
    else {
        lblLoginName.innerHTML = "*required";
    }

    return nameOk;
};

function passwordValid() {
    var inpPassword = document.getElementById("inpPassword");
    var inpRepeatPW = document.getElementById("inpRepeatPW");

    var lblPassword = document.getElementById("lblPassword");
    var lblRepeatPW = document.getElementById("lblRepeatPW");

    var pwOk = false;

    // password provided and matching
    if (inpPassword.value !== "") {
        lblPassword.innerHTML = "";

        if (inpPassword.value === inpRepeatPW.value) {

            lblRepeatPW.innerHTML = "";
            pwOk = true;
        }
        else {
            lblRepeatPW.innerHTML = "Passwörter stimmen nicht überein!";
        }
    }
    else {
        lblPassword.innerHTML = "*required";
        lblRepeatPW.innerHTML = "";
    }

    return pwOk;
};