﻿window.onload = function () {
    "use strict";
    //$('.btn').each(function () {
    //    $(this).click(eval($(this).val().toLowerCase()))
    //});
    $('.btn').click(function () {
        eval($(this).val().toLowerCase() + "()");
    });
    console.log("loading finished");
};

function berechnen() {
    "use strict";
    //console.log('berechnen');
    var startwert = parseFloat($('#inpStartwert').val());
    var endwert = parseFloat($('#inpEndwert').val());
    var schrittweite = parseFloat($('#inpSchrittweite').val());
    var leitwert = parseFloat($('#selLeitwert :selected').val());

    var tabelle = berechneWiderstandTabelle(startwert, endwert, schrittweite, leitwert);

    //console.log(tabelle);

    display(tabelle);
}

function berechneWiderstandTabelle(startwert, endwert, schrittweite, leitwert) {
    "use strict";

    //console.log('berechne tabelle');

    var anzahl = (endwert - startwert) / schrittweite + 1;
    var durchmesser = [0.5, 0.6, 0.7, 0.8, 0.9, 1.0];

    var table = [['l\\d', '0.5mm', '0.6mm', '0.7mm', '0.8mm', '0.9mm', '1.0mm']];
    for (let i = 0; i < anzahl; i++) {
        var row = [startwert + i * schrittweite + 'm'];
        for (let j = 0; j < durchmesser.length; j++) {
            row[j + 1] = widerstandBerechnen(startwert + i * schrittweite, durchmesser[j], leitwert);
        }
        table[i + 1] = row;
    }

    return table;
}

function widerstandBerechnen(laenge, durchmesser, leitwert) {
    "use strict";

    var querschnittsflaeche = Math.PI * Math.pow(durchmesser, 2) / 4;
    var widerstand = Math.round(laenge / leitwert / querschnittsflaeche * 1000) / 1000;

    return widerstand;
}

function display(tabelle) {
    //console.log('display');

    $('#artMain').append("<table id='tblOutput' class='Output'></table>");

    var html = "";

    //header
    html += '<tr>\n';
    for (let j = 0; j < tabelle[0].length; j++) {
        html += '<th class="th' + (j + 1) + ' th">' + tabelle[0][j] + '</th>\n';
    }
    html += '</tr>\n';

    //data
    for (let i = 1; i < tabelle.length; i++) {
        html += '<tr>\n';
        for (let j = 0; j < tabelle[i].length; j++) {
            html += '<td class="td'+(j+1)+'">' + tabelle[i][j] + '</td>\n';
        }
        html += '</tr>\n';
    }

    console.log(html);

    $('#tblOutput').html(html);
}
function speichern() {
    "use strict";

    var data = {
        startwert: parseFloat($('#inpStartwert').val()),
        endwert: parseFloat($('#inpEndwert').val()),
        schrittweite: parseFloat($('#inpSchrittweite').val()),
        leitwert: parseFloat($('#selLeitwert :selected').val())
    };
    
    console.log('speichern');
    localStorage.setItem("savedTable", JSON.stringify(data));
}

function laden() {
    "use strict";
    console.log('laden');
    var data = JSON.parse(localStorage.getItem("savedTable"));

    $('#inpStartwert').val(data.startwert);
    $('#inpEndwert').val(data.endwert);
    $('#inpSchrittweite').val(data.schrittweite);
    $('#selLeitwert').val(data.leitwert);

    berechnen();
    //display(berechneWiderstandTabelle(data.startwert, data.endwert, data.schrittweite, data.leitwert));


}