﻿window.onload = function () {
    initGame();
};

// game

// init game
function initGame(savegame) {
    //console.clear();
    logToConsole("initialising...");
    var data = { size: 402, thickness: 3, canvasDivName: "canvasHere", canvasId: "ticTacToe", save: savegame };

    // delete old game if exists
    document.getElementById(data.canvasDivName).innerHTML = "";
    // create new game
    //game =
    var game = new Game(data);

    // align button click events
    document.getElementById("btnReset").onclick = game.reset;
    document.getElementById("btnSave").onclick = game.save;
    document.getElementById("btnLoad").onclick = game.load;
}


// game constructor
function Game(data) {

    // add game to data for elements
    data.game = this;

    // who is next
    var nextplayer = 0;

    // create draw object
    var drawArea = new DrawArea(data);

    // create board to store field coords and player moves
    var board = makeBoard(data);

    // for saving values to replay when load
    var moves = [];
    var savedMoves = data.save === undefined || data.save.length === undefined ? [] : data.save;

    // make moves of saved game on load
    for (var i in savedMoves) {
        tryMove(savedMoves[i]);
    }

    // draw on html page
    drawArea.drawBoard();

    // save load reset
    this.save = function () {
        logToConsole('saving... (' + moves + ')');
        localStorage.setItem("savegame", JSON.stringify(moves));
    };

    this.load = function () {
        var savegame = JSON.parse(localStorage.getItem("savegame"));
        logToConsole('loading...(' + savegame + ')');
        initGame(savegame);
    };

    this.reset = function () {
        logToConsole('resetting...');
        initGame();
    };

    // if clicked
    this.clickedAt = function (coords) {
        var index = where(coords);
        if (index !== -1) {
            if (tryMove(index)) {
                switch (checkWinDraw()) {
                    case 0:
                        onceAgain("Spieler X hat gewonnen!");
                        break;
                    case 1:
                        onceAgain("Spieler O hat gewonnen!");
                        break;
                    case 2:
                        onceAgain("Unentschieden!");
                        break;
                    default:
                }
            }
        }
    };

    

    // private functions

    // get field index of click
    function where(coords) {
        var matchIndex = -1;
        for (var item in board) {
            if (board[item].xMin <= coords.x && board[item].xMax >= coords.x
                && board[item].yMin <= coords.y && board[item].yMax >= coords.y
            ) {
                matchIndex = item;
            }
        }
        return matchIndex;
    }

    // check and do move on index if possible
    function tryMove(i) {
        var moveOk = false;
        if (board[i].value < 0) {
            board[i].value = nextplayer;
            drawArea.drawSign(board[i], nextplayer);
            logToConsole('player ' + nextplayer + ' move at ' + i);
            nextplayer = (nextplayer + 1) % 2;
            moves[moves.length] = i;
            moveOk = true;
        }
        return moveOk;
    }

    // check if won or draw
    function checkWinDraw() {
        var winner = -1;
        var i = 0;
        if (board[i].value !== -10 && (
            board[i].value === board[i + 1].value && board[i].value === board[i + 2].value
            ||
            board[i].value === board[i + 3].value && board[i].value === board[i + 6].value
            ||
            board[i].value === board[i + 4].value && board[i].value === board[i + 8].value
        )
        ) {
            winner = board[i].value;
        }
        i = 4;
        if (board[i].value !== -10 && (
            board[i].value === board[i - 1].value && board[i].value === board[i + 1].value
            ||
            board[i].value === board[i - 3].value && board[i].value === board[i + 3].value
            ||
            board[i].value === board[i - 2].value && board[i].value === board[i + 2].value
        )
        ) {
            winner = board[i].value;
        }
        i = 8;
        if (board[i].value !== -10 && (
            board[i].value === board[i - 1].value && board[i].value === board[i - 2].value
            ||
            board[i].value === board[i - 3].value && board[i].value === board[i - 6].value
        )
        ) {
            winner = board[i].value;
        }
        if (winner === -1 && outOfMoves()) {
            winner = 2;
        }
        return winner;
    }

    // check if out of moves
    function outOfMoves() {
        var summe = 0;
        for (var item in board) {
            summe += board[item].value;
        }
        return summe >= 0;
    }

    // game ending message plus reload question
    function onceAgain(message) {
        logToConsole(message);
        if (confirm(message + "\n\nNochmal?")) {
            document.location.reload(true);
        }
    }
}

// draw area constructor 
function DrawArea(data) {

    var size = data.size;
    var thickness = data.thickness;
    var game = data.game;

    // html canvas element
    var canvas = (function () {
        document.getElementById(data.canvasDivName).innerHTML = "<canvas id='" + data.canvasId + "' height='" + size + "' width='" + size + "'></canvas>";
        return document.getElementById(data.canvasId);
    })();

    // set canvas onclick event
    canvas.onclick = function (event) {
        game.clickedAt({
            x: event.pageX - this.offsetLeft
            , y: event.pageY - this.offsetTop
        });
    };

    // set context
    var context = canvas.getContext("2d");

    // draw a line
    function drawLine(x1, x2, y1, y2, timesThicker) {
        context.lineWidth = thickness * timesThicker;
        context.beginPath();
        context.moveTo(x1, x2);
        context.lineTo(y1, y2);
        context.stroke();
    }

    // draw board
    this.drawBoard = function () {
        var space = (size - thickness * 2) / 3;
        var timesThicker = 1;
        drawLine(space, 0, space, size, timesThicker);
        drawLine(space * 2 + thickness, 0, space * 2 + thickness, size, timesThicker);
        drawLine(0, space, size, space, timesThicker);
        drawLine(0, space * 2 + thickness, size, space * 2 + thickness, timesThicker);
    };

    // draw sign
    this.drawSign = function (field, value) {
        if (value === 0) {
            drawX(field);
        }
        else {
            drawO(field);
        }
    };

    // draw X
    function drawX(field) {
        context.strokeStyle = "#FF0000";
        drawLine(field.xMin + 20, field.yMin + 20, field.xMax - 20, field.yMax - 20, 3);
        drawLine(field.xMin + 20, field.yMax - 20, field.xMax - 20, field.yMin + 20, 3);
        context.strokeStyle = "#000000";
    }

    // draw O
    function drawO(field) {
        context.lineWidth = thickness * 3;
        context.strokeStyle = "#0000FF";
        context.beginPath();
        context.arc((field.xMin + field.xMax) / 2, (field.yMin + field.yMax) / 2, (field.xMax - field.xMin) / 2 - 20, 0, 2 * Math.PI);
        context.stroke();
        context.strokeStyle = "#000000";
    }
}

// board creation
function makeBoard(data) {
    var thickness = data.thickness;
    var space = (data.size - 2 * thickness) / 3;
    var fields = [];
    for (let i = 0; i < 9; i++) {
        fields[i] = {
            xMin: i % 3 * (space + thickness)
            , xMax: i % 3 * (space + thickness) + space - 1
            , yMin: Math.floor(i / 3) * (space + thickness)
            , yMax: Math.floor(i / 3) * (space + thickness) + space - 1
            , value: -10
        };
    }
    return fields;
}

function logToConsole(message, origin) {
    console.log((origin === undefined ? 'ticTacToe' : origin) + ": " + message);
}