﻿window.onload = function ()
{
    // Bei Auswahl einer Datei wird das Change-Event ausgelöst
    document.getElementById("myFileUpload").onchange = handleUpload;
};

function handleUpload(fileEvent)
{
    // Wird die File-API vom Browser unterstützt?
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
        // FileReader Objekt anlegen
        var reader = new FileReader();

        // Alle gewählten Dateinamen im Array speichern
        var files = fileEvent.target.files;

        // Erste Datei lesen
        reader.readAsText(files[0]);

        // Event-Handler wird aufgerufen sobald die Datei gelesen wurde
        reader.onload = function (readerEvent)
        {
            // Aus dem Event-Objekt den Inhalt lesen
            var dateiInhalt = readerEvent.target.result;

            // Ausgabe auf der Konsole
            console.log(dateiInhalt);

            document.getElementById("fileOutput").innerHTML = "Inhalt der Datei: " + dateiInhalt;
        };
    }
}