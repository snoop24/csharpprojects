﻿/// <reference path="jquery-3.2.1.js" />

$(document).ready(function () {
    $('#btnText').click(buttonAdd);
    //$('#btnText').on('click', "", buttonAdd);
    $('input[name=select]').click(switchSelectType);
    $('#btnRemove').click(buttonRemove);
});


function switchSelectType() {

    var inpSelect = $('#inpSelect')

    if ($('#radioSingle').is(':checked')) {
        inpSelect.removeAttr('multiple');
    }
    else {
        inpSelect.attr('multiple', 'multiple');
    }
}


function buttonAdd() {
    var text = $('#inpText').val()
    if (text !== "") {
        var inpSelect = $('select#inpSelect');
        if ($('#radioAppend').is(':checked')) {
            inpSelect.append(new Option(text, text, false, true));
        }
        else {
            inpSelect.prepend(new Option(text, text, false, true));
        }
    }
}

function buttonRemove() {
    $('#inpSelect').find(':selected').remove();
}