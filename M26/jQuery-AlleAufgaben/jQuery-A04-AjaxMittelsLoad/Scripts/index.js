﻿/// <reference path="jquery-3.2.1.js" />

$(document).ready(function () {
    "use strict";

    $('#divAuswahl').hide();
    
    $('#txtAuswahl').on("input", getFilms);
});

function getFilms() {
    "use strict";
    
    var input = $('#txtAuswahl').val();
    if (input.replace(' ','').length > 2) {
        
        $('#divAuswahl').load('/Home/GetFilmTitlesHtml', { snippet: input }, function (data) {
            $('#divAuswahl').show();
            //alert(data);
        });
    }
    else {
        $('#divAuswahl').html('');
    }
}