﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace jQuery_A04_AjaxMittelsLoad.Models
{
    public class FilmRepository
    {
        private static string lastErrorMessage;
        public static string LastErrorMessage { get { return lastErrorMessage; } }

        static SqlConnection connection;
        static string connString = @"Data Source=.\SQLEXPRESS; Initial Catalog=TeachSQL;Integrated Security=True;";
        static SqlDataReader sqlReader;
        static SqlCommand sqlCom;


        public static List<Film> GetFilms()
        {
            List<Film> films = new List<Film>();

            using (connection = new SqlConnection(connString))
            {
                try
                {
                    connection.Open();

                }
                catch (Exception e)
                {
                    lastErrorMessage = e.Message;
                }
                sqlCom = connection.CreateCommand();

                sqlCom.CommandText = $"SELECT film.id,titel,jahr,person.name as regie FROM MOV_FILM as film INNER JOIN MOV_PERSON as person ON film.regie = person.id ORDER BY punkte DESC, titel; ";

                using (sqlReader = sqlCom.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        films.Add(new Film(int.Parse(sqlReader[0].ToString()), sqlReader[1].ToString(), int.Parse(sqlReader[2].ToString()), sqlReader[3].ToString()));
                    }
                }


                sqlCom.CommandText = "SELECT film.id as filmId, person.name as name FROM MOV_FILM as film INNER JOIN MOV_BESETZUNG as besetzung ON film.id = besetzung.filmid INNER JOIN MOV_PERSON as person ON besetzung.persid = person.id;";

                using (sqlReader = sqlCom.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        films.First(x => x.ID == int.Parse(sqlReader[0].ToString())).ActorNames.Add(sqlReader[1].ToString());
                    }
                }


            }

            return films;
        }

        public static Dictionary<int, string> GetFilmsIdTitle(string snippet) {

            Dictionary<int,string> films = new Dictionary<int, string>();

            using (connection = new SqlConnection(connString))
            {
                try
                {
                    connection.Open();

                }
                catch (Exception e)
                {
                    lastErrorMessage = e.Message;
                }
                sqlCom = connection.CreateCommand();

                sqlCom.CommandText = 
                    $"SELECT id, titel " +
                    $"FROM MOV_FILM " +
                    $"WHERE LOWER(titel) LIKE LOWER(@muster)" +
                    $"ORDER BY  titel; ";
                sqlCom.Parameters.AddWithValue("muster", "%" + snippet.Replace(' ', '%') + "%");

                using (sqlReader = sqlCom.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        films.Add(sqlReader.GetInt32(0),sqlReader.GetString(1));
                    }
                }


            }

            return films;
        }
    }
}