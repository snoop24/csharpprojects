﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jQuery_A04_AjaxMittelsLoad.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetFilmTitles(string snippet)
        {
            return Json(Models.FilmRepository.GetFilmsIdTitle(snippet),JsonRequestBehavior.AllowGet);
        }

        public string GetFilmTitlesHtml(string snippet)
        {
            return string.Join("", Models.FilmRepository.GetFilmsIdTitle(snippet).Select(x => "<p id=" + x.Key + ">" + x.Value + "</p>\n").ToArray());
        }
    }
}