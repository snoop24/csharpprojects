﻿/// <reference path="jquery-3.2.1.js" />

$(document).ready(function () {
    "use strict";
    $('#btnText').click(function () {

        //$.get("/Home/Data"
        //    , { data: $('#txtText').val() }
        //    , 'jquery')
        //    .done(function (data) {
        //        displayResponse(data);
        //    }
        //    );

        $.post("/Home/Data"
            , { data: $('#txtText').val() }
            , function (data) {
                displayResponse(data);
            }
            , 'json'
        );
    });

});

function displayResponse(data) {
    "use strict";
    $('#outputParagraph').html(data.html);

}