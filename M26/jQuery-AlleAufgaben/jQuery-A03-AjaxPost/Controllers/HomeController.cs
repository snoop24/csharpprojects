﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jQuery_A03_AjaxPost.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // Post: Home
        public ActionResult Data(string data) 
        {
            string message = $"Der Text hat {(data.Length==0? 0: data.Split(' ').Length)} Wort/Wörter und {data.Length} Zeichen.";
            return Json(new { html = message },JsonRequestBehavior.AllowGet);
        }
    }
}