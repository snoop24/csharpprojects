﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jQuery_A02_SehrEinfacheAnimationen.Models
{
    public class Film
    {
        public int ID { get; }
        public string Title { get; }
        public int Year { get; }

        public string Regisseur { get; }

        public List<string> ActorNames { get; }

        public Film(int id, string title, int year, string regisseur)
        {
            ID = id;
            Title = title;
            Year = year;
            Regisseur = regisseur;
            ActorNames = new List<string>();
        }
    }
}