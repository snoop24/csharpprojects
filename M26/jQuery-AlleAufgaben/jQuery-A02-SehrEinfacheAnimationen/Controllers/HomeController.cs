﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jQuery_A02_SehrEinfacheAnimationen.Models
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Models.Film> filme = Models.FilmRepository.GetFilms();
            return View(filme);
        }
    }
}