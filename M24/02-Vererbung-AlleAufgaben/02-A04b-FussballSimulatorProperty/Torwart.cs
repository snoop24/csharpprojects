﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A04b_FussballSimulatorProperty
{
    class Torwart : Spieler
    {
        // attributes
        private int reaktion;

        #region:Properties
        // get only
        public int Reaktion
        {
            get { return reaktion; }
        }
        #endregion

        // constructor
        public Torwart(string name, int alter, int stärke, int torschuss, int motivation, int reaktion)
            : base(name, alter, stärke, torschuss, motivation)
        {
            this.reaktion = reaktion;
        }
        
        #region:OtherMethods

        public bool HältDenSchuss(int schussQualität)
        {
            int halteQualität = Math.Max(1, Math.Min(10, reaktion + random.Next(3) - 1));
            if (halteQualität >= schussQualität)
            {
                return true; // gehalten
            }
            else
            {
                return false; // TOR!!!
            }
        }

        #endregion
    }
}
