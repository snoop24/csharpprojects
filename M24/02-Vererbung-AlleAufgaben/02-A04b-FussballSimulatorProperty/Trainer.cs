﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A04b_FussballSimulatorProperty
{
    class Trainer
    {
        // attributes
        string name;
        int alter;
        int erfahrung;

        #region:Properties
        // get only
        public string Name
        {
            get { return name; }
        }
        public int Alter
        {
            get { return alter; }
        }
        public int Erfahrung
        {
            get { return erfahrung; }
        }
        #endregion

        // constructor
        public Trainer(string name, int alter, int erfahrung)
        {
            this.name = name;
            this.alter = alter;
            this.erfahrung = erfahrung;
        }
    }
}
