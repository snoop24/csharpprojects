﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A04b_FussballSimulatorProperty
{
    class Spieler
    {
        // class attributes
        protected static Random random = new Random();

        // attributes
        private string name;
        private int alter;
        private int stärke;
        private int torschuss;
        private int motivation;
        private int tore;

        #region:Properties
        // get only
        public string Name
        {
            get { return name; }
        }
        public int Alter
        {
            get { return alter; }
        }
        public int Stärke
        {
            get { return stärke; }
        }
        public int Torschuss
        {
            get { return torschuss; }
        }
        public int Motivation
        {
            get { return motivation; }
        }
        public int Tore
        {
            get { return tore; }
        }
        #endregion

        // constructor
        public Spieler(string name, int alter, int stärke, int torschuss, int motivation)
        {
            this.name = name;
            this.alter = alter;
            this.stärke = stärke;
            this.torschuss = torschuss;
            this.motivation = motivation;
            tore = 0;
        }

        #region:OtherMethods
        public void AddTor()
        {
            tore++;
        }

        public int SchiesstAufTor()
        {
            torschuss = Math.Max(1, Math.Min(10, torschuss - random.Next(3)));
            int schussQualität = Math.Max(1, Math.Min(10, torschuss + random.Next(3) - 1));
            return schussQualität;
        }
        #endregion

    }
}
