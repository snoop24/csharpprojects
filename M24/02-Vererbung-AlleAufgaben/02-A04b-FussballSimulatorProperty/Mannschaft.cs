﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A04b_FussballSimulatorProperty
{
    class Mannschaft
    {
        // attributes
        private string name;
        private Trainer trainer;
        private Torwart torwart;
        private Spieler[] spieler;

        #region:Properties
        // get only
        public string Name
        {
            get { return name; }
        }
        public Trainer Trainer
        {
            get { return trainer; }
        }
        public Torwart Torwart
        {
            get { return torwart; }
        }
        public Spieler[] Spieler
        {
            get { return spieler; }
        }
        #endregion

        // constructor
        public Mannschaft(string name, Trainer trainer, Torwart torwart, Spieler[] spieler)
        {
            this.name = name;
            this.trainer = trainer;
            this.torwart = torwart;
            this.spieler = spieler;
        }

        #region:OtherMethods

        public int GetStärke()
        {
            int stärke = 0;
            for (int i = 0; i < spieler.Length; i++)
            {
                stärke += spieler[i].Stärke;
            }
            stärke += torwart.Stärke;
            stärke /= (spieler.Length+1);
            return stärke;
        }

        public int GetMotivation()
        {
            int motivation = 0;
            for (int i = 0; i < spieler.Length; i++)
            {
                motivation += spieler[i].Motivation;
            }
            motivation += torwart.Motivation;
            motivation /= (spieler.Length+1);
            return motivation;
        }

        public Spieler[] GetKader()
        {
            return spieler;
        }

        #endregion
    }
}
