﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A02_Gehaltsberechnung
{
    class CAngestellter : CMitarbeiter
    {
        private char tarifGruppe;

        public CAngestellter(string name, string vorname, int alter, char tarifGruppe)
            : base(name, vorname, alter)
        {
            this.tarifGruppe = tarifGruppe;
        }

        public override decimal Gehalt()
        {
            decimal gehalt;
            switch (tarifGruppe)
            {
                case 'A':
                    gehalt =2560;
                    break;
                case 'B':
                    gehalt =3000;
                    break;
                case 'C':
                    gehalt =3200;
                    break;
                case 'D':
                    gehalt =5400;
                    break;
                default:
                    gehalt = 0;
                    break;
            }
            gehalt = gehalt * (1 + (alter - 25m) / 100);
            return gehalt;
        }

        public new void Ausgabe()
        {
            base.Ausgabe();
            Console.Write(" : Tarifgruppe {0} : {1,7:F2} Geld\n",tarifGruppe, Gehalt());
        }
    }
}
