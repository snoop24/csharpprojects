﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A02_Gehaltsberechnung
{
    class CPraktikant:CMitarbeiter
    {
        private int abteilung;
        private enum Abteilung { Entwicklung, Produktion, Vertrieb }

        public CPraktikant(string name, string vorname, int alter, int abteilung)
            : base(name, vorname, alter)
        {
            this.abteilung = abteilung;
        }

        public override decimal Gehalt()
        {
            decimal gehalt;
            switch (abteilung)
            {
                case 1:
                    gehalt = 935;
                    break;
                case 2:
                    gehalt = 710;
                    break;
                case 3:
                    gehalt = 820;
                    break;
                default:
                    gehalt = 0;
                    break;
            }
            return gehalt;
        }

        public new void Ausgabe()
        {
            base.Ausgabe();
            Console.Write(" : Abteilung {0} :{1,7:F2} Geld\n", (Abteilung) abteilung,Gehalt());
        }
    }
}
