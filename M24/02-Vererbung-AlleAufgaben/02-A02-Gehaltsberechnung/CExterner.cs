﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A02_Gehaltsberechnung
{
    class CExterner: CMitarbeiter
    {
        private double projektStunden;

        public CExterner(string name, string vorname, int alter, double projektStunden)
            : base(name, vorname, alter)
        {
            this.projektStunden = projektStunden;
        }

        public override decimal Gehalt()
        {
            decimal gehalt;
            gehalt = (decimal)projektStunden * 75;
            return gehalt;
        }

        public new void Ausgabe()
        {
            base.Ausgabe();
            Console.Write(" : Projekstunden {0} : {1,7:F2} Geld\n",projektStunden, Gehalt());
                }
    }
}
