﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A02_Gehaltsberechnung
{
    class Program
    {
        static void Main(string[] args)
        {

            CAngestellter a1 = new CAngestellter("Buschkowski", "Horst", 56, 'A');
            a1.Ausgabe();

            CPraktikant p1 = new CPraktikant("Schmidt", "Kevin Justin Finn", 16, 2);
            p1.Ausgabe();

            CExterner e1 = new CExterner("Kutrapali", " Radsch", 25, 666);
            e1.Ausgabe();



            // general loop
            do
            {
                // loop prompt
                Console.WriteLine("\n\n ... zum Beenden ESC!");
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}
