﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A02_Gehaltsberechnung
{
    abstract class CMitarbeiter
    {
        private string name;
        private string vorname;
        protected int alter;

        public CMitarbeiter(string name, string vorname, int alter)
        {
            this.name = name;
            this.vorname = vorname;
            this.alter = alter;
        }

        public abstract decimal Gehalt();

        public void Ausgabe()
        {
            Console.Write("{0}, {1}: {2} Jahre", name, vorname, alter);
        }
    }
}
