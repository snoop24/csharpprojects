﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A06_Geometrie
{
    class Kreis:GeometrieObjekt
    {
        double r;

        public Kreis(double r)
        {
            this.r = r;
            BerechneFlaeche();
            BerechneUmfang();
        }

        public override double BerechneUmfang()
        {
            return Math.PI * r * 2;
        }

        public override double BerechneFlaeche()
        {
            return Math.PI*r*r;
        }

        public override void Ausgabe()
        {
            Console.WriteLine("\n{0} {3}\nUmfang {1}\nFläche {2}", GetType().Name, BerechneUmfang(), BerechneFlaeche(), r);
        }
    }
}
