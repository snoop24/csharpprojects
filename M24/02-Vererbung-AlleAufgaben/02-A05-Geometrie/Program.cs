﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A06_Geometrie
{
    class Program
    {
        static void Main(string[] args)
        {
            Kreis einer = new Kreis(1);
            Rechteck zweiDrei = new Rechteck(2,3);
            Quadrat vierer = new Quadrat(4);

            einer.Ausgabe();
            zweiDrei.Ausgabe();
            vierer.Ausgabe();

            // general loop
            do
            {
                // loop prompt
                Console.WriteLine("\n\n ... zum Beenden ESC!");
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}
