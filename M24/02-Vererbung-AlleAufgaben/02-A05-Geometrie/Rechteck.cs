﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A06_Geometrie
{
    class Rechteck : GeometrieObjekt
    {
        double a;
        double b;

        public Rechteck(double a, double b)
        {
            this.a = a;
            this.b = b;
        }

        public override double BerechneUmfang()
        {
            return  (a + b) * 2;
        }

        public override double BerechneFlaeche()
        {
            return  a * b;
        }

        public override void Ausgabe()
        {
            Console.WriteLine("\n{0} {3}x{4}\nUmfang {1}\nFläche {2}", GetType().Name, BerechneUmfang(), BerechneFlaeche(),a,b);
        }
    }
}
