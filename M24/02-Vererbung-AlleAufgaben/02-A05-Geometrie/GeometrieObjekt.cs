﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A06_Geometrie
{
    abstract class GeometrieObjekt
    {
        public abstract void Ausgabe();
        
        public abstract double BerechneUmfang();
        public abstract double BerechneFlaeche();
    }
}
