﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A01_Simpsons
{
    class CSimpson
    {
        private string vorname;
        private string nachname;
        private string hautfarbe;
        private string adresse;

        protected int zaehler;

        public CSimpson(string vorname)
        {
            this.vorname = vorname;
            nachname = "Simpson";
            hautfarbe = "gelb";
            adresse = "742 Evergreen Terrace, Springfield";
            zaehler = 0;
        }

        public void SichVorstellen()
        {
            Console.WriteLine("Mein Name ist {0} {1}.\nIch wohne {2}.\nMeine Haut ist {3}.",
                vorname,
                nachname,
                adresse,
                hautfarbe);
        }

        public int GetZaehler()
        {
            return zaehler;
        }
    }
}
