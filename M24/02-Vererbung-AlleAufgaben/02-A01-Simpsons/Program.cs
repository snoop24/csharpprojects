﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A01_Simpsons
{
    class Program
    {
        static void Main(string[] args)
        {
            CHomer homer = new CHomer();
            CMarge marge = new CMarge();
            CBart bart = new CBart();
            CLisa lisa = new CLisa();
            CMaggie maggie = new CMaggie();

            homer.SichVorstellen();
            Console.WriteLine(homer.GetZaehler());
            homer.DonutVerputzen();
            Console.WriteLine(homer.GetZaehler());

            bart.SichVorstellen();
            Console.WriteLine(bart.GetZaehler());
            bart.SkateboardFahren();
            Console.WriteLine(bart.GetZaehler());



            // general loop
            do
            {
                // loop prompt
                Console.WriteLine("\n\n ... zum Beenden ESC!");
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}
