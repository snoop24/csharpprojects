﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A03_Konten
{
    class Program
    {
        static void Main(string[] args)
        {
            CGiro giro1 = new CGiro("Giromaster2000", 1000, KategorieG.Standard);
            Console.WriteLine("Giro A500 A1000 E1000");
            Console.WriteLine(  giro1.Auszahlen(500));
            Console.WriteLine(   giro1.Stand());
            Console.WriteLine(giro1.Auszahlen(1000));
            Console.WriteLine(   giro1.Stand());
            Console.WriteLine(giro1.Einzahlen(1000));
            Console.WriteLine(   giro1.Stand());
            Console.WriteLine();

            CSpar spar1 = new CSpar("Sparmaster2K");
            Console.WriteLine("Spar A500 E1000 E1000");
            Console.WriteLine(spar1.Auszahlen(500));
            Console.WriteLine(spar1.Stand());
            Console.WriteLine(spar1.Einzahlen(1000));
            Console.WriteLine(spar1.Stand());
            Console.WriteLine(spar1.Einzahlen(1000));
            Console.WriteLine(spar1.Stand());
            Console.WriteLine();

            //CFest fest1 = new CFest("Festmaster 1", new DateTime(2015, 10, 24));
            CFest fest1 = new CFest("Festmaster2Kv2", new DateTime(2019, 12, 24));
            //CFest fest1 = new CFest("Festmaster 1",new DateTime(2018,10,24));
            Console.WriteLine("Fest A500 E1000 E1000 A500");
            Console.WriteLine(fest1.Auszahlen(500));
            Console.WriteLine(fest1.Stand());
            Console.WriteLine(fest1.Einzahlen(1000));
            Console.WriteLine(fest1.Stand());
            Console.WriteLine(fest1.Einzahlen(1000));
            Console.WriteLine(fest1.Stand());
            Console.WriteLine(fest1.Auszahlen(500));
            Console.WriteLine(fest1.Stand());
            Console.WriteLine(fest1.GetFestBis().ToShortDateString());
            Console.WriteLine(fest1.RestLaufZeit());
            Console.WriteLine();


            // general loop
            do
            {
                // loop prompt
                Console.WriteLine("\n\n ... zum Beenden ESC!");
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}
