﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A03_Konten
{
    class CFest : CKonto
    {
        DateTime festBis;
        public CFest(string inhaber, DateTime festBis)
            : base(inhaber, 0.02)
        {
            this.festBis = festBis;
        }


        public DateTime GetFestBis()
        {
            return festBis;
        }

        public new bool Auszahlen(decimal betrag)
        {
            bool auszahlen = false;
            if (( DateTime.Now-festBis).Days >= 0 && 0 <= betrag && betrag <= stand)
            {
                stand -= betrag;
                auszahlen = true;
            }
            return auszahlen;
        }

        public string RestLaufZeit()
        {
            string restLaufZeit;
            int tage=0, monate=0, jahre=0;
            DateTime jetzt = DateTime.Today;

            while (festBis >= jetzt.AddYears(1))
            {
                jetzt=jetzt.AddYears(1);
                jahre++;
            }

            while (festBis >= jetzt.AddMonths(1))
            {
                jetzt = jetzt.AddMonths(1);
                monate++;
            }

            while (festBis >= jetzt.AddDays(1))
            {
                jetzt = jetzt.AddDays(1);
                tage++;
            }


            restLaufZeit = tage+monate+jahre == 0 ? "ausgelaufen" : string.Format("{0} Jahr{1} {2} Monat{3} {4} Tag{5}", jahre,jahre==1?"":"e", monate,monate == 1 ? "" : "e", tage,tage == 1 ? "" : "e"); 

            return restLaufZeit;
        }
    }
}
