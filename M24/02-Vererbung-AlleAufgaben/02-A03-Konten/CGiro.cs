﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A03_Konten
{
    class CGiro : CKonto
    {
        decimal dispo;
        double sollZinssatz;
        KategorieG kategorieG;

        public CGiro(string inhaber, decimal dispo, KategorieG kategorieG) 
            : base(inhaber, 0)
        {
            this.dispo = dispo;
            sollZinssatz = 0.16;
            this.kategorieG = kategorieG;
        }

        public new bool Auszahlen(decimal betrag)
        {
            bool auszahlen = false;
            if (0 <= betrag && betrag <= stand+dispo)
            {
                stand -= betrag;
                auszahlen = true;
            }
            return auszahlen;
        }

        public new decimal Zinsen()
        {
            decimal zinsen;
            zinsen = stand > 0 ? (stand * (decimal)habensZinssatz) : stand < 0 ? (-stand * (decimal)sollZinssatz) : 0;
            return zinsen;
        }
    }
}
