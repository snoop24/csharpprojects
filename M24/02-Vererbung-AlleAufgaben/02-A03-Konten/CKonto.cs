﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A03_Konten
{
    enum KategorieG { Standard, Schueler, Student, Gewerblich }

    class CKonto
    {
        // class
        static int konten=0;

        // instance
        string inhaber;
        int nummer;
        protected decimal stand;
        DateTime erstellt;
        protected double habensZinssatz;

        public CKonto(string inhaber, double habensZinssatz)
        {
            nummer = ++konten;
            this.inhaber = inhaber;
            stand = 0;
            erstellt = DateTime.Now;
            this.habensZinssatz = habensZinssatz;
        }
        
        public bool Einzahlen(decimal betrag)
        {
            bool einzahlen=false;
            if (betrag >= 0)
            {
                stand += betrag;
                einzahlen = true;
            }
            return einzahlen;
        }

        public bool Auszahlen(decimal betrag)
        {
            bool auszahlen = false;
            if ( 0  <= betrag && betrag <= stand)
            {
                stand -= betrag;
                auszahlen = true;
            }
            return auszahlen;
        }

        public decimal Zinsen()
        {
            decimal zinsen;
            zinsen = stand > 0 ? (stand * (decimal)habensZinssatz) : 0;
            return zinsen;
        }

        public decimal Stand()
        {
            return stand;
        }

    }
}
