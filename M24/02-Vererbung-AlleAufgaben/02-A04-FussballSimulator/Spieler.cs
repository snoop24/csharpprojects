﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A04_FussballSimulator
{
    class Spieler
    {
        // class attributes
        protected static Random random = new Random();

        // attributes
        string name;
        int alter;
        int stärke;
        int torschuss;
        int motivation;
        int tore;

        // constructor
        public Spieler(string name, int alter, int stärke, int torschuss, int motivation)
        {
            this.name = name;
            this.alter = alter;
            this.stärke = stärke;
            this.torschuss = torschuss;
            this.motivation = motivation;
            tore = 0;
        }

        #region:OtherMethods

        public void AddTor()
        {
            tore++;
        }

        public int SchiesstAufTor()
        {
            torschuss = Math.Max(1, Math.Min(10, torschuss - random.Next(3)));
            int schussQualität = Math.Max(1, Math.Min(10, torschuss + random.Next(3) - 1));
            return schussQualität;
        }

        #endregion

        #region:Getter

        public string GetName()
        {
            return name;
        }

        public int GetAlter()
        {
            return alter;
        }

        public int GetStärke()
        {
            return stärke;
        }

        public int GetTorschuss()
        {
            return torschuss;
        }

        public int GetMotivation()
        {
            return motivation;
        }

        public int GetTore()
        {
            return tore;
        }

        #endregion
    }
}
