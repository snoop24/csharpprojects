﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A04_FussballSimulator
{
    class Mannschaft
    {
        // attributes
        string name;
        Trainer trainer;
        Torwart torwart;
        Spieler[] spieler;

        // constructor
        public Mannschaft(string name, Trainer trainer, Torwart torwart, Spieler[] spieler)
        {
            this.name = name;
            this.trainer = trainer;
            this.torwart = torwart;
            this.spieler = spieler;
        }

        #region:OtherMethods

        public int GetStärke()
        {
            int stärke = 0;
            for (int i = 0; i < spieler.Length; i++)
            {
                stärke += spieler[i].GetStärke();
            }
            stärke += torwart.GetStärke();
            stärke /= (spieler.Length+1);
            return stärke;
        }

        public int GetMotivation()
        {
            int motivation = 0;
            for (int i = 0; i < spieler.Length; i++)
            {
                motivation += spieler[i].GetMotivation();
            }
            motivation += torwart.GetMotivation();
            motivation /= (spieler.Length+1);
            return motivation;
        }

        public Spieler[] GetKader()
        {
            return spieler;
        }

        #endregion

        #region:Getter

        public string GetName()
        {
            return name;
        }

        public Trainer GetTrainer()
        {
            return trainer;
        }

        public Torwart GetTorwart()
        {
            return torwart;
        }

        public Spieler[] GetSpieler()
        {
            return spieler;
        }

        #endregion
    }
}
