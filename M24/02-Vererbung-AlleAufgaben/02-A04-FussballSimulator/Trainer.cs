﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A04_FussballSimulator
{
    class Trainer
    {
        // attributes
        string name;
        int alter;
        int erfahrung;

        // constructor
        public Trainer(string name, int alter, int erfahrung)
        {
            this.name = name;
            this.alter = alter;
            this.erfahrung = erfahrung;
        }

        #region:Getter

        public string GetName()
        {
            return name;
        }

        public int GetAlter()
        {
            return alter;
        }

        public int GetErfahrung()
        {
            return erfahrung;
        }

        #endregion
    }
}
