﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A05_Tiere
{
    class Kuh:Tier
    {
        public Kuh(string name)
            :base(name)
        { }

        public override void GibLaut()
        { Console.WriteLine("Muuh"); }

        public override void AusgabeArt()
        { Console.WriteLine("Kuh"); }
    }
}
