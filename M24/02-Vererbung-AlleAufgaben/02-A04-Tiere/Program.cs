﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A05_Tiere
{
    class Program
    {
        static void Main(string[] args)
        {
            Hund bello = new Hund("bello");
            Katze mieze = new Katze("mieze");
            Kuh ulrike = new Kuh("Ulrike");

            bello.AusgabeArt();
            bello.GibLaut();
            Console.WriteLine();

            mieze.AusgabeArt();
            mieze.GibLaut();
            Console.WriteLine();

            ulrike.AusgabeArt();
            ulrike.GibLaut();
            Console.WriteLine();

            // general loop
            do
            {
                // loop prompt
                Console.WriteLine("\n\n ... zum Beenden ESC!");
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}
