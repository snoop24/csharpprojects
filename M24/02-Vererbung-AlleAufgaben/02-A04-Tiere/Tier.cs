﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A05_Tiere
{
    abstract class Tier
    {
        string name;

        public Tier(string name)
        {
            this.name = name;
        }

        #region:OtherMethods

        public string GetName()
        {
            return name;
        }

        public abstract void GibLaut();

        public abstract void AusgabeArt();

        #endregion

    }
}
