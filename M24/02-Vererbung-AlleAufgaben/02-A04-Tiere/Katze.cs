﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A05_Tiere
{
    class Katze:Tier
    {
        public Katze(string name)
            :base(name)
        { }

        public override void GibLaut()
        { Console.WriteLine("Miau"); }

        public override void AusgabeArt()
        { Console.WriteLine("Katze"); }
    }
}
