﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A05_Tiere
{
    class Hund:Tier
    {
        public Hund(string name)
            :base(name)
        { }

        public override void GibLaut()
        { Console.WriteLine("Wuff"); }

        public override void AusgabeArt()
        { Console.WriteLine("Hund"); }
    }
}
