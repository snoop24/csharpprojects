﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A05_Tiere
{
    abstract class CTier
    {
        string name;
        string laut;
        string art;

        public CTier(string name,string laut, string art)
        {
            this.name = name;
            this.laut = laut;
            this.art = art;
        }

        #region:OtherMethods

        public string GetName()
        {
            return name;
        }

        public void GibLaut()
        {
            Console.WriteLine(laut);
        }

        public void AusgabeArt()
        {
            Console.WriteLine(art);
        }

        #endregion
    }
}
