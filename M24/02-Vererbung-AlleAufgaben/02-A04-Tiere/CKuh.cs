﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_A05_Tiere
{
    class CKuh:CTier
    {
        public CKuh(string name) 
            : base(name, "Kuh", "Muuh")
        { }
    }
}
