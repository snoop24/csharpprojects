﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A04_Widerstand
{
    class Parallelschaltung : WiderstandsNetz
    {
        public Parallelschaltung(Widerstand r1, Widerstand r2, params Widerstand[] rX)
            : base(r1, r2, rX) { }

        public override double BerechneWiderstand()
        {
            double ohm = 0;
            foreach (Widerstand r in NetzWiderstaende)
            {
                ohm += 1 / r.Ohm;
            }
            return 1 / ohm;
        }
    }
}
