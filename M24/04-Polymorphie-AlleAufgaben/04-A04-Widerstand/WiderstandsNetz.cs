﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A04_Widerstand
{
    abstract class WiderstandsNetz : Widerstand
    {
        public List<Widerstand> NetzWiderstaende { get; } = new List<Widerstand>();

        public WiderstandsNetz(Widerstand r1, Widerstand r2, params Widerstand[] rX)
        {
            NetzWiderstaende.Add(r1);
            NetzWiderstaende.Add(r2);
            NetzWiderstaende.AddRange(rX);

            Name = ((this is Reihenschaltung) ? "Rr" : (this is Parallelschaltung) ? "Rp" : "R?") + string.Format(" ({0}, {1}",r1.Name,r2.Name);
            foreach (Widerstand r in rX)
            {
                Name += ", "+r.Name;
            }
            Name += ") ";
            Ohm = BerechneWiderstand();
        }

        public abstract double BerechneWiderstand();

    }
}
