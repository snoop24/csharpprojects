﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A04_Widerstand
{
    class Widerstand
    {
        public string Name { get; set; }
        public double Ohm { get; set; }

        public Widerstand() { }
        public Widerstand(string name, int ohm)
        {
            Name = name;
            Ohm = ohm;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} Ohm",Name, Ohm);
        }
    }
}
