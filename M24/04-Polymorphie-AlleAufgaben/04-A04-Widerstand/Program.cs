﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A04_Widerstand
{
    class Program
    {
        static void Main(string[] args)
        {
            Widerstand r1 = new Widerstand("R1", 100);
            Widerstand r2 = new Widerstand("R2", 200);
            Widerstand r3 = new Widerstand("R3", 300);
            Widerstand r4 = new Widerstand("R4", 400);
            Widerstand r5 = new Widerstand("R5", 500);
            Widerstand r6 = new Widerstand("R6", 600);

            Parallelschaltung para = new Parallelschaltung(
                new Reihenschaltung(
                    new Parallelschaltung(
                        r1,
                        r3)
                    ,r2),
                new Reihenschaltung(
                    r4,
                    r5),
                r6);
            Console.WriteLine(para);


            Console.ReadKey();
        }
    }
}
