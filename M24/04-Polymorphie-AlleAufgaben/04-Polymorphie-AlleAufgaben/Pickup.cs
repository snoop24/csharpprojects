﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A01_AutoAndPickup
{
    class Pickup : Auto
    {
        #region:Attribute
        private int ladung;
        private int maxLadung;
        #endregion

        #region:Constructor
        public Pickup(int maxLadung, string kennzeichen)
            : base(2, kennzeichen)
        {
            ladung = 0;
            this.maxLadung = maxLadung;
        }

        public Pickup(int maxLadung)
            :base(2)
        {
            ladung = 0;
            this.maxLadung = maxLadung;
        }
        #endregion

        #region:GetSet
        // getter
        public int GetLadung()
        { return ladung; }
        #endregion

        #region:Method
        public bool Beladen(int ladungsMenge)
        {
            bool beladen = false;
            int rest;
            string message = "das kann nicht geladen werden ...";
            if (ladung < maxLadung && 0 <= ladungsMenge)
            {
                rest = ladung+ladungsMenge > maxLadung ? ladung+ladungsMenge- maxLadung : 0;
                ladung += ladungsMenge - rest;

                beladen = true;
                message = string.Format("{0} Einheiten wurden geladen. {1}", ladungsMenge - rest, rest>0? rest + " Einheiten konnten nicht geladen werden.":"");
            }
            Console.WriteLine(message);
            return beladen;
        }

        public bool Entladen(int ladungsMenge)
        {
            bool entladen = false;
            int rest;
            string message = "das kann nicht entladen werden ...";
            if ( ladung >0 && 0 <= ladungsMenge)
            {
                rest = ladung - ladungsMenge < 0 ? ladungsMenge-ladung  : 0;
                ladung -= ladungsMenge + rest;
                entladen = true;
                message = string.Format("{0} Einheiten wurden entladen. {1}",ladungsMenge-rest, rest > 0 ? rest + " Einheiten konnten nicht entladen werden." : "");
            }
            Console.WriteLine(message);
            return entladen;
        }

        protected override void VorDemWaschen()
        {
            AntenneEinfahren();
            Entladen(ladung);
        }

        public override string ToString()
        {
            return string.Format("\n" +
                "Pickup\n" +
                "Kennzeichen: {2}\n" +
                "Fahrleistung: {0}km\n" +
                "{1}-Sitzer\n" +
                "AntennenStatus: {3}gefahren\n" +
                "Ladefläche: {4} von {5} Einheiten geladen\n",
                km, sitze, kennzeichen, antenneDraussen ? "aus" : "ein", ladung, maxLadung);
        }

        #endregion
    }
}
