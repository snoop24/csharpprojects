﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A01_AutoAndPickup
{
    class Auto
    {

        #region:Attribute
        protected int km;
        protected int sitze;

        protected string kennzeichen;

        protected bool antenneDraussen;
        #endregion

        #region:Constructor
        public Auto(int sitze, string kennzeichen)
        {
            if (sitze == 2) { this.sitze = sitze; }
            else { this.sitze = 5; }
            this.kennzeichen = kennzeichen;
        }

        public Auto(int sitze) 
            : this(sitze, "DO-OM3")
        { }

        public Auto(string kennzeichen) 
            : this(5, kennzeichen)
        { }

        public Auto() 
            : this(5)
        { }
        #endregion

        #region:GetSet
        // getter
        public int GetKm()
        { return km; }

        public int GetSitze()
        { return sitze; }

        public string GetKennzeichen()
        { return kennzeichen; }

        public bool IstAntenneDraussen()
        { return antenneDraussen; }
        #endregion

        #region:Method
        public void Fahre(int km)
        {
            this.km += km;
            Console.WriteLine("Fahre {0}km im Kreis... . Bin jetzt zu  {1:F2}% verrückt.",km,this.km*100/10000.0);
        }

        public void AntenneEinfahren()
        {
            if (antenneDraussen) { Console.WriteLine("Antenne wird jetzt eingefahren..."); }
            else { Console.WriteLine("Antenne war schon eingefahren."); }
            antenneDraussen = false;
        }

        public void AntenneAusfahren()
        {
            if (!antenneDraussen) { Console.WriteLine("Antenne wird jetzt ausgefahren..."); }
            else { Console.WriteLine("Antenne war schon ausgefahren."); }
            antenneDraussen = true;
        }

        protected virtual void VorDemWaschen()
        {
            AntenneEinfahren();
        }

        public void Waschen()
        {
            Console.WriteLine("Waschvorbereitung ...");
            VorDemWaschen();
            Console.WriteLine("Dip dip dip in the wizz wizz wizz in the water in the water ...");
        }

        public override string ToString()
        {
            return string.Format("\n" +
                "Auto\n" +
                "Kennzeichen: {2}\n" +
                "Fahrleistung: {0}km\n" +
                "{1}-Sitzer\n" +
                "AntennenStatus: {3}gefahren\n",
                km,sitze,kennzeichen,antenneDraussen? "aus": "ein");
        }

        #endregion

    }
}
