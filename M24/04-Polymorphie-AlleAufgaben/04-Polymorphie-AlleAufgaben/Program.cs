﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A01_AutoAndPickup
{
    class Program
    {
        static void Main(string[] args)
        {
            Auto mein = new Auto();
            Auto dein = new Auto(4);
            Auto sein = new Auto(2, "EN-TE123");
            Auto ihr = new Auto("MS-W666");

            Pickup unser = new Pickup(200);
            Pickup euer = new Pickup(300, "W-IE50");

            Console.WriteLine("Test mein Auto():");
            Console.WriteLine(mein.ToString());
            Console.WriteLine("Antenne raus? "+mein.IstAntenneDraussen());
            mein.AntenneEinfahren();
            Console.WriteLine("Antenne raus? " + mein.IstAntenneDraussen());
            mein.AntenneAusfahren();
            Console.WriteLine("Antenne raus? " + mein.IstAntenneDraussen());
            mein.Waschen();
            Console.WriteLine(mein.ToString());
            Console.WriteLine();

            Console.WriteLine("Test dein Auto(4):");
            Console.WriteLine(dein.ToString());
            dein.Fahre(100);
            dein.Fahre(1000);
            Console.WriteLine(dein.ToString());
            Console.WriteLine();

            Console.WriteLine("Test sein Auto(2, \"EN - TE123\"):");
            Console.WriteLine(sein.ToString());
            Console.WriteLine();


            Console.WriteLine("Test ihr Auto(\"MS - W666\"):");
            Console.WriteLine(ihr.ToString());
            Console.WriteLine();
            
            Console.WriteLine("Test unser Pickup(200):");
            Console.WriteLine(unser.ToString());
            unser.Fahre(314159);
            Console.WriteLine();

            Console.WriteLine("Test euer Pickup(300, \"W - IE50\"):");
            Console.WriteLine(euer.ToString());
            euer.Beladen(400);
            euer.Beladen(100);
            Console.WriteLine(euer.ToString());
            euer.Entladen(150);
            euer.Entladen(250);
            euer.Beladen(50);
            euer.Waschen();
            Console.WriteLine(euer.ToString());
            Console.WriteLine();





            // end prompt
            Console.WriteLine(" zum Beenden beliebige Taste drücken..." );
            Console.ReadKey();
        }
    }
}
