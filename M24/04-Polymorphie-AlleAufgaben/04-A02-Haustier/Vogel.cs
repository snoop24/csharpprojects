﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A02_Haustier
{
    class Vogel : Haustier
    {
        // attributes
        public bool SingVogel { get; }
        
        // constructor
        public Vogel(string name,  decimal jahreskostenTierarzt, bool singVogel) : base(name, false, jahreskostenTierarzt)
        {
            SingVogel = singVogel;
        }

        // method
        public override string GetBeschreibung()
        {
            return string.Format("Der {0}ogel namens {1} ist {2}steuerpflichtig und kostet im Jahr {3:F2} beim Tierarzt.", SingVogel? "Singv": "V", Name, Steuerpflichtig ? "" : "nicht ", JahreskostenTierarzt);
        }
    }
}
