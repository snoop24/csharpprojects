﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A02_Haustier
{
    class Hund:Haustier
    {
        // property
        public string Rasse { get; }

        // constructor
        public Hund(string name, decimal jahreskostenTierarzt, string rasse)
            : base(name, true, jahreskostenTierarzt)
        {
            Rasse = rasse;
        }

        public override string GetBeschreibung()
        {
            return string.Format("Der {0} namens {1} ist {2}steuerpflichtig und kostet im Jahr {3:F2} beim Tierarzt.", Rasse, Name, Steuerpflichtig ? "" : "nicht ", JahreskostenTierarzt);
        }
    }
}
