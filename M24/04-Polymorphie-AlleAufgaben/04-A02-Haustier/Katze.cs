﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A02_Haustier
{
    class Katze : Haustier
    {
        // property
        public Vogel LieblingsVogel { get; set; }

        // constructor
        public Katze(string name, decimal jahreskostenTierarzt, Vogel lieblingsVogel) 
            : base(name,false, jahreskostenTierarzt)
        {
            LieblingsVogel = lieblingsVogel;
        }

        // method
        public override string GetBeschreibung()
        {
            return string.Format("Die Katze namens {1} hat den Lieblingsvogel {0}, ist {2}steuerpflichtig und kostet im Jahr {3:F2} beim Tierarzt.", LieblingsVogel.Name, Name, Steuerpflichtig ? "" : "nicht ", JahreskostenTierarzt);
        }
    }
}
