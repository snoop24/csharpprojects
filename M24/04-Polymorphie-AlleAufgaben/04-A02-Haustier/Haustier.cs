﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A02_Haustier
{
    class Haustier
    {
        // property
        public string Name { get; }
        public bool Steuerpflichtig { get; }
        public decimal JahreskostenTierarzt { get; }

        // constructor
        public Haustier(string name, bool steuerpflichtig, decimal jahreskostenTierarzt)
        {
            Name = name;
            Steuerpflichtig = steuerpflichtig;
            JahreskostenTierarzt = jahreskostenTierarzt;
        }

        // method
        public virtual string GetBeschreibung()
        {
            return string.Format("Das Haustier namens {0} ist {1}steuerpflichtig und kostet im Jahr {2:F2} beim Tierarzt.",Name,Steuerpflichtig? "":"nicht ",JahreskostenTierarzt);
        }
    }
}