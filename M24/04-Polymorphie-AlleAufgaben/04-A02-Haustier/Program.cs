﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A02_Haustier
{
    class Program
    {
        static Haustier[] hausTiere;

        static void Main(string[] args)
        {
            AnlegenHaustiere();
            Ausgeben();
            SetNeuerLieblingsvogel(hausTiere[5], hausTiere[2]);
            Console.WriteLine();
            Ausgeben();


            // end prompt
            Console.WriteLine(" zum Beenden beliebige Taste drücken...");
            Console.ReadKey();
        }

        static void AnlegenHaustiere()
        {
            hausTiere = new Haustier[6];
            hausTiere[0]= new Hund("Waldemar", 250.24m, "Dackel");
            hausTiere[1]= new Hund("Aika", 314.15m, "Husky");
            hausTiere[2]= new Vogel("Tweety", 35.70m, true);
            hausTiere[3]= new Vogel("Pieps", 0, false);
            hausTiere[4]= new Katze("Silvester", 1697.90m, (Vogel)hausTiere[2]);
            hausTiere[5]= new Katze("Mischka", 89.99m , (Vogel)hausTiere[3]);
        }

        static void SetNeuerLieblingsvogel(Haustier katze, Haustier vogel)
        {
            if (katze is Katze && vogel is Vogel)
            {
                ((Katze)katze).LieblingsVogel = (Vogel)vogel;
            }
        }

        static void Ausgeben()
        {
            decimal summeKosten=0;
            foreach (Haustier haustier in hausTiere)
            {
                Console.WriteLine("{0}: {1}\n",haustier.GetType().Name,haustier.GetBeschreibung());
                summeKosten += haustier.JahreskostenTierarzt;
            }
            Console.WriteLine("Gesamtkosten {0}",summeKosten);
        }
    }
}
