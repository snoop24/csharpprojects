﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A03_Koenigreich
{
    abstract class Einwohner
    {
        public string Name { get; }
        public int Einkommen { get; set; }

        public Einwohner(string name,int einkommen)
        {
            this.Name = name;
            this.Einkommen = einkommen;
        }
        
        public virtual int GetZuVersteuerndesEinkommen()
        {
            return Einkommen;
        }

        public virtual int BerechneSteuern()
        {
            int steuer = GetZuVersteuerndesEinkommen()/10;
            return steuer > 0 ? steuer : 1;
        }

    }
}
