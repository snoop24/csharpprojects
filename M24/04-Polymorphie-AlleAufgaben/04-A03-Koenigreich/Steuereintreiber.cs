﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A03_Koenigreich
{
    class Steuereintreiber
    {
        public List<Einwohner> Bevoelkerung { get;  } = new List<Einwohner>();

        public int GetSteuern()
        {
            int steuern=0;
            foreach (Einwohner einwohner in Bevoelkerung)
            {
                steuern += einwohner.BerechneSteuern();
            }
                return steuern;
        }
    }
}
