﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A03_Koenigreich
{
    class Koenig : Einwohner
    {
        public Koenig(string name, int einkommen) : base(name, einkommen)
        { }

        public override int BerechneSteuern()
        {
            return 0;
        }
    }
}
