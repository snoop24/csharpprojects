﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A03_Koenigreich
{
    class Adel : Einwohner
    {
        public Adel(string name, int einkommen) : base(name, einkommen)
        { }

        public override int BerechneSteuern()
        {
            int steuer = base.BerechneSteuern();
            return steuer > 20 ? steuer : 20;
        }
    }
}
