﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A03_Koenigreich
{
    class Program
    {
        static void Main(string[] args)
        {
            Steuereintreiber Dagobert = new Steuereintreiber();

            Dagobert.Bevoelkerung.Add(new Koenig("King Louie", 100));
            Dagobert.Bevoelkerung.Add(new Adel("Earl Grey", 100));
            Dagobert.Bevoelkerung.Add(new Buerger("John Doe", 100));
            Dagobert.Bevoelkerung.Add(new Leibeigener("Martin Luther King", 100));
            //Dagobert.Bevoelkerung[0].Einkommen = 10000;

            Console.WriteLine(Dagobert.GetSteuern());
            Console.ReadKey();
        }
    }
}
