﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A03_Koenigreich
{
    class Leibeigener : Einwohner
    {
        public Leibeigener(string name, int einkommen) : base(name, einkommen)
        {        }

        public override int GetZuVersteuerndesEinkommen()
        {
            return Einkommen>12? Einkommen-12:0;
        }
    }
}
