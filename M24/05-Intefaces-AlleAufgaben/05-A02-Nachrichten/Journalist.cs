﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_Nachrichten
{
    class Journalist:INachrichtenQuelle
    {
        private string name;

        public List<INachrichtenEmpfaenger> Abonennten { get; }

        public Journalist(string name)
        {
            this.name = name;
            Abonennten = new List<INachrichtenEmpfaenger>();
        }

        public void Abmelden(INachrichtenEmpfaenger abonennt)
        {
            Abonennten.Remove(abonennt);
        }

        public void Anmelden(INachrichtenEmpfaenger abonennt)
        {
            Abonennten.Add(abonennt);
        }

        public void SendeNachricht(string nachricht)
        {
            foreach (INachrichtenEmpfaenger item in Abonennten)
            {
                item.EmpfangeNachricht(nachricht+" (Autor: "+name+")");
            }
        }
    }
}
