﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_Nachrichten
{
    class Program
    {
        static void Main(string[] args)
        {
            Abonennt a0 = new Abonennt("Hans");
            Abonennt a1 = new Abonennt("Franz");
            Abonennt a2 = new Abonennt("Joe");
            Abonennt a3 = new Abonennt("Jim");
            Abonennt a4 = new Abonennt("Dave");
            Abonennt a5 = new Abonennt("Kim");
            Abonennt a6 = new Abonennt("Jane");
            Abonennt a7 = new Abonennt("Ruth");
            Abonennt a8 = new Abonennt("Ann");
            Abonennt a9 = new Abonennt("Liz");

            Zeitung z0 = new Zeitung("WAZ");
            Zeitung z1 = new Zeitung("FAZ");
            Zeitung z2 = new Zeitung("ZEIT");
            Zeitung z3 = new Zeitung("WELT");
            Zeitung z4 = new Zeitung("BILD");
            Zeitung z5 = new Zeitung("TAZ");

            Journalist j0 = new Journalist("as");
            Journalist j1 = new Journalist("hm");
            Journalist j2 = new Journalist("st");
            Journalist j3 = new Journalist("er");
            Journalist j4 = new Journalist("de");
            Journalist j5 = new Journalist("tm");

            List<Journalist> schreiberlinge = new List<Journalist>() { j0, j1, j2, j3, j4, j5 };

            z0.Anmelden(a0);
            z0.Anmelden(a1);
            z0.Anmelden(a2);
            z1.Anmelden(a2);
            z1.Anmelden(a3);
            z1.Anmelden(a4);
            z2.Anmelden(a4);
            z2.Anmelden(a5);
            z2.Anmelden(a6);
            z3.Anmelden(a6);
            z3.Anmelden(a7);
            z3.Anmelden(a8);
            z4.Anmelden(a8);
            z4.Anmelden(a9);
            z4.Anmelden(a1);
            z5.Anmelden(a2);
            z5.Anmelden(a3);
            z5.Anmelden(a4);

            j0.Anmelden(z0);
            j1.Anmelden(z0);
            j2.Anmelden(z0);
            j3.Anmelden(z1);
            j4.Anmelden(z1);
            j5.Anmelden(z1);
            j0.Anmelden(z2);
            j1.Anmelden(z2);
            j2.Anmelden(z2);
            j3.Anmelden(z3);
            j4.Anmelden(z3);
            j5.Anmelden(z3);
            j0.Anmelden(z4);
            j1.Anmelden(z4);
            j2.Anmelden(z4);
            j3.Anmelden(z5);
            j4.Anmelden(z5);
            j5.Anmelden(z5);

            j0.SendeNachricht("test0");
            j1.SendeNachricht("test1");


            Console.ReadKey();
        }
    }
}
