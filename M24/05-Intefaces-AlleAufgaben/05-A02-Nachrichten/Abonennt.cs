﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_Nachrichten
{
    class Abonennt:INachrichtenEmpfaenger
    {
        private string name;

        public Abonennt(string name)
        {
            this.name = name;
        }

        public void EmpfangeNachricht(string nachricht)
        {
            Console.WriteLine("{0} {1} empfängt die Nachricht \"{2}\".",
                GetType().Name,name,nachricht);
        }
    }
}
