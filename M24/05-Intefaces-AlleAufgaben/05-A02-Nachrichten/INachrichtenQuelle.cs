﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_Nachrichten
{
    interface INachrichtenQuelle
    {
        List<INachrichtenEmpfaenger> Abonennten {get; }

        void SendeNachricht(string nachricht);

        void Anmelden(INachrichtenEmpfaenger abonennt);
        void Abmelden(INachrichtenEmpfaenger abonennt);
    }
}
