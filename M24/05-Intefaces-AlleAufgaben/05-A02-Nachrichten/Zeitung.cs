﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_Nachrichten
{
    class Zeitung : INachrichtenEmpfaenger, INachrichtenQuelle
    {
        private string name;

        public List<INachrichtenEmpfaenger> Abonennten { get; }

        public Zeitung(string name)
        {
            this.name = name;
            Abonennten = new List<INachrichtenEmpfaenger>();
        }

        public void Abmelden(INachrichtenEmpfaenger abonennt)
        {
            Abonennten.Remove(abonennt);
        }

        public void Anmelden(INachrichtenEmpfaenger abonennt)
        {
            Abonennten.Add(abonennt);
        }

        public void EmpfangeNachricht(string nachricht)
        {
            Console.WriteLine("{0} {1} empfängt die Nachricht \"{2}\".",
                GetType().Name, name, nachricht);

            SendeNachricht(nachricht);
        }

        public void SendeNachricht(string nachricht)
        {
            Console.WriteLine();
            foreach (INachrichtenEmpfaenger item in Abonennten)
            {
                item.EmpfangeNachricht(nachricht + " ((Zeitung: " + name + "))");
            }
            Console.WriteLine();
        }
    }
}
