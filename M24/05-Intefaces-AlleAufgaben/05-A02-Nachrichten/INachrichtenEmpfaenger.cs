﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A02_Nachrichten
{
    interface INachrichtenEmpfaenger
    {
        void EmpfangeNachricht(string nachricht);
    }
}
