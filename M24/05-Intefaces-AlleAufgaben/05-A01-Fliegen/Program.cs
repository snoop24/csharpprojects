﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A01_Fliegen
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IFliegen> getier = new List<IFliegen>
            {
                new Biene(),
                new Fledermaus(),
                new Singvogel()
            };

            foreach (IFliegen item in getier)
            {
                item.Fliegen();
                if (item is ISingen) { ((ISingen)item).Singen(); }
            }
            
            Console.ReadKey();
        }
    }
}
