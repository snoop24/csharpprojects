﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A01_Fliegen
{
    class Singvogel : IFliegen, ISingen
    {
        public void Fliegen()
        {
            Console.WriteLine("Flap, flap. Alle Vögel sind schon ... *platsch*!");
        }

        public void Singen()
        {
            Console.WriteLine("Zwitscher, zwitscher. Zuviel gezwitschert. Jetsch habsch eim innen Tee drinne.");
        }
    }
}
