﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A03_Temperatur
{
    class Program
    {
        static void Main(string[] args)
        {
            Temperatur tempTest = new Temperatur();

            tempTest.Celsius = 10;
            tempTest.Ausgabe();
            ((IKelvin)tempTest).Ausgabe();
            ((IFahrenheit)tempTest).Ausgabe();
            Console.WriteLine();

            tempTest.Fahrenheit = 68;
            tempTest.Ausgabe();
            ((IKelvin)tempTest).Ausgabe();
            ((IFahrenheit)tempTest).Ausgabe();
            Console.WriteLine();

            tempTest.Kelvin = 278.15;
            tempTest.Ausgabe();
            ((IKelvin)tempTest).Ausgabe();
            ((IFahrenheit)tempTest).Ausgabe();
            Console.WriteLine();


            Console.ReadKey();
        }
    }
}
