﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A03_Temperatur
{
    interface IKelvin
    {
        double Kelvin { get; set; }

        void Ausgabe();
    }
}
