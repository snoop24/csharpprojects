﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A03_Temperatur
{
    class Temperatur : ICelsius, IKelvin, IFahrenheit
    {
        // attribute
        #region:attribute
        private double celsius;
        #endregion

        // property
        #region:property
        public double Celsius
        {
            get
            {
                return celsius;
            }
            set
            {
                celsius = value;
            }
        }

        public double Kelvin
        {
            get
            {
                return celsius + 273.15;
            }
            set
            {
                celsius = value - 273.15;
            }
        }

        public double Fahrenheit
        {
            get
            {
                return celsius * 9 / 5 + 32;
            }
            set
            {
                celsius = (value - 32) * 5 / 9;
            }
        }
        #endregion

        // methods
        #region:methods
        public void Ausgabe()
        {
            Console.WriteLine("Temperatur in Grad Celsius: {0:F2}", celsius);
        }

        void IKelvin.Ausgabe()
        {
            Console.WriteLine("Temperatur in Grad Kelvin: {0:F2}", celsius + 273.15);
        }

        void IFahrenheit.Ausgabe()
        {
            Console.WriteLine("Temperatur in Grad Fahrenheit: {0:F2}", celsius * 9 / 5 + 32);
        }
        #endregion
    }
}
