﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A03_Temperatur
{
    interface IFahrenheit
    {
        double Fahrenheit { get; set; }

        void Ausgabe();
    }
}
