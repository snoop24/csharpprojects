﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A04_DieBeobachterUndDerFluss
{
    class FlussMessStelle
    {
        static Random rnd = new Random();

        public double WasserStand { get;  set; }
        List<IBeobachter> checker = new List<IBeobachter>();

        public void MeldeWasserStand()
        {
            foreach (IBeobachter item in checker)
            {
                Console.WriteLine(  item.Meldung(WasserStand));
            }
        }

        public void AddBeobachter(IBeobachter item)
        {
            checker.Add(item);
        }
        public void RemoveBeobachter(IBeobachter item)
        {
            checker.Remove(item);
        }

        public double MesseWasserStand()
        {
            WasserStand = rnd.Next(0, 90) / 10 + 0.1;
            return WasserStand;
        }
    }
}
