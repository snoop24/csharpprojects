﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A04_DieBeobachterUndDerFluss
{
    class Program
    {
        static void Main(string[] args)
        {
            FlussMessStelle ruhrBochum1 = new FlussMessStelle();

            BueffelChecker steak = new BueffelChecker();
            WegChecker nudist = new WegChecker();
            StrassenChecker nrw = new StrassenChecker();

            ruhrBochum1.AddBeobachter(steak);
            ruhrBochum1.AddBeobachter(nudist);
            ruhrBochum1.AddBeobachter(nrw);

            Console.WriteLine();
            ruhrBochum1.WasserStand = 0.2;
            Console.WriteLine(ruhrBochum1.WasserStand);
            ruhrBochum1.MeldeWasserStand();

            Console.WriteLine();
            ruhrBochum1.WasserStand = 1;
            Console.WriteLine(ruhrBochum1.WasserStand);
            ruhrBochum1.MeldeWasserStand();

            Console.WriteLine();
            ruhrBochum1.WasserStand = 4;
            Console.WriteLine(ruhrBochum1.WasserStand);
            ruhrBochum1.MeldeWasserStand();
            
            Console.WriteLine();
            ruhrBochum1.WasserStand = 5.5;
            Console.WriteLine(ruhrBochum1.WasserStand);
            ruhrBochum1.MeldeWasserStand();

            Console.WriteLine();
            ruhrBochum1.WasserStand =6.5;
            Console.WriteLine(ruhrBochum1.WasserStand);
            ruhrBochum1.MeldeWasserStand();

            Console.WriteLine();
            ruhrBochum1.WasserStand = 7.1;
            Console.WriteLine(ruhrBochum1.WasserStand);
            ruhrBochum1.MeldeWasserStand();

            Console.WriteLine();
            ruhrBochum1.WasserStand = 10;
            Console.WriteLine(ruhrBochum1.WasserStand);
            ruhrBochum1.MeldeWasserStand();
            do
            {
                Console.WriteLine();
                ruhrBochum1.MesseWasserStand();
                Console.WriteLine(ruhrBochum1.WasserStand);
                ruhrBochum1.MeldeWasserStand();
            } while (Console.ReadKey().Key!= ConsoleKey.Escape);


            Console.ReadKey();
        }
    }
}
