﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A04_DieBeobachterUndDerFluss
{
    class WegChecker : IBeobachter
    {
        public string Meldung(double wasserStand)
        {
            string meldung;
            if (wasserStand <=1)
            {
                meldung = "Christophorus an Basis. Christophorus an Basis. Wwaawawasserstand niedrig. Fluss kann durchquert werden.";
            }
            else if (wasserStand < 6)
            {
                meldung = "Das Wandern ist des Müllers Lust, das Wandern ist des... !";
            }
            else 
            {
                meldung = "Nix Wandern. Rafting!";
            }
            return meldung;
        }
    }
}
