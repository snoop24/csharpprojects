﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A04_DieBeobachterUndDerFluss
{
    class StrassenChecker : IBeobachter
    {

        public string Meldung(double wasserStand)
        {
            string meldung;
            if (wasserStand <= 9)
            {
                meldung = "Strasse frei.";
            }
            else
            {
                meldung = "Macht die Strasse zu. Die Autos saufen ab!";
            }
            return meldung;

        }
    }
}
