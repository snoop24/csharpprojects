﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A04_DieBeobachterUndDerFluss
{
    class BueffelChecker : IBeobachter
    {
        public string Meldung(double wasserStand)
        {
            string meldung;
            if (wasserStand <= 0.3)
            {
                meldung = "Tu ma ne tu ma ne Klare! Der Sultan haett Dorscht.";
            }
            else if (wasserStand < 5)
            {
                meldung = "Alles in Butter.";
            }
            else if (wasserStand <= 7)
            {
                meldung = "Also Viecher inne Hütte wär schon besser.";
            }
            else
            {
                meldung = "Also, wenne noch Bueffel hier has, lange schwimmen die nich mehr.";
            }
            return meldung;
        }
    }
}
