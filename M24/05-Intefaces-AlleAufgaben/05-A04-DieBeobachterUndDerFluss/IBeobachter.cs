﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A04_DieBeobachterUndDerFluss
{
    interface IBeobachter
    {
        string Meldung(double wasserStand);
    }
}
