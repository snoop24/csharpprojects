﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A06_Movable
{
    class Program
    {
        static void Main(string[] args)
        {
            MovablePoint upperLeft0 = new MovablePoint(1, 10, 3, 2);
            MovablePoint lowerRight0 = new MovablePoint(6, 5, 3, 2);

            MovableRect movableRect0 = new MovableRect(upperLeft0, lowerRight0);

            movableRect0.MoveRight();
            Console.WriteLine(movableRect0);
            movableRect0.MoveRight();
            Console.WriteLine(movableRect0);
            movableRect0.MoveRight();
            Console.WriteLine(movableRect0);

            movableRect0.MoveUp();
            Console.WriteLine(movableRect0);
            movableRect0.MoveUp();
            Console.WriteLine(movableRect0);
            movableRect0.MoveUp();
            Console.WriteLine(movableRect0);

            movableRect0.MoveLeft();
            Console.WriteLine(movableRect0);
            movableRect0.MoveLeft();
            Console.WriteLine(movableRect0);

            Console.ReadKey();

            #region:spielerei
            Console.Clear();

            upperLeft0 = new MovablePoint(1, 10, 1, 1);
            lowerRight0 = new MovablePoint(6, 5, 1, 1);

            movableRect0 = new MovableRect(upperLeft0, lowerRight0);

            bool end = false;
            Console.CursorVisible = false;
            do
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKey input = Console.ReadKey(true).Key;

                    movableRect0.Print(ConsoleColor.Black, ConsoleColor.Black);
                    upperLeft0.Print(ConsoleColor.Black, ConsoleColor.Black);
                    lowerRight0.Print(ConsoleColor.Black, ConsoleColor.Black);

                    switch (input)
                    {
                        case ConsoleKey.Escape:
                            end = true;
                            break;
                        case ConsoleKey.LeftArrow:
                            movableRect0.LowerRightCorner.MoveLeft();
                            break;
                        case ConsoleKey.UpArrow:
                            movableRect0.LowerRightCorner.MoveUp();
                            break;
                        case ConsoleKey.RightArrow:
                            movableRect0.LowerRightCorner.MoveRight();
                            break;
                        case ConsoleKey.DownArrow:
                            movableRect0.LowerRightCorner.MoveDown();
                            break;
                        case ConsoleKey.A:
                            movableRect0.UpperLeftCorner.MoveLeft();
                            break;
                        case ConsoleKey.W:
                            movableRect0.UpperLeftCorner.MoveUp();
                            break;
                        case ConsoleKey.D:
                            movableRect0.UpperLeftCorner.MoveRight();
                            break;
                        case ConsoleKey.S:
                            movableRect0.UpperLeftCorner.MoveDown();
                            break;
                    }
                    movableRect0.Print(ConsoleColor.Black, ConsoleColor.White);
                    upperLeft0.Print(ConsoleColor.Black, ConsoleColor.Green);
                    lowerRight0.Print(ConsoleColor.Black, ConsoleColor.Red);
                }
            } while (!end);
            #endregion
        }



    }
}
