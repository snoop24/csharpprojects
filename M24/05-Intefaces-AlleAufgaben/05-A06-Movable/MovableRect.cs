﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A06_Movable
{
    class MovableRect : IMovable
    {
        #region:property
        public MovablePoint UpperLeftCorner { get; set; }
        public MovablePoint LowerRightCorner { get; set; }

        public double XSpeed { get; set; }
        public double YSpeed { get; set; }
        #endregion

        #region:constructor
        public MovableRect() { }

        public MovableRect(MovablePoint upperLeftCorner, MovablePoint lowerRightCorner)
        {
            UpperLeftCorner = upperLeftCorner;
            LowerRightCorner = lowerRightCorner;
            XSpeed = (UpperLeftCorner.XSpeed+ LowerRightCorner.XSpeed)/2;
            YSpeed = (UpperLeftCorner.YSpeed + LowerRightCorner.YSpeed) / 2;
        }
        #endregion

        #region:other methods
        public override string ToString()
        {
            return string.Format("coords UL( {0,3} / {1,3} ) : LR( {2,3} / {3,3} ) - speed( {4,3} / {5,3} )", UpperLeftCorner.X, UpperLeftCorner.Y, LowerRightCorner.X, LowerRightCorner.Y, XSpeed, YSpeed);
        }

        #region:move methods
        public void MoveDown()
        {
            UpperLeftCorner.MoveDown();
            LowerRightCorner.MoveDown();
        }

        public void MoveLeft()
        {
            UpperLeftCorner.MoveLeft();
            LowerRightCorner.MoveLeft();
        }

        public void MoveRight()
        {
            UpperLeftCorner.MoveRight();
            LowerRightCorner.MoveRight();
        }

        public void MoveUp()
        {
            UpperLeftCorner.MoveUp();
            LowerRightCorner.MoveUp();
        }
        #endregion

        #region:print methods
        public void Print(ConsoleColor fore, ConsoleColor back = ConsoleColor.Black)
        {
            int uLx = (int) Math.Min(UpperLeftCorner.X,LowerRightCorner.X)  ;
            int lRx = (int)Math.Max(UpperLeftCorner.X, LowerRightCorner.X);
            int uLy=(int) Math.Min(UpperLeftCorner.Y, LowerRightCorner.Y);
            int lRy=(int) Math.Max(UpperLeftCorner.Y, LowerRightCorner.Y);

            uLx = uLx > 0 ? uLx : 0;
            uLy = uLy > 0 ? uLy : 0;
            lRx = lRx > 0 ? lRx : 0;
            lRy = lRy > 0 ? lRy : 0;

            Console.ForegroundColor = fore;
            Console.BackgroundColor = back;
            for (int i = uLx; i <= lRx; i++)
            {
                Console.SetCursorPosition(i, uLy);
                Console.Write(" ");
                Console.SetCursorPosition(i, lRy);
                Console.Write(" ");
            }
            for (int i = uLy; i <= lRy; i++)
            {
                Console.SetCursorPosition(uLx, i);
                Console.Write(" ");
                Console.SetCursorPosition(lRx, i);
                Console.Write(" ");
            }
            Console.ResetColor();
        }
        #endregion

        #endregion
    }
}
