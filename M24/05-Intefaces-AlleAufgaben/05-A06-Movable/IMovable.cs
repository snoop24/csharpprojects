﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A06_Movable
{
    interface IMovable
    {
        void MoveDown();
        void MoveUp();
        void MoveLeft();
        void MoveRight();
    }
}
