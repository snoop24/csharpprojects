﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A06_Movable
{
    class MovablePoint : IMovable
    {
        public double X { get; set; }
        public double Y { get; set; }

        public double XSpeed { get; set; }
        public double YSpeed { get; set; }

        public MovablePoint() { }

        public MovablePoint(double x, double y, double xSpeed, double ySpeed)
        {
            X = x;
            Y = y;
            XSpeed = xSpeed;
            YSpeed = ySpeed;
        }
        #region:other methods
        public override string ToString()
        {
            return string.Format("c({0}/{1}) - s({2}/{3})",X,Y,XSpeed,YSpeed); 
        }

        #region:move methods
        public void MoveDown()
        {
            Y += YSpeed;
        }

        public void MoveLeft()
        {
            X -= XSpeed;
        }

        public void MoveRight()
        {
            X += XSpeed;
        }

        public void MoveUp()
        {
            Y -= YSpeed;
        }
        #endregion

        #region:print methods
        public void Print(ConsoleColor fore, ConsoleColor back = ConsoleColor.Black)
        {
            
            Console.ForegroundColor = fore;
            Console.BackgroundColor = back;
            Console.SetCursorPosition(X > 0 ? (int)X : 0, Y > 0 ? (int)Y : 0);
            Console.Write("X");
            Console.ResetColor();
        }
        #endregion

        #endregion
    }
}
