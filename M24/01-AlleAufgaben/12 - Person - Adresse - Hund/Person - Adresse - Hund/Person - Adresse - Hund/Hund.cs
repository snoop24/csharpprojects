﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person___Adresse___Hund
{
    class Hund
    {
        private string name;
        private bool gefüttert;
        private Person herrchen;

        public Hund(string name, Person herrchen)
        {
            this.name = name;
            this.herrchen = herrchen;
            this.gefüttert = true;
            herrchen.SetHund(this);
        }

        public void Füttern()
        {
            if (gefüttert == false)
            {
                Console.WriteLine("{0} hat noch keinen Hunger", name);
            }
            else
            {
                gefüttert = true;
                Console.WriteLine("Jetzt Gassi gehen...");
            }
        }

        public void GassiGehen()
        {
            if (herrchen.GetAlter() < 16)
            {
                Console.WriteLine("{0} darf nicht mit {1} Gassi gehen", herrchen.GetVorname(), this.GetName());
            }
            else
            {
                gefüttert = false;
                Console.WriteLine("{0} und {1} gehen los...", herrchen.GetVorname(), this.GetName());
            }
        }

        public void GassiGehen(Person p)
        {
            if (p.GetAlter() < 16)
            {
                Console.WriteLine("{0} darf nicht mit {1} Gassi gehen", p.GetVorname(), this.GetName());
            }
            else
            {
                gefüttert = false;
                Console.WriteLine("{0} und {1} gehen los...", p.GetVorname(), this.GetName());
            }
        }

        public string GetName()
        {
            return name;
        }
    }
}
