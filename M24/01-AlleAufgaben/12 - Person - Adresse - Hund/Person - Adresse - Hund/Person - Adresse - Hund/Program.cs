﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person___Adresse___Hund
{
    class Program
    {
        private static void Main(string[] args)
        {
            Adresse lennershof = new Adresse("Lennershofstraße", "160", "44801", "Bochum");
            Adresse konradzuse = new Adresse("Konrad Zuse Straße", "12", "44801", "Bochum");

            Person hans = new Person("Hans", "Müller", 42);
            Person lisa = new Person("Lisa", "Schulz", 32);
            Person eugen = new Person("Eugen", "Krebs", 15);

            // Einem Personen Objekt ein Adress Objekt zuweisen
            hans.SetAdresse(lennershof);

            Hund bello = new Hund("Bello", hans);

            //hans.SetHund(bello);

            hans.Ausgabe();

            bello.GassiGehen();

            bello.Füttern();
            
            bello.GassiGehen();

            hans.GetHund().GassiGehen();

            bello.GassiGehen(eugen);

            lisa.SetAdresse(konradzuse);
            lisa.Ausgabe();
        }
    }
}
