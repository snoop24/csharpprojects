﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person___Adresse___Hund
{
    class Adresse
    {
        private string strasse;
        private string hausnummer;
        private string plz;
        private string ort;

        public Adresse(string strasse, string hausnummer, string plz, string ort)
        {
            this.strasse = strasse;
            this.hausnummer = hausnummer;
            this.plz = plz;
            this.ort = ort;
        }

        public void Ausgabe()
        {
            Console.WriteLine("{0} {1}\n{2} {3}", strasse, hausnummer, plz, ort);
        }
    }
}
