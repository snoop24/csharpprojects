﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person___Adresse___Hund
{
    class Person
    {
        private string nachname;
        private string vorname;
        private int alter;
        // Referenzen auf Objekte anderer Klassen
        private Adresse anschrift;
        private Hund hund;

        public Person(string vorname, string name, int alter)
        {
            this.nachname = name;
            this.vorname = vorname;
            this.alter = alter;
        }

        public string GetVorname()
        {
            return vorname;
        }

        public string GetNachname()
        {
            return nachname;
        }

        public int GetAlter()
        {
            return alter;
        }

        public void SetNachname(string nachname)
        {
            this.nachname = nachname;
        }

        // Methode zum Ändern der Adresse mit einem bestehendem Objekt
        public void SetAdresse(Adresse neueAnschrift)
        {
            this.anschrift = neueAnschrift;
        }

        //// Überladung ohne Adresse
        //public void SetAdresse()
        //{
        //    Console.Write("Straße eingeben:");
        //    string strasse = Console.ReadLine();

        //    Console.Write("Hausnummer eingeben:");
        //    string hausnummer = Console.ReadLine();

        //    Console.Write("PLZ eingeben:");
        //    string plz = Console.ReadLine();

        //    Console.Write("Ort eingeben:");
        //    string ort = Console.ReadLine();

        //    // Neues Adress Objekt mit eingelesenen Daten erzeugen
        //    // und diesem Objekt zuweisen
        //    this.anschrift = new Adresse(strasse, hausnummer, plz, ort);
        //}

        public void Ausgabe()
        {
            Console.WriteLine("{0} {1}, Alter {2}", vorname, nachname, alter);

            // Überprüfen, ob der Person eine Adresse zugewiesen wurde
            if (this.anschrift != null)
            {
                this.anschrift.Ausgabe();
            }

            // Überprüfen, ob der Person ein Hund zugewiesen wurde
            if (this.hund != null)
            {
                Console.WriteLine("Hund: {0}", hund.GetName());
            }
            Console.WriteLine();
        }

        public void SetHund(Hund derHund)
        {
            this.hund = derHund;
        }

        public Hund GetHund()
        {
            return hund;
        }
    }
}
