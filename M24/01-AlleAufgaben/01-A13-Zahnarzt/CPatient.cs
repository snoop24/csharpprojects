﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A13_Zahnarzt
{
    class CPatient
    {
        // class 
        static int anzahl = 0;


        // instance
        int nummer;
        string name;
        DateTime gebDat;
        string adresse;
        bool vKVorgelegt;

        // relation
        CKKasse kKasse;

        #region:Constructor

        public CPatient(string name, DateTime gebDat, string adresse, CKKasse kKasse)
        {
            // instance
            nummer= ++anzahl;
            this.name = name;
            this.gebDat = gebDat;
            this.adresse = adresse;
            vKVorgelegt = true;

            // this -> relation
            this.kKasse = kKasse;

            // this <- relation
            kKasse.AddKunde(this);
        }

        public CPatient(string name, CKKasse kKasse) :
            this(name, DateTime.Today, "", kKasse)
        {
        }

        #endregion

        #region:GetSet

        // nummer

        public int GetNummer()
        {
            return nummer;
        }

        public void SetNummer(int nummer)
        {
            this.nummer = nummer;
        }

        // name

        public string GetName ()
        {
            return name;
        }
        
        public void SetName(string name)
        {
            this.name = name;
        }

        // gebDat

        public DateTime GetGebDat()
        {
            return gebDat;
        }

        public void SetGebDat (DateTime gebDat)
        {
            this.gebDat = gebDat;
        }

        // adresse

        public string GetAdresse()
        {
            return adresse;
        }

        public void SetAdresse(string adresse)
        {
            this.adresse = adresse;
        }


        // vKVorgelegt

        public bool GetVKVorgelegt()
        {
            return vKVorgelegt;
        }

        public void SetVKVorgelegt(bool vKVorgelegt)
        {
            this.vKVorgelegt = vKVorgelegt;
        }

        // KKasse

        public CKKasse GetKKasse()
        {
            return kKasse;
        }

        public void SetKKasse(CKKasse kKasse)
        {
            this.kKasse.RemKunde(this);
            this.kKasse = kKasse;
            this.kKasse.AddKunde(this);

        }
        #endregion

    }
}
