﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A13_Zahnarzt
{
    class Program
    {
        static void Main(string[] args)
        {
            //general loop
            do
            {
                // wipe screen
                Console.Clear();
                Console.SetWindowSize(80, 40);

                CKKasse kk1 = new CKKasse("1", "KKeins");
                CKKasse kk2 = new CKKasse("2", "KKzwei");

                CPatient p1 = new CPatient("Horst", kk1);
                CPatient p2 = new CPatient("Detlef", kk2);
                CPatient p3 = new CPatient("Kevin", kk1);

                PatientenAusgabe(p1);
                PatientenAusgabe(p2);
                PatientenAusgabe(p3);

                Console.WriteLine();
                foreach (CPatient item in kk1.GetKunden())
                {
                    Console.WriteLine(  item.GetName());
                }



                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }

        static void PatientenAusgabe(CPatient patient)
        {
            Console.WriteLine("\n{0}\n{1}\n{2}\n{3}\n{4}", patient.GetNummer(), patient.GetName(), patient.GetGebDat(), patient.GetKKasse().GetName(), patient.GetVKVorgelegt());
        }
    }
}
