﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A13_Zahnarzt
{
    class CKKasse
    {

        // instance
        string nummer;
        string name;

        // relation
        List<CPatient> kunden;
        
        public CKKasse(string nummer,string name)
        {
            this.nummer = nummer;
            this.name = name;
            kunden = new List<CPatient>();
        }

        public string GetNummer()
        {
            return nummer;
        }

        public string GetName()
        {
            return name;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public List<CPatient> GetKunden()
        {
            return kunden;
        }

        public void AddKunde(CPatient patient)
        {
            kunden.Add(patient);
        }

        public void RemKunde(CPatient patient)
        {
            kunden.Remove(patient);
        }        
    }
}
