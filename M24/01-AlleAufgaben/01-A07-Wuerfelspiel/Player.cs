﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A07_Wuerfelspiel
{
    class Player
    {
        private string name;
        private int score;
        static Random rnd = new Random;

        public Player(string name)
        {
            this.name = name;
            score =0;
        }

        public string GetName()
        {
            return name;
        }

        public int GetScore()
        {
            return score;
        }

        public void Gamble()
        {
            int wurf;

            Console.WriteLine("\n   {0}\n",name);
            
            for (int i = 0; i < 5; i++)
            {
                wurf = rnd.Next(1, 7);
                score += wurf;
                Console.Write("Wurf {0}: {1,2}   ",i+1,wurf);
            }
            Console.WriteLine("\n\nGesamt: {0}\n", score);
        }
    }
}
