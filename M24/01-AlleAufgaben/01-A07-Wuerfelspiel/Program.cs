﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A07_Wuerfelspiel
{
    class Program
    {
        static void Main(string[] args)
        {
            //general loop
            do
            {
                // wipe screen
                Console.Clear();

                // declare
                Player p1 = new Player("Horst");
                Player p2 = new Player("Detlef");

                p1.Gamble();
                p2.Gamble();

                Console.WriteLine("\n   "+ (p1.GetScore() > p2.GetScore() ? p1.GetName() + " hat gewonnen." : (p1.GetScore() < p2.GetScore() ? p2.GetName() + " hat gewonnen." : "Unentschieden.")));

                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
