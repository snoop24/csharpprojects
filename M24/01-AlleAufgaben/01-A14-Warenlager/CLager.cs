﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A14_Warenlager
{
    class CLager
    {
        List<CArtikel> lager;

        #region:Constructor

        public CLager()
        {
            lager = new List<CArtikel>();
        }

        #endregion

        #region:GetSetAddRem

        public void AddArtikel(CArtikel artikel)
        {
            lager.Add(artikel);
        }

        public void RemArtikel(CArtikel artikel)
        {
            lager.Remove(artikel);
        }

        public CArtikel GetArtikel(int nummer)
        {
            CArtikel artikel = lager.Find(gesucht => gesucht.GetNummer() == nummer);
            return artikel;
        }

        public CArtikel GetArtikel(string name)
        {
            CArtikel artikel = lager.Find(gesucht => gesucht.GetName() == name);
            return artikel;
        }



        #endregion


        #region:OtherMethods

        public void Bestellvorschlag()
        {
            bool vorschlaege = false;
            Console.WriteLine("\n{0,-4}|{1,-13}|{2,-11}|{3,-13}|{4,-10}", " Nr.", " Name", " Bestand", " MeldeBestand", " Bestell#?");
            Console.WriteLine("_______________________________________________________\n");
            foreach (CArtikel item in lager)
            {
                if (!(item.GetBestand() > item.GetMeldeBestand()))
                {
                    Console.WriteLine("{0,4} {1,13} {2,11} {3,13} {4,10}", item.GetNummer(), item.GetName(), item.GetBestand(), item.GetMeldeBestand(), item.GetMeldeBestand() - item.GetBestand());
                    vorschlaege = true;
                }
            }
            if (!vorschlaege) { Console.WriteLine("*** Alle Artikel über dem Meldebestand. ***"); }
        }

        #endregion


    }
}
