﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _01_A14_Warenlager
{
    class Program
    {
        static void Main(string[] args)
        {

            // wipe screen
            Console.Clear();
            Console.SetWindowSize(80, 40);


            // declare
            CLager lager = new CLager();

            List<CArtikel> artikelListe = new List<CArtikel>();

            // read artikel from file

            // file path
            string pfad = Directory.GetCurrentDirectory() + @"\\artikel.txt"; ;

            // start reader
            StreamReader inputDatei = new StreamReader(pfad);

            // read lines
            while (!inputDatei.EndOfStream)
            {
                string[] temp = inputDatei.ReadLine().Split(':');

                if (temp.Length == 7)
                {
                    artikelListe.Add(new CArtikel(int.Parse(temp[0]), temp[1], int.Parse(temp[2]), int.Parse(temp[3]), double.Parse(temp[4]), double.Parse(temp[5]), int.Parse(temp[6])));
                }
            }
            inputDatei.Close();

            foreach (CArtikel item in artikelListe)
            {
                lager.AddArtikel(item);
            }

            lager.Bestellvorschlag();


            //general loop
            do
            {
                
                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

        }
    }
}
