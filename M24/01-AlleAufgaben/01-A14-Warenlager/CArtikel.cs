﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A14_Warenlager
{
    class CArtikel
    {
        // instance

        int nummer;
        string name;
        int bestand;
        int maxBestand;
        double preis;
        double verbrauchTag;
        int bestellDauerTage;
        int meldeBestand;

        #region:Constructor

        public CArtikel(int nummer, string name, int bestand, int maxBestand, double preis, double verbrauchTag, int bestellDauerTage)
        {
            this.nummer = nummer;
            this.name = name;
            this.bestand = bestand;
            this.maxBestand = maxBestand;
            this.preis = preis;
            this.verbrauchTag = verbrauchTag;
            this.bestellDauerTage = bestellDauerTage;
            meldeBestand = (int)(verbrauchTag * (bestellDauerTage + 2));
        }

        #endregion

        #region:GetSet

        public int GetBestand()
        {
            return bestand;
        }
        public void SetBestand( int bestand)
        {
            this.bestand = bestand;
        }

        public int GetNummer()
        {
            return nummer;
        }

        public string GetName()
        {
            return name;
        }

        public int GetMeldeBestand()
        {
            return meldeBestand;
        }

        #endregion

        #region:OtherMethods

        public void Ausgabe()
        {
            Console.WriteLine("{0,4} {1,13} {2,11} {3,13}", nummer, name, bestand,meldeBestand);
        }

        #endregion
    }
}
