﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A03_Mensch
{
    class Program
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                Mensch hoemma = new Mensch("Max Mustermann", 35);
                Mensch hattu = new Mensch("Erika Typischfrau", 45);


                // Ausgabe
                Console.WriteLine(hoemma.Vorstellen());
                Console.WriteLine(hattu.Vorstellen());

                // Geburstag
                hattu.Geburtstag();

                // Ausgabe
                Console.WriteLine(hoemma.Vorstellen());
                Console.WriteLine(hattu.Vorstellen());
                
                // loop prompt
                Console.Write("... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);


        }
    }
}
