﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A03_Mensch
{
    class Mensch
    {
        private string name;
        private int alter;

        public Mensch(string name, int alter)
        {
            this.name = name;
            this.alter = alter;
        }

        public void Geburtstag()
        {
            alter++;
            Console.WriteLine("{0} hat Geburtstag und ist ein Jahr älter geworden.", name);
        }

        public string Vorstellen()
        {
            return string.Format("Mein Name ist {0} und ich bin {1} Jahre alt.",name, alter);
        }
    }
}
