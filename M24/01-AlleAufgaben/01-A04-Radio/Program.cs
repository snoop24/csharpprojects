﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A04_Radio
{
    class Program
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                Radio holzKiste = new Radio();

                // Umstellungen und ausagben;
                Console.WriteLine(holzKiste.GetRadioInfo());

                holzKiste.Einschalten();

                for (int i = 0; i < 100; i++)
                {
                    holzKiste.Lauter();
                }

                Console.WriteLine(holzKiste.GetRadioInfo());

                holzKiste.SetWaehleSender(88.74);
                holzKiste.SetWaehleSender(101.3);

                Console.WriteLine(holzKiste.GetRadioInfo());

                for (int i = 0; i < 105; i++)
                {
                    holzKiste.Leiser();
                }

                for (int i = 0; i < 10; i++)
                {
                    holzKiste.Lauter();
                }

                Console.WriteLine(holzKiste.GetRadioInfo());

                // loop prompt
                Console.WriteLine("... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
