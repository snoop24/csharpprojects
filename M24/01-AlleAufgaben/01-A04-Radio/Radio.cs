﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A04_Radio
{
    class Radio
    {
        private int lautstaerke = 5;
        private bool eingeschaltet = false;
        private double frequenz = 99.2;
        
        #region:Get

        public bool GetIstAn()
        {
            return eingeschaltet;
        }

        public string GetRadioInfo()
        {
            string radioInfo;
            radioInfo = string.Format("Radio {0}: Frequenz={1}, Lautstärke={2}", eingeschaltet ? "an" : "aus", frequenz, lautstaerke);
            return radioInfo;
        }

        #endregion

        #region:Set

        public void SetWaehleSender(double frequenz)
        {
            string message;
            if (frequenz >= 87.50 && frequenz <= 108.0 && ( (frequenz*100)%10 == 0 || (frequenz * 100) % 10 == 5))
            {
                message = "Die Frequenz {0} wurde eingestellt.";
                this.frequenz = frequenz;
            }
            else
            {
                message = "Die Frequenz {0} entspricht nicht den Vorgaben. 87,50 - 108.00 in 0,05 Schritten.";
            }
            Console.WriteLine(message, frequenz);
        }

        #endregion

        #region:OtherMethods

        public void Einschalten()
        {
            Console.WriteLine("Das Radio {0} eingeschaltet.", eingeschaltet ? "war schon" : "ist jetzt");
            eingeschaltet = true;
        }

        public void Ausschalten()
        {
            Console.WriteLine("Das Radio {0} ausgeschaltet.", !eingeschaltet ? "war schon" : "ist jetzt");
            eingeschaltet = true; ;
        }

        public void Leiser()
        {
            lautstaerke += (lautstaerke > 0) ? -1 : 0;
            Console.WriteLine("Lautstärke: {0,3}", lautstaerke);
        }

        public void Lauter()
        {
            lautstaerke += (lautstaerke > 99) ? 0 : 1;
            Console.WriteLine("Lautstärke: {0,3}", lautstaerke);
        }

        #endregion
    }
}
