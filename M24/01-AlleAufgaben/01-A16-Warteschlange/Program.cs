﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A16_Warteschlange
{
    class Program
    {
        static void Main(string[] args)
        {

            CMyLinkedQueue queue = new CMyLinkedQueue();

            Console.WriteLine("*** 1. Eintrag hinzufügen:");
            queue.Enqueue("1. Eintrag");
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** 2. Eintrag hinzufügen:");
            queue.Enqueue("2. Eintrag");
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** 3. Eintrag hinzufügen:");
            queue.Enqueue("3. Eintrag");
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** Eintrag löschen:");
            queue.Dequeue();
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** Eintrag löschen:");
            queue.Dequeue();
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** 4. Eintrag hinzufügen:");
            queue.Enqueue("4. Eintrag");
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** Eintrag löschen:");
            queue.Dequeue();
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** Eintrag löschen:");
            queue.Dequeue();
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** Eintrag löschen:");
            queue.Dequeue();
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** 5. Eintrag hinzufügen:");
            queue.Enqueue("5. Eintrag");
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** 6. Eintrag hinzufügen:");
            queue.Enqueue("6. Eintrag");
            queue.Print();
            Console.WriteLine();

            Console.WriteLine("*** 7. Eintrag hinzufügen:");
            queue.Enqueue("7. Eintrag");
            queue.Print();
            Console.WriteLine();

            //general loop
            do
            {
                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
