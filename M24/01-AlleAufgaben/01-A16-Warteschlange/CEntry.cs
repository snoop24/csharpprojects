﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A16_Warteschlange
{
    class CEntry
    {
        string data;
        CEntry next;

        public CEntry(string data)
        {
            this.data = data;
        }

        public string GetData ()
        {
            return data;
        }

        public CEntry GetNext()
        {
            return next;
        }

        public void SetNext(CEntry next)
        {
            this.next = next;
        }
    }
}
