﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A16_Warteschlange
{
    class CMyLinkedQueue
    {
        CEntry head;
        CEntry tail;

        public CMyLinkedQueue()
        { }

        public void Enqueue(string data)
        {

            CEntry temp = new CEntry(data);
            if (head == null)
            {
                head = temp;
            }
            else
            {
                tail.SetNext(temp);
            }
            tail = temp;
        }
         
        public string Dequeue()
        {
            string temp= null;

            if (head != null)
            {
                temp = head.GetData();
                head = head.GetNext();
            }

            return temp;
        }

        public bool IsEmpty()
        {
            bool isEmpty = (head == null ? true : false);
            return isEmpty;
        }

        public void Print()
        {
            CEntry temp = head;
            while (temp != null)
            {
                Console.WriteLine(temp.GetData());
                temp = temp.GetNext();
            }

            //PrintNext(head);
        }

        //public void PrintNext(CEntry temp)
        //{
        //    if (temp != null)
        //    {
        //        Console.WriteLine(temp.GetData());
        //        PrintNext(temp.GetNext());
        //    }
        //}
    }
}
