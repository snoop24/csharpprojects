﻿using System;
using System.IO;
using System.Collections.Generic;

namespace _01_Aufgabenzettel_14_Warenlager
{
    class Warenlager
    {
        // Eigenschaft
        private List<Artikel> alleArtikel;

        // Konstruktor
        public Warenlager()
        {
            alleArtikel = new List<Artikel>();

            #region StreamReader
            //// Datei öffnen
            //StreamReader streamReader = new StreamReader("warenlager.txt");

            //while (streamReader.EndOfStream == false)
            //{
            //    // Eine Zeile aus der Datei einlesen
            //    string zeile = streamReader.ReadLine();

            //    // Zeile am ; zerlegen
            //    string[] teile = zeile.Split(';');

            //    int artikelnummer = int.Parse(teile[0]);
            //    string bezeichnung = teile[1];
            //    int istbestand = int.Parse(teile[2]);
            //    int höchstbestand = int.Parse(teile[3]);
            //    decimal preis = decimal.Parse(teile[4]);
            //    int verbrauch = int.Parse(teile[5]);
            //    int bestelldauer = int.Parse(teile[6]);

            //    // Objekt erzeugen und in Liste speichern
            //    alleArtikel.Add(new Artikel(artikelnummer, bezeichnung, istbestand, höchstbestand, preis, verbrauch, bestelldauer));
            //}

            //// Datei schließen
            //streamReader.Close();
            #endregion

            #region statische Klasse File
            //Alternativ: statische Klasse File

            string[] daten = File.ReadAllLines("warenlager.txt");

            for (int i = 0; i < daten.Length; i++)
            {
                // Zeile am ";" zerlegen
                string[] teile = daten[i].Split(';');

                int artikelnummer = int.Parse(teile[0]);
                string bezeichnung = teile[1];
                int istbestand = int.Parse(teile[2]);
                int höchstbestand = int.Parse(teile[3]);
                decimal preis = decimal.Parse(teile[4]);
                int verbrauch = int.Parse(teile[5]);
                int bestelldauer = int.Parse(teile[6]);

                // Objekt erzeugen und in Liste speichern
                Artikel temp = new Artikel(artikelnummer, bezeichnung, istbestand, höchstbestand, preis, verbrauch, bestelldauer);
                

                alleArtikel.Add(temp);
            }
            #endregion
        }


        public decimal WarenwertBerechnen()
        {
            decimal summe = 0;
            for (int i = 0; i < alleArtikel.Count; i++)
            {
                summe += alleArtikel[i].GetPreis() * alleArtikel[i].GetIstBestand();
            }
            return summe;
        }

        public void AusgabeWarenliste()
        {
            // Kopfzeile
            Console.WriteLine("A-Nr.\tBez.\tIstB.\tMax.\tPreis\tVproT\tBinT\tMel.\tVors.");

            // Ausgabe der einzelnen Artikel
            for (int i = 0; i < alleArtikel.Count; i++)
            {
                alleArtikel[i].Ausgabe();
            }

            Console.WriteLine();
        }
    }
}
