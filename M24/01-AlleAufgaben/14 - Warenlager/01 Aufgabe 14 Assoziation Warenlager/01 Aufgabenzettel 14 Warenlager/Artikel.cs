﻿using System;

namespace _01_Aufgabenzettel_14_Warenlager
{
    class Artikel
    {
        private int nummer;
        private string bezeichnung;
        private int istBestand;
        private int maxBestand;
        private decimal preis;
        private int verbrauch;
        private int bestelldauer;

        public Artikel(int nummer, string bezeichnung, int istBestand, int maxBestand, decimal preis, int verbrauch, int bestelldauer)
        {
            this.nummer = nummer;
            this.bezeichnung = bezeichnung;
            this.istBestand = istBestand;
            this.maxBestand = maxBestand;
            this.preis = preis;
            this.verbrauch = verbrauch;
            this.bestelldauer = bestelldauer;
        }

        public void Ausgabe()
        {
            Console.WriteLine("{0}\t{1}\t{2,3}\t{3,3}\t{4,6}\t{5,3}\t{6,3}\t{7,3}\t{8,3}", nummer,
                                                                                           bezeichnung,
                                                                                           istBestand,
                                                                                           maxBestand,
                                                                                           preis,
                                                                                           verbrauch,
                                                                                           bestelldauer,
                                                                                           GetMeldeBestand(),
                                                                                           GetBestellVorschlag());
        }

        public decimal GetPreis()
        {
            return preis;
        }

        public int GetIstBestand()
        {
            return istBestand;
        }

        public int GetBestelldauer()
        {
            return bestelldauer;
        }

        public int GetVerbrauch()
        {
            return verbrauch;
        }

        public int GetNummer()
        {
            return nummer;
        }

        public int GetBestellVorschlag()
        {
            if (istBestand <= GetMeldeBestand())
            {
                return GetMeldeBestand() - istBestand;
            }
            return 0;
        }

        public int GetMeldeBestand()
        {
            return verbrauch * (bestelldauer + 2);
        }      
    }
}
