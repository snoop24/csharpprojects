﻿using System;

namespace _01_Aufgabenzettel_14_Warenlager
{
    class Program
    {
        static void Main(string[] args)
        {
            // Zum Testen der Artikel-Klasse
            Artikel sofa = new Artikel(100, "Sofa", 31, 75, 350m, 5, 7);
            sofa.Ausgabe();

            Warenlager dasLager = new Warenlager();

            dasLager.AusgabeWarenliste();

            Console.WriteLine("Warenwert: {0:n} Euro", dasLager.WarenwertBerechnen());

            Console.ReadKey();
        }
    }
}
