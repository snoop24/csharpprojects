﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A12_Person_Adresse_Hund
{
    class Program
    {
        static void Main(string[] args)
        {
            //general loop
            do
            {
                // wipe screen
                Console.Clear();
                Console.SetWindowSize(80, 40);

                CPerson chefVonsGanze = new CPerson("der Große", "Karl", 1260);

                chefVonsGanze.Ausgabe();
                chefVonsGanze.GetAdresse().Ausgabe();

                chefVonsGanze.SetAdresse(new CAdresse("Mittelalter", "früh", "8.Jhd", "Mitteleuropa"));

                chefVonsGanze.Ausgabe();
                chefVonsGanze.GetAdresse().Ausgabe();








                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
