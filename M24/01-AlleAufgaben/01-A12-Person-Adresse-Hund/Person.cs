﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A12_Person_Adresse_Hund
{
    class CPerson
    {
        // class 
        private string nachname;
        private string vorname;
        private int alter;

        // relation
        CAdresse adresse;
        CHund hund;
        
        public CPerson(string nachname, string vorname, int alter)
        {
            this.nachname = nachname;
            this.vorname = vorname;
            this.alter = alter;
            adresse = new CAdresse();
            hund = new CHund();
        }

        public string GetNachname()
        {
            return nachname;
        }

        public void SetNachname(string nachname)
        {
            this.nachname = nachname;
        }

        public string GetVorname()
        {
            return vorname;
        }

        public int GetAlter()
        {
            return alter;
        }

        public CAdresse GetAdresse()
        {
            return adresse;
        }

        public void SetAdresse(CAdresse adresse)
        {
            this.adresse = adresse;
        }

        public CHund GetHund()
        {
            return hund;
        }

        public void SetHund(CHund hund)
        {
            this.hund = hund;
        }

        public void Ausgabe()
        {
            Console.WriteLine("{0}\n{1}\n{2}Jahre",nachname,vorname,alter);
            if (adresse != null) { adresse.Ausgabe(); }
            if (hund != null) { hund.Ausgabe(); }
        }


    }
}
