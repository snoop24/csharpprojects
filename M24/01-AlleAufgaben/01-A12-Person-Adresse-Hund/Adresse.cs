﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A12_Person_Adresse_Hund
{
    class CAdresse
    {
        string strasse;
        string hausnummer;
        string plz;
        string ort;

        public CAdresse(string strasse, string hausnummer, string plz, string ort)
        {
            this.strasse = strasse;
            this.hausnummer = hausnummer;
            this.plz = plz;
            this.ort = ort;
        }

        public CAdresse(): this("","","","")
        {
        }

        public string GetStrasse()
        {
            return strasse;
        }

        public string GetHausnummer()
        {
            return hausnummer;
        }

        public string GetPlz()
        {
            return plz;
        }

        public string GetOrt()
        {
            return ort;
        }

        public void Ausgabe()
        {
            Console.WriteLine("{0}\n{1}\n{2}\n{3}",strasse,hausnummer,plz,ort);

        }
    }
}
