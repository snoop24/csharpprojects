﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A06_PiggyBank
{
    class Program
    {
        static void Main(string[] args)
        {
            //general loop
            do
            {
                // wipe screen
                Console.Clear();

                // declare
                PiggyBank rosa = new PiggyBank("Rosa", 200);

                rosa.Shake();
                rosa.Add10Cents(20);
                rosa.Shake();
                rosa.Add10Cents(30);
                rosa.Shake();
                rosa.Add1Cents(20);
                rosa.Shake();
                rosa.Add1Cents(30);
                rosa.Shake();
                rosa.Add50Cents(20);
                rosa.Shake();
                rosa.Add50Cents(30);
                rosa.Shake();
                rosa.Add1Euros(20);
                rosa.Shake();
                rosa.Add1Euros(55);
                rosa.Shake();

                Console.WriteLine(rosa.IsBroken());
                Console.WriteLine(  rosa.BreakInto());
                rosa.Shake();

                // loop prompt
                Console.Write("... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
