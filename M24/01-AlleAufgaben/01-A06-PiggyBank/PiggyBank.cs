﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A06_PiggyBank
{
    class PiggyBank
    {
        private string name = "";
        private int oneCent = 0;
        private int tenCent = 0;
        private int fiftyCent = 0;
        private int oneEuro = 0;
        private int maxCoins = 100;

        private bool broken = false;

        public PiggyBank(string name, int maxCoins)
        {
            this.name = name;
            this.maxCoins = maxCoins;
        }

        public int Add1Cents(int count)
        {
            int diff = maxCoins - (oneCent + tenCent + fiftyCent + oneEuro);
            if (diff > 0)
            {
                oneCent += diff >= count ? count : diff;
                count = diff >= count ? 0 : count - diff;
            }
            return count;
        }

        public int Add10Cents(int count)
        {
            int diff = maxCoins - (oneCent + tenCent + fiftyCent + oneEuro);
            if (diff > 0)
            {
                tenCent += diff >= count ? count : diff;
                count = diff >= count ? 0 : count - diff;
            }
            return count;
        }

        public int Add50Cents(int count)
        {
            int diff = maxCoins - (oneCent + tenCent + fiftyCent + oneEuro);
            if (diff > 0)
            {
                fiftyCent += diff >= count ? count : diff;
                count = diff >= count ? 0 : count - diff;
            }
            return count;
        }

        public int Add1Euros(int count)
        {
            int diff = maxCoins - (oneCent + tenCent + fiftyCent + oneEuro);
            if (diff > 0)
            {
                oneEuro += diff >= count ? count : diff;
                count = diff >= count ? 0 : count - diff;
            }
            return count;
        }

        public void Shake()
        {
            string message;
            double full = (oneCent + tenCent + fiftyCent + oneEuro) * 1.0 / maxCoins;
            message = full == 1 ? "voll" : full >= 0.8 ? "fast voll" : full >= 0.6 ? "etwa zwei Drittel voll" : full >= 0.4 ? "etwa halb voll" : full >= 0.2 ? "etwa ein Drittel voll" : full > 0 ? "fast leer":"leer";
            Console.WriteLine("Das Sparschwein {0} ist {1}.",name, message);
        }

        public bool IsBroken()
        {
            return broken;
        }

        public int BreakInto()
        {
            int cent;
            cent = oneCent + tenCent*10 + fiftyCent*50 + oneEuro * 100;
            oneCent = 0;
            tenCent = 0;
            fiftyCent = 0;
            oneEuro = 0;
            broken = true;
            return cent;
        }
    }
}
