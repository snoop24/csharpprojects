﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A15_Koordinatensystem
{
    class CVektor
    {
        CPunkt start;
        CPunkt end;

        #region:Constructor

        public CVektor(CPunkt start, CPunkt end)
        {
            this.start = start;
            this.end = end;
        }

        #endregion

        #region:GetSet

        public CPunkt GetStart()
        {
            return start;
        }

        public CPunkt GetEnd()
        {
            return end;
        }

        #endregion

        #region:OtherMethods

        public double Laenge()
        {
            double laenge;
            laenge = Math.Sqrt(Math.Pow(start.GetX1() - end.GetX1(), 2) + Math.Pow(start.GetX2() - end.GetX2(), 2));
            return laenge;
        }

        #endregion
    }
}
