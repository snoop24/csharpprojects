﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A15_Koordinatensystem
{
    class Program
    {
        static void Main(string[] args)
        {

            CPunkt ursprung = new CPunkt(0, 0);
            CPunkt punkt1 = new CPunkt(1, 1);
            CPunkt punkt2 = new CPunkt(3, 4);
            CVektor vektor1 = new CVektor(ursprung,punkt1);
            CVektor vektor2 = new CVektor(ursprung,punkt2);
            CVektor vektor1_2 = new CVektor(punkt1, punkt2);

            CKreis kreis2_5 = new CKreis(punkt2, 5);
            Console.WriteLine("ursprung");
            Console.WriteLine("({0}/{1})", ursprung.GetX1(),ursprung.GetX2());
            Console.WriteLine("punkt1");
            Console.WriteLine("({0}/{1})", punkt1.GetX1(), punkt1.GetX2());
            Console.WriteLine("punkt2");
            Console.WriteLine("({0}/{1})", punkt2.GetX1(), punkt2.GetX2());

            Console.WriteLine();
            Console.WriteLine("ursprung zu punkt1 Länge: {0}", vektor1.Laenge());
            Console.WriteLine("ursprung zu punkt2 Länge: {0}", vektor2.Laenge());
            Console.WriteLine("punk1 zu punkt2 Länge: {0}",vektor1_2.Laenge());

            Console.WriteLine();
            Console.WriteLine("Kreis Radius {0} um ({1}/{2})", kreis2_5.GetRadius(),kreis2_5.GetMitte().GetX1(), kreis2_5.GetMitte().GetX2());
            Console.WriteLine("Umfang: "+kreis2_5.Umfang());
            Console.WriteLine("Inhalt: " + kreis2_5.Inhalt());

            //general loop
            do
            {
                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
