﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A15_Koordinatensystem
{
    class CKreis
    {
        CPunkt mitte;
        double radius;

        #region:Constructor

        public CKreis(CPunkt mitte, double radius)
        {
            this.mitte = mitte;
            this.radius = radius;
        }

        #endregion

        #region:GetSet

        public CPunkt GetMitte()
        {
            return mitte;
        }

        public double GetRadius()
        {
            return radius;
        }

        #endregion

        #region:OtherMethods

        public double Umfang()
        {
            double umfang;
            umfang = Math.PI * radius * radius;
            return umfang;
        }

        public double Inhalt()
        {
            double inhalt;
            inhalt = Math.PI * radius * 2;
            return inhalt;
        }

        #endregion
    }
}
