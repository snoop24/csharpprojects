﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A15_Koordinatensystem
{
    class CPunkt
    {
        double x1;
        double x2;

        #region:Constructor

        public CPunkt(double x1, double x2)
        {
            this.x1 = x1;
            this.x2 = x2;
        }

        public CPunkt()
            : this(0, 0) { }

        public CPunkt(CPunkt punkt)
            : this(punkt.GetX1(), punkt.GetX2()) { }

        #endregion

        #region:GetSet

        public double GetX1()
        {
            return x1;
        }

        public double GetX2()
        {
            return x2;
        }

        #endregion
    }
}
