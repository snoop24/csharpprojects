﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A10_Pruefsumme
{
    class Program
    {
        static void Main(string[] args)
        {
            //general loop
            do
            {
                // wipe screen
                Console.Clear();
                Console.SetWindowSize(80, 40);

                int[] test1 = new int[4] { 8, 7, 6, 3 };
                string test2 = "1111";

                Console.WriteLine("{0}:{1}", string.Join("",test1), Pruefsumme.CheckIdentNumber(test1));
                Console.WriteLine("{0}:{1}", test2, Pruefsumme.CheckIdentNumber(test2));

                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
