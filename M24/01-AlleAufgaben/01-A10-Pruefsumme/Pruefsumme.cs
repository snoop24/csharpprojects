﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A10_Pruefsumme
{
    class Pruefsumme
    {
        public static bool CheckIdentNumber(int[] numberArray)
        {
            Boolean checkIdentNumber = false;
            int querSumme = 0;
            for (int i = 0; i < numberArray.Length; i++)
            {
                querSumme += (numberArray.Length % 2 == i % 2 ? 2 : 1) * numberArray[i]- (((numberArray.Length % 2 == i % 2 ? 2 : 1) * numberArray[i] > 9) ? 9:0);
            }
            if (querSumme % 10 == 0)
                checkIdentNumber = true;
            return checkIdentNumber;
        }

        public static bool CheckIdentNumber(string number)
        {
            Boolean checkIdentNumber = false;

            if (int.TryParse(number, out int result))
            {
                int[] numberArray = new int[number.Length];
                for (int i = 0; i < number.Length; i++)
                {
                    numberArray[i] = number[i];
                }
                checkIdentNumber = CheckIdentNumber(numberArray);
            }
            else { checkIdentNumber = false; }

            return checkIdentNumber;
        }
    }
}
