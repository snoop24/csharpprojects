﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A02_Socke
{
    class Program
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                Socke loch = new Socke("Blau",false);
                Socke tennis = new Socke("Weiss");

                // method testing
                tennis.Wasche();
                tennis.Trockne();

                loch.Ausgabe();
                tennis.Ausgabe();
               

                // loop prompt
                Console.Write("... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
