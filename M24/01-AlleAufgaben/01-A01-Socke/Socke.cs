﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A02_Socke
{
    class Socke
    {
        private string farbe;
        private bool trocken = true;

        #region:Constructor

        public Socke(string farbe)
        {
            this.farbe = farbe;
            trocken = true;
        }

        public Socke(string farbe, bool trocken)
        {
            this.farbe = farbe;
            this.trocken = trocken;
        }

        #endregion

        #region:OtherMethods

        public void Wasche()
        {
            trocken = false;
        }

        public void Trockne()
        {
            trocken = true;
        }
        
        public void Ausgabe()
        {
            Console.WriteLine("Diese Socke mit der Farbe {0} ist {1}.",farbe, trocken?"trocken":"nass");
        }

        #endregion
    }
}
