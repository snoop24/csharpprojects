﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A09_Bruch
{
    class Program
    {
        static void Main(string[] args)
        {
            //general loop
            do
            {
                // wipe screen
                Console.Clear();
                Console.SetWindowSize(80, 40);

                Bruch b1 = new Bruch(9, 12);
                Bruch b2 = new Bruch(2, 6);


                Console.WriteLine("Ausgabe b1,b2");
                b1.Ausgabe();
                b2.Ausgabe();

                Console.WriteLine("Ausgabe b1,b2");
                b1.Kuerzen();b1.Ausgabe();
                b2.Kuerzen();b2.Ausgabe();

                Console.WriteLine("Ausgabe b1+b2 ,b1-b2");
                b1.Addiere(b2).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                b1.Subtrahiere(b2).Ausgabe();

                 b1 = new Bruch(3, 4);
                 b2 = new Bruch(1, 3);

                Console.WriteLine("Ausgabe b2+b1 ,b2-b1");
                b2.Addiere(b1).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                b2.Subtrahiere(b1).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                Console.WriteLine("Ausgabe b1+5 ,b1-5");
                b1.Addiere(5).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                b1.Subtrahiere(5).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                Console.WriteLine("Ausgabe b1*b2, b1/b2");
                b1.Multipliziere(b2).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                b1.Dividiere(b2).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                Console.WriteLine("Ausgabe b2*b1, b2/b1");
                b2.Multipliziere(b1).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                b2.Dividiere(b1).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                Console.WriteLine("Ausageb b1*5,b1/5");
                b1.Multipliziere(5).Ausgabe();

                b1 = new Bruch(3, 4);
                b2 = new Bruch(1, 3);

                b1.Dividiere(5).Ausgabe();

                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
