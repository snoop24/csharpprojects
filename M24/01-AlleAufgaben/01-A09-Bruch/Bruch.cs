﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A09_Bruch
{
    class Bruch
    {
        #region:Attributes

        private int zaehler;
        private int nenner;

        #endregion
        
        #region:Constructors

        public Bruch(int zaehler, int nenner)
        {
            this.zaehler = zaehler;
            this.nenner = nenner;
        }

        public Bruch(int zaehler)
        {
            this.zaehler = zaehler;
            nenner = 1;
        }

        #endregion

        #region:Methods

        public void Ausgabe()
        {
            Console.WriteLine("{0}/{1}", zaehler, nenner);
        }

        public Bruch GetKehrwert()
        {
            Bruch bruch = new Bruch(nenner, zaehler);
            return bruch;
        }

        public void Kuerzen()
        {
            // GgT bestimmen und teilen
            int z1 = zaehler;
            int z2 = nenner;

            while (z1 % z2 != 0)
            {
                z1 = z2 + 0 * (z2 = z1 % z2);
            }

            zaehler /= z2;
            nenner /= z2;

            // Format anpassen: -- zu ++ und +- zu -+
            if (zaehler < 0 && nenner < 0)
            {
                zaehler = -zaehler;
                nenner = -nenner;
            }
            else if (nenner < 0)
            {
                zaehler = -zaehler;
                nenner = -nenner;
            }

        }

        public Bruch Addiere(Bruch bruch)
        {
            zaehler = zaehler * bruch.nenner + nenner * bruch.zaehler;
            nenner = nenner * bruch.nenner;
            Kuerzen();
            return this;
        }

        public Bruch Addiere(int zahl)
        {
            Addiere(new Bruch(zahl, 1));
            return this;
        }

        public Bruch Subtrahiere(Bruch bruch)
        {
           Addiere(bruch.Multipliziere(-1));
           return this;
        }

        public Bruch Subtrahiere(int zahl)
        {
            Addiere(new Bruch(-zahl, 1));
            return this;
        }

        public Bruch Multipliziere(Bruch bruch)
        {
            zaehler = zaehler * bruch.zaehler;
            nenner = nenner * bruch.nenner;
            Kuerzen();
            return this;
        }

        public Bruch Multipliziere(int zahl)
        {
            Multipliziere(new Bruch(zahl, 1));
            return this;
        }

        public Bruch Dividiere(Bruch bruch)
        {
            Multipliziere(bruch.GetKehrwert());
            return this;
        }

        public Bruch Dividiere(int zahl)
        {
            Multipliziere(new Bruch(1, zahl));
            return this;
        }

        #endregion

    }
}
