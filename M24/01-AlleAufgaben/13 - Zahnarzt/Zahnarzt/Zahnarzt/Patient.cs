﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahnarzt
{
    class Patient
    {
        // Eigenschaften
        int patientenNr;
        string name;
        DateTime geburtsdatum;
        bool karteVorgelegt;

        // statische Eigenschaft
        static int fortlaufendeNummer;

        // Assioziationen
        Krankenkasse kasse;
        Adresse anschrift;

        // Konstruktoren
        public Patient(string name, Adresse anschrift, DateTime geburtsDatum, Krankenkasse kasse)
        {
            this.name = name;
            this.patientenNr = ++fortlaufendeNummer;
            this.geburtsdatum = geburtsDatum;
            this.karteVorgelegt = true;
            this.anschrift = anschrift;
            this.kasse = kasse;
        }

        public Patient(string name, Adresse anschrift, string geburtsDatum, Krankenkasse kasse)
            : this(name, anschrift, Convert.ToDateTime(geburtsDatum), kasse)
        {
        }

        public void Ausgabe()
        {
            Console.WriteLine("Patientennummer: {2}\n{0}, geb: {1}", name, geburtsdatum.ToShortDateString(), patientenNr);

            Console.WriteLine("Anschrift:");
            anschrift.Ausgabe();

            Console.WriteLine();
            Console.WriteLine("Versichert bei:");
            kasse.Ausgabe();
            Console.WriteLine("{0}", karteVorgelegt ? "Karte wurde vorgelegt": "Karte wurde noch nicht vorgelegt");

        }

        public string GetName()
        {
            return name;
        }

        public void SetAdresse(Adresse anschrift)
        {
            this.anschrift = anschrift;
        }

    }
}
