﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahnarzt
{
    class Krankenkasse
    {
        // Eigschaften
        string name;
        int kassenNr;

        public Krankenkasse(string name, int kassenNr)
        {
            this.name = name;
            this.kassenNr = kassenNr;
        }

        public string GetName()
        {
            return name;
        }

        public void Ausgabe()
        {
            Console.WriteLine("\nKrankenkasse: {0} ({1})", name, kassenNr);
        }
    }
}
