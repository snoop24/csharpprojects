﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahnarzt
{
    class Adresse
    {
        // Eigenschaften
        string strasse;
        string hausnummer;
        string plz;
        string ort;

        // Konstruktor
        public Adresse(string strasse, string hausnummer, string plz, string ort)
        {
            this.strasse = strasse;
            this.hausnummer = hausnummer;
            this.plz = plz;
            this.ort = ort;

        }


       public void Ausgabe()
        {
            Console.WriteLine("\n{0} {1}\n{2} {3}", strasse, hausnummer, plz, ort);
        }
    }
}
