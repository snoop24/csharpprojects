﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zahnarzt
{
    class Program
    {
        static void Main(string[] args)
        {
            Krankenkasse aok = new Krankenkasse("AOK", 986124837);
            Krankenkasse bkk = new Krankenkasse("BKK", 2345678);

            Adresse konradzuse = new Adresse("Konrad Zuse Straße", "12", "44801", "Bochum");

            DateTime geburtsDatum = Convert.ToDateTime("18.10.1997");

            Patient karl = new Patient("Karl", konradzuse, geburtsDatum, aok);

            karl.Ausgabe();


            Patient lisa = new Patient("Lisa", konradzuse, "17.10.1996", bkk);

            lisa.Ausgabe();

        }
    }
}
