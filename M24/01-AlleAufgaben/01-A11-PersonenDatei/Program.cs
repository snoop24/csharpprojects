﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _01_A11_PersonenDatei
{
    class Program
    {
        static void Main(string[] args)
        {
            //general loop
            do
            {
                // wipe screen
                Console.Clear();
                Console.SetWindowSize(80, 40);


                // declare
                string pfad = Directory.GetCurrentDirectory() + @"\\Personen.txt";
                double alterSchnitt = 0;

                // get count of lines in file
                int count = File.ReadLines(pfad).Count();

                // make array
                Person[] gruppe = new Person[count];

                // start reader
                StreamReader inputDatei = new StreamReader(pfad);

                // read lines
                for (int i = 0; i < count; i++)
                {
                    string[] temp = inputDatei.ReadLine().Split(':');

                    if (temp.Length == 2 && DateTime.TryParse(temp[1],out DateTime gebTag))
                    {
                        gruppe[i] = new Person(temp[0], gebTag);
                        alterSchnitt += gruppe[i].GetAge()*1.0 / count;
                    }
                    else
                    {
                        gruppe[i] = new Person(temp[0], DateTime.Today);
                    }
                }

                Console.WriteLine("\nAltersdurchschnitt: {0} Jahre\n",Math.Round(alterSchnitt,2));
                for (int i = 0; i < gruppe.Length; i++)
                {
                    Console.WriteLine("{0,20} : {1} : {2}", gruppe[i].GetName(), gruppe[i].GetGebDat().ToShortDateString(), gruppe[i].GetAge());
                }
               

                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
