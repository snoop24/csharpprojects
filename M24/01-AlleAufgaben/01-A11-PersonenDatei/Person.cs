﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A11_PersonenDatei
{
    class Person
    {
        private string name;
        private DateTime gebDat;

        public Person(string name, DateTime gebDat)
        {
            this.name = name;
            this.gebDat = gebDat;
        }

        public string GetName()
        {
            return name;
        }

        public DateTime GetGebDat()
        {
            return gebDat;
        }

        public int GetAge()
        {
            int age = DateTime.Today.Year - gebDat.Year;
            age -= DateTime.Today < gebDat.AddYears(age) ? 1 : 0;
            return age;
        }
    }
}
