﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A08_Boxer
{
    class Program
    {
        static void Main(string[] args)
        { 
            //general loop
            do
            {
                // wipe screen
                Console.Clear();
                Console.SetWindowSize(80, 60);

                // declare
                Boxer b1 = new Boxer("Horst");
                Boxer b2 = new Boxer("Detlef");
                int schlagB1;
                int schlagB2;
                string message;

                Console.WriteLine("\n {0}:{1,3} Stärke\t{2}:{3,3} Stärke\n", b1.GetName(), b1.GetStaerke(), b2.GetName(), b2.GetStaerke());
                Console.WriteLine("\n {0}:{1} Vitalität\t{2}:{3} Vitalität\n", b1.GetName(), b1.GetVitality(), b2.GetName(), b2.GetVitality());

                do
                {
                    schlagB1 = b1.Schlagen(b2);
                    schlagB2= b2.Schlagen(b1);
                    message = string.Format("{0} trifft {1,6} {2} trifft {3,6}", b1.GetName(),schlagB1 ==0 ? "nicht." : "-"+ schlagB1+".", b2.GetName(), schlagB2 == 0 ? "nicht" : "-" + schlagB2 + ".");
                    Console.WriteLine(message);
                } while (b1.GetVitality() > 0 && b2.GetVitality() > 0);

                Console.WriteLine("\n {0}:{1} Vitalität\t{2}:{3} Vitalität\n", b1.GetName() , b1.GetVitality(), b2.GetName(), b2.GetVitality());

                Console.WriteLine("\n   " + (b1.GetVitality() > 0 && 1> b2.GetVitality()  ? b1.GetName() + " hat gewonnen." : (b1.GetVitality() < 1 && 0 < b2.GetVitality() ? b2.GetName() + " hat gewonnen." : "Unentschieden. Simultaner KO Treffer")));

                // loop prompt
                Console.Write("\n\n... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
