﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A08_Boxer
{
    class Boxer
    {
        static Random rnd = new Random();

        string name = "";
        int vitality = 100;
        int staerke;

        public int Schlagen(Boxer gegner)
        {
            int schlagen = 0;
            if (rnd.Next(0, 2) == 0)
            {
                schlagen =0;
            }
            else
            {
                schlagen = rnd.Next(1, 11) + staerke;
                gegner.SetVitality(schlagen);
            }
            return schlagen;
        }

        #region:ConstructGetSet

        public Boxer(string name)
        {
            this.name = name;
            staerke = rnd.Next(1, 11);
        }
        
        public void SetVitality(int hit)
        {
            vitality = hit > vitality ? 0 : vitality - hit;
        }

        public int GetVitality()
        {
            return vitality;
        }

        public string GetName()
        {
            return name;
        }

        public int GetStaerke()
        {
            return staerke;
        }


        #endregion
    }
}
