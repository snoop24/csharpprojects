﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A05_Filiale
{
    class Program
    {
        static void Main(string[] args)
        {
            //general loop
            do
            {
                // wipe screen
                Console.Clear();

                // declare
                Filiale kaschemme = new Filiale("Heidis Kostbarkeiten", 5,20m);

                // method testing
                kaschemme.FilialInfo();
                for (int i = 0; i < 3; i++)
                {
                    if (kaschemme.Einkauf())
                    { Console.WriteLine("Eingekauft"); }
                    else
                    { Console.WriteLine("Kein Geld. Alles im Laden stehen lassen."); }
                }

                kaschemme.FilialInfo();

                for (int i = 0; i < 8; i++)
                {
                    if (kaschemme.Verkauf())
                    { Console.WriteLine("Verkauft"); }
                    else
                    { Console.WriteLine("Kein Ware da. Scheiss Einkauf!"); }
                }

                kaschemme.FilialInfo();

                // loop prompt
                Console.Write("... zum Beenden ESC!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
