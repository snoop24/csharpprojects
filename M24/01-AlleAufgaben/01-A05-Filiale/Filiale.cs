﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_A05_Filiale
{
    class Filiale
    {
        private string name = "";
        private int warenBestand = 0;
        private decimal kasse = 0;

        public Filiale(string name,int warenBestand, decimal kasse)
        {
            this.name = name;
            this.warenBestand = warenBestand > 0 ? warenBestand : 0;
            this.kasse = kasse > 0 ? kasse :0;
        }

        public void FilialInfo()
        {
            Console.WriteLine("Filiae {0} hat {1} Stück im Lager und {2} Geld in der Kasse.",name, warenBestand,kasse);
        }

        public bool Einkauf()
        {
            bool eingekauft;
            if (kasse >= 10)
            {
                warenBestand++;
                kasse -= 10;
                eingekauft = true;
            }
            else
            {
                eingekauft = false;
            }
            return eingekauft;
        }

        public bool Verkauf()
        {
            bool verkauft;
            if (warenBestand > 0)
            {
                warenBestand--;
                kasse += 20;
                verkauft = true;
            }
            else
            {
                verkauft = false;
            }
            return verkauft;
        }
    }
}
