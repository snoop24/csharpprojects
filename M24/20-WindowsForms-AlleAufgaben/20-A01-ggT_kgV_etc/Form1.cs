﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _20_A01_ggT_kgV_etc
{
    public partial class Form1 : Form
    {
        public const string vergleich = "abcdefghijklmnopqrstuvwxyzäöüß0123456789";

        public Form1()
        {
            InitializeComponent();
        }

        #region:Aufgabe1_ggT_kgV
        private void ButtonA1Calc_Click(object sender, EventArgs e)
        {
            int a, b, ggT, kgV;
            if (int.TryParse(textBoxA1Zahl1.Text, out a) && int.TryParse(textBoxA1Zahl2.Text, out b) && a > 0 && b > 0)
            {
                ggT = CalcGgT(a, b);
                kgV = a * b / ggT;
                labelA1ggT.Text = "ggT = " + ggT;
                labelA1kgV.Text = "kgV = " + kgV;
            }
            else
            {
                labelA1ggT.Text = "Haha! Ganze positive Zahlen bitte.";
                labelA1kgV.Text = "";
            }
        }

        private void ButtonA1Clear_Click(object sender, EventArgs e)
        {
            textBoxA1Zahl1.Text = "1";
            textBoxA1Zahl2.Text = "1";
            labelA1ggT.Text = "ggT = 1";
            labelA1kgV.Text = "kgV = 1";
        }

        private int CalcGgT(int a, int b)
        {
            while (a % b != 0)
            {
                a = b + 0 * (b = a % b);
            }

            return b;
        }
        #endregion

        #region:Aufgabe2_Palindrom

        private void ButtonA2Palindrom_Click(object sender, EventArgs e)
        {
            labelA2Ausgabe.Text = " ist " + (IsPalindrom(CleanToLower(textBoxA2Eingabe.Text)) ? "" : "k") + "ein Palindrom.";

        }

        private void ButtonA2Clear_Click(object sender, EventArgs e)
        {
            textBoxA2Eingabe.Text = "Palindromzeichenfolge ...";
            labelA2Ausgabe.Text = "";
        }

        private bool IsPalindrom(string test)
        {
            bool isPalindrom = true;

            for (int i = 0; i < test.Length / 2 ; i++)
            {
                if (test[i] != test[test.Length - 1 - i]) { isPalindrom = false; }
            }
            
            return isPalindrom;
        }
        #endregion

        #region:Aufgabe3_Frequenzanalyse
        private void ButtonA3Analyze_Click(object sender, EventArgs e)
        {
            List<string> ausgabe = FrequenzAnalyse(textBoxA3Eingabe.Text);

            labelA3Ausgabe.Text = ausgabe.ElementAt(ausgabe.Count - 1);

            ausgabe.RemoveAt(ausgabe.Count - 1);

            listBoxA3Ausgabe.DataSource = ausgabe;
            listBoxA3Ausgabe.ClearSelected();
        }

        private void ButtonA3Clear_Click(object sender, EventArgs e)
        {
            textBoxA3Eingabe.Text = "Analysetext ...";
            labelA3Ausgabe.Text = "";
            listBoxA3Ausgabe.DataSource = null;
        }

        private List<string> FrequenzAnalyse(string test)
        {
            // clean
            test = CleanToLower(test);

            // declare
            List<string> analyse = new List<string>();
            int[] letterCount = new int[vergleich.Length];
            int letters = 0;
            int count = 0;

            // count 
            foreach (char letter in test)
            {
                int i = 0;
                i = vergleich.IndexOf(letter);
                if (i > -1)
                {
                    letterCount[i]++;
                }
            }

            // create output
            for (int i = 0; i < letterCount.Length; i++)
            {
                if (letterCount[i] > 0)
                {
                    analyse.Add(string.Format("{0}={1}", vergleich[i], letterCount[i]));
                    letters++;
                    count += letterCount[i];
                }
            }
            analyse.Add(string.Format("{1} insgesamt : {0} unterschiedliche", letters, count));

            // return
            return analyse;
        }
        #endregion

        #region:Tools
        private string CleanToLower(string test)
        {
            StringBuilder temp = new StringBuilder();

            foreach (char letter in test.ToLower())
            {
                if (vergleich.Contains(letter))
                {
                    temp.Append(letter);
                }
            }
            return temp.ToString();
        }
        #endregion

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt)
            {
                switch (e.KeyCode)
                {
                    case Keys.B:
                        ButtonA1Calc_Click(sender, e);
                        break;
                    case Keys.P:
                        ButtonA2Palindrom_Click(sender, e);
                        break;
                    case Keys.A:
                        ButtonA3Analyze_Click(sender, e);
                        break;
                    case Keys.L:
                        switch (ActiveControl.Parent.Name)
                        {
                            case "groupBoxA1":
                                ButtonA1Clear_Click(sender, e);
                                break;
                            case "groupBoxA2":
                                ButtonA2Clear_Click(sender, e);
                                break;
                            case "groupBoxA3":
                                ButtonA3Clear_Click(sender, e);
                                break;
                        }
                        break;
                }
            }
        }
    }
}