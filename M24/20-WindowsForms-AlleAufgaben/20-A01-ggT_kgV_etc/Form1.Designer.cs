﻿namespace _20_A01_ggT_kgV_etc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxA1 = new System.Windows.Forms.GroupBox();
            this.labelA1kgV = new System.Windows.Forms.Label();
            this.labelA1ggT = new System.Windows.Forms.Label();
            this.textBoxA1Zahl2 = new System.Windows.Forms.TextBox();
            this.textBoxA1Zahl1 = new System.Windows.Forms.TextBox();
            this.buttonA1Clear = new System.Windows.Forms.Button();
            this.buttonA1Calc = new System.Windows.Forms.Button();
            this.groupBoxA2 = new System.Windows.Forms.GroupBox();
            this.labelA2Ausgabe = new System.Windows.Forms.Label();
            this.textBoxA2Eingabe = new System.Windows.Forms.TextBox();
            this.buttonA2Clear = new System.Windows.Forms.Button();
            this.buttonA2Palindrom = new System.Windows.Forms.Button();
            this.groupBoxA3 = new System.Windows.Forms.GroupBox();
            this.labelA3Ausgabe = new System.Windows.Forms.Label();
            this.textBoxA3Eingabe = new System.Windows.Forms.TextBox();
            this.buttonA3Clear = new System.Windows.Forms.Button();
            this.buttonA3Analyze = new System.Windows.Forms.Button();
            this.listBoxA3Ausgabe = new System.Windows.Forms.ListBox();
            this.groupBoxA1.SuspendLayout();
            this.groupBoxA2.SuspendLayout();
            this.groupBoxA3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxA1
            // 
            this.groupBoxA1.Controls.Add(this.labelA1kgV);
            this.groupBoxA1.Controls.Add(this.labelA1ggT);
            this.groupBoxA1.Controls.Add(this.textBoxA1Zahl2);
            this.groupBoxA1.Controls.Add(this.textBoxA1Zahl1);
            this.groupBoxA1.Controls.Add(this.buttonA1Clear);
            this.groupBoxA1.Controls.Add(this.buttonA1Calc);
            this.groupBoxA1.Location = new System.Drawing.Point(12, 12);
            this.groupBoxA1.Name = "groupBoxA1";
            this.groupBoxA1.Size = new System.Drawing.Size(416, 94);
            this.groupBoxA1.TabIndex = 0;
            this.groupBoxA1.TabStop = false;
            this.groupBoxA1.Text = "A1: ggT - kgV";
            // 
            // labelA1kgV
            // 
            this.labelA1kgV.AutoSize = true;
            this.labelA1kgV.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelA1kgV.Location = new System.Drawing.Point(202, 64);
            this.labelA1kgV.Name = "labelA1kgV";
            this.labelA1kgV.Size = new System.Drawing.Size(56, 17);
            this.labelA1kgV.TabIndex = 5;
            this.labelA1kgV.Text = "kgV = 1";
            // 
            // labelA1ggT
            // 
            this.labelA1ggT.AutoSize = true;
            this.labelA1ggT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelA1ggT.Location = new System.Drawing.Point(6, 64);
            this.labelA1ggT.Name = "labelA1ggT";
            this.labelA1ggT.Size = new System.Drawing.Size(57, 17);
            this.labelA1ggT.TabIndex = 4;
            this.labelA1ggT.Text = "ggT = 1";
            // 
            // textBoxA1Zahl2
            // 
            this.textBoxA1Zahl2.Location = new System.Drawing.Point(6, 41);
            this.textBoxA1Zahl2.Name = "textBoxA1Zahl2";
            this.textBoxA1Zahl2.Size = new System.Drawing.Size(149, 20);
            this.textBoxA1Zahl2.TabIndex = 1;
            this.textBoxA1Zahl2.Text = "1";
            // 
            // textBoxA1Zahl1
            // 
            this.textBoxA1Zahl1.Location = new System.Drawing.Point(6, 19);
            this.textBoxA1Zahl1.Name = "textBoxA1Zahl1";
            this.textBoxA1Zahl1.Size = new System.Drawing.Size(149, 20);
            this.textBoxA1Zahl1.TabIndex = 0;
            this.textBoxA1Zahl1.Text = "1";
            // 
            // buttonA1Clear
            // 
            this.buttonA1Clear.Location = new System.Drawing.Point(286, 19);
            this.buttonA1Clear.Name = "buttonA1Clear";
            this.buttonA1Clear.Size = new System.Drawing.Size(124, 42);
            this.buttonA1Clear.TabIndex = 3;
            this.buttonA1Clear.Text = "&Löschen";
            this.buttonA1Clear.UseVisualStyleBackColor = true;
            // 
            // buttonA1Calc
            // 
            this.buttonA1Calc.Location = new System.Drawing.Point(161, 19);
            this.buttonA1Calc.Name = "buttonA1Calc";
            this.buttonA1Calc.Size = new System.Drawing.Size(119, 42);
            this.buttonA1Calc.TabIndex = 2;
            this.buttonA1Calc.Text = "&Berechnen";
            this.buttonA1Calc.UseVisualStyleBackColor = true;
            this.buttonA1Calc.Click += new System.EventHandler(this.ButtonA1Calc_Click);
            // 
            // groupBoxA2
            // 
            this.groupBoxA2.Controls.Add(this.labelA2Ausgabe);
            this.groupBoxA2.Controls.Add(this.textBoxA2Eingabe);
            this.groupBoxA2.Controls.Add(this.buttonA2Clear);
            this.groupBoxA2.Controls.Add(this.buttonA2Palindrom);
            this.groupBoxA2.Location = new System.Drawing.Point(12, 112);
            this.groupBoxA2.Name = "groupBoxA2";
            this.groupBoxA2.Size = new System.Drawing.Size(416, 104);
            this.groupBoxA2.TabIndex = 6;
            this.groupBoxA2.TabStop = false;
            this.groupBoxA2.Text = "A2: Palindrom";
            // 
            // labelA2Ausgabe
            // 
            this.labelA2Ausgabe.AutoSize = true;
            this.labelA2Ausgabe.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelA2Ausgabe.Location = new System.Drawing.Point(6, 42);
            this.labelA2Ausgabe.Name = "labelA2Ausgabe";
            this.labelA2Ausgabe.Size = new System.Drawing.Size(0, 17);
            this.labelA2Ausgabe.TabIndex = 8;
            // 
            // textBoxA2Eingabe
            // 
            this.textBoxA2Eingabe.Location = new System.Drawing.Point(6, 19);
            this.textBoxA2Eingabe.Name = "textBoxA2Eingabe";
            this.textBoxA2Eingabe.Size = new System.Drawing.Size(404, 20);
            this.textBoxA2Eingabe.TabIndex = 7;
            this.textBoxA2Eingabe.Text = "Palindromzeichenfolge...";
            // 
            // buttonA2Clear
            // 
            this.buttonA2Clear.Location = new System.Drawing.Point(205, 71);
            this.buttonA2Clear.Name = "buttonA2Clear";
            this.buttonA2Clear.Size = new System.Drawing.Size(205, 28);
            this.buttonA2Clear.TabIndex = 10;
            this.buttonA2Clear.Text = "&Löschen";
            this.buttonA2Clear.UseVisualStyleBackColor = true;
            this.buttonA2Clear.Click += new System.EventHandler(this.ButtonA2Clear_Click);
            // 
            // buttonA2Palindrom
            // 
            this.buttonA2Palindrom.Location = new System.Drawing.Point(6, 71);
            this.buttonA2Palindrom.Name = "buttonA2Palindrom";
            this.buttonA2Palindrom.Size = new System.Drawing.Size(193, 28);
            this.buttonA2Palindrom.TabIndex = 9;
            this.buttonA2Palindrom.Text = "&Palindrom?";
            this.buttonA2Palindrom.UseVisualStyleBackColor = true;
            this.buttonA2Palindrom.Click += new System.EventHandler(this.ButtonA2Palindrom_Click);
            // 
            // groupBoxA3
            // 
            this.groupBoxA3.Controls.Add(this.labelA3Ausgabe);
            this.groupBoxA3.Controls.Add(this.textBoxA3Eingabe);
            this.groupBoxA3.Controls.Add(this.buttonA3Clear);
            this.groupBoxA3.Controls.Add(this.buttonA3Analyze);
            this.groupBoxA3.Controls.Add(this.listBoxA3Ausgabe);
            this.groupBoxA3.Location = new System.Drawing.Point(12, 222);
            this.groupBoxA3.Name = "groupBoxA3";
            this.groupBoxA3.Size = new System.Drawing.Size(416, 191);
            this.groupBoxA3.TabIndex = 11;
            this.groupBoxA3.TabStop = false;
            this.groupBoxA3.Text = "A3: Frequenzanalyse";
            // 
            // labelA3Ausgabe
            // 
            this.labelA3Ausgabe.AutoSize = true;
            this.labelA3Ausgabe.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelA3Ausgabe.Location = new System.Drawing.Point(6, 79);
            this.labelA3Ausgabe.MaximumSize = new System.Drawing.Size(410, 0);
            this.labelA3Ausgabe.Name = "labelA3Ausgabe";
            this.labelA3Ausgabe.Size = new System.Drawing.Size(0, 17);
            this.labelA3Ausgabe.TabIndex = 13;
            // 
            // textBoxA3Eingabe
            // 
            this.textBoxA3Eingabe.Location = new System.Drawing.Point(6, 19);
            this.textBoxA3Eingabe.Multiline = true;
            this.textBoxA3Eingabe.Name = "textBoxA3Eingabe";
            this.textBoxA3Eingabe.Size = new System.Drawing.Size(404, 57);
            this.textBoxA3Eingabe.TabIndex = 12;
            this.textBoxA3Eingabe.Text = "Analysetext ...";
            // 
            // buttonA3Clear
            // 
            this.buttonA3Clear.Location = new System.Drawing.Point(205, 157);
            this.buttonA3Clear.Name = "buttonA3Clear";
            this.buttonA3Clear.Size = new System.Drawing.Size(205, 28);
            this.buttonA3Clear.TabIndex = 15;
            this.buttonA3Clear.Text = "&Löschen";
            this.buttonA3Clear.UseVisualStyleBackColor = true;
            this.buttonA3Clear.Click += new System.EventHandler(this.ButtonA3Clear_Click);
            // 
            // buttonA3Analyze
            // 
            this.buttonA3Analyze.Location = new System.Drawing.Point(6, 157);
            this.buttonA3Analyze.Name = "buttonA3Analyze";
            this.buttonA3Analyze.Size = new System.Drawing.Size(193, 28);
            this.buttonA3Analyze.TabIndex = 14;
            this.buttonA3Analyze.Text = "&Analysieren";
            this.buttonA3Analyze.UseVisualStyleBackColor = true;
            this.buttonA3Analyze.Click += new System.EventHandler(this.ButtonA3Analyze_Click);
            // 
            // listBoxA3Ausgabe
            // 
            this.listBoxA3Ausgabe.ColumnWidth = 40;
            this.listBoxA3Ausgabe.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxA3Ausgabe.FormattingEnabled = true;
            this.listBoxA3Ausgabe.Location = new System.Drawing.Point(6, 99);
            this.listBoxA3Ausgabe.MultiColumn = true;
            this.listBoxA3Ausgabe.Name = "listBoxA3Ausgabe";
            this.listBoxA3Ausgabe.Size = new System.Drawing.Size(404, 56);
            this.listBoxA3Ausgabe.TabIndex = 16;
            this.listBoxA3Ausgabe.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 423);
            this.Controls.Add(this.groupBoxA3);
            this.Controls.Add(this.groupBoxA2);
            this.Controls.Add(this.groupBoxA1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ggT, kgV etc.";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.groupBoxA1.ResumeLayout(false);
            this.groupBoxA1.PerformLayout();
            this.groupBoxA2.ResumeLayout(false);
            this.groupBoxA2.PerformLayout();
            this.groupBoxA3.ResumeLayout(false);
            this.groupBoxA3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxA1;
        private System.Windows.Forms.TextBox textBoxA1Zahl2;
        private System.Windows.Forms.TextBox textBoxA1Zahl1;
        private System.Windows.Forms.Button buttonA1Clear;
        private System.Windows.Forms.Button buttonA1Calc;
        private System.Windows.Forms.GroupBox groupBoxA2;
        private System.Windows.Forms.Label labelA2Ausgabe;
        private System.Windows.Forms.TextBox textBoxA2Eingabe;
        private System.Windows.Forms.Button buttonA2Clear;
        private System.Windows.Forms.Button buttonA2Palindrom;
        private System.Windows.Forms.Label labelA1ggT;
        private System.Windows.Forms.Label labelA1kgV;
        private System.Windows.Forms.GroupBox groupBoxA3;
        private System.Windows.Forms.Label labelA3Ausgabe;
        private System.Windows.Forms.TextBox textBoxA3Eingabe;
        private System.Windows.Forms.Button buttonA3Clear;
        private System.Windows.Forms.Button buttonA3Analyze;
        private System.Windows.Forms.ListBox listBoxA3Ausgabe;
    }
}

