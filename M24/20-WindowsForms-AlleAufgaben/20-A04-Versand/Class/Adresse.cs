﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_A04_Versand
{
    class Adresse
    {
        
        #region:property
        public string Nachname { get; set; }
        public string Vorname { get; set; }
        public string StrasseHsNr { get; set; }
        public string PlzOrt { get; set; }
        public string Land { get; set; }
        public string FullInfo
        {
            get {
                return Nachname + ";" +
                  Vorname + ";" +
                  StrasseHsNr + ";"  +
                  PlzOrt + ";"  +
                  Land;
            }
        }
        public string DisplayInfo
        {
            get
            {
                return Nachname + ", " +
                  Vorname + " | " +
                  StrasseHsNr + " | " +
                  PlzOrt + " | " +
                  Land;
            }
        }

        #endregion

        #region:constructor
        public Adresse(string nachname, string vorname, string strasseHsNr, string plzOrt, string land = "Deutschland")
        {
            Nachname = nachname;
            Vorname = vorname;
            StrasseHsNr = strasseHsNr;
            PlzOrt = plzOrt;
            Land = land;
        }
        #endregion

        #region:other methods
        public bool IstGueltig()
        {
            bool istGueltig = false;

            if (Nachname.Length > 0 && Vorname.Length > 0 && StrasseHsNr.Length > 0 && PlzOrt.Length > 0 && Land.Length > 0)
            {
                istGueltig = true;
            }

            return istGueltig;
        }

        public override string ToString()
        {
            string toString = string.Format(
                "{1} {0}\n" +
                "{2}\n" +
                "{3}\n" +
                "{4}\n"
                , Nachname, Vorname, StrasseHsNr, PlzOrt, Land
                );

            return toString;
        }
        #endregion
    }
}
