﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_A04_Versand
{
    class Postsendung
    {
        #region:attribute
        private Adresse absender;
        private Adresse empfaenger;
        private bool zugestellt;
        #endregion

        #region:property
        public string ID { get; }
        public bool Zugestellt
        {
            get
            {
                return zugestellt;
            }
            set
            {
                zugestellt = value;
                if (value && this is VerfolgbaresPaket) { ((VerfolgbaresPaket)this).SetzeStation(this.Absender.PlzOrt); }
            }
        }
        public Adresse Absender
        {
            get { return absender; }
            set { if (value.IstGueltig()) absender = value; }
        }
        public Adresse Empfaenger
        {
            get { return empfaenger; }
            set { if (value.IstGueltig()) empfaenger = value; }
        }
        public string FullInfo
        {
            get
            {
                string fullInfo = ID + ";" + Zugestellt + ";" + Absender.FullInfo + ";" + Empfaenger.FullInfo + ";";

                if (this is Brief)
                {
                    fullInfo += (int)((Brief)this).Kategorie + ";;;";
                }
                else if (this is VerfolgbaresPaket)
                {
                    fullInfo += ";" + ((VerfolgbaresPaket)this).Gewicht + ";" + ((VerfolgbaresPaket)this).Versichert + ";True";
                }
                else if (this is Paket)
                {
                    fullInfo += ";" + ((Paket)this).Gewicht + ";" + ((Paket)this).Versichert + ";False";
                }
                else
                {
                    fullInfo = "no such shipment type";
                }

                return fullInfo;
            }
        }
        public string ListInfo
        {
            get
            {
                string partInfo = "ID: " + ID + " | " + (Zugestellt ? "" : "nicht ") + "versand | " + Absender.Nachname + " - " + Absender.PlzOrt + " -> " + Empfaenger.Nachname + " - " + Empfaenger.PlzOrt + " | ";

                if (this is Brief)
                {
                    partInfo += ((Brief)this).Kategorie;
                }
                else
                {
                    partInfo += "Paket : Gewicht " + ((Paket)this).Gewicht + " kg, " + (((Paket)this).Versichert ? "" : "un") + "versichert" + ((this is VerfolgbaresPaket) ? ", verfolgbar" : "");
                }

                return partInfo;
            }
        }
        public string DetailsInfo
        {
            get
            {
                string detailsInfo = "SendungsID: " + ID + " \n\n";

                if (this is Brief)
                {
                    detailsInfo += ((Brief)this).Kategorie + "\n";
                }
                else
                {
                    detailsInfo += ((this is VerfolgbaresPaket) ? "verfolgbares " : "") + "Paket, Gewicht " + ((Paket)this).Gewicht + " kg,\n" + (((Paket)this).Versichert ? "" : "un") + "versichert\n";
                }

                detailsInfo += (Zugestellt ? "" : "nicht ") + "versand";

                return detailsInfo;
            }
        }
        #endregion

        #region:constructor
        public Postsendung(string id, Adresse absender, Adresse empfaenger)
        {
            ID = id;
            this.absender = absender;
            this.empfaenger = empfaenger;
        }
        #endregion

        #region:other methods
        public override string ToString()
        {
            string toString = string.Format(
                "ID:{0}\n" +
                "Ab:\n" +
                "{1}" +
                "An:\n" +
                "{2}"
                , ID, absender.ToString(), empfaenger.ToString()
                );

            return toString;
        }
        #endregion
    }
}
