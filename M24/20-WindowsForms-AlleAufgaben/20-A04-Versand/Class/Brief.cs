﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_A04_Versand
{
    class Brief:Postsendung
    {
        #region:enumeration
        internal enum BriefKategorie { Standard, Eilbrief, Einschreiben }
        #endregion

        #region:property
        public BriefKategorie Kategorie { get; set; }
        #endregion

        #region:constructor
        public Brief(string id, Adresse absender, Adresse empfaenger, BriefKategorie kategorie)
            : base(id, absender,empfaenger)
        {
            Kategorie = kategorie;
        }
        #endregion

        #region:other methods
        public override string ToString()
        {
            string toString = string.Format(
                "{0}" +
                "Art: {1}\n"
                , base.ToString(), Kategorie.ToString()
                );

            return toString;
        }
        #endregion
    }
}
