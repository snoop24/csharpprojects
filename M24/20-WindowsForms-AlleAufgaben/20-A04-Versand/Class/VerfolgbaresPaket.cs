﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_A04_Versand
{
    class VerfolgbaresPaket : Paket
    {

        #region:property
        public List<Station> Stationen { get; } = new List<Station>();
        #endregion

        #region:constructor
        public VerfolgbaresPaket(string id, Adresse absender, Adresse empfaenger, double gewicht, bool versichert = false)
            : base(id, absender, empfaenger, gewicht, versichert)
        {
           // SetzeStation(absender.PlzOrt);
        }
        #endregion

        #region:other methods
        public void SetzeStation(string ort)
        {
            Stationen.Add(new Station(ort));
        }

        public override string ToString()
        {
            int count = 0;
            StringBuilder toString = new StringBuilder(base.ToString());

            toString.Append("Stationen:\n");

            if (Zugestellt)
            {
                foreach (Station item in Stationen)
                {
                    count++;
                    toString.Append(count + " - " + item.ToString());
                }
            }
            return toString.ToString();
        }
        #endregion
    }
}
