﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_A04_Versand
{
    class Station
    {
        #region:property
        public string Ort { get; }
        public DateTime TimeStamp { get; }
        #endregion

        #region:constructor
        public Station(string ort, DateTime timeStamp)
        {
            Ort = ort;
            TimeStamp = timeStamp;
        }

        public Station(string ort)
            : this(ort, DateTime.Now) { }
        #endregion

        #region:other methods
        public override string ToString()
        {
            string toString = string.Format(
                "{0}: {1}\n"
                , Ort, TimeStamp.ToString("u")
                );

            return toString;
        }
        #endregion
    }
}
