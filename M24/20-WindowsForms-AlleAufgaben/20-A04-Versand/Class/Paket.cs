﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_A04_Versand
{
    class Paket:Postsendung
    {

        #region:property
        public double Gewicht { get; set; }
        public bool Versichert { get; set; }
        #endregion

        #region:constructor
        public Paket(string id, Adresse absender, Adresse empfaenger, double gewicht, bool versichert=false)
            : base(id, absender,empfaenger)
        {
            Gewicht = gewicht;
            Versichert = versichert;
        }
        #endregion

        #region:other methods
        public override string ToString()
        {
            string toString = string.Format(
                "{0}" +
                "Gewicht: {1}\n" +
                "Versichert: {2}\n"
                , base.ToString(), Gewicht, Versichert? "Ja":"Nein"
                );

            return toString;
        }
        #endregion
    }
}
