﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_A04_Versand
{
    class Versandservice
    {
        #region:attribute
        private int count=0;
        private List<Postsendung> sendungen = new List<Postsendung>();
        private List<Adresse> adressen= new List<Adresse>();
        #endregion
        

        #region:other methods
        public Brief NeuerBrief(Adresse absender, Adresse empfaenger, Brief.BriefKategorie kategorie,string ID = "0")
        {
            count++;
            Brief temp = new Brief(ID == "0" ? count.ToString() : ID.ToString(), absender, empfaenger, kategorie);
            sendungen.Add(temp);
            return temp;
        }

        public Paket NeuesPaket(Adresse absender, Adresse empfaenger, double gewicht, bool versichert, string ID = "0")
        {
            count++;
            Paket temp = new Paket(ID == "0" ? count.ToString() : ID.ToString(), absender, empfaenger, gewicht, versichert);
            sendungen.Add(temp);
            return temp;
        }

        public VerfolgbaresPaket NeuesVerfPaket(Adresse absender, Adresse empfaenger, double gewicht, bool versichert, string ID = "0")
        {
            count++;
            VerfolgbaresPaket temp = new VerfolgbaresPaket(ID == "0" ? count.ToString() : ID.ToString(), absender, empfaenger, gewicht, versichert);
            sendungen.Add(temp);
            return temp;
        }

        public int Ausliefern()
        {
            int ausliefern=0;

            foreach (Postsendung item in sendungen)
            {
                if (!item.Zugestellt && item.Absender.IstGueltig() && item.Empfaenger.IstGueltig())
                {
                    item.Zugestellt = true;
                    ausliefern++;
                }
            }


            return ausliefern;
        }

        public List<Postsendung> GetSendungen()
        {
            return sendungen;
        }
        
        public List<Adresse> GetAdressen()
        {
            return adressen;
        }

        public Adresse AddAdresse(Adresse adresse)
        {
            Adresse addAdresse;

            if (adressen.Exists(item =>
                    item.Nachname == adresse.Nachname &&
                    item.Vorname == adresse.Vorname &&
                    item.StrasseHsNr == adresse.StrasseHsNr &&
                    item.PlzOrt == adresse.PlzOrt &&
                    item.Land == adresse.Land))
            {
                addAdresse = adressen.Find(item =>
                    item.Nachname == adresse.Nachname &&
                    item.Vorname == adresse.Vorname &&
                    item.StrasseHsNr == adresse.StrasseHsNr &&
                    item.PlzOrt == adresse.PlzOrt &&
                    item.Land == adresse.Land);
            }
            else
            {
                if(adresse.IstGueltig()) adressen.Add(adresse);
                addAdresse = adresse;
            }
            //adressen.Sortieren();
            return addAdresse;
        }

        public void RemoveAdresse(Adresse adresse)
        {
            adressen.Remove(adresse);
        }
        public void RemoveAdresse(int index)
        {
            adressen.RemoveAt(index);
        }

        public Postsendung AddShipment(string fullInfo)
        {
            Postsendung sendung=null;
            string[] temp = fullInfo.Split(';');
            if (!sendungen.Exists(item => item.ID == temp[0]))
            {
                if (temp[12] != "")
                {
                    sendung = NeuerBrief(AddAdresse(new Adresse(temp[2], temp[3], temp[4], temp[5], temp[6])), AddAdresse(new Adresse(temp[7], temp[8], temp[9], temp[10], temp[11])), (Brief.BriefKategorie)int.Parse(temp[12]), temp[0]);
                }
                else if (temp[15] == "False")
                {
                    sendung =NeuesPaket(AddAdresse(new Adresse(temp[2], temp[3], temp[4], temp[5], temp[6])), AddAdresse(new Adresse(temp[7], temp[8], temp[9], temp[10], temp[11])), double.Parse(temp[13]),bool.Parse(temp[14]), temp[0]);
                }
                else
                {
                    sendung = NeuesVerfPaket(AddAdresse(new Adresse(temp[2], temp[3], temp[4], temp[5], temp[6])), AddAdresse(new Adresse(temp[7], temp[8], temp[9], temp[10], temp[11])), double.Parse(temp[13]), bool.Parse(temp[14]), temp[0]);
                }
               
            }
            sendung.Zugestellt = bool.Parse(temp[1]);
            return sendung;
        }
        #endregion
    }
}
