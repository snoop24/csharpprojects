﻿namespace _20_A04_Versand
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBeenden = new System.Windows.Forms.Button();
            this.cB_Sendungen = new System.Windows.Forms.ComboBox();
            this.l_Absender = new System.Windows.Forms.Label();
            this.l_Empfaenger = new System.Windows.Forms.Label();
            this.l_Details = new System.Windows.Forms.Label();
            this.gB_Sendungen = new System.Windows.Forms.GroupBox();
            this.gB_Stationen = new System.Windows.Forms.GroupBox();
            this.l_Station = new System.Windows.Forms.Label();
            this.tB_Station = new System.Windows.Forms.TextBox();
            this.btn_Hinzufuegen = new System.Windows.Forms.Button();
            this.lBox_Stationen = new System.Windows.Forms.ListBox();
            this.gB_Adressen = new System.Windows.Forms.GroupBox();
            this.btn_AdresseLoeschen = new System.Windows.Forms.Button();
            this.btn_AdresseAnlegen = new System.Windows.Forms.Button();
            this.btn_AlsEmpfaenger = new System.Windows.Forms.Button();
            this.rB_AdressenNeu = new System.Windows.Forms.RadioButton();
            this.rB_AdressenListe = new System.Windows.Forms.RadioButton();
            this.btn_AlsAbsender = new System.Windows.Forms.Button();
            this.tB_Land = new System.Windows.Forms.TextBox();
            this.l_Land = new System.Windows.Forms.Label();
            this.tB_PlzOrt = new System.Windows.Forms.TextBox();
            this.l_PlzOrt = new System.Windows.Forms.Label();
            this.tB_StrasseHsNr = new System.Windows.Forms.TextBox();
            this.l_StrasseHsNr = new System.Windows.Forms.Label();
            this.tB_Vorname = new System.Windows.Forms.TextBox();
            this.l_Vorname = new System.Windows.Forms.Label();
            this.tB_Name = new System.Windows.Forms.TextBox();
            this.l_Name = new System.Windows.Forms.Label();
            this.cB_Adressen = new System.Windows.Forms.ComboBox();
            this.gB_NeueSendung = new System.Windows.Forms.GroupBox();
            this.btn_SendungErstellen = new System.Windows.Forms.Button();
            this.l_GewichtKG = new System.Windows.Forms.Label();
            this.tB_Gewicht = new System.Windows.Forms.TextBox();
            this.checkB_Verfolgbar = new System.Windows.Forms.CheckBox();
            this.checkB_Versichert = new System.Windows.Forms.CheckBox();
            this.cB_Kategorie = new System.Windows.Forms.ComboBox();
            this.rB_Paket = new System.Windows.Forms.RadioButton();
            this.rB_Brief = new System.Windows.Forms.RadioButton();
            this.btn_Ausliefern = new System.Windows.Forms.Button();
            this.gB_Sendungen.SuspendLayout();
            this.gB_Stationen.SuspendLayout();
            this.gB_Adressen.SuspendLayout();
            this.gB_NeueSendung.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonBeenden
            // 
            this.buttonBeenden.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBeenden.Location = new System.Drawing.Point(20, 556);
            this.buttonBeenden.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBeenden.Name = "buttonBeenden";
            this.buttonBeenden.Size = new System.Drawing.Size(112, 46);
            this.buttonBeenden.TabIndex = 4;
            this.buttonBeenden.Text = "&Beenden";
            this.buttonBeenden.UseVisualStyleBackColor = true;
            this.buttonBeenden.Click += new System.EventHandler(this.buttonBeenden_Click);
            // 
            // cB_Sendungen
            // 
            this.cB_Sendungen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cB_Sendungen.FormattingEnabled = true;
            this.cB_Sendungen.Location = new System.Drawing.Point(6, 22);
            this.cB_Sendungen.Name = "cB_Sendungen";
            this.cB_Sendungen.Size = new System.Drawing.Size(820, 24);
            this.cB_Sendungen.TabIndex = 10;
            this.cB_Sendungen.SelectedIndexChanged += new System.EventHandler(this.cB_Sendungen_SelectedIndexChanged);
            // 
            // l_Absender
            // 
            this.l_Absender.AutoSize = true;
            this.l_Absender.Location = new System.Drawing.Point(282, 63);
            this.l_Absender.Name = "l_Absender";
            this.l_Absender.Size = new System.Drawing.Size(73, 17);
            this.l_Absender.TabIndex = 11;
            this.l_Absender.Text = "Absender:";
            // 
            // l_Empfaenger
            // 
            this.l_Empfaenger.AutoSize = true;
            this.l_Empfaenger.Location = new System.Drawing.Point(597, 63);
            this.l_Empfaenger.Name = "l_Empfaenger";
            this.l_Empfaenger.Size = new System.Drawing.Size(81, 17);
            this.l_Empfaenger.TabIndex = 12;
            this.l_Empfaenger.Text = "Empfänger:";
            // 
            // l_Details
            // 
            this.l_Details.AutoSize = true;
            this.l_Details.Location = new System.Drawing.Point(6, 63);
            this.l_Details.Name = "l_Details";
            this.l_Details.Size = new System.Drawing.Size(55, 17);
            this.l_Details.TabIndex = 13;
            this.l_Details.Text = "Details:";
            // 
            // gB_Sendungen
            // 
            this.gB_Sendungen.Controls.Add(this.gB_Stationen);
            this.gB_Sendungen.Controls.Add(this.cB_Sendungen);
            this.gB_Sendungen.Controls.Add(this.l_Empfaenger);
            this.gB_Sendungen.Controls.Add(this.l_Details);
            this.gB_Sendungen.Controls.Add(this.l_Absender);
            this.gB_Sendungen.Location = new System.Drawing.Point(20, 13);
            this.gB_Sendungen.Name = "gB_Sendungen";
            this.gB_Sendungen.Size = new System.Drawing.Size(831, 289);
            this.gB_Sendungen.TabIndex = 14;
            this.gB_Sendungen.TabStop = false;
            this.gB_Sendungen.Text = "Sendungen";
            // 
            // gB_Stationen
            // 
            this.gB_Stationen.Controls.Add(this.l_Station);
            this.gB_Stationen.Controls.Add(this.tB_Station);
            this.gB_Stationen.Controls.Add(this.btn_Hinzufuegen);
            this.gB_Stationen.Controls.Add(this.lBox_Stationen);
            this.gB_Stationen.Location = new System.Drawing.Point(269, 160);
            this.gB_Stationen.Name = "gB_Stationen";
            this.gB_Stationen.Size = new System.Drawing.Size(555, 123);
            this.gB_Stationen.TabIndex = 16;
            this.gB_Stationen.TabStop = false;
            this.gB_Stationen.Text = "Stationen";
            this.gB_Stationen.Visible = false;
            // 
            // l_Station
            // 
            this.l_Station.AutoSize = true;
            this.l_Station.Location = new System.Drawing.Point(18, 65);
            this.l_Station.Name = "l_Station";
            this.l_Station.Size = new System.Drawing.Size(94, 17);
            this.l_Station.TabIndex = 20;
            this.l_Station.Text = "Stationsname";
            // 
            // tB_Station
            // 
            this.tB_Station.Location = new System.Drawing.Point(16, 85);
            this.tB_Station.Name = "tB_Station";
            this.tB_Station.Size = new System.Drawing.Size(112, 23);
            this.tB_Station.TabIndex = 27;
            // 
            // btn_Hinzufuegen
            // 
            this.btn_Hinzufuegen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Hinzufuegen.Location = new System.Drawing.Point(17, 23);
            this.btn_Hinzufuegen.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Hinzufuegen.Name = "btn_Hinzufuegen";
            this.btn_Hinzufuegen.Size = new System.Drawing.Size(112, 30);
            this.btn_Hinzufuegen.TabIndex = 20;
            this.btn_Hinzufuegen.Text = "&Hinzufügen";
            this.btn_Hinzufuegen.UseVisualStyleBackColor = true;
            this.btn_Hinzufuegen.Click += new System.EventHandler(this.btn_Hinzufuegen_Click);
            // 
            // lBox_Stationen
            // 
            this.lBox_Stationen.FormattingEnabled = true;
            this.lBox_Stationen.ItemHeight = 16;
            this.lBox_Stationen.Location = new System.Drawing.Point(136, 17);
            this.lBox_Stationen.Name = "lBox_Stationen";
            this.lBox_Stationen.Size = new System.Drawing.Size(413, 100);
            this.lBox_Stationen.TabIndex = 14;
            // 
            // gB_Adressen
            // 
            this.gB_Adressen.Controls.Add(this.btn_AdresseLoeschen);
            this.gB_Adressen.Controls.Add(this.btn_AdresseAnlegen);
            this.gB_Adressen.Controls.Add(this.btn_AlsEmpfaenger);
            this.gB_Adressen.Controls.Add(this.rB_AdressenNeu);
            this.gB_Adressen.Controls.Add(this.rB_AdressenListe);
            this.gB_Adressen.Controls.Add(this.btn_AlsAbsender);
            this.gB_Adressen.Controls.Add(this.tB_Land);
            this.gB_Adressen.Controls.Add(this.l_Land);
            this.gB_Adressen.Controls.Add(this.tB_PlzOrt);
            this.gB_Adressen.Controls.Add(this.l_PlzOrt);
            this.gB_Adressen.Controls.Add(this.tB_StrasseHsNr);
            this.gB_Adressen.Controls.Add(this.l_StrasseHsNr);
            this.gB_Adressen.Controls.Add(this.tB_Vorname);
            this.gB_Adressen.Controls.Add(this.l_Vorname);
            this.gB_Adressen.Controls.Add(this.tB_Name);
            this.gB_Adressen.Controls.Add(this.l_Name);
            this.gB_Adressen.Controls.Add(this.cB_Adressen);
            this.gB_Adressen.Location = new System.Drawing.Point(289, 308);
            this.gB_Adressen.Name = "gB_Adressen";
            this.gB_Adressen.Size = new System.Drawing.Size(562, 294);
            this.gB_Adressen.TabIndex = 15;
            this.gB_Adressen.TabStop = false;
            this.gB_Adressen.Text = "Adressen";
            // 
            // btn_AdresseLoeschen
            // 
            this.btn_AdresseLoeschen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AdresseLoeschen.Location = new System.Drawing.Point(450, 23);
            this.btn_AdresseLoeschen.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AdresseLoeschen.Name = "btn_AdresseLoeschen";
            this.btn_AdresseLoeschen.Size = new System.Drawing.Size(112, 46);
            this.btn_AdresseLoeschen.TabIndex = 19;
            this.btn_AdresseLoeschen.Text = "&Löschen";
            this.btn_AdresseLoeschen.UseVisualStyleBackColor = true;
            this.btn_AdresseLoeschen.Click += new System.EventHandler(this.btn_AdresseLoeschen_Click);
            // 
            // btn_AdresseAnlegen
            // 
            this.btn_AdresseAnlegen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AdresseAnlegen.Location = new System.Drawing.Point(16, 23);
            this.btn_AdresseAnlegen.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AdresseAnlegen.Name = "btn_AdresseAnlegen";
            this.btn_AdresseAnlegen.Size = new System.Drawing.Size(112, 46);
            this.btn_AdresseAnlegen.TabIndex = 17;
            this.btn_AdresseAnlegen.Text = "&Nur Anlegen";
            this.btn_AdresseAnlegen.UseVisualStyleBackColor = true;
            this.btn_AdresseAnlegen.Visible = false;
            this.btn_AdresseAnlegen.Click += new System.EventHandler(this.btn_AdresseAnlegen_Click);
            this.btn_AdresseAnlegen.MouseEnter += new System.EventHandler(this.btn_AdresseAnlegen_MouseEnter);
            this.btn_AdresseAnlegen.MouseLeave += new System.EventHandler(this.btn_AdresseAnlegen_MouseLeave);
            // 
            // btn_AlsEmpfaenger
            // 
            this.btn_AlsEmpfaenger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AlsEmpfaenger.Location = new System.Drawing.Point(256, 23);
            this.btn_AlsEmpfaenger.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AlsEmpfaenger.Name = "btn_AlsEmpfaenger";
            this.btn_AlsEmpfaenger.Size = new System.Drawing.Size(112, 46);
            this.btn_AlsEmpfaenger.TabIndex = 18;
            this.btn_AlsEmpfaenger.Text = "Als &Empfänger Setzen";
            this.btn_AlsEmpfaenger.UseVisualStyleBackColor = true;
            this.btn_AlsEmpfaenger.Click += new System.EventHandler(this.btn_AlsEmpfaenger_Click);
            // 
            // rB_AdressenNeu
            // 
            this.rB_AdressenNeu.AutoSize = true;
            this.rB_AdressenNeu.Location = new System.Drawing.Point(21, 121);
            this.rB_AdressenNeu.Name = "rB_AdressenNeu";
            this.rB_AdressenNeu.Size = new System.Drawing.Size(50, 21);
            this.rB_AdressenNeu.TabIndex = 13;
            this.rB_AdressenNeu.Text = "neu";
            this.rB_AdressenNeu.UseVisualStyleBackColor = true;
            this.rB_AdressenNeu.CheckedChanged += new System.EventHandler(this.rB_AdressenNeu_CheckedChanged);
            // 
            // rB_AdressenListe
            // 
            this.rB_AdressenListe.AutoSize = true;
            this.rB_AdressenListe.Checked = true;
            this.rB_AdressenListe.Location = new System.Drawing.Point(21, 94);
            this.rB_AdressenListe.Name = "rB_AdressenListe";
            this.rB_AdressenListe.Size = new System.Drawing.Size(56, 21);
            this.rB_AdressenListe.TabIndex = 12;
            this.rB_AdressenListe.TabStop = true;
            this.rB_AdressenListe.Text = "Liste";
            this.rB_AdressenListe.UseVisualStyleBackColor = true;
            this.rB_AdressenListe.CheckedChanged += new System.EventHandler(this.rB_AdressenListe_CheckedChanged);
            // 
            // btn_AlsAbsender
            // 
            this.btn_AlsAbsender.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AlsAbsender.Location = new System.Drawing.Point(136, 23);
            this.btn_AlsAbsender.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AlsAbsender.Name = "btn_AlsAbsender";
            this.btn_AlsAbsender.Size = new System.Drawing.Size(112, 46);
            this.btn_AlsAbsender.TabIndex = 16;
            this.btn_AlsAbsender.Text = "Als &Absender Setzen";
            this.btn_AlsAbsender.UseVisualStyleBackColor = true;
            this.btn_AlsAbsender.Click += new System.EventHandler(this.btn_AlsAbsender_Click);
            // 
            // tB_Land
            // 
            this.tB_Land.Enabled = false;
            this.tB_Land.Location = new System.Drawing.Point(128, 263);
            this.tB_Land.Name = "tB_Land";
            this.tB_Land.Size = new System.Drawing.Size(427, 23);
            this.tB_Land.TabIndex = 11;
            // 
            // l_Land
            // 
            this.l_Land.AutoSize = true;
            this.l_Land.Enabled = false;
            this.l_Land.Location = new System.Drawing.Point(18, 266);
            this.l_Land.Name = "l_Land";
            this.l_Land.Size = new System.Drawing.Size(40, 17);
            this.l_Land.TabIndex = 10;
            this.l_Land.Text = "Land";
            // 
            // tB_PlzOrt
            // 
            this.tB_PlzOrt.Enabled = false;
            this.tB_PlzOrt.Location = new System.Drawing.Point(128, 234);
            this.tB_PlzOrt.Name = "tB_PlzOrt";
            this.tB_PlzOrt.Size = new System.Drawing.Size(427, 23);
            this.tB_PlzOrt.TabIndex = 9;
            // 
            // l_PlzOrt
            // 
            this.l_PlzOrt.AutoSize = true;
            this.l_PlzOrt.Enabled = false;
            this.l_PlzOrt.Location = new System.Drawing.Point(18, 237);
            this.l_PlzOrt.Name = "l_PlzOrt";
            this.l_PlzOrt.Size = new System.Drawing.Size(58, 17);
            this.l_PlzOrt.TabIndex = 8;
            this.l_PlzOrt.Text = "PLZ Ort";
            // 
            // tB_StrasseHsNr
            // 
            this.tB_StrasseHsNr.Enabled = false;
            this.tB_StrasseHsNr.Location = new System.Drawing.Point(128, 205);
            this.tB_StrasseHsNr.Name = "tB_StrasseHsNr";
            this.tB_StrasseHsNr.Size = new System.Drawing.Size(427, 23);
            this.tB_StrasseHsNr.TabIndex = 7;
            // 
            // l_StrasseHsNr
            // 
            this.l_StrasseHsNr.AutoSize = true;
            this.l_StrasseHsNr.Enabled = false;
            this.l_StrasseHsNr.Location = new System.Drawing.Point(18, 208);
            this.l_StrasseHsNr.Name = "l_StrasseHsNr";
            this.l_StrasseHsNr.Size = new System.Drawing.Size(110, 17);
            this.l_StrasseHsNr.TabIndex = 6;
            this.l_StrasseHsNr.Text = "Strasse Hausnr.";
            // 
            // tB_Vorname
            // 
            this.tB_Vorname.Enabled = false;
            this.tB_Vorname.Location = new System.Drawing.Point(128, 176);
            this.tB_Vorname.Name = "tB_Vorname";
            this.tB_Vorname.Size = new System.Drawing.Size(427, 23);
            this.tB_Vorname.TabIndex = 5;
            // 
            // l_Vorname
            // 
            this.l_Vorname.AutoSize = true;
            this.l_Vorname.Enabled = false;
            this.l_Vorname.Location = new System.Drawing.Point(18, 179);
            this.l_Vorname.Name = "l_Vorname";
            this.l_Vorname.Size = new System.Drawing.Size(65, 17);
            this.l_Vorname.TabIndex = 4;
            this.l_Vorname.Text = "Vorname";
            // 
            // tB_Name
            // 
            this.tB_Name.Enabled = false;
            this.tB_Name.Location = new System.Drawing.Point(128, 147);
            this.tB_Name.Name = "tB_Name";
            this.tB_Name.Size = new System.Drawing.Size(427, 23);
            this.tB_Name.TabIndex = 3;
            // 
            // l_Name
            // 
            this.l_Name.AutoSize = true;
            this.l_Name.Enabled = false;
            this.l_Name.Location = new System.Drawing.Point(18, 150);
            this.l_Name.Name = "l_Name";
            this.l_Name.Size = new System.Drawing.Size(45, 17);
            this.l_Name.TabIndex = 2;
            this.l_Name.Text = "Name";
            // 
            // cB_Adressen
            // 
            this.cB_Adressen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cB_Adressen.FormattingEnabled = true;
            this.cB_Adressen.Location = new System.Drawing.Point(83, 93);
            this.cB_Adressen.Name = "cB_Adressen";
            this.cB_Adressen.Size = new System.Drawing.Size(473, 24);
            this.cB_Adressen.TabIndex = 1;
            // 
            // gB_NeueSendung
            // 
            this.gB_NeueSendung.BackColor = System.Drawing.SystemColors.Control;
            this.gB_NeueSendung.Controls.Add(this.btn_SendungErstellen);
            this.gB_NeueSendung.Controls.Add(this.l_GewichtKG);
            this.gB_NeueSendung.Controls.Add(this.tB_Gewicht);
            this.gB_NeueSendung.Controls.Add(this.checkB_Verfolgbar);
            this.gB_NeueSendung.Controls.Add(this.checkB_Versichert);
            this.gB_NeueSendung.Controls.Add(this.cB_Kategorie);
            this.gB_NeueSendung.Controls.Add(this.rB_Paket);
            this.gB_NeueSendung.Controls.Add(this.rB_Brief);
            this.gB_NeueSendung.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gB_NeueSendung.Location = new System.Drawing.Point(20, 308);
            this.gB_NeueSendung.Name = "gB_NeueSendung";
            this.gB_NeueSendung.Size = new System.Drawing.Size(263, 208);
            this.gB_NeueSendung.TabIndex = 16;
            this.gB_NeueSendung.TabStop = false;
            this.gB_NeueSendung.Text = "Neue Sendung";
            // 
            // btn_SendungErstellen
            // 
            this.btn_SendungErstellen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SendungErstellen.Location = new System.Drawing.Point(21, 23);
            this.btn_SendungErstellen.Margin = new System.Windows.Forms.Padding(4);
            this.btn_SendungErstellen.Name = "btn_SendungErstellen";
            this.btn_SendungErstellen.Size = new System.Drawing.Size(225, 46);
            this.btn_SendungErstellen.TabIndex = 18;
            this.btn_SendungErstellen.Text = "&Sendung erstellen";
            this.btn_SendungErstellen.UseVisualStyleBackColor = true;
            this.btn_SendungErstellen.Click += new System.EventHandler(this.btn_SendungErstellen_Click);
            // 
            // l_GewichtKG
            // 
            this.l_GewichtKG.AutoSize = true;
            this.l_GewichtKG.Enabled = false;
            this.l_GewichtKG.Location = new System.Drawing.Point(152, 136);
            this.l_GewichtKG.Name = "l_GewichtKG";
            this.l_GewichtKG.Size = new System.Drawing.Size(23, 17);
            this.l_GewichtKG.TabIndex = 26;
            this.l_GewichtKG.Text = "kg";
            // 
            // tB_Gewicht
            // 
            this.tB_Gewicht.Enabled = false;
            this.tB_Gewicht.Location = new System.Drawing.Point(82, 133);
            this.tB_Gewicht.Name = "tB_Gewicht";
            this.tB_Gewicht.Size = new System.Drawing.Size(64, 23);
            this.tB_Gewicht.TabIndex = 25;
            // 
            // checkB_Verfolgbar
            // 
            this.checkB_Verfolgbar.AutoSize = true;
            this.checkB_Verfolgbar.Enabled = false;
            this.checkB_Verfolgbar.Location = new System.Drawing.Point(82, 180);
            this.checkB_Verfolgbar.Name = "checkB_Verfolgbar";
            this.checkB_Verfolgbar.Size = new System.Drawing.Size(91, 21);
            this.checkB_Verfolgbar.TabIndex = 24;
            this.checkB_Verfolgbar.Text = "verfolgbar";
            this.checkB_Verfolgbar.UseVisualStyleBackColor = true;
            // 
            // checkB_Versichert
            // 
            this.checkB_Versichert.AutoSize = true;
            this.checkB_Versichert.Enabled = false;
            this.checkB_Versichert.Location = new System.Drawing.Point(82, 162);
            this.checkB_Versichert.Name = "checkB_Versichert";
            this.checkB_Versichert.Size = new System.Drawing.Size(89, 21);
            this.checkB_Versichert.TabIndex = 23;
            this.checkB_Versichert.Text = "versichert";
            this.checkB_Versichert.UseVisualStyleBackColor = true;
            // 
            // cB_Kategorie
            // 
            this.cB_Kategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cB_Kategorie.FormattingEnabled = true;
            this.cB_Kategorie.Location = new System.Drawing.Point(82, 91);
            this.cB_Kategorie.Name = "cB_Kategorie";
            this.cB_Kategorie.Size = new System.Drawing.Size(164, 24);
            this.cB_Kategorie.TabIndex = 22;
            // 
            // rB_Paket
            // 
            this.rB_Paket.AutoSize = true;
            this.rB_Paket.Location = new System.Drawing.Point(21, 133);
            this.rB_Paket.Name = "rB_Paket";
            this.rB_Paket.Size = new System.Drawing.Size(62, 21);
            this.rB_Paket.TabIndex = 21;
            this.rB_Paket.Text = "Paket";
            this.rB_Paket.UseVisualStyleBackColor = true;
            this.rB_Paket.CheckedChanged += new System.EventHandler(this.rB_Paket_CheckedChanged);
            // 
            // rB_Brief
            // 
            this.rB_Brief.AutoSize = true;
            this.rB_Brief.Checked = true;
            this.rB_Brief.Location = new System.Drawing.Point(21, 91);
            this.rB_Brief.Name = "rB_Brief";
            this.rB_Brief.Size = new System.Drawing.Size(55, 21);
            this.rB_Brief.TabIndex = 20;
            this.rB_Brief.TabStop = true;
            this.rB_Brief.Text = "Brief";
            this.rB_Brief.UseVisualStyleBackColor = true;
            this.rB_Brief.CheckedChanged += new System.EventHandler(this.rB_Brief_CheckedChanged);
            // 
            // btn_Ausliefern
            // 
            this.btn_Ausliefern.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Ausliefern.Location = new System.Drawing.Point(171, 556);
            this.btn_Ausliefern.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Ausliefern.Name = "btn_Ausliefern";
            this.btn_Ausliefern.Size = new System.Drawing.Size(112, 46);
            this.btn_Ausliefern.TabIndex = 17;
            this.btn_Ausliefern.Text = "Auslie&fern";
            this.btn_Ausliefern.UseVisualStyleBackColor = true;
            this.btn_Ausliefern.Click += new System.EventHandler(this.btn_Ausliefern_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 628);
            this.Controls.Add(this.btn_Ausliefern);
            this.Controls.Add(this.gB_NeueSendung);
            this.Controls.Add(this.gB_Adressen);
            this.Controls.Add(this.gB_Sendungen);
            this.Controls.Add(this.buttonBeenden);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Versandservice";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gB_Sendungen.ResumeLayout(false);
            this.gB_Sendungen.PerformLayout();
            this.gB_Stationen.ResumeLayout(false);
            this.gB_Stationen.PerformLayout();
            this.gB_Adressen.ResumeLayout(false);
            this.gB_Adressen.PerformLayout();
            this.gB_NeueSendung.ResumeLayout(false);
            this.gB_NeueSendung.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonBeenden;
        private System.Windows.Forms.ComboBox cB_Sendungen;
        private System.Windows.Forms.Label l_Absender;
        private System.Windows.Forms.Label l_Empfaenger;
        private System.Windows.Forms.Label l_Details;
        private System.Windows.Forms.GroupBox gB_Sendungen;
        private System.Windows.Forms.GroupBox gB_Adressen;
        private System.Windows.Forms.ComboBox cB_Adressen;
        private System.Windows.Forms.TextBox tB_Land;
        private System.Windows.Forms.Label l_Land;
        private System.Windows.Forms.TextBox tB_PlzOrt;
        private System.Windows.Forms.Label l_PlzOrt;
        private System.Windows.Forms.TextBox tB_StrasseHsNr;
        private System.Windows.Forms.Label l_StrasseHsNr;
        private System.Windows.Forms.TextBox tB_Vorname;
        private System.Windows.Forms.Label l_Vorname;
        private System.Windows.Forms.TextBox tB_Name;
        private System.Windows.Forms.Label l_Name;
        private System.Windows.Forms.RadioButton rB_AdressenNeu;
        private System.Windows.Forms.RadioButton rB_AdressenListe;
        private System.Windows.Forms.Button btn_AdresseAnlegen;
        private System.Windows.Forms.Button btn_AlsEmpfaenger;
        private System.Windows.Forms.Button btn_AlsAbsender;
        private System.Windows.Forms.Button btn_AdresseLoeschen;
        private System.Windows.Forms.GroupBox gB_NeueSendung;
        private System.Windows.Forms.CheckBox checkB_Versichert;
        private System.Windows.Forms.ComboBox cB_Kategorie;
        private System.Windows.Forms.RadioButton rB_Paket;
        private System.Windows.Forms.RadioButton rB_Brief;
        private System.Windows.Forms.CheckBox checkB_Verfolgbar;
        private System.Windows.Forms.Button btn_SendungErstellen;
        private System.Windows.Forms.Label l_GewichtKG;
        private System.Windows.Forms.TextBox tB_Gewicht;
        private System.Windows.Forms.Button btn_Ausliefern;
        private System.Windows.Forms.ListBox lBox_Stationen;
        private System.Windows.Forms.GroupBox gB_Stationen;
        private System.Windows.Forms.Label l_Station;
        private System.Windows.Forms.TextBox tB_Station;
        private System.Windows.Forms.Button btn_Hinzufuegen;
    }
}

