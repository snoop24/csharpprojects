﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace _20_A04_Versand
{
    public partial class Form1 : Form
    {
        Versandservice versand = new Versandservice();

        public Form1()
        {
            InitializeComponent();
        }

        #region:ButtonClicks
        private void buttonBeenden_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Ausliefern_Click(object sender, EventArgs e)
        {
            int index = cB_Sendungen.SelectedIndex;

            //foreach (Postsendung item in versand.GetSendungen())
            //{
            //    if (item.Absender.IstGueltig() && item.Empfaenger.IstGueltig())
            //    {
            //        item.Zugestellt = true;
            //    }
            //}
            MessageBox.Show(versand.Ausliefern() + " Paket(e) wurden ausgeliefert.","Ausliefern");

            ReLoad();
            cB_Sendungen.SelectedIndex=index;
        }

        private void btn_SendungErstellen_Click(object sender, EventArgs e)
        {
            bool worked = true;
            Adresse temp = new Adresse("", "", "", "", "");
            if (rB_Brief.Checked)
            {
                versand.NeuerBrief(temp, temp, (Brief.BriefKategorie)cB_Kategorie.SelectedIndex);
            }
            else if (rB_Paket.Checked && double.TryParse(tB_Gewicht.Text, out double result))
            {
                if (checkB_Verfolgbar.Checked)
                {
                    versand.NeuesVerfPaket(temp, temp, result, checkB_Versichert.Checked);
                }
                else
                {
                    versand.NeuesPaket(temp, temp, result, checkB_Versichert.Checked);
                }
            }
            else
            {
                worked = false;
            }

            if (worked)
            {
                ReLoad();
                cB_Sendungen.SelectedIndex = versand.GetSendungen().Count - 1;


                cB_Kategorie.SelectedIndex = 0;
                checkB_Versichert.Checked = false;
                checkB_Verfolgbar.Checked = false;
                tB_Gewicht.Text = "";
            }
            else
            {
                MessageBox.Show("Erlaubtes Gewicht eingeben!", "Fehler!");
            }
        }

        private void btn_AdresseAnlegen_Click(object sender, EventArgs e)
        {
            AdresseAnlegen();
        }

        private void btn_AdresseLoeschen_Click(object sender, EventArgs e)
        {
            int index = cB_Adressen.SelectedIndex;
            if(index >-1)
            {
                versand.RemoveAdresse(index);
                ReLoad();
            }
        }

        private void btn_AlsAbsender_Click(object sender, EventArgs e)
        {
            int index = cB_Sendungen.SelectedIndex;
            if (index > -1) { AlsAdresse(true); }
        }

        private void btn_AlsEmpfaenger_Click(object sender, EventArgs e)
        {
            int index = cB_Sendungen.SelectedIndex;
            if (index > -1) { AlsAdresse(false); }
        }

        private void btn_Hinzufuegen_Click(object sender, EventArgs e)
        {
            int index = cB_Sendungen.SelectedIndex;
            VerfolgbaresPaket temp = (VerfolgbaresPaket)versand.GetSendungen()[index];
            if (temp.Zugestellt)
            {
                temp.SetzeStation(tB_Station.Text);
                ReLoad();
                cB_Sendungen.SelectedIndex = index;
            }
        }

        private void rB_AdressenListe_CheckedChanged(object sender, EventArgs e)
        {
            if (rB_AdressenListe.Checked)
            {
                foreach (Control item in gB_Adressen.Controls)
                {
                    if (item is TextBox || item is Label) item.Enabled = false;
                }
                cB_Adressen.Enabled = true;
                btn_AdresseAnlegen.Visible = false;

                cB_Adressen.DataSource = null;
                cB_Adressen.DataSource = versand.GetAdressen();
                cB_Adressen.DisplayMember = "DisplayInfo";
            }
        }
        private void rB_AdressenNeu_CheckedChanged(object sender, EventArgs e)
        {
            if (rB_AdressenNeu.Checked)
            {
                foreach (Control item in gB_Adressen.Controls)
                {
                    if (item is TextBox || item is Label) item.Enabled = true;
                }
                cB_Adressen.Enabled = false;
                btn_AdresseAnlegen.Visible = true;
            }
        }

        private void rB_Brief_CheckedChanged(object sender, EventArgs e)
        {
            if (rB_Brief.Checked)
            {
                cB_Kategorie.Enabled = true;

                tB_Gewicht.Enabled = false;
                l_GewichtKG.Enabled = false;
                checkB_Verfolgbar.Enabled = false;
                checkB_Versichert.Enabled = false;

            }
        }
        private void rB_Paket_CheckedChanged(object sender, EventArgs e)
        {
            cB_Kategorie.Enabled = false;

            tB_Gewicht.Enabled = true;
            l_GewichtKG.Enabled = true;
            checkB_Verfolgbar.Enabled = true;
            checkB_Versichert.Enabled = true;
        }
        #endregion

        #region:save and load
        private void AddressLoad()
        {
            StreamReader adressDatei = new StreamReader("address.csv");
            while (!adressDatei.EndOfStream)
            {
                string temp1 = adressDatei.ReadLine();
                string[] temp = temp1.Split(';');
                Adresse adresse = versand.AddAdresse(new Adresse(temp[0], temp[1], temp[2], temp[3], temp[4]));
            }
            adressDatei.Close();
        }

        private void AddressSave()
        {
            StreamWriter adressDatei = new StreamWriter("address.csv");
            foreach (Adresse item in versand.GetAdressen())
            {
                adressDatei.WriteLine(item.FullInfo);
            }
            adressDatei.Close();
        }

        private void ShipmentsLoad()
        {
            StreamReader shipmentsDatei = new StreamReader("shipments.csv");
            while (!shipmentsDatei.EndOfStream)
            {
                versand.AddShipment(shipmentsDatei.ReadLine());
            }
            shipmentsDatei.Close();
        }

        private void ShipmentsSave()
        {
            StreamWriter shipmentsDatei = new StreamWriter("shipments.csv");
            foreach (Postsendung item in versand.GetSendungen())
            {
                shipmentsDatei.WriteLine(item.FullInfo);
            }
            shipmentsDatei.Close();
        }

        #endregion

        private void cB_Sendungen_SelectedIndexChanged(object sender, EventArgs e)
        {
            StationenVisible(false);
            if (cB_Sendungen.SelectedIndex > -1 && cB_Sendungen.SelectedIndex < cB_Sendungen.Items.Count)
            {
                Postsendung temp = versand.GetSendungen()[cB_Sendungen.SelectedIndex];
                l_Absender.Text = "Ab:\n" + temp.Absender.ToString();
                l_Empfaenger.Text = "An:\n" + temp.Empfaenger.ToString();
                l_Details.Text = temp.DetailsInfo;
                if (temp is VerfolgbaresPaket)
                {
                    lBox_Stationen.DataSource = null;
                    lBox_Stationen.DataSource = ((VerfolgbaresPaket)temp).Stationen;
                    if (temp.Zugestellt) { StationenVisible(true); }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AddressLoad();
            ShipmentsLoad();

            cB_Sendungen.DataSource = versand.GetSendungen();
            cB_Sendungen.DisplayMember = "ListInfo";
            cB_Adressen.DataSource = versand.GetAdressen();
            cB_Adressen.DisplayMember = "DisplayInfo";
            cB_Kategorie.DataSource = Enum.GetValues(typeof(Brief.BriefKategorie));
        }

        private void ReLoad()
        {
            cB_Sendungen.DataSource = null;
            cB_Sendungen.DataSource = versand.GetSendungen();
            cB_Sendungen.DisplayMember = "ListInfo";
            cB_Sendungen.SelectedIndex = 0;
            cB_Adressen.DataSource = null;
            cB_Adressen.DataSource = versand.GetAdressen();
            cB_Adressen.DisplayMember = "DisplayInfo";
            cB_Adressen.SelectedIndex = 0;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            AddressSave();
            ShipmentsSave();
        }

        private Adresse AdresseAnlegen()
        {
            Adresse temp = new Adresse(tB_Name.Text, tB_Vorname.Text, tB_StrasseHsNr.Text, tB_PlzOrt.Text, tB_Land.Text);
            if (temp.IstGueltig()) temp=versand.AddAdresse(temp);
            ReLoad();
            return temp;
        }

        private void ClearAdresse()
        {
            tB_Name.Text = "";
            tB_Vorname.Text = "";
            tB_StrasseHsNr.Text = "";
            tB_PlzOrt.Text = "";
            tB_Land.Text = "";
        }

        private void AlsAdresse(bool absender)
        {
            int index = cB_Sendungen.SelectedIndex;
            Adresse temp;
            if (rB_AdressenListe.Checked)
            {
                temp = versand.GetAdressen()[cB_Adressen.SelectedIndex];
            }
            else
            {
                temp = AdresseAnlegen();
            }
            if (index > -1 && !versand.GetSendungen()[index].Zugestellt)
            {
                if (absender) { versand.GetSendungen()[index].Absender = temp; }
                else { versand.GetSendungen()[index].Empfaenger = temp; }

                if (temp.IstGueltig()) { ClearAdresse(); }
                else { MessageBox.Show("Bitte gültige Adresse anlegen!", "Fehler!"); }
            }
            else { MessageBox.Show("Keine Sendung ausgewählt oder Sendung bereits versand.", "Fehler!"); }
            ReLoad();
            cB_Sendungen.SelectedIndex = index;
        }

        private void StationenVisible(bool temp)
        {
            //lBox_Stationen.Visible = temp;
            gB_Stationen.Visible = temp;

        }

        private void btn_AdresseAnlegen_MouseEnter(object sender, EventArgs e)
        {
            btn_AdresseAnlegen.BackColor = Color.Orange;
        }

        private void btn_AdresseAnlegen_MouseLeave(object sender, EventArgs e)
        {
            btn_AdresseAnlegen.BackColor = Color.LightGray;
        }
    }
}
