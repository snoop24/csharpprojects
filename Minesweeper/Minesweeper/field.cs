﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper
{
    class Field
    {
        private enum FieldType { empty, surr1, surr2, surr3, surr4, surr5, surr6, surr7, surr8, mine };

        public bool open { get; private set; }

        private bool flagged;

        private FieldType mineFieldType = new FieldType();

        public  FieldfieldType
        {
            get { return mineFieldType; }
            private set { mineFieldType = value; }
        }


        public Field()
        {
            open=false;
            flagged=false;
            fieldType = 0;
        }
    }
}
