﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdw
{
    class Enemy
    {
        // class attributes
        static private Random rnd = new Random();

        // properties
        public int Life { get; set; }
        public int MinAttack { get; set; }
        public int MaxAttack { get; set; }
        public string Name { get; set; }
        public string[] PicAttack { get; set; }
        public string PicStand { get; set; }
       
        public int NextAttack { get { return CalcNextAttack(); } }
        
        // constructor all
        public Enemy(int life, int minAttack, int maxAttack, string name, string[] picAttack, int picAttackCount, string picStand)
        {
            this.Life = life;
            this.MinAttack = minAttack;
            this.MaxAttack = maxAttack;
            this.Name = name;
            this.PicAttack = picAttack;
            this.PicStand = picStand;
        }
        // constructor empty
        public Enemy() { }

        // next attacks value
        private int CalcNextAttack()
        {
            return rnd.Next(MinAttack, MaxAttack + 1);
        }
    }
}
