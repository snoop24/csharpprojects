﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdw.Classes
{
    static class Commentator
    {
        // class attribute
        static private Random rnd = new Random();

        // methods
        public static string StartMessage()
        {
            string[] startMessage = new string[]
            {
                "..und so beginnt also der Kampf.",
                "Der Kampf beginnt!",
                "nun entscheidet sich erneut das Schicksal des Gott der Wälder.",
                "Aleae iacientur. Die Würfel werden geworfen.",
                 "nun beginnt der epische Kampf.."
            };
            return startMessage[rnd.Next(startMessage.Length)];
        }

        static public string GdwHit(int damage, int weapon)
        {
            string result = $"Waffe nicht gefunden. Trotzdem {damage}DMG.";
            string[][] gdwHit = new string[][]
            {
                new string[] {
                    $"Der Gott der Wälder verpasst seinem Gegner einen heftigen Kick. Oh! Da! Ein herber Punch! Der Gott der Wälder fügt seinem ehrlosen Gegner \n{damage}DMG zu.",
                    $"Der GDW zeigt einen Roundhousekick und anschließend einen Punch. Der hat gesessen.   {damage} DMG",
                    $"Aah! Unser Held demütigt den Feind mit einer schönen Kombo! BÄM! BÄM-BÄM! HAHAHA!   {damage}DMG",
                    $"BÄM-BÄM-BÄMBÄMBÄM-BÄMBÄM-BÄÄÄM-BÄÄÄÄMBÄMBÄMBÄÄÄM! BÄÄÄÄÄÄÄÄÄÄM!  {damage}DMG",
                    $"Und er zeigt was er kann! Der Gott der Wälder prügelt die Scheiße aus dem Unwürdigen, der es wagte, gegen ihn anzutreten und fügt ihm {damage}DMG zu."
                },
                new string[] {
                    $"Mit dem Kochlöffel haut der GdW seinem Gegner die Fresse zu Klump. \n {damage}DMG!",
                    $"ZACK! Der GdW haut dem Gegner mit dem Löffel einen vor die Löffel. {damage}DMG",
                    $"KRÄSCH! Der Kochlöffel saust immer wieder auf den Feind herab wie ein Schwarm Bienen. {damage}DMG",
                    $"Wieder kann der Gott der Wälder seinen Feind löffeln. Er fügt ihm {damage}DMG zu.",
                    $"Er kann zwar nicht kochen, aber er weiß, wie man einen Löffel richtig einsetzt! Der GdW fügt seinem Gegner {damage}DMG zu."
                }
            };
            if (weapon >= 0 && weapon < gdwHit.Length)
            {
                result = gdwHit[weapon][rnd.Next(gdwHit[weapon].Length)];
            }
            return result;
        }

        // enemyhit fehlt noch ... ggf Waffen implementieren und entsprechende INI files ggf anders erstellen

    }
}
