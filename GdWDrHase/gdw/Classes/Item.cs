﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdw
{
    class Item
    {
        // properties
        public string Name { get; set; }
        public string Prop { get; set; }
        public string Prop2 { get; set; }
        public int Value { get; set; }
        public string Descr { get; set; }
        public int Pic { get; set; }

        // constructor
        public Item(string name, string prop, string prop2, int value, string descr, int pic)
        {
            Name = name;
            Prop = prop;
            Prop2 = prop2;
            Value = value;
            Descr = descr;
            Pic = pic;
        }
    }
}
