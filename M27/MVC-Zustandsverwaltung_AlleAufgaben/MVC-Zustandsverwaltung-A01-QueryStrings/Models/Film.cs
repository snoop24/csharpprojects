﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Zustandsverwaltung_A01_QueryStrings.Models
{
    public class Film
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [DisplayFormat(DataFormatString ="YYYY")]
        public DateTime Year {get;set;}
    }
}