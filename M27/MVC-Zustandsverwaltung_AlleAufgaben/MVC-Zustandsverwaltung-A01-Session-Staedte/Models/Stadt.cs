﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Zustandsverwaltung_A01_Session_Staedte.Models
{
    public class Stadt
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Bundesland { get; set; }
        public List<Attraktion> Attraktionen { get; set; }
        public int AnzahlZiele { get; set; }
        public int Bewertung { get; set; }
    }
}

//{"Id":1,"Name":"Dortmund","Bundesland":"Nordrhein-Westfalen","Attraktionen":[{"Id":1,"Name":"Dortmunder U","Anschrift":"Brinkhoffstraße 4, 44137 Dortmund","Bewertung":8,"Url":"http://www.dortmunder-u.de/"},{"Id":2,"Name":"Deutsches Fußballmuseum","Anschrift":"Platz der Deutschen Einheit 1, 44137 Dortmund","Bewertung":8,"Url":"https://www.fussballmuseum.de"}],"AnzahlZiele":2,"Bewertung":8}