﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;

namespace MVC_Zustandsverwaltung_A01_Session_Staedte.Models
{
    public static class ASBVerwaltung
    {
        public static List<string> Bundeslaender { get; set; }
        public static List<Stadt> Staedte { get; set; }
        public static List<Attraktion> Attraktionen { get; set; }

        public static string LoadAll()
        {

            string message = "";

            message += LoadBundeslaender() + "\n";
            message += LoadAttraktionen() + "\n";
            message += LoadStaedte() + "\n";

            return message;
        }

        public static string LoadBundeslaender()
        {
            string message = "B load succesful";

            try
            {
                Bundeslaender = new JavaScriptSerializer().Deserialize<List<string>>(File.ReadAllText(@"D:\data\Bundesländer.json"));
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        public static string LoadAttraktionen()
        {
            string message = "A load succesful";

            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                using (StreamReader sr = new StreamReader(@"D:\data\Attraktionen.json"))
                {
                    while (!sr.EndOfStream)
                    {
                        Attraktionen = js.Deserialize<List<Attraktion>>(sr.ReadLine());
                    }

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        public static string LoadStaedte()
        {
            string message = "S load succesful";

            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                using (StreamReader sr = new StreamReader(@"D:\data\Städte.json"))
                {
                    while (!sr.EndOfStream)
                    {
                        Staedte = js.Deserialize<List<Stadt>>(sr.ReadLine());
                    }

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }
    }
}