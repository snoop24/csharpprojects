﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Zustandsverwaltung_A01_Session_Staedte.Models
{
    public class Attraktion
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Anschrift { get; set; }
        public int Bewertung { get; set; }
        public string URL { get; set; }
    }
}
//{"Id":1,"Name":"Dortmunder U","Anschrift":"Brinkhoffstraße 4, 44137 Dortmund","Bewertung":8,"Url":"http://www.dortmunder-u.de/"}