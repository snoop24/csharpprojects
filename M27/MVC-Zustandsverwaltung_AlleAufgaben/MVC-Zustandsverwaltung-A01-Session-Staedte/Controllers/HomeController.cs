﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_Zustandsverwaltung_A01_Session_Staedte.Models;

namespace MVC_Zustandsverwaltung_A01_Session_Staedte.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ASBVerwaltung.LoadAll();
            return View();
        }

        public ActionResult TopTen()
        {
            return View(ASBVerwaltung.Staedte.OrderBy(item => -item.Bewertung).Take(10).ToList());
        }

        public ActionResult NachBL()
        {
            Dictionary<string, string> filterListe = ASBVerwaltung.Bundeslaender.ToDictionary(x => x, x => x+"  ("+ASBVerwaltung.Staedte.Where(item => item.Bundesland == x).ToList().Count.ToString()+" Treffer)");
            ViewData["filterFor"] = new SelectList(filterListe,"Key","Value");
            return View();
        }

        public ActionResult Suche()
        {
            return View();
        }

        public PartialViewResult GetStaedteByBL(string filterFor)
        {
            return PartialView("_StaedteTable", ASBVerwaltung.Staedte.Where(item => item.Bundesland == filterFor).ToList());
        }

        public PartialViewResult GetStaedteByName(string filterFor)
        {
            if (filterFor != null && filterFor.Length > 0)
            {
                Queue<string> suchFilter;
                if (Session["suchFilter"] == null)
                {
                    suchFilter = new Queue<string>();
                }
                else
                {
                    suchFilter = Session["suchFilter"] as Queue<string>;
                    while (suchFilter.Count > 4)
                    {
                        suchFilter.Dequeue();
                    }
                }
                suchFilter.Enqueue(filterFor);
                Session["suchFilter"] = suchFilter;
            }
            return PartialView("_StaedteTable", ASBVerwaltung.Staedte.Where(item => item.Name.ToLower().Contains(filterFor.ToLower())).ToList());
        }

    }
}