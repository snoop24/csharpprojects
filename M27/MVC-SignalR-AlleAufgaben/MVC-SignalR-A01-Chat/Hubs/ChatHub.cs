﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace MVC_SignalR_A01_Chat.Hubs
{
    public class ChatHub : Hub
    {
        static Dictionary<string, string> users = new Dictionary<string, string>();

        static JavaScriptSerializer js = new JavaScriptSerializer();

        public void UserLeave()
        {
            string userKey = users.FirstOrDefault(item => item.Value == Context.ConnectionId).Key;
            if (userKey != null)
                users.Remove(userKey);
            Clients.All.updateList(js.Serialize(users.Keys));
        }

        public void UserJoin(string userName)
        {
            userName = SaveGetValidUserName(userName);

            Clients.All.updateList(js.Serialize(users.Keys));
            Clients.Client(Context.ConnectionId).setUserName(userName);
            Clients.All.receiveMessage("*** ChatHub", users[Context.ConnectionId] + " has joined the chat. ***", 1);
        }

        public void SendMessage(string user, string clientMessage)
        {
            // TODO
        }

        public void SendMessage(string clientMessage)
        {
            
            Clients.All.receiveMessage(users[Context.ConnectionId], clientMessage, 0);
        }

        private string SaveGetValidUserName(string userName)
        {
            userName = userName == "" || userName == "null" ? "ChatUser" : userName;

            string userKey = users.FirstOrDefault(item => item.Value == Context.ConnectionId).Key;
            if(userKey != null)
            users.Remove(userKey);


            int i = 0;
            string userNameTemp = userName;
            while (users.ContainsKey(userNameTemp))
            {
                i++;
                userNameTemp = userName + $" ({i})";
            }
            userName = userNameTemp;
            users.Add(userName, Context.ConnectionId);


            return userName;
        }
    }
}