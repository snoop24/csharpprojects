﻿using Microsoft.Owin;
using Owin;
using MVC_SignalR_A01_Chat;

[assembly: OwinStartupAttribute(typeof(MVC_SignalR_A01_Chat.Startup))]

namespace MVC_SignalR_A01_Chat
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}