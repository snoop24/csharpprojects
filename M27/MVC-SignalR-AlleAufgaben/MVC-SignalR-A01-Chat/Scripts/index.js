﻿$(document).ready(function () {

    var userName = prompt("Bitte wähle Deinen Namen: ");

    initHubFunctions(userName);

    $("#btnText").click(senden);
});

var hub=$.connection.chatHub;

$(window).unload( function () {
    hub.server.userLeave();
});

function initHubFunctions (userName) {
    
    // receive general chat message
    hub.client.receiveMessage = function (sender, srvMessage, type) {
        var line = "<p class='type" + type + "'><span class='sender'>" + sender + ": </span>" + srvMessage + "<p>";
        $("#divOutput").html(line + $("#divOutput").html());
    };

    // update user list
    hub.client.updateList = function (users) {
        users = JSON.parse(users);
        console.log(users);
        var selectString = "<select size='10' id='userList' name='selUser'>";
        
        for (var user in users) {
            selectString += "<option name='selUser' value='" + users[user] + "'>" + users[user] + "</option>";
        }
        
        selectString += "</select>";
        $("#divList").html(selectString);
    };

    // show userName
    hub.client.setUserName = function (userName) {
        console.log("setUserName: " + userName);
        $("#h3UserName").html("Your name: "+userName);
    };

    // do after startup
    $.connection.hub.start().done(function () {
        hub.server.userJoin(userName);
    });
}

function senden() {
    var message = $("#inpText").val();
    $("#inpText").val("");
    // TODO implement single or group whisper
    hub.server.sendMessage(message);
}