﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A03_ValidierungISBN.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // GET: Home
        public JsonResult ISBN(string ISBN)
        {
            bool state = Models.Book.ValidISBN(ISBN);
            return Json(state, JsonRequestBehavior.AllowGet);
        }
    }
}