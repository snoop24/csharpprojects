﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ASPNET_MVC_A03_ValidierungISBN.Models
{
    public class Book
    {
        private static int count = 0;

        public int Id { get; }

        [Required]
        [StringLength(25, MinimumLength = 2)]
        public string Title { get; set; }

        [Range(20, 2000)]
        public int Pages { get; set; }

        [MaxLength(13)]
        [ISBN]
        [Remote("ISBN", "Home", ErrorMessage = "Die ISBN ist nicht korrekt.")]
        public int ISBN { get; set; }

        public Book(string title, int pages, int iSBN)
        {
            Id = ++count;
            Title = title;
            Pages = pages;
            ISBN = iSBN;
        }

        // static
        public static bool ValidISBN(string isbn)
        {
            bool isbnOk = false;
            int isbnLength = isbn.Length;
            if (isbnLength == 10)
            {
                int sum = 0;
                for (var i = 0; i < isbnLength; i++)
                {
                    sum += (i + 1) * (isbn[i] - '0');
                }
                isbnOk = (sum % 11 == 0);
            }
            else if (isbnLength == 13)
            {
                int sum = 0;
                for (var i = 0; i < isbnLength; i++)
                {
                    sum += ((i % 2) * 2 + 1) * (isbn[i] - '0');
                }
                isbnOk = (sum % 10 == 0);
            }
            return isbnOk;
        }
    }

    public class ISBNAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return Book.ValidISBN((string)value) ? ValidationResult.Success : new ValidationResult("ISBN ungültig");
        }
    }
}