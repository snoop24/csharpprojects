﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A05_UploadDocumentStore.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index(HttpPostedFileBase dieDatei)
        {
            if (dieDatei != null && dieDatei.ContentLength > 0)
            {
                Models.SqlFileRepository.FileToDB(dieDatei);
            }

            List<Models.SqlFile> fileList = Models.SqlFileRepository.GetDBFileList();

            return View(fileList);
        }
    }
}