﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPNET_MVC_A05_UploadDocumentStore.Models
{
    public class SqlFile
    {
        public int ID { get; set; }
        public string DocName { get; set; }
        public string ContentType { get; set; }
        public long ContentLength { get; set; }
        public DateTime InsertionDate { get; set; }

        public SqlFile(string docName, string contentType, long contentLength, DateTime insertionDate, int id = 0)
        {
            ID = id;
            DocName = docName;
            ContentType = contentType;
            ContentLength = contentLength;
            InsertionDate = insertionDate == null ? DateTime.Now : insertionDate ;
        }
    }
}