﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace ASPNET_MVC_A05_UploadDocumentStore.Models
{
    public static class SqlFileRepository
    {
        static SqlConnection connection;
        static string connString = @"Data Source=.\SQLEXPRESS; Initial Catalog=NETDB;Integrated Security=True;";
        static SqlDataReader sqlReader;
        static SqlCommand sqlCom;

        static SqlFile fileObject;

        public static void FileToDB(HttpPostedFileBase fileBase)
        {

            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "INSERT INTO dbo.DocStore VALUES (@DocName, @DocData, @ContentType, @ContentLength, @InsertionDate)";
                    
                    sqlCom.Parameters.AddWithValue("DocName", fileBase.FileName);
                    sqlCom.Parameters.AddWithValue("DocData", fileBase.InputStream);
                    sqlCom.Parameters.AddWithValue("ContentType", fileBase.ContentType);
                    sqlCom.Parameters.AddWithValue("ContentLength", fileBase.ContentLength);
                    sqlCom.Parameters.AddWithValue("InsertionDate", DateTime.Now);

                    sqlCom.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }


        }

        public static SqlFile DBToFile(int id)
        {


            return fileObject;
        }

        public static List<SqlFile> GetDBFileList()
        {
            List<SqlFile> getDBFileList = new List<SqlFile>();
            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "SELECT DocName,ContentType,ContentLength,InsertionDate,DocID FROM dbo.DocStore;";

                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {

                        // Alle Banken in einer Liste speichern
                        getDBFileList.Add(new SqlFile(
                            sqlReader.GetString(0)
                            , sqlReader.GetString(1)
                            , sqlReader.GetInt64(2)
                            , sqlReader.GetDateTime(3)
                            , sqlReader.GetInt32(4)
                        ));
                    }
                    sqlReader.Close();
                }
            }
            catch (Exception e)
            {
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }

            return getDBFileList;
        }

    }
}