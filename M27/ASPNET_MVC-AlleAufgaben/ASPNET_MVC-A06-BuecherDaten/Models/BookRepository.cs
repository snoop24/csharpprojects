﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace ASPNET_MVC_A06_BuecherDaten.Models
{
    public class BookRepository
    {
        static string connString = @"Data Source=.\SQLEXPRESS; Initial Catalog=BooksDB;Integrated Security=True;";
        static SqlConnection connection;
        static SqlDataReader sqlReader;
        static SqlCommand sqlCom;

        public static List<Book> GetBooksByCategoryID(int categoryID)
        {
            List<Book> books = new List<Book>();
            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "" +
                        "SELECT b.ISBN as ISBN, b.Title as Titel, b.[Year] as 'Year', c.CategoryName as CategoryName " +
                        "FROM Books as b " +
                        "INNER JOIN Categories as c " +
                        "ON b.CategoryID = c.CategoryID " +
                        "WHERE b.CategoryID = @CategoryID" +
                        ";";

                    sqlCom.Parameters.AddWithValue("CategoryID", categoryID);

                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        books.Add(new Book(
                            sqlReader.GetString(0)
                            , sqlReader.GetString(1)
                            , sqlReader.GetDateTime(2)
                            , sqlReader.GetString(3)
                        ));

                    }

                }
            }
            catch (Exception e)
            {
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }

            return books;
        }

        public static List<Book> GetBooksByTitle(string bookTitle)
        {
            List<Book> books = new List<Book>();
            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "" +
                        "SELECT b.ISBN as ISBN, b.Title as Title, b.[Year] as 'Year', c.CategoryName as CategoryName " +
                        "FROM Books as b " +
                        "INNER JOIN Categories as c " +
                        "ON b.CategoryID = c.CategoryID " +
                        "WHERE b.Title LIKE @Title" +
                        ";";

                    sqlCom.Parameters.AddWithValue("Title", "%"+bookTitle+"%" );

                    sqlReader = sqlCom.ExecuteReader();
                    Debug.WriteLine(sqlCom.CommandText);
                    while (sqlReader.Read())
                    {
                        books.Add(new Book(
                            sqlReader.GetString(0)
                            , sqlReader.GetString(1)
                            , sqlReader.GetDateTime(2)
                            , sqlReader.GetString(3)
                        ));

                    }
                }
            }
            catch (Exception e)
            {
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }

            return books;
        }
        
        public static List<Book> GetAllBooks()

        {
            List<Book> books = new List<Book>();
            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "" +
                        "SELECT b.ISBN as ISBN, b.Title as Title, b.[Year] as 'Year', c.CategoryName as CategoryName " +
                        "FROM Books as b " +
                        "INNER JOIN Categories as c " +
                        "ON b.CategoryID = c.CategoryID; ";

                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        books.Add(new Book(
                            sqlReader.GetString(0)
                            , sqlReader.GetString(1)
                            , sqlReader.GetDateTime(2)
                            , sqlReader.GetString(3)
                        ));

                    }

                }
            }
            catch (Exception e)
            {
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }

            return books;
        }

        public static Dictionary<int,string> GetAllCategories()
        {
            Dictionary<int, string> categories = new Dictionary<int, string>();
            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "SELECT CategoryID,CategoryName FROM Categories;";

                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        categories.Add(
                            sqlReader.GetInt32(0)
                            , sqlReader.GetString(1)
                            );

                    }

                }
            }
            catch (Exception e)
            {
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }

            return categories;
        }

        

    }
}