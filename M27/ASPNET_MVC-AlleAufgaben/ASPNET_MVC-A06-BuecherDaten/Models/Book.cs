﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPNET_MVC_A06_BuecherDaten.Models
{
    public class Book
    {
        public string ISBN { get; set; }
        public string Title { get; set; }
        public DateTime Year { get; set; }
        public string CategoryName { get; set; }

        public Book() { }

        public Book(string iSBN, string title, DateTime year, string categoryName)
        {
            ISBN = iSBN;
            Title = title;
            Year = year;
            CategoryName = categoryName;
        }
    }
}