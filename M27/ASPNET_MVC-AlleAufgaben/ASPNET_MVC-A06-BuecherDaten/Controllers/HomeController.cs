﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A06_BuecherDaten.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
            //return RedirectToAction("AllBooks");
        }

        // GET: Search by Title
        public ActionResult SearchByTitle(string title)
        {
            List<Models.Book> books = null;
            if (title != null)
            {
                books = Models.BookRepository.GetBooksByTitle("net");
            }
            return View();
        }

        // GET: Search by Category
        public ActionResult SearchByCategory()
        {
            Dictionary<int, string> categories = Models.BookRepository.GetAllCategories();
            ViewData["searchFor"] = new SelectList(categories, "Key", "Value");

            return View(Models.BookRepository.GetBooksByCategoryID(1));
        }

        // GET: All Books
        public ActionResult AllBooks()
        {

            return View(Models.BookRepository.GetAllBooks());
        }

        // GET: Results Table

        public PartialViewResult GetResultsTableByTitle(string searchFor)
        {
            List<Models.Book> books = null;
            if (searchFor != null && searchFor.Length != 0)
            {
                books = Models.BookRepository.GetBooksByTitle(searchFor);
            }
            return PartialView("_GetResultsTable", books);
        }

        public PartialViewResult GetResultsTableByCategory(int searchFor)
        {
            List<Models.Book> books = null;
            if (searchFor != 0)
            {
                books = Models.BookRepository.GetBooksByCategoryID(searchFor);
            }
            return PartialView("_GetResultsTable", books);
        }
    }
}