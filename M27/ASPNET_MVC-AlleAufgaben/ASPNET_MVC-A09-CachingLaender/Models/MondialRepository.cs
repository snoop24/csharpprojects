﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ASPNET_MVC_A09_CachingLaender.Models
{
    public static class MondialRepository
    {
        static string connString = System.Configuration.ConfigurationManager.ConnectionStrings["MondialConnString"].ConnectionString;
        static SqlConnection connection;
        static SqlDataReader sqlReader;
        static SqlCommand sqlCom;


        public static List<Continent> GetContinents()
        {
            List<Continent> continents = new List<Continent>();
            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "" +
                        "SELECT Name, Area " +
                        "FROM dbo.continent " +
                        ";";

                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        continents.Add(new Continent(
                            sqlReader.GetString(0)
                            , sqlReader.GetInt32(1)
                        ));

                    }

                }
            }
            catch (Exception e)
            {
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }

            return continents;
        }

        public static List<Country> GetCountriesByContinent(string continentName)
        {
            List<Country> countries = new List<Country>();
            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText =
                        "SELECT [Name], [Code], [Capital], [Province], [Area], [Population], [Continent] " +
                        "FROM [Mondial].[dbo].[country] AS c " +
                            "INNER JOIN " +
                                "[Mondial].[dbo].[encompasses] AS e " +
                            "ON c.[Code] = e.[Country] " +
                        "WHERE e.[Continent]= @ContinentName; ";

                    sqlCom.Parameters.AddWithValue("ContinentName", continentName);

                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        countries.Add(new Country(
                            sqlReader.GetString(0)
                            , sqlReader.GetString(1)
                            , sqlReader.GetString(2)
                            , sqlReader.GetString(3)
                            , sqlReader.GetInt32(4)
                            , sqlReader.GetInt32(5)
                            , sqlReader.GetString(6)
                        ));

                    }
                }
            }
            catch (Exception e)
            {
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }

            return countries;
        }
    }
}