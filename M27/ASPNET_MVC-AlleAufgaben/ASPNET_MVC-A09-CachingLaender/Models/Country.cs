﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPNET_MVC_A09_CachingLaender.Models
{
    public class Country
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Capital { get; set; }
        public string Province { get; set; }
        public int Area { get; set; }
        public int Population { get; set; }
        public string ContinentName { get; set; }

        public Country(string name, string code, string capital, string province, int area, int population, string continentName)
        {
            Name = name;
            Code = code;
            Capital = capital;
            Province = province;
            Area = area;
            Population = population;
            ContinentName = continentName;
        }
    }
}