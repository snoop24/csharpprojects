﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPNET_MVC_A09_CachingLaender.Models
{
    public class Continent
    {
        public string Name { get; }
        public int Area { get; }

        public Continent(string name, int area)
        {
            Name = name;
            Area = area;
        }
    }
}