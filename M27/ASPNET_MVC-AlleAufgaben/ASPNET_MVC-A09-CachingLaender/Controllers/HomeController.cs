﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASPNET_MVC_A09_CachingLaender.Models;

namespace ASPNET_MVC_A09_CachingLaender.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewData["continentName"] = new SelectList(MondialRepository.GetContinents(), "Name", "Name");
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 3600, SqlDependency = "Mondial:dbo.country")]
        public PartialViewResult GetCountriesForContinent(string continentName)
        {
            return PartialView("_CountriesTable",MondialRepository.GetCountriesByContinent(continentName));
        }
    }
}