﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASPNET_MVC_A02_Helper.Utilities;

namespace ASPNET_MVC_A02_Helper.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Film> filme = FilmList.GetFilms();
            return View(filme);
        }

        [HttpPost]
        public ActionResult Index(string[] filmID)
        {
            List<Film> filme = FilmList.GetFilms();
            ViewBag.SelectedIDs = filmID;
            return View(filme);
        }
    }
}