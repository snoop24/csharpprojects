﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Diagnostics;

namespace ASPNET_MVC_A02_Helper.Utilities
{
    public class Film
    {
        public int Id { get; }
        public string Title { get; }
        public int Year { get; }
        public string Regie { get; }

        public Film(int id, string title, int year, string regie)
        {
            Id = id;
            Title = title;
            Year = year;
            Regie = regie;
        }
    }

    public static class FilmList {

        static List<Film> films;

        public static List<Film> GetFilms()
        {
            films = new List<Film>();

            
            try
            {
                using (StreamReader sr = new StreamReader(@"D:\BitBucket\csharpProjects\M27\ASPNET_MVC-AlleAufgaben\ASPNET_MVC-A02-Helper\filme.txt"))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] split = line.Split(';');
                        films.Add(new Film(int.Parse(split[0]), split[1], int.Parse(split[2]), split[3]));
                    }
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }


            return films;
        }

    }

}