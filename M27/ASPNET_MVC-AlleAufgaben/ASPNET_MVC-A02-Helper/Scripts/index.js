﻿window.onload = function () {
    var selectedFilms = document.getElementById('lblfilmsSelected');
    var cboxListe = document.getElementsByClassName('cboxsFilm');
    for (var item in cboxListe) {
        cboxListe[item].onclick = function () {
            var html = '';
            for (var item in cboxListe) {
                if (cboxListe[item].checked === true) {
                    html += (html === '' ? '' : ', ')+cboxListe[item].value ;
                }
            }
            selectedFilms.innerHTML = html;
        }
    }
}