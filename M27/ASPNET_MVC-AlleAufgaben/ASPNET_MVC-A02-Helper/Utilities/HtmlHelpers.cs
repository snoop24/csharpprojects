﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A02_Helper.Utilities
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString CheckBoxList(this HtmlHelper myHelper, string title,
            SelectList list)
        {
            return CheckBoxList(myHelper, title, list, null);
        }

        public static MvcHtmlString CheckBoxList(this HtmlHelper myHelper, string title,
            SelectList list, object[] objects)
        {
            string[] selectedIDs = (string[])objects;
            string idMessage = selectedIDs != null ? string.Join(", ", selectedIDs) : "keine Filme ausgewählt";

            string html = $"<h2>{title}</h2>\n";
            html += $"<p><label> IDs der übertragenen Filme: </label><label>{idMessage}</label></p>";
            html += $"<p><label> IDs der ausgewählten Filme: </label><label id=\"lblfilmsSelected\">{idMessage}</label></p>";

            foreach (var item in list)
            {
                string attrChecked = string.Empty;
                if (selectedIDs != null && selectedIDs.Contains(item.Value))
                {
                    attrChecked = " checked=\"checked\"";
                }

                html += $"<input type=\"checkbox\" class=\"cboxsFilm\" id=\"chBox{item.Value}\" name=\"filmID\" value=\"{item.Value}\"{attrChecked}/> <label for=\"chBox{item.Value}\" >{item.Text}</label><br/>\n";
            }

            return new MvcHtmlString(html);
        }
    }
}