﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A08_ActionFilter.Filters
{
    public class StopWatchFilterAttribute : FilterAttribute, IActionFilter, IResultFilter
    {
        private static Stopwatch sw = new Stopwatch();

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //sw.Stop();
            //filterContext.Controller.ViewBag.sw = sw.ElapsedMilliseconds;
            //Debug.WriteLine("ActionExecuted: time in ms " + sw.ElapsedMilliseconds);
            Debug.WriteLine("ActionExecuted: fertig");
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            sw.Reset();
            sw.Start();
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            sw.Stop();
            filterContext.Controller.ViewBag.sw = sw.ElapsedMilliseconds;
            Debug.WriteLine("ResultExecuted: time in ms " + sw.ElapsedMilliseconds);
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Debug.WriteLine("ResultExecuting: watch is running? " + sw.IsRunning);
            //sw.Stop();
            //filterContext.Controller.ViewBag.sw = sw.ElapsedMilliseconds;
            //Debug.WriteLine("ResultExecuting: time in ms " + sw.ElapsedMilliseconds);
        }
    }
}