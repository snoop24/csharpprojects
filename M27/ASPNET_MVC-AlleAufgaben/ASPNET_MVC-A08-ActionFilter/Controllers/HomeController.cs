﻿using ASPNET_MVC_A08_ActionFilter.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A08_ActionFilter.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [StopWatchFilter]
        public ActionResult StopIt()
        {
            System.Threading.Thread.Sleep(500);
            return View("Index");
        }

        [ExceptionFilter]
        public ActionResult ErrorIt()
        {

            int i = 1;
            int j = 1;
            i = i / (i - j);

            return View("Index");
        }
    }
}