﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A01_PasswortGenerieren.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int count, int minUpper, int minNumber, int minSpecial)
        {
            ViewData["password"] = Models.PasswordGenerator.GeneratePassword(count, minUpper, minNumber, minSpecial);
            return View();
        }
    }
}