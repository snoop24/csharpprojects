﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPNET_MVC_A01_PasswortGenerieren.Models
{
    static public class PasswordGenerator
    {
        static private Random rnd = new Random();

        static public void Test() {

            GeneratePassword(10,4);

        }


        static public string GeneratePassword(int count=6, int minUpper =2, int minNumber=2, int minSpecial=1)
        {
            // generate new pw aray
            char[] arrayPw = new char[count];

            // generate counts for character types
            int countSpecial;
            int countUpper;
            int countNumber;

            // generate loop until valid pw
            do
            {
                // reset counter
                countSpecial = 0;
                countUpper = 0;
                countNumber = 0;

                // generate password
                for (int i = 0; i < arrayPw.Length; i++)
                {
                    // add character and set counter
                    int temp = rnd.Next(33, 127);
                    if (temp > 32 && temp < 47)
                    {
                        arrayPw[i] = (char)temp;
                        countSpecial++;
                    }
                    else if (temp >= 48 && temp <= 57)
                    {
                        arrayPw[i] = (char)temp;
                        countNumber++;
                    }
                    else if (temp >= 65 && temp <= 90)
                    {
                        arrayPw[i] = (char)temp;
                        countUpper++;
                    }
                    else if (temp >= 97 && temp <= 122)
                    {
                        arrayPw[i] = (char)temp;
                    }
                }

            } while (!(countUpper >= minUpper && countNumber >= minNumber && countSpecial >= minSpecial && count> countUpper+countNumber+countSpecial));


            return arrayPw.ToString();
        }
    }
}