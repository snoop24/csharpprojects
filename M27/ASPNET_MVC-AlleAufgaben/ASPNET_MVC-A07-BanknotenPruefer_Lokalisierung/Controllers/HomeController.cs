﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A07_BanknotenPruefer_Lokalisierung.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        
        public MvcHtmlString SingleCheck(string serial) {
            return new MvcHtmlString(Models.BanknoteSerialChecker.CheckSerial(serial.Trim()));
        }

        public MvcHtmlString MultiCheck(string serials)
        {
            StringBuilder  validSB = new StringBuilder();

            string[] multiSerials = serials.Replace(System.Environment.NewLine,";").Split(';');

            foreach (var serial in multiSerials)
            {
                //if (!Models.BanknoteSerialChecker.IsValid(serial.Trim())) {
                    validSB.Append(Models.BanknoteSerialChecker.CheckSerial(serial.Trim()) + "<br/>");
                //}
            }

            return new MvcHtmlString(validSB.ToString());
        }
    }
}