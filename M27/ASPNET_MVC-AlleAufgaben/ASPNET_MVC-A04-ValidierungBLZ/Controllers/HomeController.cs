﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNET_MVC_A04_ValidierungBLZ.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            //List<Models.Bank> banken = Models.BankRepository.LoadBanken();
            return View(new Models.Konto());
        }

        public JsonResult CheckBLZ(string blz)
        {
            bool state = Models.BankRepository.CheckBLZ(blz);

            return Json(state, JsonRequestBehavior.AllowGet);
        }
    }
}