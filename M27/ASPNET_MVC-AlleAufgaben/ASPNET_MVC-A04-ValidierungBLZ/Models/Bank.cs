﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPNET_MVC_A04_ValidierungBLZ.Models
{
    public class Bank
    {
        public string BLZ { get; set; }
        public int Merkmal { get; set; }
        public string Bezeichnung { get; set; }
        public int PLZ {get; set;}
        public string Ort {get; set;}
        public string Kurzbezeichnung {get; set;}
        public int PAN {get; set;}
        public string BIC {get; set;}
        public string Pruefzifferberechnungsmethode {get; set;}
        public int Datensatznummer { get; set; }
        public string Aenderungskennzeichen {get; set;}
        public int Bankleitzahlloeschung {get; set;}
        public string NachfolgeBLZ { get; set; }

        public Bank(string bLZ, int merkmal, string bezeichnung, int pLZ, string ort, string kurzbezeichnung, int pAN, string bIC, string pruefzifferberechnungsmethode, int datensatznummer, string aenderungskennzeichen, int bankleitzahlloeschung, string nachfolgeBLZ)
        {
            BLZ = bLZ;
            Merkmal = merkmal;
            Bezeichnung = bezeichnung;
            PLZ = pLZ;
            Ort = ort;
            Kurzbezeichnung = kurzbezeichnung;
            PAN = pAN;
            BIC = bIC;
            Pruefzifferberechnungsmethode = pruefzifferberechnungsmethode;
            Datensatznummer = datensatznummer;
            Aenderungskennzeichen = aenderungskennzeichen;
            Bankleitzahlloeschung = bankleitzahlloeschung;
            NachfolgeBLZ = nachfolgeBLZ;
        }

        public Bank() { }
    }
}