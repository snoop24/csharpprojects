﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ASPNET_MVC_A04_ValidierungBLZ.Models
{
    public class Konto
    {
        //[StringLength(8,MinimumLength =8)]
        [RegularExpression("^[0-9]{8}$",ErrorMessage ="genau 8 Ziffern angeben")]
        [Remote("CheckBLZ","Home",ErrorMessage ="BLZ unbekannt")]
        public string BLZ { get; set; }

        [RegularExpression("^[0-9]{3,}", ErrorMessage = "nur Ziffern (min. 3)")]
        public string Nummer { get; set; }
    }
}