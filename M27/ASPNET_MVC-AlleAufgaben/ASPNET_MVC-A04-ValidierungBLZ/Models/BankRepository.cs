﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;


namespace ASPNET_MVC_A04_ValidierungBLZ.Models
{
    public static class BankRepository
    {
        static SqlConnection connection;
        static string connString = @"Data Source=.\SQLEXPRESS; Initial Catalog=TeachSQL;Integrated Security=True;";
        static SqlDataReader sqlReader;
        static SqlCommand sqlCom;

        public static List<Bank> Banken { get; set; }

        public static List<Bank> LoadBanken()
        {
            Banken = new List<Bank>();

            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();

                    sqlCom = connection.CreateCommand();

                    // alle Banken abfragen
                    sqlCom.CommandText = "SELECT * FROM dbo.BLZTabelle";

                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {

                        // Alle Banken in einer Liste speichern
                        Banken.Add(new Bank(
                            sqlReader.GetString(0)
                            , sqlReader.GetInt16(1)
                            , sqlReader.GetString(2)
                            , sqlReader.GetInt32(3)
                            , sqlReader.GetString(4)
                            , sqlReader.GetString(5)
                            , (sqlReader.IsDBNull(6) ? 0 : sqlReader.GetInt32(6))
                            , (sqlReader.IsDBNull(7) ? "" : sqlReader.GetString(7))
                            , sqlReader.GetString(8)
                            , sqlReader.GetInt32(9)
                            , sqlReader.GetString(10)
                            , sqlReader.GetInt16(11)
                            , sqlReader.GetString(12)
                        ));
                    }
                    sqlReader.Close();
                }
            }
            catch (Exception e)
            {
                // TODO Fehlermeldung an den Client senden
                Debug.Indent();
                Debug.WriteLine(e.Message);
            }

            return Banken;
        }

        public static List<Bank> GetBankByBLZ(string blz)
        {
            List<Bank> bankenByBLZ = new List<Bank>();

            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();

                    sqlCom = connection.CreateCommand();

                    // Id und Titel aller Filme abfragen
                    sqlCom.CommandText = "SELECT * FROM dbo.BLZTabelle WHERE BLZ=@BLZ";

                    sqlCom.Parameters.AddWithValue("BLZ", blz);

                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        // Alle Filme in einer Liste speichern
                        bankenByBLZ.Add(new Bank(
                            sqlReader.GetString(0)
                            , sqlReader.GetInt16(1)
                            , sqlReader.GetString(2)
                            , sqlReader.GetInt32(3)
                            , sqlReader.GetString(4)
                            , sqlReader.GetString(5)
                            , (sqlReader.IsDBNull(6) ? 0 : sqlReader.GetInt32(6))
                            , (sqlReader.IsDBNull(7) ? "" : sqlReader.GetString(7))
                            , sqlReader.GetString(8)
                            , sqlReader.GetInt32(9)
                            , sqlReader.GetString(10)
                            , sqlReader.GetInt16(11)
                            , sqlReader.GetString(12)
                        ));
                    }
                    sqlReader.Close();
                }
            }
            catch
            {
                // TODO Fehlermeldung an den Client senden
            }

            return bankenByBLZ;
        }

        public static bool CheckBLZ(string blz)
        {
            return GetBankByBLZ(blz).Count > 0;
        }
    }
}