﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace MVC_DI_A01_KontakteDateienDB.Models
{
    public class DBContactRepository : IContactRepository
    {
        private static string connString = @"Data Source=.\SQLEXPRESS; Initial Catalog=NETDB;Integrated Security=True;";
        private SqlConnection connection;
        private SqlDataReader sqlReader;
        private SqlCommand sqlCom;

        public List<Contact> LoadAll()
        {
            List<Contact> contactList = new List<Contact>();

            try
            {
                using (connection = new SqlConnection(connString)) {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "SELECT Name, Vorname, Telefon FROM dbo.tblKontakt";
                    sqlReader = sqlCom.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        contactList.Add(new Contact()
                        {
                            LastName = sqlReader.GetString(0)
                            ,FirstName = sqlReader.GetString(1)
                            ,PhoneNr = sqlReader.GetString(2)
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("DPContactRepos.LoadAll: "+e.Message);
            }
            return contactList;
        }

        public void SaveContact(Contact contact)
        {
            try
            {
                using (connection = new SqlConnection(connString))
                {
                    connection.Open();
                    sqlCom = connection.CreateCommand();
                    sqlCom.CommandText = "INSERT INTO dbo.tblKontakt " +
                        "VALUES (@Name, @Vorname, @Telefon, @EMail);";
                    sqlCom.Parameters.AddWithValue("Name", contact.LastName);
                    sqlCom.Parameters.AddWithValue("Vorname", contact.FirstName);
                    sqlCom.Parameters.AddWithValue("Telefon", contact.PhoneNr);
                    sqlCom.Parameters.AddWithValue("EMail", "default@default.def");
                    sqlCom.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("DPContactRepos.SaveContact: " + e.Message);
            }
        }
    }
}