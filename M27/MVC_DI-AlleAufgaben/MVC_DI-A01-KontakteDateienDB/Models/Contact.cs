﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_DI_A01_KontakteDateienDB.Models
{
    public class Contact
    {
        [Display(Name="Surname")]
        public string LastName { get; set; }
        [Display(Name = "Forename")]
        public string FirstName { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNr { get; set; }
    }
}