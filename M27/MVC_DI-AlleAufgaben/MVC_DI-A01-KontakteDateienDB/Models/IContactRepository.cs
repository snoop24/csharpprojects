﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC_DI_A01_KontakteDateienDB.Models
{
    public interface IContactRepository
    {
        void SaveContact(Contact item);
        List<Contact> LoadAll();
    }
}
