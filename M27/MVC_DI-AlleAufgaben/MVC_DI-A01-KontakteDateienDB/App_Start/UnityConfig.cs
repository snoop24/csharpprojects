using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using MVC_DI_A01_KontakteDateienDB;

namespace MVC_DI_A01_KontakteDateienDB
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<Models.IContactRepository, Models.DBContactRepository>();
            container.Resolve<Controllers.HomeController>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}