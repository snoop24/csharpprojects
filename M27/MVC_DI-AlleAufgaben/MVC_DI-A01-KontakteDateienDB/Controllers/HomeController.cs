﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_DI_A01_KontakteDateienDB.Models;

namespace MVC_DI_A01_KontakteDateienDB.Controllers
{
    public class HomeController : Controller
    {
        private IContactRepository contacts;

        public HomeController(Models.IContactRepository repository) {
            contacts = repository;
        }
        
        // GET: Home
        public ActionResult Index()
        {
            return View(contacts.LoadAll());
        }

        public ActionResult NewContact(Contact contact) {
            if (contact != null && contact.PhoneNr+contact.FirstName + contact.LastName != "") {
                contact.LastName = contact.LastName ?? "defaultLastName";
                contact.FirstName = contact.FirstName ?? "defaultFirstName";
                contact.PhoneNr = contact.PhoneNr ?? "555-55555";
                contacts.SaveContact(contact);
            }
            return View(new Contact());
        }
    }
}