﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTest_A01_MeileKilometerYard;

namespace UnitTest_A01
{
    [TestClass]
    public class KilometerTests
    {
        [TestMethod]
        public void KilometerTest()
        {
            // Arrange
            Kilometer kilometer = new Kilometer(3.14159);

            // Act


            // Assert
            Assert.AreEqual(3.14159, kilometer.Value );
        }

        [TestMethod]
        public void ValueTest()
        {
            // Arrange
            Kilometer kilometer = new Kilometer(3.14159);

            // Act
            kilometer.Value = 1.41;

            // Assert
            Assert.AreEqual(1.41, kilometer.Value);
        }

        [TestMethod]
        public void ToStringTest()
        {
            // Arrange
            Kilometer kilometer = new Kilometer(1);
            string expectedString = "1 Kilometer";

            // Act
            string kilometerToString = kilometer.ToString();

            // Assert
            Assert.AreEqual(expectedString, kilometerToString);
        }

        [TestMethod]
        public void ConversionTest()
        {
            // Arrange
            Meter meter = new Meter(1);
            Meile meile = new Meile(1);
            Yard yard = new Yard(1);

            double expKiloMeter = 0.001;
            double expKiloMeile = 1.609344;
            double expKiloYard = 0.0009144;

            // Act
            Kilometer kiloMeter = (Kilometer) meter;
            Kilometer kiloMeile = (Kilometer)meile;
            Kilometer kiloYard = (Kilometer)yard;

            // Assert
            Assert.AreEqual(kiloMeter.Value, expKiloMeter);
            Assert.AreEqual(kiloMeile.Value, expKiloMeile);
            Assert.AreEqual(kiloYard.Value, expKiloYard);
        }

        [TestMethod]
        public void AdditionTest()
        {
            // Arrange
            Kilometer kilometer = new Kilometer(10);
            Kilometer kilometer2 = new Kilometer(2);
            Meter meter = new Meter(5);
            Meile meile = new Meile(1);
            Yard yard = new Yard(10);

            double expKilo12 = 12;
            double expKiloMeter = 10.005;
            double expKiloMeile = 11.609344;
            double expKiloYard = 10.009144;

            // Act
            Kilometer kilo12 = kilometer + kilometer2;
            Kilometer kiloMeter = kilometer + meter;
            Kilometer kiloMeile = kilometer + meile;
            Kilometer kiloYard = kilometer + yard;

            // Assert
            Assert.AreEqual(kilo12.Value, expKilo12);
            Assert.AreEqual(kiloMeter.Value, expKiloMeter);
            Assert.AreEqual(kiloMeile.Value, expKiloMeile);
            Assert.AreEqual(kiloYard.Value, expKiloYard);
        }
    }
}
