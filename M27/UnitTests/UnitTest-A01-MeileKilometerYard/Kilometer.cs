﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest_A01_MeileKilometerYard
{
    class Kilometer
    {
        private double value = 0;

        public double Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public Kilometer(double value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString() + " Kilometer";
        }

        #region operator calculation
        // addition
        public static Kilometer operator +(Kilometer x, Kilometer y)
        {
            return new Kilometer(x.value + y.value);
        }
        public static Kilometer operator +(Kilometer x, Yard y)
        {
            return x + (Kilometer)y;
        }
        public static Kilometer operator +(Kilometer x, Meter y)
        {
            return x + (Kilometer)y;
        }
        public static Kilometer operator +(Kilometer x, Meile y)
        {
            return x + (Kilometer)y;
        }

        // subtraction
        public static Kilometer operator -(Kilometer x, Kilometer y)
        {
            return new Kilometer(x.value - y.value);
        }
        public static Kilometer operator -(Kilometer x, Yard y)
        {
            return x - (Kilometer)y;
        }
        public static Kilometer operator -(Kilometer x, Meter y)
        {
            return x - (Kilometer)y;
        }
        public static Kilometer operator -(Kilometer x, Meile y)
        {
            return x - (Kilometer)y;
        }
        #endregion

        #region conversion
        public static explicit operator Kilometer(Meter x)
        {
            return new Kilometer(x.Value / 1000);
        }
        public static explicit operator Kilometer(Yard x)
        {
            return new Kilometer(x.Value * 0.9144 / 1000);
        }
        public static explicit operator Kilometer(Meile x)
        {
            return new Kilometer(x.Value * 1760 * 0.9144 / 1000);
        }
        #endregion
    }
}
