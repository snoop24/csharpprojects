﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest_A01_MeileKilometerYard
{
    class Yard
    {
        private double value = 0;

        public double Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public Yard(double value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString() + " Yard(s)";
        }

        #region operator calculation
        // addition
        public static Yard operator +(Yard x, Yard y)
        {
            return new Yard(x.value + y.value);
        }
        public static Yard operator +(Yard x, Kilometer y)
        {
            return x + (Yard)y;
        }
        public static Yard operator +(Yard x, Meter y)
        {
            return x + (Yard)y;
        }
        public static Yard operator +(Yard x, Meile y)
        {
            return x + (Yard)y;
        }

        // subtraction
        public static Yard operator -(Yard x, Yard y)
        {
            return new Yard(x.value - y.value);
        }
        public static Yard operator -(Yard x, Kilometer y)
        {
            return x - (Yard)y;
        }
        public static Yard operator -(Yard x, Meter y)
        {
            return x - (Yard)y;
        }
        public static Yard operator -(Yard x, Meile y)
        {
            return x - (Yard)y;
        }
        #endregion

        #region conversion
        public static explicit operator Yard(Meile x)
        {
            return new Yard(x.Value * 1760);
        }
        public static explicit operator Yard(Meter x)
        {
            return new Yard(x.Value / 0.9144);
        }
        public static explicit operator Yard(Kilometer x)
        {
            return new Yard(x.Value * 1000 / 0.9144);
        }
        #endregion
    }
}
