﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace EF_ModelFirst_A01_Kurse.Models
{
    public partial class Kurs
    {
        private sealed class Metadata
        {
            [DisplayName("Class")]
            public string Name { get; set; }
            [DisplayName("Start Date")]
            public System.DateTime StartDate { get; set; }
            [DisplayName("Duration in Days")]
            public int DurationD { get; set; }

        }

        public string ToCSVLine()
        {
            return $"{this.Id };{this.Name};{this.StartDate};{this.DurationD}";
        }

        public static string AddKursFromCSV(HttpPostedFileBase file)
        {
            string message = "file successfully uploaded";
            try
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(file.InputStream))
                {
                    KursVerwaltungContainer ctx = new KursVerwaltungContainer();
                    if (!sr.EndOfStream) { sr.ReadLine(); }
                    while (!sr.EndOfStream)
                    {
                        string[] contents = sr.ReadLine().Split(';');
                        ctx.KursSet.Add(new Kurs()
                        {
                            Name = contents[1]
                               ,
                            StartDate = Convert.ToDateTime(contents[2])
                               ,
                            DurationD = int.Parse(contents[3])
                        });


                    }

                    ctx.SaveChanges();

                }

            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        public static byte[] GetAsCSV()
        {
            KursVerwaltungContainer ctx = new KursVerwaltungContainer();

            StringBuilder sb = new StringBuilder();
            sb.Append("KursId;Kursname;Starttermin;DauerInTagen" + System.Environment.NewLine);
            ctx.KursSet.ToList().ForEach(item => sb.Append(item.ToCSVLine()+System.Environment.NewLine));;

            return Encoding.UTF8.GetBytes(sb.ToString());
        }
    }
}