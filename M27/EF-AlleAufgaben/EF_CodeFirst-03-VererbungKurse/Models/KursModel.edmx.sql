
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/13/2018 12:05:55
-- Generated from EDMX file: D:\BitBucket\csharpProjects\M27\EF-AlleAufgaben\EF_CodeFirst-03-VererbungKurse\Models\KursModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [EF_Kurse];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'KurseSet'
CREATE TABLE [dbo].[KurseSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Nachweis] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'KurseSet_Online'
CREATE TABLE [dbo].[KurseSet_Online] (
    [URL] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'KurseSet_InHouse'
CREATE TABLE [dbo].[KurseSet_InHouse] (
    [Location] nvarchar(max)  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [DurationD] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'KurseSet'
ALTER TABLE [dbo].[KurseSet]
ADD CONSTRAINT [PK_KurseSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KurseSet_Online'
ALTER TABLE [dbo].[KurseSet_Online]
ADD CONSTRAINT [PK_KurseSet_Online]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KurseSet_InHouse'
ALTER TABLE [dbo].[KurseSet_InHouse]
ADD CONSTRAINT [PK_KurseSet_InHouse]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Id] in table 'KurseSet_Online'
ALTER TABLE [dbo].[KurseSet_Online]
ADD CONSTRAINT [FK_Online_inherits_Kurse]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[KurseSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'KurseSet_InHouse'
ALTER TABLE [dbo].[KurseSet_InHouse]
ADD CONSTRAINT [FK_InHouse_inherits_Kurse]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[KurseSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------