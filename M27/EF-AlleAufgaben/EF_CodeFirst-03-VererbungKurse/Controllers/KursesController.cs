﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EF_CodeFirst_03_VererbungKurse.Models;

namespace EF_CodeFirst_03_VererbungKurse.Controllers
{
    public class KursesController : Controller
    {
        private KursModelContainer db = new KursModelContainer();

        // GET: Kurses
        public ActionResult Index()
        {
            return View(db.KurseSet.ToList());
        }

        // GET: Kurses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kurse kurse = db.KurseSet.Find(id);
            if (kurse == null)
            {
                return HttpNotFound();
            }
            return View(kurse);
        }

        // GET: Kurses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kurses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Nachweis")] Kurse kurse)
        {
            if (ModelState.IsValid)
            {
                db.KurseSet.Add(kurse);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kurse);
        }

        // GET: Kurses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kurse kurse = db.KurseSet.Find(id);
            if (kurse == null)
            {
                return HttpNotFound();
            }
            return View(kurse);
        }

        // POST: Kurses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Nachweis")] Kurse kurse)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kurse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kurse);
        }

        // GET: Kurses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kurse kurse = db.KurseSet.Find(id);
            if (kurse == null)
            {
                return HttpNotFound();
            }
            return View(kurse);
        }

        // POST: Kurses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kurse kurse = db.KurseSet.Find(id);
            db.KurseSet.Remove(kurse);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
