﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EF_CodeFirst_03_VererbungKurse.Models;

namespace EF_CodeFirst_03_VererbungKurse.Controllers
{
    public class InHousesController : Controller
    {
        private KursModelContainer db = new KursModelContainer();

        // GET: InHouses
        public ActionResult Index()
        {
            return View(db.InHouses.ToList());
        }

        // GET: InHouses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InHouse inHouse = db.InHouses.Find(id);
            if (inHouse == null)
            {
                return HttpNotFound();
            }
            return View(inHouse);
        }

        // GET: InHouses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InHouses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Location,StartDate,DurationD,Name,Nachweis")] InHouse inHouse)
        {
            if (ModelState.IsValid)
            {
                db.InHouses.Add(inHouse);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inHouse);
        }

        // GET: InHouses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InHouse inHouse = db.InHouses.Find(id);
            if (inHouse == null)
            {
                return HttpNotFound();
            }
            return View(inHouse);
        }

        // POST: InHouses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Location,StartDate,DurationD,Name,Nachweis")] InHouse inHouse)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inHouse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inHouse);
        }

        // GET: InHouses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InHouse inHouse = db.InHouses.Find(id);
            if (inHouse == null)
            {
                return HttpNotFound();
            }
            return View(inHouse);
        }

        // POST: InHouses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InHouse inHouse = db.InHouses.Find(id);
            db.InHouses.Remove(inHouse);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
