namespace EF_CodeFirst_02_Migrations_Buecherdaten.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class PublisherRepository : DbContext
    {
        // Your context has been configured to use a 'PublisherRepository' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'EF_CodeFirst_02_Migrations_Buecherdaten.Models.PublisherRepository' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'PublisherRepository' 
        // connection string in the application configuration file.
        public PublisherRepository()
            : base("name=PublisherRepository")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Publisher> Publishers { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}