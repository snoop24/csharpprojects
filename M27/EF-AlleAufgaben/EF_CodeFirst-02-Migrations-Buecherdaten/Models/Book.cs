﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EF_CodeFirst_02_Migrations_Buecherdaten.Models
{
    public class Book
    {
        [Key]
        [Required]
        public int ID { get; set; }

        [Required]
        public string Title { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int PageCount { get; set; }

        public string Language { get; set; }

        public virtual Publisher Publisher { get; set; }

        public int PublisherID { get; set; }
    }
}