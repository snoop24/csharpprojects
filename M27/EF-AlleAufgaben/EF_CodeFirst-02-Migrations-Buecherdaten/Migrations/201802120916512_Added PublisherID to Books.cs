namespace EF_CodeFirst_02_Migrations_Buecherdaten.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPublisherIDtoBooks : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.Books SET Publisher_ID = 1 WHERE Publisher_ID IS NULL;");
            DropForeignKey("dbo.Books", "Publisher_ID", "dbo.Publishers");
            DropIndex("dbo.Books", new[] { "Publisher_ID" });
            RenameColumn(table: "dbo.Books", name: "Publisher_ID", newName: "PublisherID");
            AlterColumn("dbo.Books", "PublisherID", c => c.Int(nullable: false,defaultValue: 1));
            CreateIndex("dbo.Books", "PublisherID");
            AddForeignKey("dbo.Books", "PublisherID", "dbo.Publishers", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "PublisherID", "dbo.Publishers");
            DropIndex("dbo.Books", new[] { "PublisherID" });
            AlterColumn("dbo.Books", "PublisherID", c => c.Int());
            RenameColumn(table: "dbo.Books", name: "PublisherID", newName: "Publisher_ID");
            CreateIndex("dbo.Books", "Publisher_ID");
            AddForeignKey("dbo.Books", "Publisher_ID", "dbo.Publishers", "ID");
        }
    }
}
