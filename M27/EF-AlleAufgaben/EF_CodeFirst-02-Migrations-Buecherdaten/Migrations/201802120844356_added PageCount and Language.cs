namespace EF_CodeFirst_02_Migrations_Buecherdaten.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedPageCountandLanguage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "PageCount", c => c.Int(nullable: false));
            AddColumn("dbo.Books", "Language", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "Language");
            DropColumn("dbo.Books", "PageCount");
        }
    }
}
