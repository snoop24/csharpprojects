namespace EF_CodeFirst_02_Migrations_Buecherdaten.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPublisher : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Publishers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Books", "Publisher_ID", c => c.Int());
            CreateIndex("dbo.Books", "Publisher_ID");
            AddForeignKey("dbo.Books", "Publisher_ID", "dbo.Publishers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "Publisher_ID", "dbo.Publishers");
            DropIndex("dbo.Books", new[] { "Publisher_ID" });
            DropColumn("dbo.Books", "Publisher_ID");
            DropTable("dbo.Publishers");
        }
    }
}
