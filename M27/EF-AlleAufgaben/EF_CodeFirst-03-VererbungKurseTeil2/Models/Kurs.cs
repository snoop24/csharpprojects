﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EF_CodeFirst_03_VererbungKurseTeil2.Models
{
    public abstract class Kurs
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Nachweis { get; set; }
    }
}