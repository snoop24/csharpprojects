namespace EF_CodeFirst_03_VererbungKurseTeil2.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class KurseRepository : DbContext
    {
        // Your context has been configured to use a 'KurseRepository' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'EF_CodeFirst_03_VererbungKurseTeil2.Models.KurseRepository' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'KurseRepository' 
        // connection string in the application configuration file.
        public KurseRepository()
            : base("name=KurseRepository")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        public DbSet<Kurs> Kurse { get; set; }

        public DbSet<InHouse> InHouses { get; set; }

        public DbSet<Online> Onlines { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}