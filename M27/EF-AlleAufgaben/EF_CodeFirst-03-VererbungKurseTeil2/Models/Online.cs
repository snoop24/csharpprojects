﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EF_CodeFirst_03_VererbungKurseTeil2.Models
{
    [Table("Online")]
    public class Online: Kurs
    {
        public string URL { get; set; }
    }
}