﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EF_CodeFirst_03_VererbungKurseTeil2.Models
{
    [Table("InHouse")]
    public class InHouse: Kurs
    {
        public string Location { get; set; }
        public DateTime StartDate { get; set; }
        public int DurationD { set; get; }
    }
}