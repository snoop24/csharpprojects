﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EF_CodeFirst_03_VererbungKurseTeil2.Models;

namespace EF_CodeFirst_03_VererbungKurseTeil2.Controllers
{
    public class OnlinesController : Controller
    {
        private KurseRepository db = new KurseRepository();

        // GET: Onlines
        public ActionResult Index()
        {
            return View(db.Kurse.OfType<Online>().ToList());
        }

        // GET: Onlines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Online online = db.Kurse.Find(id) as Online;
            if (online == null)
            {
                return HttpNotFound();
            }
            return View(online);
        }

        // GET: Onlines/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Onlines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Nachweis,URL")] Online online)
        {
            if (ModelState.IsValid)
            {
                db.Kurse.Add(online);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(online);
        }

        // GET: Onlines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Online online = db.Kurse.Find(id) as Online;
            if (online == null)
            {
                return HttpNotFound();
            }
            return View(online);
        }

        // POST: Onlines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Nachweis,URL")] Online online)
        {
            if (ModelState.IsValid)
            {
                db.Entry(online).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(online);
        }

        // GET: Onlines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Online online = db.Kurse.Find(id) as Online;
            if (online == null)
            {
                return HttpNotFound();
            }
            return View(online);
        }

        // POST: Onlines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Online online = db.Kurse.Find(id) as Online;
            db.Kurse.Remove(online);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
