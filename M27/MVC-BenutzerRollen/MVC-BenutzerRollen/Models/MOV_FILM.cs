//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC_BenutzerRollen.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MOV_FILM
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MOV_FILM()
        {
            this.MOV_BESETZUNG = new HashSet<MOV_BESETZUNG>();
        }
    
        public int id { get; set; }
        public string titel { get; set; }
        public Nullable<decimal> jahr { get; set; }
        public Nullable<double> punkte { get; set; }
        public Nullable<int> stimmen { get; set; }
        public Nullable<int> regie { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MOV_BESETZUNG> MOV_BESETZUNG { get; set; }
        public virtual MOV_PERSON MOV_PERSON { get; set; }
    }
}
