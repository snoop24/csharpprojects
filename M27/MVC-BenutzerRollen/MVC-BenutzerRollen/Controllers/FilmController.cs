﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_BenutzerRollen.Models;

namespace MVC_BenutzerRollen.Controllers
{
    [Authorize]
    public class FilmController : Controller
    {
        private TeachSQLEntities2 db = new TeachSQLEntities2();

        // GET: Film
        [AllowAnonymous]
        public ActionResult Index()
        {
            var mOV_FILM = db.MOV_FILM.Include(m => m.MOV_PERSON);
            return View(mOV_FILM.OrderBy(x => -x.punkte).Take(10).ToList());
        }

        // GET: Film/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOV_FILM mOV_FILM = db.MOV_FILM.Find(id);
            if (mOV_FILM == null)
            {
                return HttpNotFound();
            }
            return View(mOV_FILM);
        }

        // GET: Film/Create
        public ActionResult Create()
        {
            ViewBag.regie = new SelectList(db.MOV_PERSON, "id", "name");
            return View();
        }

        // POST: Film/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,titel,jahr,punkte,stimmen,regie")] MOV_FILM mOV_FILM)
        {
            if (ModelState.IsValid)
            {
                db.MOV_FILM.Add(mOV_FILM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.regie = new SelectList(db.MOV_PERSON, "id", "name", mOV_FILM.regie);
            return View(mOV_FILM);
        }

        // GET: Film/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOV_FILM mOV_FILM = db.MOV_FILM.Find(id);
            if (mOV_FILM == null)
            {
                return HttpNotFound();
            }
            ViewBag.regie = new SelectList(db.MOV_PERSON, "id", "name", mOV_FILM.regie);
            return View(mOV_FILM);
        }

        // POST: Film/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,titel,jahr,punkte,stimmen,regie")] MOV_FILM mOV_FILM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mOV_FILM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.regie = new SelectList(db.MOV_PERSON, "id", "name", mOV_FILM.regie);
            return View(mOV_FILM);
        }

        // GET: Film/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOV_FILM mOV_FILM = db.MOV_FILM.Find(id);
            if (mOV_FILM == null)
            {
                return HttpNotFound();
            }
            return View(mOV_FILM);
        }

        // POST: Film/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MOV_FILM mOV_FILM = db.MOV_FILM.Find(id);
            db.MOV_FILM.Remove(mOV_FILM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
