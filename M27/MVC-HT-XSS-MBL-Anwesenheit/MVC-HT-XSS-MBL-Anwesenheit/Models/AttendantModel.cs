namespace MVC_HT_XSS_MBL_Anwesenheit.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class AttendantModel : DbContext
    {
        // Your context has been configured to use a 'AttendantModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'MVC_HT_XSS_MBL_Anwesenheit.Models.AttendantModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'AttendantModel' 
        // connection string in the application configuration file.
        public AttendantModel()
            : base("name=AttendantModel")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Attendant> Attendants { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}