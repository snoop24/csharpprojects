﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_HT_XSS_MBL_Anwesenheit.Models
{
    public class Attendant
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Present { get; set; }
        public int AbsentDays { get; set; }
    }
}