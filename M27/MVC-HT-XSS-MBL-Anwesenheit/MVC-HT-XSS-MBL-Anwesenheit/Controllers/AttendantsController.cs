﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_HT_XSS_MBL_Anwesenheit.Models;

namespace MVC_HT_XSS_MBL_Anwesenheit.Controllers
{
    public class AttendantsController : Controller
    {
        private AttendantModel db = new AttendantModel();

        // GET: Attendants
        public ActionResult Index()
        {
            return View(db.Attendants.ToList());
        }

        // GET: Attendants/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendant attendant = db.Attendants.Find(id);
            if (attendant == null)
            {
                return HttpNotFound();
            }
            return View(attendant);
        }

        // GET: Attendants/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Attendants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Present,AbsentDays")] Attendant attendant)
        {
            if (ModelState.IsValid)
            {
                db.Attendants.Add(attendant);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(attendant);
        }

        // GET: Attendants/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendant attendant = db.Attendants.Find(id);
            if (attendant == null)
            {
                return HttpNotFound();
            }
            return View(attendant);
        }

        // POST: Attendants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Present,AbsentDays")] Attendant attendant)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attendant).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(attendant);
        }

        // GET: Attendants/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendant attendant = db.Attendants.Find(id);
            if (attendant == null)
            {
                return HttpNotFound();
            }
            return View(attendant);
        }

        // POST: Attendants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Attendant attendant = db.Attendants.Find(id);
            db.Attendants.Remove(attendant);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
