﻿using System.Web;
using System.Web.Mvc;

namespace MVC_HT_XSS_MBL_Anwesenheit
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
