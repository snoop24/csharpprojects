﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MVC_HT_XSS_MBL_Anwesenheit.Startup))]
namespace MVC_HT_XSS_MBL_Anwesenheit
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
