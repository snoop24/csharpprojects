namespace MVC_BenutzerRollenITSurvey.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class Query : DbContext
    {
        // Your context has been configured to use a 'Query' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'MVC_BenutzerRollenITSurvey.Models.Query' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Query' 
        // connection string in the application configuration file.
        public Query()
            : base("name=Query")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UsersQuestionAnswer> UsersQuestionAnswers { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}