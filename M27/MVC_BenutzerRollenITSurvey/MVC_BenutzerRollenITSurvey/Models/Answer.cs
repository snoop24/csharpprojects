﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_BenutzerRollenITSurvey.Models
{
    public class Answer
    {
        [Key]
        public int Id { get; set; }
        public string Text { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
    }
}