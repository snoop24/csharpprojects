﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_BenutzerRollenITSurvey.Models
{
    public class UsersQuestionAnswer
    {
        [ForeignKey("Id")]
        public virtual User User { get; set; }
        [ForeignKey("Id")]
        public virtual Question Question { get; set; }
        [ForeignKey("Id")]
        public virtual Answer Answer { get; set; }
    }
}