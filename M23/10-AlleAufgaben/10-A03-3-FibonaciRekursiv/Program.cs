﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_A03_3_FibonaciRekursiv
{
    class Program
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                int n, nTemp, cut;
                ulong f;
                string fString;
                string[] a = new string[2];
                string[] fTemp = new string[2];

                // wipe screen and print title
                TitleOutput("n. Fibonacci Zahl (n +int)");

                // getinputs
                string prompt, error = "\n   Zahl? > 0 ? \n";

                //read n
                prompt = " n: ";
                n = GetInputInt(prompt, error, 1);
                Console.WriteLine();

                a[0] = "0";
                a[1] = "1";
                cut = 5000;
                // calc
                //f = FibonacciSlow(n);
                //f = FibonacciFast(n, ulong.Parse(a[1]), ulong.Parse(a[0]));
                //f = FibonacciFastPrint(n, ulong.Parse(a[1]), ulong.Parse(a[0]));

                // for bigger numbers
                //while (n > cut)
                //{
                //    fString = FibonacciFastPrintString(n, a[1], a[0]);
                //    nTemp = nTemp - cut;
                //}
                fTemp[0] = string.Join("", a[0].Reverse().ToArray());
                fTemp[1] = string.Join("", a[1].Reverse().ToArray());
                nTemp = n;
                while (nTemp > cut)
                {
                    fTemp = FibonacciFastPrintString(cut+1, fTemp[1], fTemp[0]);
                    nTemp -= cut;
                }
                fTemp = FibonacciFastPrintString(nTemp, fTemp[1], fTemp[0]);
                fString = string.Join("", fTemp[0].Reverse().ToArray());


                // output
                //ForeColoredOutput(string.Format("\n Die {0}. Fibonacci-Zahl ist {1}.\n", n, f));

                // output AddString
                ForeColoredOutput(string.Format("\n Die {0}. Fibonacci-Zahl ist\n  {1}.\n", n, fString));

                // loop prompt 
                ForeColoredOutput("\n... zum Beenden (Esc).", ConsoleColor.Red);

            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
        /// <summary>
        /// calculates the n. fibonacci number
        /// </summary>
        /// <param name="count"></param>
        /// <returns>ulong</returns>
        private static ulong FibonacciSlow(int count)
        {
            return count > 2 ? (FibonacciSlow(count - 1) + FibonacciSlow(count - 2)) : count == 2 ? (ulong)1 : (ulong)0;
        }

        /// <summary>
        /// calculates the n. fibonacci number
        /// </summary>
        /// <param name="count"></param>
        /// <returns>ulong</returns>
        private static ulong FibonacciFast(int count, ulong n_1, ulong n_2)
        {
            return count == 1 ? n_2 : FibonacciFast(count - 1, n_1 + n_2, n_1);
        }

        /// <summary>
        /// calculates and prints the first n fibonacci numbers
        /// </summary>
        /// <param name="count"></param>
        /// <returns>ulong</returns>
        private static ulong FibonacciFastPrint(int count, ulong n_1, ulong n_2)
        {
            Console.WriteLine(n_2);
            return count == 1 ? n_2 : FibonacciFastPrint(count - 1, n_1 + n_2, n_1);
        }

        private static string AddString(string n_1, string n_2)
        {
            StringBuilder b = new StringBuilder();
            StringBuilder bReturn = new StringBuilder();

            // fix length
            int l = Math.Max(n_1.Length, n_2.Length);
            while (n_1.Length < l + 2)
            {
                n_1 = n_1 + "0";
            }

            while (n_2.Length < l + 2)
            {
                n_2 = n_2 + "0";
            }
            l = l + 2;

            // Add
            int rest = 0;
            for (int i = 0; i < l - 1; i++)
            {
                b.Append(((int)n_1[i] - 48 + (int)n_2[i] - 48 + rest) % 10);
                rest = ((int)n_1[i] - 48 + (int)n_2[i] - 48 + rest) / 10;
            }

            // trailing zeros
            int count = 0;
            for (int i = b.Length - 1; i >= 0; i--)
            {
                if (b[i] == '0') { count++; }
                else { break; }
            }
            // remove trailing zeros
            for (int i = 0; i < b.Length - count; i++)
            {
                bReturn.Append(b[i]);
            }
            // return
            return bReturn.ToString();
        }

        /// <summary>
        /// calculates and prints the first n fibonacci numbers
        /// </summary>
        /// <param name="count"></param>
        /// <returns>ulong</returns>
        private static string[] FibonacciFastPrintString(int count, string n_1, string n_2)
        {
            string[] temp = new string[2];
            //Console.WriteLine(string.Join("", n_2.Reverse().ToArray()));
            if (count == 1)
            {
                temp[0] = n_2;
                temp[1] = n_1;
            }
            else temp = FibonacciFastPrintString(count - 1, AddString(n_1, n_2), n_1);
            return temp;
        }

        #region:Tools

        #region:GeTinput

        #region:int
        /// <summary>
        /// prompts for/returns a int and gives error when no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>int value </returns>
        static int GetInputInt(string promptMessage, string errorMessage)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>int value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>int value element of [min,max]</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min, int max)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">int array of valid values</param>
        /// <returns>int value element range"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int[] range)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #region:double
        /// <summary>
        /// prompts for/returns a double and gives error when no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>double value </returns>
        static double GetInputDouble(string promptMessage, string errorMessage)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>double value greater or equal "min"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>double value element of [min,max]</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min, double max)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">double array of valid values</param>
        /// <returns>double value element range"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double[] range)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #endregion

        #region:Output

        /// <summary>
        /// clears console and prints title with leading and trailing ****** and newline before and after 
        /// </summary>
        /// <param name="title">string to print</param>
        static void TitleOutput(string title)
        {
            Console.Clear();
            Console.Write("\n***** {0} *****\n\n", title);
        }

        /// <summary>
        /// prints a string with a given backgroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for background</param>
        static void BackColoredOutput(string message, ConsoleColor background = ConsoleColor.Red)
        {
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given foregroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ForeColoredOutput(string message, ConsoleColor foreground = ConsoleColor.Cyan)
        {
            Console.ForegroundColor = foreground;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given colors
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ColoredOutput(string message, ConsoleColor foreground = ConsoleColor.White, ConsoleColor background = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        #endregion

        #endregion
    }
}
