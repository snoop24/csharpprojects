﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_A02_ggTrekursiv
{
    class Program
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                int a, b, ggT;

                // wipe screen and print title
                TitleOutput("ggT von a und b   (+integer)");

                // getinputs
                string prompt, error = "\n   Zahl? Positiv? \n";

                //read a
                prompt = " a: ";
                a = GetInputInt(prompt, error, 0);

                //read n
                prompt = " b: ";
                b = GetInputInt(prompt, error, 0);

                // calc
                ggT = GgT(a, b);

                // output
                ForeColoredOutput(string.Format("\n Der ggT von {0} und {1} ist {2}.\n",  a,b,ggT));

                // loop prompt 
                ForeColoredOutput("\n... (Esc) zum Beenden.", ConsoleColor.Red);

            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }

        private static int GgT(int a, int b)
        {
            return b == 0 ? a : GgT(b,a%b);
        }

        #region:Tools

        #region:GeTinput

        #region:int
        /// <summary>
        /// prompts for/returns a int and gives error when no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>int value </returns>
        static int GetInputInt(string promptMessage, string errorMessage)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>int value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>int value element of [min,max]</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min, int max)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">int array of valid values</param>
        /// <returns>int value element range"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int[] range)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #region:double
        /// <summary>
        /// prompts for/returns a double and gives error when no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>double value </returns>
        static double GetInputDouble(string promptMessage, string errorMessage)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>double value greater or equal "min"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>double value element of [min,max]</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min, double max)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">double array of valid values</param>
        /// <returns>double value element range"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double[] range)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #endregion

        #region:Output

        /// <summary>
        /// clears console and prints title with leading and trailing ****** and newline before and after 
        /// </summary>
        /// <param name="title">string to print</param>
        static void TitleOutput(string title)
        {
            Console.Clear();
            Console.Write("\n***** {0} *****\n\n", title);
        }

        /// <summary>
        /// prints a string with a given backgroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for background</param>
        static void BackColoredOutput(string message, ConsoleColor background = ConsoleColor.Red)
        {
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given foregroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ForeColoredOutput(string message, ConsoleColor foreground = ConsoleColor.Cyan)
        {
            Console.ForegroundColor = foreground;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given colors
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ColoredOutput(string message, ConsoleColor foreground = ConsoleColor.White, ConsoleColor background = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        #endregion

        #endregion
    }
}
