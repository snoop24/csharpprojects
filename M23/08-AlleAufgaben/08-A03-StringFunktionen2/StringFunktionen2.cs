﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace _08_A03_StringFunktionen2
{
    class StringFunktionen2
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen
                Console.Clear();
                Console.SetWindowSize(160, 40);
                // prepare
                #region:stringDeclare
                string s = "Im Zuge dieses Tests führt ein menschlicher Fragesteller über eine Tastatur und einen Bildschirm ohne Sicht- und Hörkontakt mit zwei ihm unbekannten Gesprächspartnern eine Unterhaltung. Der eine Gesprächspartner ist ein Mensch, der andere eine Maschine. Beide versuchen, den Fragesteller davon zu überzeugen, dass sie denkende Menschen sind. Wenn der Fragesteller nach der intensiven Befragung nicht klar sagen kann, welcher von beiden die Maschine ist, hat die Maschine den Turing-Test bestanden, und es wird der Maschine ein dem Menschen ebenbürtiges Denkvermögen unterstellt." +
                        "Es ist eine Reihe von Argumenten vorgebracht worden, die den Turing-Test als ungeeignet zur Feststellung von Intelligenz ansehen:" +
                        "Der Turing-Test prüfe nur auf Funktionalität, nicht auf das Vorhandensein von Intentionalität oder eines Bewusstseins. Dieses Argument wurde unter anderem von John Searle in seinem Gedankenexperiment des Chinesischen Zimmers ausgearbeitet. Turing war sich dieser Problematik bereits bei der Formulierung seines Tests bewusst, war allerdings der Ansicht, dass dieser auch als Nachweis für ein Bewusstsein gelten könne. Searle lehnt dies hingegen ab." +
                        "Turing vermutete, dass es bis zum Jahr 2000 möglich sein werde, Computer so zu programmieren, dass der durchschnittliche Anwender eine höchstens 70-prozentige Chance habe, Mensch und Maschine erfolgreich zu identifizieren, nachdem er fünf Minuten mit ihnen „gesprochen“ hat. Dass sich diese Vorhersage bisher nicht erfüllte, sehen viele als einen Beleg für die Unterschätzung der Komplexität natürlicher Intelligenz." +
                        "Programme wie ELIZA sind Versuchspersonen gegenüber kurzzeitig als menschlich erschienen, ohne dass sie den Turing-Test formal bestehen könnten. In ihrer Antwortstrategie gingen sie nur scheinbar auf ihr Gegenüber ein; den Versuchspersonen war nicht bewusst, dass sie es mit nichtmenschlichen Gesprächspartnern zu tun haben könnten." +
                        "Im Oktober 2008 wurde bei einem Experiment an der University of Reading, bei dem sechs Computerprogramme teilnahmen, die 30-Prozent-Marke knapp verfehlt. Das beste Programm schaffte es, 25 Prozent der menschlichen Versuchsteilnehmer zu täuschen. " +
                        "Am 3. September 2011 nahm die KI-Webapplikation Cleverbot zusammen mit echten Menschen an einem dem Turing-Test angelehnten Versuch beim technischen Festival 2011 am indischen Institut IIT Guwahati teil. Die Ergebnisse wurden am 4. September bekannt gegeben. 59 % von 1334 Personen hielten Cleverbot für einen Menschen. Die menschlichen Konkurrenten hingegen erzielten 63 %. Allerdings durften diese Personen Cleverbot nicht selbst befragen, sondern waren lediglich Zuschauer. Durch die unterschiedliche Methodik sind diese Prozentzahlen mit dem ursprünglichen Turing-Test nicht vergleichbar." +
                        "Ob der Chatbot Eugene Goostman 2014 den Turing-Test bestand, gilt als umstritten." +
                        "Bei der Abwehr von Spam ist es erforderlich, automatisierte Eingaben von solchen zu unterscheiden, die von Menschen stammen. Das dafür häufig verwendete CAPTCHA-Verfahren leitet seinen Namen vom Turing-Test ab (Completely Automated Public Turing test to tell Computers and Humans Apart). Eine andere Bezeichnung für diese Methode ist Human Interaction Proof (HIP)." +
                        "Der Loebner-Preis ist seit 1991 ausgeschrieben und soll an das Computerprogramm verliehen werden, das als erstes einen erweiterten Turing-Test besteht, bei dem auch Multimedia-Inhalte wie Musik, Sprache, Bilder und Videos verarbeitet werden müssen. Der Preis ist nach Hugh G. Loebner benannt und mit 100.000 US-Dollar und einer Goldmedaille dotiert, eine Silbermedaille und 25.000 Dollar gibt es für das Bestehen des schriftlichen Turing-Tests. Bisher konnte jedoch kein Computerprogramm die nötigen Voraussetzungen erfüllen. Weiterhin wird jährlich ein Loebner-Preis an das Computerprogramm verliehen, das einem menschlichen Gespräch am nächsten kommt. Dieser ist mit 2.000 US-Dollar und einer Bronzemedaille dotiert." +
                        "Philip K. Dick verwendete in seinem 1968 erschienenen Roman Träumen Androiden von elektrischen Schafen? (1982 verfilmt unter dem Titel Blade Runner) den so genannten Voigt-Kampff-Test, eine Variante des Turing-Tests. Im Jahr 1992 (bzw. in späteren Ausgaben 2021) werden dort künstliche Menschen, so genannte Replikanten, die physisch den Menschen gleichen, einem Empathietest unterzogen, der durch lange Befragungen ihre emotionale Reaktion prüft und hervorbringen soll, ob sie Mensch oder Replikant sind." +
                        "Beim 2K BotPrize prüfen menschliche Tester im Computerspiel Unreal Tournament 2004 Bots auf menschliche Reaktionen. Ziel ist es, einen Bot so zu programmieren, dass er von einem menschlichen Spieler nicht mehr zu unterscheiden ist. " +
                        "In Ian McDonalds Science-Fiction-Roman River of Gods (2004; deutsch: Cyberabad, 2012), wird eine Welt entworfen, in der künstliche Intelligenzen ab einer höher entwickelten, dem Menschen vergleichbaren Stufe verboten sind. Der Turing-Test zur Prüfung des Grades einer KI wird hier mit dem Argument verworfen, dass eine genügend hoch entwickelte Intelligenz das eigene Scheitern bei dem Test auch selbst provozieren könnte." +
                        "In Alex Garlands Regiedebüt Ex Machina soll der Programmierer Caleb die künstliche Intelligenz Ava mithilfe eines Turing-Tests in abgewandelter Form testen, denn ihm ist bereits bekannt, dass Ava ein Roboter ist.";
                #endregion

                
                //1.)	Entfernen Sie alle Satzzeichen
                Console.WriteLine("1.) Entfernen Sie alle Satzzeichen");

                //Console.WriteLine(s.Replace("," , "").Replace(".", "").Replace(";", "").Replace(":", "").Replace("!", "").Replace("?", ""));
                //Console.WriteLine(Regex.Replace(s, @"[,\.;:!?-]", ""));
                
                StringBuilder sTemp = new StringBuilder("");
                for (int i = 0; i < s.Length; i++)
                {
                    sTemp.Append(@",.;:!?-".Contains(s[i]) ? "" : s[i].ToString());
                }
                Console.WriteLine(sTemp);

                Console.ReadKey(true);

                //2.)	Zählen Sie alle „die“s
                Console.Clear();
                Console.WriteLine("2.) Zählen Sie alle „die“s");

                int count = 0;
                for (int i = 0; i < s.Length - 4; i++)
                {
                    if ((i< s.Length-4 && s[i] == ' ' && s[i+1] == 'd' && s[i + 2] == 'i' && s[i + 3] == 'e' && s[i+4] == ' ' )
                        ||
                        ( s[i] == 'D' && s[i + 1] == 'i' && s[i + 2] == 'e' && s[i + 3] == ' ' ))
                        count++;
                }
                Console.WriteLine(count);
                Console.WriteLine(Regex.Matches(s, @"\x20die\x20").Count+ Regex.Matches(s, @"Die\x20").Count);

                Console.ReadKey(true);

                //3.)	Wandeln Sie alle Leerzeichen zu Unterstrichen um
                Console.Clear();
                Console.WriteLine("3.) Wandeln Sie alle Leerzeichen zu Unterstrichen um");

                Console.WriteLine(s.Replace(" ", "_"));

                Console.ReadKey(true);

                //4.)	Verwandeln Sie alles in Kleinbuchstaben
                Console.Clear();
                Console.WriteLine("4.) Verwandeln Sie alles in Kleinbuchstaben");
               
                Console.WriteLine(s.ToLower());

                Console.ReadKey(true);

                //5.)	Ersetzen Sie jede Zahl durch „BLÖD“ 
                Console.Clear();
                // Zahl oder Ziffer???
                Console.WriteLine("5.) Ersetzen Sie jede Zahl durch „BLÖD“ ");

                //Console.WriteLine(Regex.Replace(s, @"\p{N}+", "BLÖD"));

                sTemp.Clear();
                for (int i = 0; i < s.Length; i++)
                {
                    if (i < s.Length - 1 && char.IsNumber(s[i]) && char.IsNumber(s[i+1]))
                    { }
                    else if (char.IsNumber(s[i]))
                    { sTemp.Append("BLÖD"); }
                    else { sTemp.Append(s[i]); }
                }
                Console.WriteLine(sTemp);

                Console.ReadKey(true);

                //6.)	Entfernen Sie alle Sonderzeichen
                Console.Clear();
                Console.WriteLine("6.) Entfernen Sie alle Sonderzeichen");

                Console.WriteLine(Regex.Replace(s, @"[^\w\.:;!?\-\x20]", ""));

                Console.ReadKey(true);

                //7.)	Zählen Sie alle Wörter(grob)
                Console.Clear();
                Console.WriteLine("7.) Zählen Sie alle Wörter(grob)");

                count = 0;
                for (int i = 0; i < s.Length - 3; i++)
                {
                    if (s[i] == ' ') { count++; }
                }
                Console.WriteLine(count+1);

                Console.WriteLine(Regex.Matches(s, " ").Count+1);

                Console.WriteLine("\n");
            } while (OnceAgain(ConsoleKey.Escape, ConsoleKey.Enter));
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("... again(");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write("), end(");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(")!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
