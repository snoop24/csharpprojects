﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_A01_StringVsStringBuilder
{
    class StringVsStringBuilder
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                Console.Clear();
                long N = 100000;
                long vorher, diff;
                string s = "*";
                StringBuilder t = new StringBuilder("*");

                if (args.Length > 0)
                {
                    if (long.TryParse(args[0], out N))
                    {
                        // string manipulation
                        vorher = DateTime.Now.Ticks;
                        while (N > DateTime.Now.Ticks-vorher)
                        {
                            s = s + "*";
                        }

                        diff = s.Length;
                        Console.WriteLine("Länge String-Manipulation:        {0,12}", diff);

                        // string builder manipulation
                        vorher = DateTime.Now.Ticks;
                        while (N > DateTime.Now.Ticks-vorher)
                        {
                            t.Append("*");
                        }
                        diff = t.Length;
                        Console.WriteLine("Länge StringBuilder-Manipulation: {0,12}", diff);

                    }
                    else
                    {
                        Console.WriteLine(" Bitte als ersten Parameter eine Zahl im LongInt-Bereich. Danke!");
                    }
                }
                else
                {
                    // string manipulation
                    vorher = DateTime.Now.Ticks;
                    for (int i = 0; i < N; i++)
                    {
                        s = s + "*";
                    }

                    diff = DateTime.Now.Ticks - vorher;
                    Console.WriteLine("Zeit für String-Manipulation:        {0,12}", diff);

                    // string builder manipulation
                    vorher = DateTime.Now.Ticks;
                    for (int i = 0; i < N; i++)
                    {
                        t.Append("*");
                    }
                    diff = DateTime.Now.Ticks - vorher;
                    Console.WriteLine("Zeit für StringBuilder-Manipulation: {0,12}", diff);
                }
            } while (OnceAgain(ConsoleKey.Escape, ConsoleKey.Enter));
            //string[] args2 = new string[1] { "10000" };
            //Main(args2);
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("... again(");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write("), end(");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(")!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
