﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_A02_StringFunktionen
{
    class StringFunktionen
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen
                Console.Clear();

                // 1)
                string test;
                Console.WriteLine("1)");

                test = "stairway heaven";
                Console.WriteLine(test);

                test = test.Insert(test.IndexOf(" "), " to");
                Console.WriteLine(test);

                test = test.Insert(test.Length, " and hell");
                Console.WriteLine(test);

                // 2)
                Console.WriteLine("2)");

                test = test.Remove(test.IndexOf(" and hell"), test.Length - test.IndexOf(" and hell"));
                Console.WriteLine(test);

                // 3)
                Console.WriteLine("3)");

                test = test.Replace(' ', '_');
                Console.WriteLine(test);

                test = test.Replace("_", "---");
                Console.WriteLine(test);

                test = test.Replace("---", " ");
                Console.WriteLine(test);

                // 4)
                Console.WriteLine("4)");

                test = "          Hallo          ";
                Console.WriteLine(test + "|");

                Console.WriteLine(test.TrimStart() + "|");
                Console.WriteLine(test.TrimEnd() + "|");
                Console.WriteLine(test.Trim() + "|");


                // 5)
                Console.WriteLine("5)");

                string com = "Commodore 64", ami = "Amiga 500";
                decimal comP = 850m, amiP = 960.99m;
                Console.WriteLine("{0}{1,7:f2}", com.PadRight(23,'.'),comP);
                Console.WriteLine("{0}{1,7:f2}", ami.PadRight(23, '.'), amiP);

                // 6)
                Console.WriteLine("6)");

                test = "stairway to heaven";
                Console.WriteLine(test.Substring(test.IndexOf("h")));

                // 7)
                Console.WriteLine("7)");
                Console.Write("Email: ");
                test = Console.ReadLine();
                Console.WriteLine(test.IndexOf("@")+1);

                Console.WriteLine();

            } while (OnceAgain(ConsoleKey.Escape, ConsoleKey.Enter));
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("... again(");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write("), end(");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(")!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
