﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterMind
{
    class Board
    {
        #region:Attributes

        // class
        private static Random rnd = new Random();


        // instance
        const int borderWH = 1;
        const int offsetLT = 0;
        const int fieldW = 5;
        const int fieldH = 3;

        int[] maxTriesPerDiff = new int[3] { 8, 10, 12 };
        int[] maxFieldsPerDiff = new int[3] { 4, 5, 6 };
        int[] fieldOffsetLeftPerDiff = new int[3] { 6, 3, 0 };
        int triesCount;
        int fieldsCount;
        int fieldsOffset;
        int actualTry = 0;

        Field[] help; // row array
        Field title;
        Field[] masterFields; // col array
        Field[] playerFields; // col array
        Field message;


        #endregion

        #region:ConstructorMethods
        public Board(int difficulty)
        {
            // set board sizes
            triesCount = maxTriesPerDiff[difficulty];
            fieldsCount = maxFieldsPerDiff[difficulty];
            fieldsOffset = fieldOffsetLeftPerDiff[difficulty];

            // set field default width and heigth
            Field.fieldW = fieldW;
            Field.fieldH = fieldH;

            // init field arrays
            InitHelpField();
            InitTitleField();
            InitMasterFields();
            InitPlayerFields();

        }

        private void InitHelpField()
        {
            // initialise and set coordinates
            help = new Field[4] {
             new Field("UP/DOWN/0-9       change color",borderWH+offsetLT+1,borderWH+offsetLT,33,1,ConsoleColor.White),
             new Field("LEFT/RIGHT/SPACE  prev/next field",borderWH+offsetLT+1,borderWH+offsetLT+1,33,1),
             new Field("ENTER             submit line",borderWH+offsetLT+1,borderWH+offsetLT+2,33,1,ConsoleColor.White),
             new Field("F1                menu",borderWH+offsetLT+1,borderWH+offsetLT+3,33,1)
            };
        }

        private void InitTitleField()
        {
            // initialise and set coordinates
            title = new Field(
                "        M a s t e r M I N D",
                offsetLT + borderWH * 1,
                offsetLT + borderWH * 2 + help.Length + 1,
                35, 3, ConsoleColor.Cyan, ConsoleColor.Black);
        }

        private void InitMasterFields()
        {
            // initialise
            masterFields = new Field[fieldsCount];

            // random colors and coordinates
            for (int i = 0; i < fieldsCount; i++)
            {
                masterFields[i] = new Field();
                masterFields[i].BackGround = Field.Colors[rnd.Next(1, 11)];
                masterFields[i].Top = offsetLT + borderWH * 3 + help.Length + title.Height;
                masterFields[i].Left = offsetLT + borderWH + fieldsOffset + i * (5 + 1);
            }

            // set coordinates
        }

        private void InitPlayerFields()
        {
            // initialise
            playerFields = new Field[fieldsCount];

            // set coordinates
            for (int i = 0; i < fieldsCount; i++)
            {
                playerFields[i] = new Field();
                playerFields[i].BackGround = ConsoleColor.DarkGray;
                playerFields[i].Top = offsetLT + borderWH * (4 + actualTry) + help.Length + title.Height + fieldH * (1+ actualTry);
                playerFields[i].Left = offsetLT + borderWH + fieldsOffset + i * (5 + 1);
            }
        }

        #endregion

        #region:OtherMethods

        public void InitBoard()
        {
            PrintFieldArray(help);
            PrintField(title);
            PrintMasterFieldsRandom();
            PrintPMFieldArray(playerFields);

            // testing
            actualTry = 1;
            InitPlayerFields();
            PrintPMFieldArray(playerFields);
        
        }


        private void PrintFieldArray(Field[] fieldArray)
        {
            foreach (Field item in fieldArray)
            {
                PrintField(item);
            }
        }

        private void PrintField(Field field)
        {
            Output(field.Name, field.Left, field.Top, field.ForeGround, field.BackGround);
        }
        
        private void PrintPMFieldArray(Field[] fieldArray)
        {
            foreach (Field item in fieldArray)
            {
                PrintPMField(item);
            }
        }

        private void PrintPMField(Field field)
        {
            StringBuilder message;
            for (int j = 0; j < fieldH; j++)
            {
                message = new StringBuilder();
                for (int i = 0; i < fieldW; i++)
                {
                    message.Append(" ");
                }
                Output(message.ToString(), field.Left, field.Top + j, field.ForeGround, field.BackGround);
            }
        }

        private void PrintMasterFieldsRandom()
        {
            // generate colored master field
            for (int k = 0; k < fieldsCount; k++)
            {
                for (int j = 0; j < fieldH; j++)
                {
                    for (int i = 0; i < fieldW; i++)
                    {
                        Output(" ", masterFields[k].Left + i, masterFields[k].Top + j, Field.Colors[0], Field.Colors[rnd.Next(1, 11)]);
                    }
                }
            }
        }

        /// <summary>
        /// writes message at given position with given colors
        /// </summary>
        /// <param name="message"></param>
        /// <param name="offsetLeft"></param>
        /// <param name="offsetTop"></param>
        /// <param name="foreground"></param>
        /// <param name="background"></param>
        public static void Output(string message, int offsetLeft, int offsetTop, ConsoleColor foreground, ConsoleColor background)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.SetCursorPosition(offsetLeft, offsetTop);
            Console.Write(message);
            Console.ResetColor();
        }

        #endregion
    }
}
