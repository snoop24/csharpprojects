﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterMind
{
    class Game
    {
        // defaults
        private static bool gameEnd = false;
        private static bool newGame = false;
        private static bool playerWon = false;

        // game settings
        private static int difficulty=2;

        // game field
        private static Board board;

        public static void Init()
        {
            board = new Board(difficulty);

            board.InitBoard();
        }
    }
}
