﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterMind
{
    class ZZZField
    {
        #region:declare
        // DEFAULTS
        // field size and colors
        private ConsoleColor[] colors = new ConsoleColor[11]
        { ConsoleColor.DarkGray,
            ConsoleColor.Yellow, ConsoleColor.DarkYellow,
            ConsoleColor.Red, ConsoleColor.DarkRed,
            ConsoleColor.Magenta, ConsoleColor.DarkMagenta,
            ConsoleColor.Cyan, ConsoleColor.DarkCyan,
            ConsoleColor.Green, ConsoleColor.DarkGreen };
        private int squareSizeLeft = 5;
        private int squareSizeTop = 3;

        private int titleBoxHeight = 3;
        private string title = "MasterMIND";

        // field positioning
        private int offSetLeft = 0;
        private int offSetTop = 0;
        private int borderWidth = 1;

        // actual cursor position in field coords;
        private int cursorLeft = 0;
        private int cursorTop = 1;

        // DERIVED
        // from defaults and settings
        private int squareCountLeft;
        private int squareCountTop;

        private int[] squarePosLeft;
        private int[] squarePosTop;

        private int[,] squareColor;

        private int winWidth;
        private int winHeight;

        private int messagePosLeft;
        private int messagePosTop;
        private int messageMaxLength;

        private int legendPosLeft;
        private int legendPosTop;
        private int legendLength;
        private string[] legend;

        #endregion

        // constructor
        public ZZZField(int maxTries, int fieldCount)
        {
            // square field dimensions
            squareCountLeft = fieldCount;
            squareCountTop = maxTries + 1;

            // square positions
            squarePosLeft = new int[squareCountLeft];
            squarePosTop = new int[squareCountTop];

            // calculate square positions
            for (int i = 0; i < squareCountLeft; i++)
            {
                squarePosLeft[i] = offSetLeft + borderWidth + (borderWidth + squareSizeLeft) * i;
            }
            for (int i = 0; i < squareCountTop; i++)
            {
                squarePosTop[i] = offSetTop + titleBoxHeight + borderWidth * 2 + (borderWidth + squareSizeTop) * i;
            }

            // init square colors and random master square colors
            squareColor = new int[squareCountLeft, squareCountLeft];
            Random rnd = new Random();
            for (int i = 0; i < squareCountLeft; i++)
            {
                squareColor[i, 0] = rnd.Next(1, 11);
            }


            // bottom (message) line pos length
            messagePosLeft = offSetLeft + borderWidth;
            messagePosTop = offSetTop + borderWidth + (borderWidth + squareSizeTop) * (squareCountTop + 1);
            messageMaxLength = borderWidth * (squareCountLeft - 1) + squareSizeLeft * squareCountLeft - 2;

            //legend (Esc, F1, F2)
            legendPosLeft = offSetLeft + borderWidth * (squareCountLeft + 1) + squareSizeLeft * squareCountLeft;
            legendPosTop = offSetTop + borderWidth;
            legend = new string[7] { " Esc  End Game ", " F1   Settings ", " F2   New Game ", "", " Up/Down/0-9 Colors ", " Left/Right  Move", " Enter       Validate " };
            legendLength = 22;
            
        }
    }
}
