﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterMind
{
    class Field
    {
        // class

        static private ConsoleColor[] colorsAvailable = new ConsoleColor[11]
        { ConsoleColor.DarkGray,
            ConsoleColor.Yellow, ConsoleColor.DarkYellow,
            ConsoleColor.Red, ConsoleColor.DarkRed,
            ConsoleColor.Magenta, ConsoleColor.DarkMagenta,
            ConsoleColor.Cyan, ConsoleColor.DarkCyan,
            ConsoleColor.Green, ConsoleColor.DarkGreen };

        static public ConsoleColor[] Colors
        {
            get { return colorsAvailable; }
        }

        static public int fieldW;
        static public int fieldH;


        // instance

        public string Name { get; }

        public int Left { get; set; }
        public int Top { get; set; }

        public int Width { get; }
        public int Height { get; }

        public ConsoleColor ForeGround { get; set; }
        public ConsoleColor BackGround { get; set; }


        #region:Constructor

        public Field(
            string name,
            int left,
            int top,
            int width,
            int height,
            ConsoleColor foreGround,
            ConsoleColor backGround)
        {
            Name = name;
            Left = left;
            Top = top;
            Width = width;
            Height = height;
            ForeGround = foreGround;
            BackGround = backGround;
        }

        public Field(string name, int left, int top, int width, int height,ConsoleColor foreGround)
            : this(name, left, top, width, height, foreGround, ConsoleColor.Black)
        { }

        public Field(string name, int left, int top, int width, int height)
            : this(name, left, top, width, height, ConsoleColor.Gray, ConsoleColor.Black)
        { }

        public Field(string name)
            : this(name, 0, 0, fieldW, fieldH, ConsoleColor.Gray, ConsoleColor.Black)
        { }

        public Field()
            : this(" ", 0, 0, fieldW, fieldH, ConsoleColor.Gray, ConsoleColor.Black)
        { }

        #endregion
    }
}
