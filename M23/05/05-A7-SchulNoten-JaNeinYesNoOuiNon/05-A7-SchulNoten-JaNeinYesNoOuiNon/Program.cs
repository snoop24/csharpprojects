﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A7_SchulNoten_JaNeinYesNoOuiNon
{
    class Program
    {
        static void Main(string[] args)
        {
            //SchulNoten();
            //JaNeinYesNoOuiNon();
        }


        static void SchulNoten()
        {
            string errMsg, inpErrMsg, inpPrompt;

            string textNote, outMess;
            int note;

            do
            {
                // read note
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt = "   Bitte Note eingeben: \n\n     ";
                do
                {
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out note));

                switch (note)
                {
                    case 1:
                        outMess = "sehr gut";
                        break;
                    case 2:
                        outMess = "gut";
                        break;
                    case 3:
                        outMess = "befriedigend";
                        break;
                    case 4:
                        outMess = "ausreichend";
                        break;
                    case 5:
                        outMess = "mangelhaft";
                        break;
                    case 6:
                        outMess = "ungenügend";
                        break;
                    default:
                        outMess = "DEPP! Wenn schon besch****n, dann richtig!";
                        break;
                }


                // output
                Console.WriteLine("\n   Die Note in Worten ist:\n\n       {0}", outMess);



                // nochmal?
                Console.WriteLine(
                    "\n" +
                    "Zum Beenden \"x\"");

            } while (Console.ReadKey().Key != ConsoleKey.X);
        }

        static void JaNeinYesNoOuiNon()
        {
            string outMess;
            ConsoleKeyInfo antwort;

            do
            {
                Console.Clear();
                // read note
                Console.WriteLine(
                    "\n" +
                    "   Europäische Union, ein Erfolgsrezept?\n" +
                    "\n" +
                    "     (J/j)a, (Y/y)es, (O/o)ui\n" +
                    "\n" +
                    "              oder/or/ou\n" +
                    "\n" +
                    "     (N/n)ein, (N/n)o, (N/n)on");

                antwort = Console.ReadKey(true);

                switch (antwort.Key)
                {
                    case ConsoleKey.J:
                    case ConsoleKey.O:
                    case ConsoleKey.Y:
                        outMess = "\n\n   Ja, sicha dat! Yessssssss! Oui, oui!\n";
                        break;
                    case ConsoleKey.N:
                        outMess = "\n\n   Nein, neva eva! No no, never! Non!\n";
                        break;
                    default:
                        outMess = "\n\n   Zu Blöd eine Frage mit Ja oder Nein zu beantworten?\n";
                        break;
                }

                // output
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(outMess);
                Console.ResetColor();
                
                // nochmal?
                Console.WriteLine(
                    "\n" +
                    "Zum Beenden \"x\"");

            } while (Console.ReadKey().Key != ConsoleKey.X);
        }
    }
}
