﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A6_BetragZahl_Grundrechenarten
{
    class Program
    {
        static void Main(string[] args)
        {
            //BetragZahl();
            //Grundrechenarten();
            Taschenrechner2();
        }

        static void BetragZahl()
        {
            string errMsg, inpErrMsg, inpPrompt;

            double zahl, betrag;

            do
            {
                // read zahl
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt =
                    "   Bitte Zahl eingeben: " +
                    "\n" +
                    "\n     ";
                do
                {
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!double.TryParse(Console.ReadLine(), out zahl));

                if (zahl < 0)
                {
                    betrag = zahl * (-1);
                }
                else
                {
                    betrag = zahl;
                }

                Console.WriteLine(
                    "\n" +
                    "   Der Betrag von {0} ist \n" +
                    "\n" +
                    "       {1}." +
                    "\n"
                    , zahl, betrag);


                // nochmal?
                Console.WriteLine(
                    "\n" +
                    "Zum Beenden \"x\"");

            } while (Console.ReadKey().Key != ConsoleKey.X);
        }

        static void Grundrechenarten()
        {
            string errMsg, inpErrMsg, inpPrompt;

            string rechenart, rest;
            double zahl1, zahl2, ergebnis;

            do
            {
                rest = "";

                // read zahl1
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt =
                    "   Bitte Zahl1 eingeben: " +
                    "\n" +
                    "\n     ";
                do
                {
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!double.TryParse(Console.ReadLine(), out zahl1));

                // read rechenart
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe!                  \n";
                inpPrompt =
                    "   Bitte \"+\", \"-\", \"*\" oder \"/\" eingeben: \n" +
                    "                                                  \n" +
                    "   ";

                do
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                    rechenart = Console.ReadLine();
                } while (!"+-*/".Contains(rechenart) || rechenart.Length != 1);


                // read zahl2
                errMsg = "";
                inpPrompt =
                    "   Bitte Zahl2 eingeben: " +
                    "\n" +
                    "\n     ";
                do
                {
                    do
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.WriteLine(errMsg);
                        Console.ResetColor();
                        Console.Write(inpPrompt);
                        errMsg = inpErrMsg;
                    } while (!double.TryParse(Console.ReadLine(), out zahl2));

                    errMsg = "\n   Division durch 0, oder was? \n";
                } while (zahl2 == 0 && rechenart == "/");

                // actual calc

                switch (rechenart)
                {
                    case "+":
                        ergebnis = zahl1 + zahl2;
                        break;
                    case "-":
                        ergebnis = zahl1 - zahl2;
                        break;
                    case "*":
                        ergebnis = zahl1 * zahl2;
                        break;
                    case "/":
                        ergebnis = (int)(zahl1 / zahl2);
                        rest = " Rest " + (zahl1 % zahl2).ToString();
                        break;
                    default:
                        ergebnis = 0;
                        Console.WriteLine("Ausgetrickst, aber wie?");
                        Console.ReadKey();
                        break;
                        break;
                }

                Console.WriteLine(
                    "\n" +
                    "   Das Ergebnis von {0} {1} {2} ist \n" +
                    "\n" +
                    "      {3}{4}." +
                    "\n"
                    , zahl1, rechenart, zahl2, ergebnis, rest);


                // nochmal?
                Console.WriteLine(
                    "\n" +
                    "Zum Beenden \"x\"");

            } while (Console.ReadKey().Key != ConsoleKey.X);
        }

        static void Taschenrechner2()
        {
            string rechenart, rest, buffer, inputString;
            double zahl1, zahl2, ergebnis;
            bool enterPressed;
            ConsoleKeyInfo input;

            do
            {
                rest = "";
                buffer = "";
                rechenart = "";
                enterPressed = false;

                // read zahl1
                Console.Clear();
                Console.Write(" Eingabe der einfachen Berechnung a (+-*/) b (enter entspricht =):\n\n\t");
                do
                {
                    input = Console.ReadKey(true);
                    inputString = input.KeyChar.ToString();
                    if (!"1234567890,+-*/".Contains(inputString))
                    {
                        Console.Write("\a");
                    }
                    else if ("1234567890,".Contains(inputString))
                    {
                        buffer += inputString;
                        Console.Write(inputString);
                    }
                    else
                    {
                        rechenart = inputString;
                        Console.Write(inputString);
                    }
                } while (rechenart == "");
                zahl1 = double.Parse(buffer);

                buffer = "";
                do
                {
                    input = Console.ReadKey(true);
                    inputString = input.KeyChar.ToString();
                    if (input.Key == ConsoleKey.Enter)
                    {
                        if (buffer.Length > 0) { enterPressed = true; }
                    }
                    else if (!"1234567890,".Contains(inputString))
                    {
                        Console.Write("\a");
                    }
                    else
                    {
                        buffer += inputString;
                        Console.Write(inputString);
                    }
                } while (!enterPressed);
                zahl2 = double.Parse(buffer);

                // actual calc

                switch (rechenart)
                {
                    case "+":
                        ergebnis = zahl1 + zahl2;
                        break;
                    case "-":
                        ergebnis = zahl1 - zahl2;
                        break;
                    case "*":
                        ergebnis = zahl1 * zahl2;
                        break;
                    case "/":
                        if (zahl2 == 0)
                        {
                            ergebnis = 0;
                            rest = " willste als Divisor nutzen oder wie? Deppert?!";
                        }
                        else
                        {
                            ergebnis = (int)(zahl1 / zahl2);
                            rest = " Rest " + (zahl1 % zahl2).ToString() + "  = " + zahl1 / zahl2;
                        }
                        break;
                    default:
                        ergebnis = 0;
                        Console.WriteLine("Ausgetrickst, aber wie?");
                        Console.ReadKey();
                        break;
                        break;
                }

                Console.WriteLine(
                    "\n" +
                    "\n" +
                    "   Das Ergebnis von {0} {1} {2} ist \n" +
                    "\n" +
                    "\t{3}{4}" +
                    "\n"
                    , zahl1, rechenart, zahl2, ergebnis, rest);


                // nochmal?
                Console.WriteLine(
                    "\n" +
                    " Zum Beenden \"x\". Weiter mit dem \"ANY\" Key.");

            } while (Console.ReadKey().Key != ConsoleKey.X);
        }
    }
}
