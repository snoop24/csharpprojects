﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A2_StückzahlEinzelpreisRabatt_AntwortJaNein
{
    class Program
    {
        static void Main(string[] args)
        {
            //StuEinzRab();
            //AntwortJN_IF();
            //AntwortJN_SC();
        }

        static void StuEinzRab()
        {
            decimal stueck, einzPreis, rabatt, gesamt;
            string zM, fEinMessage, prompt, errorM;

            errorM = "\n Fehlerhafte Eingabe! \n";
            // einzPreis einlesen
            fEinMessage = "";
            prompt = "\n Einzelpreis: ";
            do
            {
                Console.Write(fEinMessage);
                Console.Write(prompt);
                fEinMessage = errorM;
            } while (!decimal.TryParse(Console.ReadLine(), out einzPreis));

            // stueckzahl einlesen
            fEinMessage = "";
            prompt = "\n Stueckzahl: ";
            do
            {
                Console.Write(fEinMessage);
                Console.Write(prompt);
                fEinMessage = errorM;
            } while (!decimal.TryParse(Console.ReadLine(), out stueck));

            // zahlungsmethode zM einlesen
            fEinMessage = "";
            prompt = "\n Bar(b)/Karte(k): ";
            do
            {
                Console.Write(fEinMessage);
                Console.Write(prompt);
                zM = Console.ReadLine().ToUpper();
                fEinMessage = errorM;
            } while (!("BK").Contains(zM) || zM.Length != 1);

            // check if stueck rabatt
            if (stueck < 10)
            {
                rabatt = 1m;
            }
            else if (stueck < 50)
            {
                rabatt = 0.95m;
            }
            else rabatt = 0.9m;

            // check if skonto
            if (zM == "B")
            {
                rabatt = rabatt * 0.97m;
                zM = "Barzahlung";
            }
            else zM = "Kartenzahlung";

            //calc gesamtpreis
            gesamt = stueck * einzPreis * rabatt;

            //output
            Console.WriteLine("\n\n      {4,10} von\n\n   {0,13:0.000} Menge\n   {2,12:0.00}  Preis/Stück\n   {1,11:0.0}%  Rabatt\n\n   {3,12:0.00}  gesamt.\n", stueck, Math.Round(rabatt * 100, 1), einzPreis, Math.Round(gesamt, 2), zM);

            Console.ReadKey();

        }

        static void AntwortJN_IF()
        {
            string jOderN, fEinMessage = "";

            // Antwort einlesen

            do
            {
                Console.Clear();
                Console.Write(fEinMessage);
                Console.Write("\n Antworten Sie NUR mit Ja (j/J) oder Nur (n/N): ");
                jOderN = Console.ReadLine().ToUpper();
                fEinMessage = "\n Aufgabe verstanden? Lesen Sie doch noch einmal genau!\n";
            } while (!(jOderN == "J" || jOderN == "N") || jOderN.Length != 1);

            if (jOderN == "J")
            {
                Console.WriteLine("\n Herzlichen Glückwünsch zu Ihrer neuen Waschmaschine!\n");
            }
            else
            {
                Console.WriteLine("\n Spielverderber!\n");
            }

            Console.ReadKey();
        }

        static void AntwortJN_SC()
        {
            string jOderN, fEinMessage = "";
            bool endIt = false;

            // Antwort einlesen
            do
            {
                Console.Clear();
                Console.Write(fEinMessage);
                Console.Write("\n Antworten Sie NUR mit Ja (j/J) oder Nur (n/N): ");
                jOderN = Console.ReadLine().ToUpper();
                switch (jOderN)
                {
                    case "J":
                        Console.WriteLine("\n Herzlichen Glückwünsch zu Ihrer neuen Waschmaschine!\n");
                        endIt = true;
                        break;
                    case "N":
                        Console.WriteLine("\n Spielverderber!\n");
                        endIt = true;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n Aufgabe verstanden? Lesen Sie doch noch einmal genau!\n");
                        endIt = false;
                        break;
                }

            } while (!endIt);

            Console.ReadKey();
        }

    }
}
