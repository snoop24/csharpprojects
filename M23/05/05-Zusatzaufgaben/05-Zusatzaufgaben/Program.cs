﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_Zusatzaufgaben
{
    class Program
    {
        static void Main(string[] args)
        {
            //Kilo_1();
            //BankKonten_2();
            //PaketKosten_3_5();
        }

        static void Kilo_1()
        {
            string errMsg, inpErrMsg, inpPrompt;

            string outMess;
            double gewicht;




            // read gewicht
            errMsg = "";
            inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
            inpPrompt = "   Bitte Gewicht eingeben: \n\n     ";
            do
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(errMsg);
                Console.ResetColor();
                Console.Write(inpPrompt);
                errMsg = inpErrMsg;
            } while (!double.TryParse(Console.ReadLine(), out gewicht) || gewicht < 0);

            if (gewicht < 235 || gewicht > 265)
            {
                outMess = "\n   Fliegengewicht oder Fettsack?\n\n   Egal, Hauptsache NICHT dabei!";
            }
            else
            {
                outMess = "\n   Schwergewicht? Passt!";
            }

            Console.WriteLine(outMess);


            Console.ReadKey();
        }

        static void BankKonten_2()
        {
            string errMsg, inpErrMsg, inpPrompt;

            string outMess;
            decimal giroKontoGuth, sparKontoGuth;




            // read Girokontostand
            errMsg = "";
            inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
            inpPrompt = "   Bitte Girokontostand eingeben: \n\n     ";
            do
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(errMsg);
                Console.ResetColor();
                Console.Write(inpPrompt);
                errMsg = inpErrMsg;
            } while (!decimal.TryParse(Console.ReadLine(), out giroKontoGuth));

            // read Sparkontostand
            errMsg = "";
            inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
            inpPrompt = "   Bitte Sparkontostand eingeben: \n\n     ";
            do
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(errMsg);
                Console.ResetColor();
                Console.Write(inpPrompt);
                errMsg = inpErrMsg;
            } while (!decimal.TryParse(Console.ReadLine(), out sparKontoGuth));

            if (giroKontoGuth > 1500 || sparKontoGuth > 1500)
            {
                outMess = "Die Transaktion ist kostenlos.";
            }
            else
            {
                outMess = "Es wird eine Transaktionsgebühr von 0,15 Euro erhoben.";
            }

            Console.Clear();
            Console.WriteLine("\n   " + outMess);

            Console.ReadKey();
        }

        static void PaketKosten_3_5()
        {
            string errMsg, inpErrMsg, inpPrompt;

            double gewicht;

            decimal versandKosten3, versandKosten5, faktor = 0.25m;
            int nKiloUeber10 = 0;



            // read Girokontostand
            errMsg = "";
            inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
            inpPrompt = "   Bitte Paketgewicht in kg eingeben: \n\n     ";
            do
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(errMsg);
                Console.ResetColor();
                Console.Write(inpPrompt);
                errMsg = inpErrMsg;
            } while (!double.TryParse(Console.ReadLine(), out gewicht));


            // calc angefangen Kilo ueber 10kg

            if (gewicht > 10)
            {
                if (gewicht % 1 == 0)
                {
                    nKiloUeber10 = (int)gewicht - 10;
                }
                else
                {
                    nKiloUeber10 = (int)gewicht - 9;
                }
            }

            versandKosten3 = 3+(decimal)nKiloUeber10 * faktor;

            if (gewicht > 20)
            {
                faktor = 0.5m;
                versandKosten5 = 3+ (decimal) nKiloUeber10 * faktor;
            }
            else
            {
                versandKosten5 = versandKosten3;
            }
            Console.WriteLine(
                "\n" +
                "   Für ein {0} kg Paket betragen die Versandkosten:\n" +
                "\n" +
                "   {1,7:F2} Euro nach Aufgabe 3\n" +
                "\n" +
                "   {2,7:F2} Euro nach Aufgabe 5"
                , gewicht, versandKosten3, versandKosten5);

            Console.ReadKey();

        }
    }
}
