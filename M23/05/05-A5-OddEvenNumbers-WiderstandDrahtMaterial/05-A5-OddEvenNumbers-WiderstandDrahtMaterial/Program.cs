﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A5_OddEvenNumbers_WiderstandDrahtMaterial
{
    class Program
    {
        static void Main(string[] args)
        {
            //OddEvenNumb();
            //WiderstandDrahtMaterial();

        }
        static void OddEvenNumb()
        {
            int zahl;
            string outPut;
            string inpErrMsg, errMsg, inpPrompt;


            //read number
            errMsg = "";
            inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
            inpPrompt = "   Zahl von 1 bis 100: \n\n     ";
            do
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(errMsg);
                Console.ResetColor();
                Console.Write(inpPrompt);
                errMsg = inpErrMsg;
            } while (!int.TryParse(Console.ReadLine(), out zahl) || zahl < 1 || zahl > 100);

            if (zahl % 2 == 0)
            {
                outPut = "gerade";
            }
            else
            {
                outPut = "ungerade";
            }

            Console.WriteLine("\n   Die Zahl {0} ist {1}.", zahl, outPut);

            Console.ReadKey();

        }

        static void WiderstandDrahtMaterial()
        {
            double widerstand=0, laenge, koeffizient = 0, durchmesser, flaeche;
            string material = "";
            bool validMaterial = true;

            string errMsg, inpErrMsg, inpPrompt;

            //read drahtlänge l
            errMsg = "";
            inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
            inpPrompt = "   Drahtlänge in m: \n\n     ";
            do
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(errMsg);
                Console.ResetColor();
                Console.Write(inpPrompt);
                errMsg = inpErrMsg;
            } while (!double.TryParse(Console.ReadLine(), out laenge) || laenge < 0);

            //read drahtdurchmesser d
            errMsg = "";
            inpPrompt = "   Drahtdurchmesser in mm: \n\n     ";
            do
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(errMsg);
                Console.ResetColor();
                Console.Write(inpPrompt);
                errMsg = inpErrMsg;
            } while (!double.TryParse(Console.ReadLine(), out durchmesser) || durchmesser < 0);

            // set material menu via prompt
            inpPrompt = "\n" +
                        "   Wählen Sie ein Material\n\n" +
                        "     Silber\n\n" +
                        "     Kupfer\n\n" +
                        "     Aluminium\n\n" +
                        "     Messing\n\n" +
                        "   Ihre Wahl: ";

            // material loop 
            while (koeffizient == 0)
            {
                // print message and menu, get input
                Console.Clear();
                if (!validMaterial) // print error message if needed
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("\n Ihre Eingabe \"{0}\" war ungültig!\n", material);
                    Console.ResetColor();
                }
                Console.Write(inpPrompt);
                material = Console.ReadLine();
                Console.Clear();


                // do selected option
                switch (material.ToUpper())
                {
                    case "SILBER":
                        koeffizient = 60.6;
                        break;
                    case "KUPFER":
                        koeffizient = 56.8;
                        break;
                    case "ALUMINIUM":
                        koeffizient = 36;
                        break;
                    case "MESSING":
                        koeffizient = 13.3;
                        break;
                    default:
                        validMaterial = false; // trigger error message on menu print
                        break;
                }

                flaeche = Math.PI * durchmesser * durchmesser / 4;
                widerstand = laenge / koeffizient / flaeche;
            }

            Console.Clear();
            Console.WriteLine("\n"+
                              " Ein {0}m langer {1}mm dicker Draht aus {2} hat den Widerstand\n\n"+
                              "     {3:#,##0.###} Ohm.", laenge, durchmesser, material, Math.Round(widerstand,3));
            Console.ReadKey();
        }

        
    }
}
