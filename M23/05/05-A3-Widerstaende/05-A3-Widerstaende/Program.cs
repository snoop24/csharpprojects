﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A3_Widerstaende
{
    class Program
    {
        static void Main(string[] args)
        {
            string outUnitR1, outUnitR2, outUnitRGesamtParallel, outUnitRGesamtReihe, inputPrompt, errorMsg, falscheEingabeMessage;
            double r1, r2, rGesamtReihe, rGesamtParallel;

            errorMsg = "\n Fehlerhafte Eingabe! \n";
            // r1 einlesen
            falscheEingabeMessage = "";
            inputPrompt = "\n Widerstand R1: ";
            do
            {
                Console.Write(falscheEingabeMessage);
                Console.Write(inputPrompt);
                falscheEingabeMessage = errorMsg;
            } while (!double.TryParse(Console.ReadLine(), out r1) || r1 <= 0);

            // r2 einlesen
            falscheEingabeMessage = "";
            inputPrompt = "\n Widerstand R2: ";
            do
            {
                Console.Write(falscheEingabeMessage);
                Console.Write(inputPrompt);
                falscheEingabeMessage = errorMsg;
            } while (!double.TryParse(Console.ReadLine(), out r2) || r2 <= 0);

            //calc 
            rGesamtReihe = r1 + r2;
            rGesamtParallel = (r1 * r2) / (r1 + r2);

            //format values
            if (rGesamtReihe < 1)
            {
                rGesamtReihe = Math.Round(rGesamtReihe * 1000, 3);
                outUnitRGesamtReihe = "m";
            }
            else if (rGesamtReihe < 1000)
            {
                rGesamtReihe = Math.Round(rGesamtReihe, 3);
                outUnitRGesamtReihe = " ";
            }
            else if (rGesamtReihe < 1000000)
            {
                rGesamtReihe = Math.Round(rGesamtReihe / 1000, 3);
                outUnitRGesamtReihe = "k";
            }
            else
            {
                rGesamtReihe = Math.Round(rGesamtReihe / 1000000, 3);
                outUnitRGesamtReihe = "M";
            }

            if (rGesamtParallel < 1)
            {
                rGesamtParallel = Math.Round(rGesamtParallel * 1000, 3);
                outUnitRGesamtParallel = "m";
            }
            else if (rGesamtParallel < 1000)
            {
                rGesamtParallel = Math.Round(rGesamtParallel, 3);
                outUnitRGesamtParallel = " ";
            }
            else if (rGesamtParallel < 1000000)
            {
                rGesamtParallel = Math.Round(rGesamtParallel / 1000, 3);
                outUnitRGesamtParallel = "k";
            }
            else
            {
                rGesamtParallel = Math.Round(rGesamtParallel / 1000000, 3);
                outUnitRGesamtParallel = "M";
            }
                        
            if (r1 < 1)
            {
                r1 = Math.Round(r1 * 1000, 3);
                outUnitR1 = "m";
            }
            else if (r1 < 1000)
            {
                r1 = Math.Round(r1, 3);
                outUnitR1 = " ";
            }
            else if (r1 < 1000000)
            {
                r1 = Math.Round(r1 / 1000, 3);
                outUnitR1 = "k";
            }
            else
            {
                r1 = Math.Round(rGesamtReihe / 1000000, 3);
                outUnitR1 = "M";
            }

            if (r2 < 1)
            {
                r2 = Math.Round(r2 * 1000, 3);
                outUnitR2 = "m";
            }
            else if (r2 < 1000)
            {
                r2 = Math.Round(r2, 3);
                outUnitR2 = " ";
            }
            else if (r2 < 1000000)
            {
                r2 = Math.Round(r2 / 1000, 3);
                outUnitR2 = "k";
            }
            else
            {
                r2 = Math.Round(r2 / 1000000, 3);
                outUnitR2 = "M";
            }


            // output
            Console.Clear();
            Console.WriteLine(
                "\n" +
                "   Widerstände mit den Werten\n\n" +
                "        R1 = {0,10:F3} {1}Ohm\n\n" +
                "   und\n\n" +
                "        R2 = {2,10:F3} {3}Ohm\n\n" +
                "   ergeben\n\n" +
                "   in Reihe: {4,10:F3} {5}Ohm\n\n" +
                "   parallel: {6,10:F3} {7}Ohm"
                , r1, outUnitR1, r2, outUnitR2, rGesamtReihe, outUnitRGesamtReihe, rGesamtParallel, outUnitRGesamtParallel
                );

            Console.ReadKey();
        }
    }
}
