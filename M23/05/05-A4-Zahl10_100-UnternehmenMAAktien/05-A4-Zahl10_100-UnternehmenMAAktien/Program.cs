﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_A4_Zahl10_100_UnternehmenMAAktien
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zahl10_100();
            //UntMAAktien();
        }

        static void Zahl10_100()
        {
            double zahl;
            string fEinMessage;

            // Zahl einlesen
            fEinMessage = "";
            do
            {
                Console.Write(fEinMessage);
                Console.Write("\n Zahl von 10 bis 100 eingeben: \n   ");
                fEinMessage = "\n Fehlerhafte Eingabe! \n";
            } while (!double.TryParse(Console.ReadLine(), out zahl));

            if (zahl >= 10 && zahl <= 100)
            {
                Console.WriteLine("   && Rischtisch");
            }
            else
            {
                Console.WriteLine("   && Faaaaahhaaalschh");
            }
            if (!(zahl < 10 || zahl > 100))
            {
                Console.WriteLine("   || Rischtisch");
            }
            else
            {
                Console.WriteLine("   || Faaaaahhaaalschh");
            }

            Console.ReadKey();
        }

        static void UntMAAktien()
        {
            int jahreBetrZuge;
            string jN, errorM, fEinMessage, prompt;

            errorM = "\n Fehlerhafte Eingabe! \n";

            // Status Beschäftigungsverhältnis einlesen
            fEinMessage = "";
            prompt = "\n Besteht das Beschäftigungsverhältnis noch? (J/N): \n   ";
            do
            {
                Console.Write(fEinMessage); // write input error message
                Console.WriteLine(prompt);
                fEinMessage = errorM; // set messsage if input error
                jN = Console.ReadLine().ToUpper();
            } while (!(jN == "J" || jN == "N"));

            if (jN == "N")
            {
                Console.WriteLine("\n Leider keine Aktien für Dich.");
                Console.ReadKey();
                System.Environment.Exit(0);
            }

            // Jahre Betriebszugehörigkeit einlesen
            fEinMessage = "";
            prompt = "\n Wieviele volle Jahre bist Du schon hier beschäftigt?: \n   ";
            do
            {
                Console.Write(fEinMessage); // write input error message
                Console.WriteLine(prompt);
                fEinMessage = errorM; // set messsage if input error
            } while (!int.TryParse(Console.ReadLine(), out jahreBetrZuge) || jahreBetrZuge < 0);


            // Aktien abhängig von Zugehörigkeit
            if (jahreBetrZuge < 2)
            {
                Console.WriteLine("\n Leider keine Aktien für Dich.");
            }
            else if (jahreBetrZuge < 10)
            {
                Console.WriteLine("\n Bis zu 10 Aktien für Dich.");
            }
            else
            {
                Console.WriteLine("\n Bis zu 20 Aktien für Dich.");
            }

            Console.ReadKey();

        }
    }
}
