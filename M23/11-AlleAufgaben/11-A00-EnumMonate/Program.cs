﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_A00_EnumMonate
{
    class Program
    {
        private enum Monate : ushort
        {
            Januar = 2,
            Februar = 4,
            März = 6,
            April = 8,
            Mai = 10,
            Juni = 12,
            Juli = 14,
            August = 16,
            September = 18,
            Oktober = 20,
            November = 22,
            Dezember = 24
        }

        static void Main(string[] args)
        {
            // general loop
            do
            {

                // wipe screen and print title
                TitleOutput("Monatsnamen");

                // declare 
                Monate aktMonat = new Monate();

                //// a) read aktMonat
                //aktMonat = GetMonate("\nBitte geben Sie einen Monat ein: ", "\n   Hmm?! \n");

                // b) read aktMonat
                aktMonat = GetMonateNoNumbers("\nBitte geben Sie einen Monat ein: ", "\n   Hmm?! \n");
                
                // output
                Console.WriteLine();
                Console.WriteLine(aktMonat);


                // loop prompt
                ForeColoredOutput("\n\n\n ... zum Beenden ESC.", ConsoleColor.Red);

            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }


        /// <summary>
        /// prompts for/returns a month and gives error when no month or enum number
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>month value </returns>
        static Monate GetMonate(string promptMessage, string errorMessage)
        {
            string eMess = "";
            Monate value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!Enum.TryParse(Console.ReadLine(), out value) || !Enum.IsDefined(typeof(Monate), value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a month and gives error when no month
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>month value </returns>
        static Monate GetMonateNoNumbers(string promptMessage, string errorMessage)
        {
            string eMess = "";
            string input;
            ushort temp;
            Monate value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
                input = Console.ReadLine();
            } while (!Enum.TryParse(input, out value) || ushort.TryParse(input, out temp));
            return value;
        }

        #region:Tools

        #region:GeTinput

        #region:int
        /// <summary>
        /// prompts for/returns a int and gives error when no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>int value </returns>
        static int GetInputInt(string promptMessage, string errorMessage)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>int value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>int value element of [min,max]</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min, int max)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">int array of valid values</param>
        /// <returns>int value element range"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int[] range)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #region:double
        /// <summary>
        /// prompts for/returns a double and gives error when no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>double value </returns>
        static double GetInputDouble(string promptMessage, string errorMessage)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>double value greater or equal "min"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>double value element of [min,max]</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min, double max)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">double array of valid values</param>
        /// <returns>double value element range"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double[] range)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #endregion

        #region:Output

        /// <summary>
        /// clears console and prints title with leading and trailing ****** and newline before and after 
        /// </summary>
        /// <param name="title">string to print</param>
        static void TitleOutput(string title)
        {
            Console.Clear();
            Console.Write("\n***** {0} *****\n\n", title);
        }

        /// <summary>
        /// prints a string with a given backgroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for background</param>
        static void BackColoredOutput(string message, ConsoleColor background = ConsoleColor.Red)
        {
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given foregroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ForeColoredOutput(string message, ConsoleColor foreground = ConsoleColor.Cyan)
        {
            Console.ForegroundColor = foreground;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given colors
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ColoredOutput(string message, ConsoleColor foreground = ConsoleColor.White, ConsoleColor background = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        #endregion

        #endregion
    }
}
