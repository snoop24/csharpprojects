﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_A02_Sportler
{
    class Program
    {
        struct SportlerStruct
        {
            #region:Eigenschaften

            string nachname;
            string vorname;
            char geschlecht;
            DateTime geburtstag;
            bool istVerletzt;
            string sportart;
            int wettkaempfeGewonnen;
            int wettkaempfe;

            int staerke;
            int verletzungProzent;

            #endregion

            #region:Konstruktor

            public SportlerStruct(
                string nachname,
                string vorname,
                char geschlecht,
                DateTime geburtstag,
                bool istVerletzt,
                string sportart,
                int wettkaempfeGewonnen,
                int staerke = 25,
                int verletzungProzent = 20,
                int wettkaempfe = 0)
            {
                this.nachname = nachname;
                this.vorname = vorname;
                this.geschlecht = geschlecht;
                this.geburtstag = geburtstag;
                this.istVerletzt = istVerletzt;
                this.sportart = sportart;
                this.wettkaempfeGewonnen = wettkaempfeGewonnen;
                this.wettkaempfe = wettkaempfe;
                this.staerke = staerke;
                this.verletzungProzent = verletzungProzent;
            }

            #endregion

            #region:Methoden

            /// <summary>
            /// wettkampfteilnahme. gewonnen? danach verletzt?
            /// </summary>
            /// <param name="staerke">prozentanteil der wettkaempfe die gewonnen werden</param>
            /// <returns>bool gewonnen</returns>
            public bool MacheWettkampf(int staerke = -1, int verletzungProzent = -1)
            {
                // deklarieren
                bool gewonnen = false;
                Random rnd = new Random();
                System.Threading.Thread.Sleep(5);
                // verletzt nach wettkampf?
                VerletztSich(verletzungProzent);

                // wettkampf gewonnen?
                staerke = (staerke == -1 ? this.staerke : staerke);
                gewonnen = rnd.Next(0, 100) < staerke ? true : false;

                // zaehler anpassen
                wettkaempfeGewonnen += gewonnen ? 1 : 0;

                // rueckgabe
                return gewonnen;
            }

            /// <summary>
            /// erleidet verletzung mit prozent wahrscheinlichkeit
            /// </summary>
            /// <param name="verletzungProzent"></param>
            /// <returns>bool istVerletzt</returns>
            public bool VerletztSich(int verletzungProzent = -1)
            {
                // deklarieren
                Random rnd = new Random();
                System.Threading.Thread.Sleep(5);

                // verletzt nach wettkampf?
                verletzungProzent = (verletzungProzent == -1 ? this.verletzungProzent : verletzungProzent);
                istVerletzt = rnd.Next(0, 100) < verletzungProzent ? true : istVerletzt;

                return istVerletzt;
            }

            /// <summary>
            /// untersucht (und heilt eine verletzung)
            /// </summary>
            /// <returns>string ergebnis des arztbesuchs</returns>
            public string BesucheArzt()
            {
                // deklarieren
                string rueckgabe;

                // geheilt falls verletzt sonst nur untersucht
                rueckgabe = vorname + " " + nachname + " wurde untersucht und " + (istVerletzt ? "die Verletzung geheilt." : "war gesund.");

                // geheilt setzen
                istVerletzt = false;

                // ergebnis zurueckgeben
                return rueckgabe;
            }

            /// <summary>
            /// gibt sportlerdaten aus
            /// </summary>
            public void AusgabeSportlerdaten()
            {
                Console.WriteLine(
                    "{0,15}, " +
                    "{1,-15} " +
                    "m/w/u: {2,-2} " +
                    "Geb.: {3,-11}" +
                    "verletzt: {4,-5} " +
                    "verletW.: {5,-2}% " +
                    "Sportart: {6,-11} " +
                    "gewonnen: {7,-4} " +
                    "Stärke: {8,-2}\n"
                    , nachname, vorname, geschlecht, geburtstag.Date.ToShortDateString(), istVerletzt ? "ja" : "nein", verletzungProzent, sportart, wettkaempfeGewonnen, staerke);
            }

            #region:Get

            public string GetNachname()
            {
                return nachname;
            }

            public string GetVorname()
            {
                return vorname;
            }

            public int GetStaerke()
            {
                return staerke;
            }

            public int GetGewonnen()
            {
                return wettkaempfeGewonnen;
            }

            public bool GetIstVerletzt()
            {
                return istVerletzt;
            }

            #endregion

            #endregion
        }

        struct Liga
        {
            SportlerStruct[] teilnehmer;
            int[] teilnehmerPlatz;
            int spieltag;
            int[,,] spielplan;

            public Liga(int anzahlTeilnehmer, int[,,] spielplan, string[,] sportler)
            {
                // deklarieren
                Random rnd = new Random();
                char geschlecht;
                DateTime geburtstag;
                int staerke, verletzungProzent, rndTemp;

                // lege sportler an
                teilnehmer = new SportlerStruct[anzahlTeilnehmer];
                teilnehmerPlatz = new int[anzahlTeilnehmer];
                for (int i = 0; i < anzahlTeilnehmer; i++)
                {
                    // (m)aennlich, (w)eiblich, (u)nsicher
                    rndTemp = rnd.Next(1, 101);
                    geschlecht = (rndTemp < 45 ? 'm' : (rndTemp < 90 ? 'w' : 'u'));

                    geburtstag = new DateTime(rnd.Next(1970, 2000), rnd.Next(1, 13), rnd.Next(1, 29));

                    staerke = rnd.Next(1, 101);
                    verletzungProzent = rnd.Next(10, 26);

                    teilnehmer[i] = new SportlerStruct(sportler[i, 0], sportler[i, 1], geschlecht, geburtstag, false, sportler[i, 2], 0, staerke, verletzungProzent, 0);
                    teilnehmerPlatz[i] = i;
                }
                spieltag = 0;
                this.spielplan = spielplan;
            }

            public void Tabelle()
            {
                int platz = 1;
                Console.WriteLine("\n -----------------------------------\n" +
                    "\tTabelle {0}. Spieltag\n", spieltag);
                for (int i = 0; i < teilnehmer.Length; i++)
                {
                    if (i != 0 && teilnehmer[teilnehmerPlatz[i-1]].GetGewonnen() > teilnehmer[teilnehmerPlatz[i]].GetGewonnen())
                    {
                        platz++;
                    }
                    Console.Write(" {0,2}. ", platz);
                    teilnehmer[teilnehmerPlatz[i]].AusgabeSportlerdaten();
                }
            }

            public void SortierenTabelle()
            {
                for (int i = 0; i < teilnehmer.Length; i++)
                {
                    int position = i;
                    for (int j = i + 1; j < teilnehmer.Length; j++)
                    {
                        if (teilnehmer[teilnehmerPlatz[position]].GetGewonnen() < teilnehmer[teilnehmerPlatz[j]].GetGewonnen())
                        {
                            position = j;
                        }

                    }
                    if (position != i)
                    {
                        int temp;
                        temp = teilnehmerPlatz[i];
                        teilnehmerPlatz[i] = teilnehmerPlatz[position];
                        teilnehmerPlatz[position] = temp;
                    }
                }
            }

            public void GehtZumArzt(string mussZumArzt)
            {
                // zum Arzt schicken
                Console.WriteLine();
                foreach (int item in mussZumArzt)
                {
                    if (teilnehmer[item-48].GetIstVerletzt())
                    {
                        Console.WriteLine(teilnehmer[item-48].BesucheArzt());
                    }
                }
            }

            public void NaechsterSpieltag()
            {
                string mussZumArzt = "";
                Console.WriteLine();
                for (int i = 0; i < spielplan.GetLength(1); i++)
                {
                    Random rnd = new Random();
                    //System.Threading.Thread.Sleep(5);
                    bool sieger1 = false;
                    
                    int staerke1, defaultWin = 0;
                    int sportler1 = spielplan[spieltag, i, 0];
                    int sportler2 = spielplan[spieltag, i, 1];
                    if (teilnehmer[sportler1].GetIstVerletzt() && teilnehmer[sportler2].GetIstVerletzt())
                    {
                        staerke1 = rnd.Next(0, 2) * 100;
                        defaultWin = 3;
                        mussZumArzt += sportler1 + sportler2;
                    }
                    else if (teilnehmer[sportler1].GetIstVerletzt())
                    {
                        staerke1 = 0;
                        defaultWin = 2;
                        mussZumArzt += sportler1;
                    }
                    else if (teilnehmer[sportler2].GetIstVerletzt())
                    {
                        staerke1 = 100;
                        defaultWin = 1;
                        mussZumArzt += sportler2;
                    }
                    else
                    {
                        staerke1 = teilnehmer[sportler1].GetStaerke() *100 / (teilnehmer[sportler1].GetStaerke() + teilnehmer[sportler2].GetStaerke());
                    }
                    sieger1 = teilnehmer[sportler1].MacheWettkampf(staerke1);
                    teilnehmer[sportler2].MacheWettkampf((sieger1 ? 0 : 100));


                    string winMessage;
                    switch (defaultWin)
                    {
                        case 1:
                            winMessage = " Spiel {0,2} Sieger {1,10}, {2,-10}- Verlierer {3,10}, {4,-10} war verletzt.";
                            break;
                        case 2:
                            winMessage = " Spiel {0,2} Sieger {3,10}, {4,-10}- Verlierer {1,10}, {2,-10} war verletzt.";
                            break;
                        case 3:
                            if (sieger1)
                            {
                                winMessage = " Spiel {0,2} Sieger {1,10}, {2,-10}- Verlierer {3,10}, {4,-10} Beide waren verletzt, daher wurde gewürfelt. ";
                            }
                            else
                            {
                                winMessage = " Spiel {0,2} Sieger {3,10}, {4,-10}- Verlierer {1,10}, {2,-10} Beide waren verletzt, daher wurde gewürfelt. ";
                            }
                            break;
                        default:
                            if (sieger1)
                            {
                                winMessage = " Spiel {0,2} Sieger {1,10}, {2,-10}- Verlierer {3,10}, {4,-10} ";
                            }
                            else
                            {
                                winMessage = " Spiel {0,2} Sieger {3,10}, {4,-10}- Verlierer {1,10}, {2,-10} ";
                            }
                            break;
                    }

                    Console.WriteLine(winMessage, i + 1, teilnehmer[sportler1].GetNachname(), teilnehmer[sportler1].GetVorname(), teilnehmer[sportler2].GetNachname(), teilnehmer[sportler2].GetVorname());

                }

                GehtZumArzt(mussZumArzt);
                SortierenTabelle();
                
                spieltag++;
            }

            public void Saison()
            {
                // spieltage = sportlerzahl - 1
                for (int i = 0; i < teilnehmer.Length - 1; i++)
                {
                    // prompt
                    Console.WriteLine("... zum nächsten Spieltag? Taste drücken.");
                    Console.ReadKey(true);
                    //Console.Clear();

                    Console.WriteLine("\n --- Ereignisse des {0}. Spieltags ---",spieltag+1);

                    

                    // spieltag durchführen
                    NaechsterSpieltag();
                    
                    // tabelle anzeigen
                    Tabelle();
                    

                }
            }
        }

        static void Main(string[] args)
        {

            Console.SetWindowSize(200, 60);

            // general loop
            do
            {
                // bildschirm putzen
                Console.Clear();

                // deklarieren
                Liga test;
                string[,] sportler = new string[10, 3]
                {
                        {"Sapfel","Adam","Badminton"},
                        {"Zufall","Rainer","Badminton"},
                        {"Racho","Volker","Badminton"},
                        {"Schweiss","Axel","Badminton"},
                        {"Baba","Ali","Badminton"},
                        {"Bolika","Anna","Badminton"},
                        {"Brandwein","Franz","Badminton"},
                        {"Bude","Anne","Badminton"},
                        {"Buktu","Tim","Badminton"},
                        {"Fall","Klara","Badminton"}
                };
                int[,,] spielplan = new int[9, 5, 2] {
                { { 1, 0 }, { 2, 9 }, { 3, 8 }, { 4, 7 }, { 5, 6 } },
                { { 2, 0 }, { 3, 1 }, { 4, 9 }, { 5, 8 }, { 6, 7 } },
                { { 3, 0 }, { 4, 2 }, { 5, 1 }, { 6, 9 }, { 7, 8 } },
                { { 4, 0 }, { 5, 3 }, { 6, 2 }, { 7, 1 }, { 8, 9 } },
                { { 5, 0 }, { 6, 4 }, { 7, 3 }, { 8, 2 }, { 9, 1 } },
                { { 2, 1 }, { 3, 9 }, { 4, 8 }, { 5, 7 }, { 6, 0 } },
                { { 3, 2 }, { 4, 1 }, { 5, 9 }, { 6, 8 }, { 7, 0 } },
                { { 4, 3 }, { 5, 2 }, { 6, 1 }, { 7, 9 }, { 8, 0 } },
                { { 5, 4 }, { 6, 3 }, { 7, 2 }, { 8, 1 }, { 9, 0 } }
            };

                // neue Liga
                test = new Liga(sportler.GetLength(0), spielplan, sportler);

                // tabelle darstellen
                test.Tabelle();

                // saision starten
                test.Saison();


                // loop prompt
                Console.Write("\n\n\n ... zum Beenden ESC.");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

        }
    }
}
