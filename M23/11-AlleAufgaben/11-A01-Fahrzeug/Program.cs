﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_A01_Fahrzeug
{
    class Program
    {
        struct Fahrzeug
        {
            // properties
            string hersteller;
            string modell;
            string farbe;
            int baujahr;
            int leistung;
            int hubraum;
            decimal preis;

            // custom methods

            /// <summary>
            /// returns alter of fahrzeug
            /// </summary>
            /// <returns>ushort alter</returns>
            public ushort GetAlter()
            {
                ushort age;
                age = (ushort)(DateTime.Now.Year - baujahr);
                return age;
            }

            /// <summary>
            /// returns leistung in ps
            /// </summary>
            /// <returns>int leistung</returns>
            public int GetPS()
            {
                return (int)Math.Round(leistung * 1.35962);
            }

            /// <summary>
            /// rereads all properties of fahrzeug
            /// </summary>
            public void WerteEinlesen()
            {
                int minInt, maxInt;
                decimal minDec;
                string prompt, error;

                // read and set Hersteller
                prompt = "Hersteller";
                Console.Write(string.Format("\n   {0,15}: ", prompt));
                SetHersteller(Console.ReadLine());

                // read and set modell
                prompt = "Modell";
                Console.Write(string.Format("\n   {0,15}: ", prompt));
                SetModell(Console.ReadLine());

                // read and set farbe
                prompt = "Farbe";
                Console.Write(string.Format("\n   {0,15}: ", prompt));
                SetFarbe(Console.ReadLine());

                // read and set baujahr
                minInt = -5000;
                maxInt = DateTime.Now.Year;
                prompt = "Baujahr";
                error = string.Format("\n   Int? {0}-{1} ", minInt, maxInt);
                SetBaujahr(GetInputInt(string.Format("\n   {0,15}: ", prompt), error, minInt, maxInt));

                // read and set leistung
                minInt = 0;
                prompt = "Leistung(KW)";
                error = string.Format("\n   Int? Positiv? ");
                SetLeistung(GetInputInt(string.Format("\n   {0,15}: ", prompt), error, minInt));

                // read and set hubraum
                minInt = 0;
                prompt = "Hubraum";
                error = string.Format("\n   Int? Positiv? ");
                SetBaujahr(GetInputInt(string.Format("\n   {0,15}: ", prompt), error, minInt));

                // read and set preis
                minDec = 0m;
                prompt = "Preis";
                error = string.Format("\n   Int? Positiv? ");
                SetPreis(GetInputDecimal(string.Format("\n   {0,15}: ", prompt), error, minDec));
            }
            
            
            /// <summary>
            /// print fahrzeug data
            /// </summary>
            public void DatenOutput()
            {
                Console.Write(
                    "\t     Hersteller: {0}\n" +
                    "\t         Modell: {1}\n" +
                    "\t          Farbe: {2}\n" +
                    "\t  Baujahr/Alter: {3}/{4}\n" +
                    "\tLeistung(KW/PS): {5}/{6}\n" +
                    "\t        Hubraum: {7}\n" +
                    "\t          Preis: {8}\n"
                    , GetHersteller(), GetModell(), GetFarbe(), GetBaujahr(),GetAlter(), GetLeistung(),GetPS(), GetHubraum(), GetPreis());
            }
            
            #region:Constructor

            /// <summary>
            /// constructs new fahrzeug / if baujahr 0 then actual year
            /// </summary>
            /// <param name="hersteller"></param>
            /// <param name="modell"></param>
            /// <param name="farbe"></param>
            /// <param name="baujahr"></param>
            /// <param name="leistung"></param>
            /// <param name="hubraum"></param>
            /// <param name="preis"></param>
            public Fahrzeug(
                string hersteller,
                string modell,
                string farbe,
                int baujahr,
                int leistung,
                int hubraum,
                decimal preis)
            {
                this.hersteller = hersteller;
                this.modell = modell;
                this.farbe = farbe;
                this.baujahr = baujahr == 0 ? DateTime.Now.Year : baujahr;
                this.leistung = leistung;
                this.hubraum = hubraum;
                this.preis = preis;
            }

            /// <summary>
            /// constructs new fahrzeug without baujahr as param and sets baujahr default to actual year
            /// </summary>
            /// <param name="hersteller"></param>
            /// <param name="modell"></param>
            /// <param name="farbe"></param>
            /// <param name="leistung"></param>
            /// <param name="hubraum"></param>
            /// <param name="preis"></param>
            public Fahrzeug(
                string hersteller,
                string modell,
                string farbe,
                int leistung,
                int hubraum,
                decimal preis)
            {
                this.hersteller = hersteller;
                this.modell = modell;
                this.farbe = farbe;
                this.baujahr = DateTime.Now.Year;
                this.leistung = leistung;
                this.hubraum = hubraum;
                this.preis = preis;
            }

            #endregion

            #region:GetSet

            // hersteller
            public string GetHersteller()
            {
                return hersteller;
            }

            public void SetHersteller(string hersteller)
            {
                this.hersteller = hersteller;
            }

            // modell
            public string GetModell()
            {
                return modell;
            }

            public void SetModell(string modell)
            {
                this.modell = modell;
            }

            // farbe
            public string GetFarbe()
            {
                return farbe;
            }

            public void SetFarbe(string farbe)
            {
                this.farbe = farbe;
            }

            // baujahr
            public int GetBaujahr()
            {
                return baujahr;
            }

            public void SetBaujahr(int baujahr)
            {
                this.baujahr = baujahr;
            }

            // leistung
            public int GetLeistung()
            {
                return leistung;
            }

            public void SetLeistung(int leistung)
            {
                this.leistung = leistung;
            }

            // hubraum
            public int GetHubraum()
            {
                return hubraum;
            }

            public void SetHubraum(int hubraum)
            {
                this.hubraum = hubraum;
            }

            // preis
            public decimal GetPreis()
            {
                return preis;
            }

            public void SetPreis(decimal preis)
            {
                this.preis = preis;
            }


            #endregion
        }

        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                Fahrzeug[] fuhrpark;

                // wipe screen and print title
                TitleOutput("Fahrzeuge");

                // fuhrpark fuellen
                fuhrpark = NeuerFuhrPark();

                // output
                FuhrparkPrint(fuhrpark);

                // output
                FuhrparkPreisAenderung(fuhrpark,-3);

                // output
                FuhrparkPrint(fuhrpark);


                // loop prompt
                ForeColoredOutput("\n\n\n ... zum Beenden ESC.", ConsoleColor.DarkMagenta);
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

        }

        static Fahrzeug[] NeuerFuhrPark()
        {
            // declare
            Fahrzeug[] fuhrpark;
            int anzahlFahrzeuge;

            // prompt variables
            int minVal;
            string prompt, error;

            // read anzahl fahrzeuge im fuhrpark
            minVal = 1;
            prompt = "\n   Anzahl Fahrzeuge (min. 1): ";
            error = "\n   Wert? Im Bereich? ";
            anzahlFahrzeuge = GetInputInt(prompt, error, minVal);
            Console.Write("\n\n");

            // get properties of fahrzeug
            fuhrpark = new Fahrzeug[anzahlFahrzeuge];
            for (int i = 0; i < anzahlFahrzeuge; i++)
            {
                // new fahrzeug
                Fahrzeug temp = new Fahrzeug();

                // values for fahrzeug i
                Console.Write("\n   Fahrzeug Nr: {0}\n", i + 1);

                // get values
                temp.WerteEinlesen();

                // save to fuhrpark
                fuhrpark[i] = temp;
            }

            return fuhrpark;
        }

        static void FuhrparkPrint(Fahrzeug[] fuhrpark)
        {
            int count = 0;
            foreach (Fahrzeug item in fuhrpark)
            {
                count++;
                Console.Write(
                    "\n" +
                    "   FahrzeugNr.: {0}\n", count);
                item.DatenOutput();
            }
        }

        static void FuhrparkPreisAenderung(Fahrzeug[] fuhrpark, int prozent)
        {
            foreach (Fahrzeug item in fuhrpark)
            {
                item.SetPreis(item.GetPreis()*(1+ prozent /100));
            }
        }

        #region:Tools

        #region:GeTinput

        #region:int
        /// <summary>
        /// prompts for/returns a int and gives error when no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>int value </returns>
        static int GetInputInt(string promptMessage, string errorMessage)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>int value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>int value element of [min,max]</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min, int max)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">int array of valid values</param>
        /// <returns>int value element range"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int[] range)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #region:double
        /// <summary>
        /// prompts for/returns a double and gives error when no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>double value </returns>
        static double GetInputDouble(string promptMessage, string errorMessage)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>double value greater or equal "min"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>double value element of [min,max]</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min, double max)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">double array of valid values</param>
        /// <returns>double value element range"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double[] range)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #region:decimal
        /// <summary>
        /// prompts for/returns a decimal and gives error when no decimal
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>decimal value </returns>
        static decimal GetInputDecimal(string promptMessage, string errorMessage)
        {
            string eMess = "";
            decimal value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!decimal.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a decimal and gives error when not in range or no decimal
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>decimal value greater or equal "min"</returns>
        static decimal GetInputDecimal(string promptMessage, string errorMessage, decimal min)
        {
            string eMess = "";
            decimal value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!decimal.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a decimal and gives error when not in range or no decimal
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>decimal value element of [min,max]</returns>
        static decimal GetInputDecimal(string promptMessage, string errorMessage, decimal min, decimal max)
        {
            string eMess = "";
            decimal value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!decimal.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a decimal and gives error when not in range or no decimal
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">decimal array of valid values</param>
        /// <returns>decimal value element range"</returns>
        static decimal GetInputDecimal(string promptMessage, string errorMessage, decimal[] range)
        {
            string eMess = "";
            decimal value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!decimal.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #endregion

        #region:Output

        /// <summary>
        /// clears console and prints title with leading and trailing ****** and newline before and after 
        /// </summary>
        /// <param name="title">string to print</param>
        static void TitleOutput(string title)
        {
            Console.Clear();
            Console.Write("\n***** {0} *****\n\n", title);
        }

        /// <summary>
        /// prints a string with a given backgroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for background</param>
        static void BackColoredOutput(string message, ConsoleColor background = ConsoleColor.Red)
        {
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given foregroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ForeColoredOutput(string message, ConsoleColor foreground = ConsoleColor.Cyan)
        {
            Console.ForegroundColor = foreground;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given colors
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ColoredOutput(string message, ConsoleColor foreground = ConsoleColor.White, ConsoleColor background = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        #endregion

        #endregion
    }
}
