﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NimmSpiel
{
    class Program
    {
        static void Main(string[] args)
        {
            // declare helps
            ConsoleKey select;
            bool newSetup = true, endGame, selectAtEnd;
            bool lastMoveWins = true, player1NextMove;
            int player1Type = 1, player2Type = 0; // 0 human, 1 easy, 2 medium, 3 hard
            string player1Name = "NPC-Horst", player2Name = "NPC-Detlef";

            string rules = "\n\n\t*** Nimm-Spiel ***\n\n Regeln:\n 1) Es wird gewählt mit wieviel Hölzchen gestartet wird und ob der letzte oder vorletzte gültige Zug gewinnt.\n 2) Jeder Spieler muss 1-3 Hölzchen bei jedem Zug entnehmen.\n 3) Ein Spieler legt die Anzahl der Hölzchen fest (min. 20), der andere beginnt Hölzchen zu entnehmen.";


            // start game
            do
            {
                //defaults every game
                player1NextMove = true;
                endGame = false;
                selectAtEnd = false;

                // let user choose some things
                if (newSetup)
                {
                    // wipe screen
                    Console.Clear();
                    Console.WriteLine(rules);

                    // GAME SETUP
                    string message;
                    string error = "\n   Bitte 0-3 eingeben! \n";
                    int min = 0;
                    int max = 3;

                    // get player1 isHuman, name
                    message = "\n Spieler1 ist 0=human, 1=easy, 2=medium, 3=hard : ";
                    player1Type = HowMany.GetInput(message, error, min, max);

                    Console.Write("\n Name Spieler1: ");
                    player1Name = Console.ReadLine();


                    // get player2 name, isHuman
                    message = "\n Spieler2 ist 0=human, 1=easy, 2=medium, 3=hard : ";
                    player2Type = HowMany.GetInput(message, error, min, max);

                    Console.Write("\n Name Spieler2: ");
                    player2Name = Console.ReadLine();

                    // get win condition
                    message = "\n Vorletzter(0) oder letzter (1) Zug gewinnt : ";
                    error = "\n   Bitte 0-1 eingeben! \n";
                    min = 0;
                    max = 1;
                    lastMoveWins = HowMany.GetInput(message, error, min, max) == 1 ? true : false;

                    newSetup = false;
                }

                // objects, be!
                #region:create
                // give NPC the win condition
                HowMany.lastMoveWins = lastMoveWins;

                // create box, players
                Box schachtel = new Box(lastMoveWins);
                Player player1 = new Player(player1Name, player1Type, schachtel);
                Player player2 = new Player(player2Name, player2Type, schachtel);
                #endregion


                // start game with player move and loop until schachtel.gameStart is true again
                do
                {
                    // switch next player and do next move
                    if (player1NextMove)
                    {
                        player1NextMove = false;
                        player1.NextMove();
                    }
                    else
                    {
                        player1NextMove = true;
                        player2.NextMove();
                    }
                } while (!schachtel.GameStart());

                // output winner etc
                #region:winner
                // determine and print winner
                string winner;
                if (player1NextMove != lastMoveWins)
                {
                    winner = player1Name;
                }
                else
                {
                    winner = player2Name;
                }
                schachtel.PrintCountItems(true);
                Console.Write("\nGewonnen hat ", winner);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(winner);
                Console.ResetColor();
                Console.WriteLine(", denn der {0}letzte Zug gewinnt!\n\n\n\n", lastMoveWins ? "" : "vor");
                #endregion

                // end menu loop
                while (!selectAtEnd)
                {
                    selectAtEnd = true;
                    Console.CursorVisible = false;
                    Console.Write("Ende(Escape), nochmal(Enter), neu(F2)!");
                    select = Console.ReadKey(true).Key;
                    Console.CursorVisible = true;

                    switch (select)
                    {
                        case ConsoleKey.Escape:
                            endGame = true;
                            break;

                        case ConsoleKey.F2:
                            newSetup = true;
                            break;

                        case ConsoleKey.Enter:
                            // declare temps for switch
                            string tempName;
                            int tempType;

                            // switch players
                            tempName = player1Name;
                            player1Name = player2Name;
                            player2Name = tempName;

                            tempType = player1Type;
                            player1Type = player2Type;
                            player2Type = tempType;
                            break;

                        default:
                            selectAtEnd = false;
                            Console.WriteLine("Die richtige Taste war nicht dabei!");
                            break;
                    }
                }

            } while (!endGame);
        }

    }

    class Box
    {
        // box needs to be filled
        private bool gameStart;

        // items in box
        private int countItems;

        // set defaults on construct
        public Box(bool lastMoveWins)
        {
            gameStart = true;
            countItems = 0;
        }

        // get game state
        public bool GameStart()
        {
            return gameStart;
        }

        // add count items to box
        public bool AddItems(int count)
        {
            if (count < 0)
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("   Hinzufügen und nicht wegnehmen! \n");
                Console.ResetColor();
                return false;
            }
            else
            {
                countItems += count;
                gameStart = false;
                return true;
            }
        }

        // remove count items to box
        public bool RemoveItems(int count)
        {
            if (countItems < count)
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("   In der Schachtel sind nur {0} Hölzchen. Du kannst also nicht {1} Hölzchen entnehmen! ", countItems, count);
                Console.ResetColor();
                return false;
            }
            else if (count < 0)
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("   Wegnehmen und nicht dazulegen! ");
                Console.ResetColor();
                return false;
            }
            else
            {
                countItems -= count;
                if (PrintCountItems() == 0)
                {
                    gameStart = true;
                }
                return true;
            }
        }

        // get countItem (and print it)
        public int PrintCountItems(bool print = false)
        {
            if (print)
            {
                Console.Write("Die Schachtel enthält ", countItems);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(countItems);
                Console.ResetColor();
                Console.WriteLine(" Hölzchen.", countItems);
            }
            return countItems;
        }

    }

    class Player
    {
        // likes to be called
        private string name;

        // is 0 human, 1 easy, 2 medium, 3 hard
        private int type;

        // plays with this box
        private Box schachtel;

        // constructor
        public Player(string name, int type, Box schachtel)
        {
            this.name = name;
            this.type = type;
            this.schachtel = schachtel;
        }

        // next move is called
        public void NextMove()
        {
            // print player who has to make move
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("\n\t{0}", name);
            Console.ResetColor();
            Console.WriteLine(" ist am Zug.");

            // print actual item count
            schachtel.PrintCountItems(true);

            // delay for non human players
            if (type > 0) { System.Threading.Thread.Sleep(1500); }

            // call add (start of game) or remove (rest of game)
            if (schachtel.GameStart()) { Add(); }
            else { Remove(); }
        }

        // add items to box
        private void Add()
        {
            int count;
            count = HowMany.Type(type, 0, schachtel.PrintCountItems());
            PrintOutput(name, count, true);
            schachtel.AddItems(count);
            // give NPC start count for difficulty calc
            HowMany.itemStart = schachtel.PrintCountItems();
        }

        // remove items from box
        private void Remove()
        {
            int count;
            count = HowMany.Type(type, 1, schachtel.PrintCountItems());
            PrintOutput(name, count, false);
            schachtel.RemoveItems(count);
        }

        // print output after move
        private void PrintOutput(string name, int count, bool add)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write(name);
            Console.ResetColor();
            Console.Write(": Ich {0} ", add ? "lege" : "entnehme");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write(count);
            Console.ResetColor();
            Console.WriteLine(" Hölzchen{0}.", add ? " hinein" : "");
        }
    }

    class HowMany
    {
        // for AI difficulty calculation
        public static int itemStart;

        // for AI to decide whats the best move
        public static bool lastMoveWins;

        // settings for getting user input on move if human player
        private static string[,] add0Remove1MessErrMinMax = new string[2, 4]
            { { "Wieviele Hölzchen sollen hineingelegt werden?   ",
                   "\n   Es müssen 20-100 Hölzchen hineingelegt werden! \n\n" ,
                    "20",
                "100"},
                {"Wieviele Hölzchen sollen entnommen werden?   ",
                   "" ,
                  "1"  ,
                ""} }
            ;

        // determine method to get next move amount
        public static int Type(int type, int addOrRemove, int itemCount)
        {
            if (type == 0) { return TypeHuman(addOrRemove, itemCount); }
            else { return TypeNPC(type, addOrRemove, itemCount); }
        }

        // next move amaount human
        private static int TypeHuman(int addOrRemove, int itemCount)
        {
            // set variable remove values (error, max)
            add0Remove1MessErrMinMax[1, 1] = string.Format("\n   1{0} Hölzchen entnehmen! \n\n", itemCount == 1 ? "" : itemCount == 2 ? "-2" : "-3");
            add0Remove1MessErrMinMax[1, 3] = (itemCount < 3 ? itemCount : 3).ToString();


            return GetInput(add0Remove1MessErrMinMax[addOrRemove, 0], add0Remove1MessErrMinMax[addOrRemove, 1], int.Parse(add0Remove1MessErrMinMax[addOrRemove, 2]), int.Parse(add0Remove1MessErrMinMax[addOrRemove, 3]));
        }

        // next move amaount NPC
        private static int TypeNPC(int type, int addOrRemove, int itemCount)
        {
            Random rnd = new Random();

            if (addOrRemove == 0)
            {
                int number, lastMoveWinsCorrect = 0;
                if (lastMoveWins) { lastMoveWinsCorrect = 1; }
                do
                {
                    number = rnd.Next(30, 51);
                } while (type == 3 && !(number % 4 + lastMoveWinsCorrect == 1));
                return number;
            }
            else
            {
                if (type == 1 && itemCount > HowMany.itemStart / 3)
                {
                    return rnd.Next(1, 4);
                }
                else if (type == 2 && itemCount > HowMany.itemStart * 2 / 3)
                {
                    return rnd.Next(1, 4);
                }
                else
                {
                    if (lastMoveWins)
                    {
                        if (itemCount % 4 == 0)
                        {
                            return rnd.Next(1, 4);
                        }
                        else
                        {
                            return (itemCount % 4);
                        }
                    }
                    else
                    {
                        if (itemCount == 1)
                        {
                            return 1;
                        }
                        else if (itemCount % 4 == 1)
                        {
                            return rnd.Next(1, 4);
                        }
                        else
                        {
                            return (itemCount % 4 == 0 ? 3 : itemCount % 4 - 1);
                        }
                    }
                }

            }
        }
        
        // get console input with conditions
        public static int GetInput(string message, string error, int min, int max)
        {
            string errorMessage = "";
            int value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(errorMessage);
                Console.ResetColor();
                Console.Write(message);
                errorMessage = error;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }
    }
}
