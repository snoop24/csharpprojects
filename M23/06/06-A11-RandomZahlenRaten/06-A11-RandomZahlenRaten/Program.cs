﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A11_RandomZahlenRaten
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // declare and defaults
                int rateMin = 1;
                int rateMax = 100;
                int rateZahl = 0;
                int rateCount = 0;
                Random rnd = new Random();
                int gesuchteZahl;
                bool erraten = false;

                gesuchteZahl = rnd.Next(rateMin, rateMax+1);
                Console.Clear();
                Console.WriteLine("\n  --- munteres Zahlenraten für ahnungsloseManager ---");
                
                // test
                //Console.WriteLine(gesuchteZahl);
            
                while (!erraten)
                {
                    //read RateZahl
                    string errMsg, inpErrMsg, inpPrompt;
                    errMsg = "";
                    inpErrMsg = "\n   Zahl? Im Intervall?\n";
                    inpPrompt = string.Format("\n   Zahl zwischen {0} und {1}:  ",rateMin, rateMax);
                    do
                    {
                        //Console.Clear();
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.Write(errMsg);
                        Console.ResetColor();
                        Console.Write(inpPrompt);
                        errMsg = inpErrMsg;
                    } while ((!int.TryParse(Console.ReadLine(), out rateZahl) || rateZahl < rateMin || rateZahl > rateMax) && rateZahl != 314159);

                    rateCount++;

                    if (rateZahl == 314159)
                    {
                        erraten = true;
                    }
                    else if (rateZahl < gesuchteZahl)
                    {
                        Console.WriteLine("   {0} ist zu klein.", rateZahl);
                        rateMin = rateZahl + 1;
                    }
                    else if (rateZahl > gesuchteZahl)
                    {
                        Console.WriteLine("   {0} ist zu groß.", rateZahl);
                        rateMax = rateZahl + 1;
                    }
                    else
                    {
                        erraten = true;
                    }

                }

                Console.WriteLine();
                if (rateCount < 6)
                {
                    Console.WriteLine("   {0} mal geraten. Super!",rateCount);
                }
                else if (rateCount < 11)
                {
                    Console.WriteLine("   {0} mal geraten. Geht so, ne?",rateCount);
                }
                else
                {
                    Console.WriteLine("   Hömma, was war das? {0} mal geraten. \n\n", rateCount);
                }

                Console.WriteLine();
            } while (OnceAgain());
        }

        #region:utility
        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }
        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }
        #endregion
    }
}
