﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A2_Fakultät1_n_GeradeUngeradeAusgabe
{
    class Program
    {
        static void Main(string[] args)
        {
            //Fakultaet1_n();
            GeradeUngeradeAusgabe();
        }

        static void Fakultaet1_n()
        {
            do
            {
                double count, ende, produkt = 1;

                string errMsg, inpErrMsg, inpPrompt;
                // read ende
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt = "   Bitte n für n! eingeben: \n\n     ";
                do
                {
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!double.TryParse(Console.ReadLine(), out ende) || ende < 0);


                Console.Clear();
                Console.Write("\n\tFakultät: {0}! \n\n\t", ende);
                for (count = 1; count < ende + 1; count++)
                {
                    if (ende < 7)
                    {
                        if (count == ende)
                        {
                            Console.Write(" {0}", count);
                        }
                        else
                        {
                            Console.Write(" {0} *", count);
                        }
                    }
                    else
                    {
                        if (count < 4) Console.Write(" {0} *", count);
                        if (count == 4) Console.Write(" ... ", count);
                        if (count > ende - 2) Console.Write("* {0} ", count);
                    }
                    produkt *= count;
                }
                Console.WriteLine("\n\n\t\t{0}!= {1}",ende, produkt);



                Console.WriteLine();


                // nochmal?
                Console.WriteLine(" Weiter mit dem (ANY) Key. Zum Beenden (x).");

            } while (Console.ReadKey(true).Key != ConsoleKey.X);
        }

        static void GeradeUngeradeAusgabe()
        {
            do
            {
                int ende, ungDiff1;
                char unGerade;

                string errMsg, inpErrMsg, inpPrompt;
                // read ende
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt = "   Bitte Zahl eingeben:     ";
                do
                {
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out ende) || ende < 0);

                // read gerade/ungerade
                inpPrompt = "\n   (g)erade oder (u)ngerade? ";
                do
                {
                    Console.Write(inpPrompt);
                    unGerade = char.Parse(Console.ReadKey(true).Key.ToString().ToLower());
                    Console.Write(unGerade + "\b");
                } while (!(unGerade=='g' || unGerade == 'u'));
                if (unGerade == 'g')
                {
                    ungDiff1 = 0;
                }
                else
                {
                    ungDiff1 = 1;
                }

                Console.WriteLine("\n\n");
                for (int count = 0; count < ende;)
                {
                    count = count + 2;
                    Console.Write("{0,8}", count - ungDiff1);
                }
                


                Console.WriteLine("\n\n\n");


                // nochmal?
                Console.WriteLine(" Weiter mit dem (ANY) Key. Zum Beenden (x).");

            } while (Console.ReadKey(true).Key != ConsoleKey.X);
        }
    }
}
