﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A9_Prüfsumme
{
    class Program
    {
        static void Main(string[] args)
        {
            // variablen
            long pruefSumme, pruefZahl, pruefZahlBak;
            int count;

            do
            {
                // wipe screen
                Console.Clear();

                //get inputs
                string errMsg, inpErrMsg, inpPrompt;
                //read pyramidSize
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt = "    Zu prüfende Zahl (1.000-9.999.999.999) eingeben: ";
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write(inpPrompt);
                    Console.ResetColor();
                    errMsg = inpErrMsg;
                } while (!long.TryParse(Console.ReadLine(), out pruefZahl) || pruefZahl < 999 || pruefZahl > 9999999999);

                // initialisieren
                pruefSumme = 0;
                count = 0;
                pruefZahlBak = pruefZahl;

                // sum loop
                while (pruefZahl > 0)
                {
                    pruefSumme += (pruefZahl % 10) * (1 + count % 2) > 9 ? (pruefZahl % 10) * (1 + count % 2) -9: (pruefZahl % 10) * (1 + count % 2) ;
                    pruefZahl /= 10;
                    count++;
                }

                // output
                Console.WriteLine("\n    Die Zahl {0} mit Quersumme {1} ist {2}gültig.\n\n", pruefZahlBak, pruefSumme, pruefSumme % 10 == 0 ? "" : "un");

            } while (OnceAgain(ConsoleKey.X, ConsoleKey.N));

        }


        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion

    }
}
