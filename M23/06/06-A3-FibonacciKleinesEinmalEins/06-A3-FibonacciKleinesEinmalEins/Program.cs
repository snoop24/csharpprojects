﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A3_FibonacciKleinesEinmalEins
{
    class Program
    {
        static void Main(string[] args)
        {
            Fibonacci();
            //KleinesEinmalEins();
        }

        static void Fibonacci()
        {
            do
            {
                int anzahl;
                decimal fiboN = 1;
                decimal fiboNminus1 = 0;
                decimal fiboTemp;

                //read anzahl
                string errMsg, inpErrMsg, inpPrompt;
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt = "\t     F     i     b     o     n     a     c     c     i\n" +
                    "\n\t\t     Bitte Anzahl eingeben (3-60):  ";
                do
                {
                    Console.ResetColor();
                    Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write(inpPrompt);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out anzahl) || anzahl < 3 || anzahl > 60);
                Console.ResetColor();

                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n\t     F     i     b     o     n     a     c     c     i");
                Console.ResetColor();

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("\n" +
                            " {0,3} - {1,3}", 1, 5);
                Console.ResetColor();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("{0,13}{1,13}",fiboNminus1, fiboN);
                Console.ResetColor();

                for (int count = 3; count <= anzahl; count++)
                {
                    fiboTemp = fiboNminus1;
                    fiboNminus1 = fiboN;
                    fiboN = fiboTemp + fiboNminus1;

                    if (count % 5 == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write("\n\n" +
                            " {0,3} - {1,3}", count, count+4  < anzahl ? count +  4: anzahl);
                        Console.ResetColor();
                    }
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("{0,13}", fiboN);
                    Console.ResetColor();
                }
                Console.WriteLine();

                } while (Nochmal());
        }

        static void KleinesEinmalEins()
        {

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("{0,4}{1,4}{2,4}{3,4}{4,4}{5,4}{6,4}{7,4}{8,4}{9,4}{10,4}\n",
                    "1*1", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
                for (int countZeile = 1; countZeile <= 10; countZeile++)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("{0,4}", countZeile);
                    Console.ResetColor();

                    for (int countSpalte = 1; countSpalte <= 10; countSpalte++)
                    {
                        if (countZeile == countSpalte) { Console.ForegroundColor = ConsoleColor.Red; }
                        Console.Write("{0,4}", countZeile * countSpalte);
                        Console.ResetColor();
                    }
                    Console.WriteLine("\n");
                }


            } while (Nochmal());

        }

        static bool Nochmal()
        {
            // nochmal?
            Console.WriteLine();
            Console.Write(" ... weiter (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("ANY-Key");
            Console.ResetColor();
            Console.Write(") und beenden (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("x");
            Console.ResetColor();
            Console.Write(")!");
            if (Console.ReadKey(true).Key == ConsoleKey.X)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
