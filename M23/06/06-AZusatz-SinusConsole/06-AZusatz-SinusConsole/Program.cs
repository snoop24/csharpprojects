﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_AZusatz_SinusConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // declare variables
            int funHeight;

            do
            {
                
                
                // wipe screen
                Console.Clear();
                Console.WriteLine("Sinus\n-------\n");
                //get inputs
                string errMsg, inpErrMsg, inpPrompt;
                //read pyramidSize
                errMsg = "";
                inpErrMsg = "\nFehlerhafte Eingabe! \n";
                inpPrompt = "Hoehe(5-40)? ";
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write(inpPrompt);
                    Console.ResetColor();
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out funHeight) || funHeight < 4|| funHeight > 40);
                Console.WriteLine();
                Console.WindowHeight = 8 + funHeight * 2;
                Console.WindowWidth =  funHeight * 5+20 ;
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                for (int left = 0; left < Console.WindowWidth; left++)
                {
                    for (int top = 0; top < funHeight * 2 + 1; top++)
                    {
                        Console.SetCursorPosition(left, 6 + top);
                        Console.Write(" ");
                    }
                    Console.SetCursorPosition(left, 6 + funHeight);
                    Console.Write("-");
                    Console.SetCursorPosition(left, (int)Math.Round(6 + funHeight -funHeight* Math.Sin((double)left /funHeight ),0));                    
                    Console.Write("*");
                    System.Threading.Thread.Sleep(5);
                }
                for (int left = 0; left < Console.WindowWidth; left++)
                {
                    Console.SetCursorPosition(left, (int)Math.Round(6 + funHeight - funHeight * Math.Cos((double)left / funHeight), 0));
                    Console.Write("*");
                    System.Threading.Thread.Sleep(5);
                }

                Console.ResetColor();


            } while (OnceAgain(ConsoleKey.X, ConsoleKey.N,0,7));
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
