﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A4_HypothenuseTabelle_WiderstandLDTabelle
{
    class Program
    {
        static void Main(string[] args)
        {
            HypothenuseTabelle();
            //WiderstandLDTabelle();
        }

        static void HypothenuseTabelle()
        {
            do
            {
                // clear console and print title
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\n\t\t Länge der Hypothenuse C mit Katheten A und B\n");
                Console.ResetColor();

                // first field
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("{0,14}", "C=sqrt(A²+B²)");
                Console.ResetColor();

                // start loops to print table
                for (double b = 0; b < 11; b++)
                {
                    for (double a = 1; a < 9; a++)
                    {
                        if (b == 0) // print remainder of top line
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write("{0,6}{1,2}", "A=", a);
                            Console.ResetColor();
                        }
                        else
                        {
                            if (a == 1) // print newline and leftmost field
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.Write("\n\n{0,12}{1,2}", "B=", b);
                                Console.ResetColor();
                            }

                            // print remaining fields in line
                            if (Math.Sqrt(a * a + b * b) % 1 == 0) // colorize pythagorean triple
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                            }
                            Console.Write("{0,8:F2}", Math.Round(Math.Sqrt(a * a + b * b), 2));
                            Console.ResetColor();
                        }
                    }
                }
                Console.WriteLine();

                // once again?
            } while (Nochmal());
        }

        static void WiderstandLDTabelle()
        {
            do
            {
                // variables
                decimal vonLaenge = 1m;
                decimal bisLaenge = 1m;
                decimal schrittweite = 1m;


                // clear console and print title
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\n\t Widerstand ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("R");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" eines Kupferdrahtes aus (");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("L");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(")änge und (");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("D");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(")urchmesser \n");
                Console.ResetColor();

                
                //get inputs
                string errMsg, inpErrMsg, inpPrompt;

                //read vonLaenge
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt = "    Startlänge ( 0 - 9999,9 )m: ";
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write(inpPrompt);
                    Console.ResetColor();
                    errMsg = inpErrMsg;
                } while (!decimal.TryParse(Console.ReadLine(), out vonLaenge) || vonLaenge < 0 || (vonLaenge * 10) % 1 != 0);

                //read bisLaenge
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt = "    Endlänge ( " + (vonLaenge + 0.1m) + " - 10.000 )m: ";
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write(inpPrompt);
                    Console.ResetColor();
                    errMsg = inpErrMsg;
                } while (!decimal.TryParse(Console.ReadLine(), out bisLaenge) || bisLaenge > 10000 || bisLaenge <= vonLaenge || (bisLaenge * 10) % 1 != 0);

                //read schrittweite
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                string schritteMoeglich = string.Format("{0,2} ({1,7:F1}{2}", 1,bisLaenge - vonLaenge,"m)");
                for (int i = 2; i < 21; i++)
                {
                    if (((((bisLaenge - vonLaenge) / i) * 10) % 1) == 0 )
                    {
                        schritteMoeglich += string.Format("\n\t\t{0,2} ({1,7:F1}{2}",i,(bisLaenge - vonLaenge) / i,"m)");
                    }
                }
                inpPrompt =  string.Format("    Schrittzahl {0} : ", schritteMoeglich);
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write(inpPrompt);
                    Console.ResetColor();
                    errMsg = inpErrMsg;
                } while (!decimal.TryParse(Console.ReadLine(), out decimal schrittanzahl) || schrittanzahl < 1 || !decimal.TryParse(((bisLaenge - vonLaenge) / schrittanzahl).ToString(), out schrittweite) || !(((schrittweite*10)%1 == 0 && (bisLaenge - vonLaenge) / schrittweite < 21)));
                inpErrMsg = "\n   Fehlerhafte Eingabe!  \n";




                // clear console and print title
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\n\t Widerstand ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("R");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" eines Kupferdrahtes aus (");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("L");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(")änge und (");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("D");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(")urchmesser \n");
                Console.ResetColor();

                // first field
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("{0,2}", "R");
                Console.ResetColor();
                Console.Write(" in Ohm | ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("L ");
                Console.ResetColor();
                Console.Write("\\ ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("D");
                Console.ResetColor();
                Console.Write(" ");
                Console.ResetColor();

                // start loops to print table
                for (decimal l = vonLaenge - schrittweite; l <= bisLaenge; l += schrittweite)
                {
                    for (decimal d = 0.5m; d <= 1; d += 0.1m)
                    {
                        if (l == vonLaenge - schrittweite) // print remainder of top line
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write("{0,7:0.0} mm", d);
                            Console.ResetColor();
                        }
                        else
                        {
                            if (d == 0.5m) // print newline and leftmost field
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.Write("\n{0,15:0.0} m ", l);
                                Console.ResetColor();
                            }
                            // print field
                            Console.Write("{0,9:F3} ", Math.Round(l / 56.8m /  (decimal)Math.PI / d / d * 4m, 3));
                        }
                    }
                }
                Console.WriteLine();

                // once again?


            } while (Nochmal());
        }

        static bool Nochmal()
        {
            // nochmal?
            Console.WriteLine();
            Console.Write(" ... weiter (");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write("any");
            Console.ResetColor();
            Console.Write(") und beenden (");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write("x");
            Console.ResetColor();
            Console.Write(")!");
            if (Console.ReadKey(true).Key == ConsoleKey.X)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
