﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A1_Quadratzahlen_Summe1_n
{
    class Program
    {
        static void Main(string[] args)
        {
            Quadratzahlen();
            //Summen1_n();
        }

        static void Quadratzahlen()
        {
            do
            {
                int i;

                Console.Clear();
                Console.WriteLine("\n\tFor-Loop\n");
                for (i = 1; i < 11; i++)
                {
                    Console.WriteLine("   Das Quadruat von {0,2} ist {1,3}.", i, i * i);
                }
                Console.WriteLine();
                Console.ReadKey(true);

                Console.Clear();
                Console.WriteLine("\n\tFor-Each-Loop\n");
                int[] zahlen = { 1, 2, 3, 4, 5,6, 7, 8, 9, 10 };
                foreach ( int j in zahlen)
                {
                    i = j;
                    Console.WriteLine("   Das Quadruat von {0,2} ist {1,3}.", i, i * i);
                }
                Console.WriteLine();
                Console.ReadKey(true);

                Console.Clear();
                Console.WriteLine("\n\tDo-While-Loop\n");
                i = 1;
                do
                {
                    Console.WriteLine("   Das Quadruat von {0,2} ist {1,3}.", i, i * i);
                    i++;
                } while (i < 11);
                Console.WriteLine();
                Console.ReadKey(true);

                Console.Clear();
                Console.WriteLine("\n\tWhile-Loop\n");
                i = 1;
                while (i<11)
                {
                    Console.WriteLine("   Das Quadruat von {0,2} ist {1,3}.", i, i * i);
                    i++;
                } 
                Console.WriteLine();
                
                // nochmal?
                Console.WriteLine("zum Beenden (x) ...");

            } while ( Console.ReadKey(true).Key != ConsoleKey.X);

        }

        static void Summen1_n()
        {
            do
            {
                double count, anfang, ende, summe=0;
                Random rnd = new Random();
                anfang = rnd.Next(1, 5);
                ende = rnd.Next(50, 10000);

                Console.Clear();
                Console.Write("\n\t     Summe von i={0} bis n={1}  \n\n\t", anfang, ende);
                for (count = anfang; count < ende + 1; count++)
                {
                    if (count < anfang + 5) Console.Write(" {0} +",count);
                    if (count == 25) Console.Write(" ... ", count);
                    if (count > ende - 5) Console.Write("+ {0} ", count);
                    summe += count;
                }
                Console.WriteLine("\n\n\t\t= {0}",summe);

                Console.WriteLine("\n\n\tKontrolle mit n(n+1)/2 - (i-1)((i-)+1)/2 = {2}",ende,anfang,ende*(ende+1)/2-(anfang-1)*(anfang-1+1)/2);
                


                Console.WriteLine();
                

                // nochmal?
                Console.WriteLine("zum Beenden (x) ...");

            } while (Console.ReadKey(true).Key != ConsoleKey.X);

        }
    }
}
