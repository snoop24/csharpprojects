﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A6_Pyramids
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Clear();
                int pyramidSize;
                string pyramid = "*";

                //get inputs
                string errMsg, inpErrMsg, inpPrompt;
                //read pyramidSize
                errMsg = "";
                inpErrMsg = "\n   Fehlerhafte Eingabe! \n";
                inpPrompt = "    Größe der Pyramide (1-10): ";
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write(inpPrompt);
                    Console.ResetColor();
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out pyramidSize) || pyramidSize < 0 || pyramidSize > 10);

                for (int i = 0; i < pyramidSize; i++)
                {

                    Console.SetCursorPosition(2 , Console.CursorTop+1);
                    for (int j = 0; j < i+1; j++)
                    {
                        Console.Write(pyramid);
                    }

                    Console.SetCursorPosition(4+2*pyramidSize -i, Console.CursorTop);
                    for (int j = 0; j < i*2+1; j++)
                    {
                        Console.Write(pyramid);
                    }

                    Console.SetCursorPosition(6+3*pyramidSize +i, Console.CursorTop);
                    for (int j = (pyramidSize-i)*2-1; j > 0; j--)
                    {
                        Console.Write(pyramid);
                    }

                }


                Console.WriteLine("\n");
            } while (OnceAgain());

        }

        #region:utility
        static void PrintBorders(
            int borderWidth = 20,
            int borderHeight = 15,
            ConsoleColor borderColor = ConsoleColor.Blue,
            int anchorLeft = 0,
            int anchorTop = 0)
        {
            Console.BackgroundColor = borderColor;
            // print top and bottom
            for (int i = 0; i < borderWidth; i++)
            {
                Console.SetCursorPosition(anchorLeft + i, anchorTop);
                Console.Write(" ");
                Console.SetCursorPosition(anchorLeft + i, anchorTop + borderHeight - 1);
                Console.Write(" ");
            }
            // print left and right
            for (int i = 1; i < borderHeight - 1; i++)
            {
                Console.SetCursorPosition(anchorLeft, anchorTop + i);
                Console.Write(" ");
                Console.SetCursorPosition(anchorLeft + borderWidth - 1, anchorTop + i);
                Console.Write(" ");
            }
            Console.ResetColor();
        }


        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }
        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }
        #endregion
    }
}
