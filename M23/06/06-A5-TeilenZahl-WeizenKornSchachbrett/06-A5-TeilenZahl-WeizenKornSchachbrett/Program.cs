﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A5_TeilenZahl_WeizenKornSchachbrett
{
    class Program
    {
        static void Main(string[] args)
        {
            //TeilenZahl();
            WeizenKornSchachbrettNoTruck();
            //WeizenKornSchachbrett();
        }

        static void TeilenZahl()
        {
            do
            {
                double zahl = 123456;
                int count;
                //calc loop
                for (count = 0; 1 <= zahl; count++)
                {
                    zahl = zahl / 2.5;
                }
                // output
                Console.Clear();
                Console.WriteLine("\n {0} / 2,5^{1} = {2:F2}.", 123456, count, zahl);

            } while (OnceAgain());
        }

        static void WeizenKornSchachbrettNoTruck()
        {
            do
            {
                decimal trucks = 0;
                decimal stauLaenge = 0;
                decimal summe = 1;

                Console.Clear();
                Console.SetWindowSize(Console.WindowWidth, 40);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\t\t   1 Weizenkorn pro Schachbrettfeld ...");
                Console.ResetColor();
                for (int count = 0; count < 64; count++)
                {
                    if (count == 63) { Console.WriteLine(); }
                    if (count % 3 == 0) { Console.WriteLine(); }
                    Console.Write("  {0,2}: {1,20}", count + 1, summe);
                    summe *= 2;
                }
                summe--;

                Console.Write(" Körner addiert {1}\n   Insgesamt sind das ca. {0:#,##0}t", summe * 0.00000005m,summe);
                trucks = summe * 0.00000005m / 25m;
                stauLaenge = trucks * 0.019m;

                Console.Write("\n\n\tca. {0,15:#,##0} Trucks und ca.{1,15:#,##0.0} km Stau", trucks + 1, stauLaenge);
                Console.WriteLine();
                Console.WriteLine();
            } while (OnceAgain());
        }

        static void WeizenKornSchachbrett()
        {
            do
            {
                decimal trucks = 0;
                decimal stauLaenge = 0;
                decimal summe = 1;

                Console.Clear();
                Console.SetWindowSize(Console.WindowWidth, 40);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("\t\t   1 Weizenkorn pro Schachbrettfeld ...");
                Console.ResetColor();
                for (int count = 0; count < 64; count++)
                {
                    if (count == 63) { Console.WriteLine(); }
                    if (count % 3 == 0) { Console.WriteLine(); }
                    Console.Write("  {0,2}: {1,20}", count + 1, summe);
                    summe *= 2;
                }
                summe--;

                Console.Write(" Körner addiert {1}\n   Insgesamt sind das ca. {0:#,##0}t", summe  * 0.00000005m,summe);
                trucks = summe  * 0.00000005m / 25m;
                stauLaenge = trucks * 0.019m;

                Console.Write("\n\n    ca. {0,15:#,##0} Trucks und ca.{1,15:#,##0.0} km Stau", trucks + 1, stauLaenge);
                //System.Threading.Thread.Sleep(2000);

                //Console.WriteLine("\n" +
                //    "                   ______________________________________________________\n" +
                //    "                  |                                                      |\n" +
                //    "             /    |               ca.{0,15:#,##0} Trucks              |\n" +
                //    "            /---, |                                                      |\n" +
                //    "       -----# ==| |               ca.{1,15:#,##0.0} km Stau             |\n" +
                //    "       | :) # ==| |                                                      |\n" +
                //    "  -----'----#   | |______________________________________________________|\n" +
                //    "  |)___()  '#   |______====____   \\___________________________________|\n" +
                //    " [_/,-,\\\"--\"------ //,-,  ,-,\\\\\\   |/             //,-,  ,-,  ,-,\\ __#\n" +
                //    "   ( 0 )|===******||( 0 )( 0 )||-  o              '( 0 )( 0 )( 0 )||\n" +
                //    "----'-'--------------'-'--'-'-----------------------'-'--'-'--'-'--------------"
                //    , trucks+1, stauLaenge);

                PrintBorders(80, 13, ConsoleColor.DarkCyan, 0, Console.CursorTop +1);

                // set cursor
                int cursorHeight = 11;
                int cursorWidth = 75;
                string[] cursor = new string[cursorHeight];
                cursor[0] = "                  ______________________________________________________        ";
                cursor[1] = "                 |                                                      |       ";
                cursor[2] = "            /    |                                                      |       ";
                cursor[3] = "           /---, |                                                      |       ";
                cursor[4] = "      -----# ==| |                                                      |       ";
                cursor[5] = "      | :) # ==| |                                                      |       ";
                cursor[6] = " -----'----#   | |______________________________________________________|       ";
                cursor[7] = " |)___()  '#   |______====____   \\___________________________________|          ";
                cursor[8] = "[_/,-,\\\"--\"------ //,-,  ,-,\\\\\\   |/             //,-,  ,-,  ,-,\\ __#          ";
                cursor[9] = "  ( 0 )|===******||( 0 )( 0 )||-  o              '( 0 )( 0 )( 0 )||             ";
                cursor[10] = "   '-'              '-'  '-'                       '-'  '-'  '-'                ";

                // print cursor
                Console.SetCursorPosition(1, Console.CursorTop-1 );
                int k = 0;
                string[] cursor2 = new string[cursorHeight];
                Console.ForegroundColor = ConsoleColor.Red;
                do
                {
                    for (int j = 0; j < cursorHeight; j++) cursor2[j] = "";
                    for (int j = 0; j < cursorHeight; j++)
                        for (int i = 0+k; i < 78+k; i++)
                            cursor2[j] += cursor[j].Substring(i%75,1);

                    Console.SetCursorPosition(1, Console.CursorTop - cursorHeight);
                    for (int i = 0; i < cursorHeight; i++)
                    {
                        Console.SetCursorPosition(1, Console.CursorTop+1);
                        Console.Write(cursor2[i]) ;
                    }
                    k++;
                    System.Threading.Thread.Sleep(5);
                } while (!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.Escape);
                Console.ResetColor();
                Console.WriteLine("\n");
            } while (OnceAgain(ConsoleKey.X, 0, 0));
        }

        static void PrintBorders(
            int borderWidth = 20,
            int borderHeight = 15,
            ConsoleColor borderColor = ConsoleColor.Blue,
            int anchorLeft = 0,
            int anchorTop = 0)
        {
            Console.BackgroundColor = borderColor;
            // print top and bottom
            for (int i = 0; i < borderWidth; i++)
            {
                Console.SetCursorPosition(anchorLeft + i, anchorTop);
                Console.Write(" ");
                Console.SetCursorPosition(anchorLeft + i, anchorTop + borderHeight - 1);
                Console.Write(" ");
            }
            // print left and right
            for (int i = 1; i < borderHeight - 1; i++)
            {
                Console.SetCursorPosition(anchorLeft, anchorTop + i);
                Console.Write(" ");
                Console.SetCursorPosition(anchorLeft + borderWidth - 1, anchorTop + i);
                Console.Write(" ");
            }
            Console.ResetColor();
        }

        #region:utility
        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }
        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }
        #endregion
    }
}
