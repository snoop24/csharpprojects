﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_A10_LaufernderCursor
{
    class Program
    {
        static void Main(string[] args)
        {
            //EasyCursor();
            //EasyBall();
            Snake();
        }
        static void EasyCursor()
        {
            // declare variables
            int borderWidth, borderHeight;
            int cursorLeft, cursorTop;
            int moveLeft, moveTop;
            int speed;
            string cursor;
            ConsoleColor cursorColor, borderColor;


            // defaults
            borderHeight = 40;
            borderWidth = 80;

            cursorLeft = 1;
            cursorTop = 1;

            moveLeft = 1;
            moveTop = 1;
            speed = 20;

            cursor = "*";
            cursorColor = ConsoleColor.Red;
            borderColor = ConsoleColor.Blue;

            // set console size
            Console.SetWindowSize(borderWidth, borderHeight);

            // print borders
            // border color
            Console.BackgroundColor = borderColor;
            // top border
            Console.SetCursorPosition(0, 0);
            for (int i = 0; i < borderWidth; i++)
            {
                Console.Write(" ");
            }
            // bottom border
            Console.SetCursorPosition(0, borderHeight - 1);
            for (int i = 0; i < borderWidth; i++)
            {
                Console.Write(" ");
            }
            // left and right border
            for (int i = 0; i < borderHeight; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.WriteLine(" ");
                Console.SetCursorPosition(borderWidth - 1, i);
                Console.Write(" ");
            }
            Console.ResetColor();
            Console.SetCursorPosition(0, 0);


            // set foreground color for cursor
            Console.ForegroundColor = cursorColor;
            while (!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.Escape)
            {

                // print cursor
                Console.SetCursorPosition(cursorLeft, cursorTop);
                Console.Write(cursor);

                // check if next move hits border then invert move
                if (cursorLeft + moveLeft == 0 || cursorLeft + moveLeft == borderWidth - 1)
                {
                    moveLeft = -moveLeft;
                }
                if (cursorTop + moveTop == 0 || cursorTop + moveTop == borderHeight - 1)
                {
                    moveTop = -moveTop;
                }

                // wait  aka movementspeed
                System.Threading.Thread.Sleep(1000 / speed);

                // unprint cursor
                Console.SetCursorPosition(cursorLeft, cursorTop);
                Console.Write(" ");

                // next cursorposition
                cursorLeft = cursorLeft + moveLeft;
                cursorTop = cursorTop + moveTop;
            }
        }
        static void EasyBall()
        {
            // declare variables
            int borderWidth, borderHeight;
            int cursorLeft, cursorTop;
            int moveLeft, moveTop;
            int speed;
            int cursorHeight, cursorWidth;
            int cursorArt;
            ConsoleColor cursorColor, borderColor;


            // defaults
            borderHeight = 50;
            borderWidth = 100;

            moveLeft = 1;
            moveTop = 0;
            speed = 40;
            cursorArt = 1;

            //set cursor form and size
            //cursorHeight = 1;
            //cursorWidth = 1;
            //string[] cursor = new string[cursorHeight];
            //cursor[0] = "*";

            cursorHeight = 11;
            cursorWidth = 73;
            string[] cursor = new string[cursorHeight];
            cursor[0] = "                  ______________________________________________________";
            cursor[1] = "                 |                                                      |";
            cursor[2] = "            /    |                                                      |";
            cursor[3] = "           /---, |                                                      |";
            cursor[4] = "      -----# ==| |                                                      |";
            cursor[5] = "      | :) # ==| |                                                      |";
            cursor[6] = " -----'----#   | |______________________________________________________|";
            cursor[7] = " |)___()  '#   |______====____   \\___________________________________|";
            cursor[8] = "[_/,-,\\\"--\"------ //,-,  ,-,\\\\\\   |/             //,-,  ,-,  ,-,\\ __#";
            cursor[9] = "  ( 0 )|===******||( 0 )( 0 )||-  o              '( 0 )( 0 )( 0 )||";
            cursor[10] = "   '-'              '-'  '-'                       '-'  '-'  '-'";



            //cursorHeight = 4;
            //cursorWidth = 13;
            //string[] cursor = new string[cursorHeight];
            //cursor[0] = "  ______     ";
            //cursor[1] = " /|_||_\\`.__";
            //cursor[2] = "(  _    _ _ \\";
            //cursor[3] = "=`-(_)--(_)-'";

            //cursorHeight = 17;
            //cursorWidth = 12;
            //string[] cursor = new string[cursorHeight];
            //cursor[0] = "    ,.,";
            //cursor[1] = "('=(\"\"\")= ')";
            //cursor[2] = "'))|*_*|(('";
            //cursor[3] = "(' \\ = / ')";
            //cursor[4] = " ' _) (_ '";
            //cursor[5] = " /|'~~~'|\\";
            //cursor[6] = "/,(_   _),\\";
            //cursor[7] = "\\\\ \\___/ //";
            //cursor[8] = " '\\/   \\/'";
            //cursor[9] = "  :__,__:";
            //cursor[10] = "  \\  /  /";
            //cursor[11] = "   \\/  /";
            //cursor[12] = "   (  ;";
            //cursor[13] = "    \\  \\";
            //cursor[14] = "     \\ |";
            //cursor[15] = "     \\\\/)";
            //cursor[16] = "   .//_/";


            //cursorHeight = 5;
            //cursorWidth = 11;
            //string[] cursor = new string[cursorHeight];
            //cursor[0] = "    .-.";
            //cursor[1] = "   ( . )";
            //cursor[2] = " .-.':'.-.";
            //cursor[3] = "(  =,!,=  )";
            //cursor[4] = " '-' | '-'";

            cursorLeft = 1;
            cursorTop = 1;

            cursorColor = ConsoleColor.Red;
            borderColor = ConsoleColor.Blue;

            // set console size
            Console.SetWindowSize(borderWidth, borderHeight);

            // print borders
            // border color
            Console.BackgroundColor = borderColor;
            // top border
            Console.SetCursorPosition(0, 0);
            for (int i = 0; i < borderWidth; i++)
            {
                Console.Write(" ");
            }
            // bottom border
            Console.SetCursorPosition(0, borderHeight - 1);
            for (int i = 0; i < borderWidth; i++)
            {
                Console.Write(" ");
            }
            // left and right border
            for (int i = 0; i < borderHeight; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.WriteLine(" ");
                Console.SetCursorPosition(borderWidth - 1, i);
                Console.Write(" ");
            }
            Console.ResetColor();
            Console.SetCursorPosition(0, 0);


            // set foreground color for cursor
            Console.ForegroundColor = cursorColor;
            while (!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                while (!(!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.UpArrow))
                {
                    moveTop = -1;
                }
                while (!(!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.DownArrow))
                {
                    moveTop = 1;
                }
                // print cursor
                Console.ForegroundColor = cursorColor;
                for (int i = 0; i < cursorHeight; i++)
                {
                    Console.SetCursorPosition(cursorLeft, cursorTop + i);
                    Console.Write(cursor[i]);
                }

                // check if next move hits border then invert speed
                if (cursorLeft + moveLeft == 0 || cursorLeft + moveLeft + cursorWidth - 1 == borderWidth - 1)
                {
                    moveLeft = -moveLeft;
                }
                if (cursorTop + moveTop == 0 || cursorTop + moveTop + cursorHeight - 1 == borderHeight - 1)
                {
                    moveTop = -moveTop;
                }

                // wait  aka movementspeed
                System.Threading.Thread.Sleep(1000 / speed);

                // unprint cursor
                Console.ForegroundColor = Console.BackgroundColor;
                for (int i = 0; i < cursorHeight; i++)
                {
                    Console.SetCursorPosition(cursorLeft, cursorTop + i);
                    Console.Write(cursor[i]);
                }

                // next cursorposition
                cursorLeft = cursorLeft + moveLeft;
                cursorTop = cursorTop + moveTop;
            }
        }

        static void Snake()
        {
            do
            {
                // declare variables
                int borderWidth, borderHeight;
                int moveLeft, moveTop;
                int speed, speedLvlup;
                string cursor;
                int cursorLengthMax;
                int cursorLength, cursorLengthLvlup;
                ConsoleColor cursorColor, borderColor, backColor;
                bool end = false;
                ConsoleKey direction;


                // defaults
                borderHeight = 40;
                borderWidth = 80;

                moveLeft = 1;
                moveTop = 0;
                speed = 20;
                speedLvlup = 1;
                cursor = " ";

                // set default snake positions

                cursorLengthMax = 110;
                cursorLength = 10;
                cursorLengthLvlup = 5;

                int[] cursorTop = new int[cursorLengthMax];
                int[] cursorLeft = new int[cursorLengthMax];

                for (int i = 0; i < cursorLength; i++)
                {
                    cursorLeft[i] = borderWidth / 2 - i;
                    cursorTop[i] = borderHeight / 2;
                }


                cursorColor = ConsoleColor.Red;
                borderColor = ConsoleColor.Blue;
                backColor = ConsoleColor.Black;

                // set console size
                Console.SetWindowSize(borderWidth, borderHeight);

                // print borders
                Console.Clear();
                // border color
                Console.BackgroundColor = borderColor;
                // top border
                Console.SetCursorPosition(0, 0);
                for (int i = 0; i < borderWidth; i++)
                {
                    Console.Write(" ");
                }
                // bottom border
                Console.SetCursorPosition(0, borderHeight - 1);
                for (int i = 0; i < borderWidth; i++)
                {
                    Console.Write(" ");
                }
                // left and right border
                for (int i = 0; i < borderHeight; i++)
                {
                    Console.SetCursorPosition(0, i);
                    Console.WriteLine(" ");
                    Console.SetCursorPosition(borderWidth - 1, i);
                    Console.Write(" ");
                }
                Console.ResetColor();
                Console.SetCursorPosition(0, 0);

                // set lvl up
                Random rnd = new Random();
                int lvlupTop = 1, lvlupLeft = 1;
                int lvlup = 65;
                bool positionOk = false;

                // lvlup test
                //positionOk = true;
                //lvlupTop = borderHeight / 2;
                //lvlupLeft = borderWidth   / 2 + 10;
                //Console.SetCursorPosition(lvlupLeft, lvlupTop);
                //Console.Write((char)lvlup);

                // set foreground color for cursor
                Console.ForegroundColor = cursorColor;
                while (!end)
                {
                    //set lvlup position
                    while (!positionOk)
                    {
                        lvlupTop = rnd.Next(1, borderHeight - 2);
                        lvlupLeft = rnd.Next(1, borderWidth - 2);
                        positionOk = true;
                        for (int i = 0; i < cursorLength; i++)
                        {
                            if (cursorTop[i] == lvlupTop && cursorLeft[i] == lvlupLeft)
                            {
                                positionOk = false;
                            }
                        }
                        if (positionOk)  // print lvlup
                        {
                            Console.SetCursorPosition(lvlupLeft, lvlupTop);
                            Console.Write((char)lvlup);
                        }
                    }


                    // end or direction change
                    if (Console.KeyAvailable)
                    {
                        direction = Console.ReadKey(true).Key;

                        if (direction == ConsoleKey.UpArrow)
                        {
                            if (moveTop == 0)
                            {
                                moveLeft = 0;
                                moveTop = -1;
                            }
                        }
                        else if (direction == ConsoleKey.DownArrow)
                        {
                            if (moveTop == 0)
                            {
                                moveLeft = 0;
                                moveTop = 1;
                            }
                        }
                        else if (direction == ConsoleKey.LeftArrow)
                        {
                            if (moveLeft == 0)
                            {
                                moveLeft = -1;
                                moveTop = 0;
                            }
                        }
                        else if (direction == ConsoleKey.RightArrow)
                        {
                            if (moveLeft == 0)
                            {
                                moveLeft = 1;
                                moveTop = 0;
                            }
                        }
                        else if (direction == ConsoleKey.F10)
                        {
                            lvlupLeft = cursorLeft[0] + moveLeft;
                            lvlupTop = cursorTop[0] + moveTop;
                        }
                        else if (direction == ConsoleKey.Escape)
                        {
                            end = true;
                        }
                    }

                    // move snake
                    Console.BackgroundColor = cursorColor;
                    Console.SetCursorPosition(cursorLeft[0], cursorTop[0]);
                    Console.Write(cursor);
                    Console.BackgroundColor = backColor;
                    Console.SetCursorPosition(cursorLeft[cursorLength - 1], cursorTop[cursorLength - 1]);
                    Console.Write(cursor);


                    // check if next move hits lvlup
                    if (cursorLeft[0] + moveLeft == lvlupLeft && cursorTop[0] + moveTop == lvlupTop)
                    {
                        if (cursorLength + cursorLengthLvlup > cursorLengthMax)
                        {
                            Console.SetCursorPosition(borderWidth / 2 - 5, borderHeight / 2);
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.BackgroundColor = ConsoleColor.Blue;
                            Console.Write("YOU WIN!");
                            Console.SetCursorPosition(borderWidth / 2 - 10, borderHeight / 2 + 3);
                            end = true;
                        }
                        speed += speedLvlup;
                        for (int i = 0; i < cursorLengthLvlup; i++)
                        {
                            cursorLeft[cursorLength+i] = cursorLeft[cursorLength-1];
                            cursorTop[cursorLength+i] = cursorTop[cursorLength - 1];
                        }
                        cursorLength += cursorLengthLvlup;
                        positionOk = false;
                        lvlup++;
                        
                    }

                    // check if next move hits border then end
                    if (cursorLeft[0] + moveLeft == 0 || cursorLeft[0] + moveLeft == borderWidth - 1 || cursorTop[0] + moveTop == 0 || cursorTop[0] + moveTop == borderHeight - 1)
                    {
                        end = true;
                    }
                    // check if next move hits snake then end
                    for (int i = 1; i < cursorLength - 1; i++)
                    {
                        if (cursorLeft[0] + moveLeft == cursorLeft[i] && cursorTop[0] + moveTop == cursorTop[i])
                        {
                            end = true;
                        }
                    }

                    // wait aka movementspeed
                    System.Threading.Thread.Sleep(1000 / speed);

                    // switch cursorpositions back
                    for (int i = cursorLength; i > 0; i--)
                    {
                        cursorLeft[i] = cursorLeft[i - 1];
                        cursorTop[i] = cursorTop[i - 1];
                    }
                    // next cursorposition
                    cursorLeft[0] = cursorLeft[0] + moveLeft;
                    cursorTop[0] = cursorTop[0] + moveTop;

                }
                System.Threading.Thread.Sleep(500);
                Console.SetCursorPosition(borderWidth / 2 - 5, borderHeight / 2);
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Magenta;
                Console.Write("The END is NEAR!");
                Console.SetCursorPosition(borderWidth / 2 - 10, borderHeight / 2 + 3);

                System.Threading.Thread.Sleep(750);
            } while (Nochmal());
        }
        static bool Nochmal()
        {
            // nochmal?
            Console.ResetColor();
            Console.Write(" end (");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write("any");
            Console.ResetColor();
            Console.Write(") or once again (");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write("x");
            Console.ResetColor();
            Console.Write(")!");
            Console.ReadKey(true);
            if (Console.ReadKey(true).Key == ConsoleKey.X)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
