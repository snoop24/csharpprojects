﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadlineMitVorgabeUndPrüfung
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Test: {0}", getIntMinMax("Mach hinne und gib ein von 0 - 10! ", 0,10));
            Console.ReadKey();
        }

        static int getIntMinMax(
            string prompt,
            int xMin = -2147483648,
            int xMax = 2147483647
            )
        {
            Console.Write(prompt);
            string sInput = Console.ReadLine();
            int iInput;
            while (int.TryParse(sInput, out iInput)== false || xMin > iInput || iInput > xMax)
            {
                Console.WriteLine("Die Eingabe \"{0}\" entspricht nicht den Anforderungen!", sInput);
                Console.Write(prompt, xMin, xMax);
                sInput = Console.ReadLine();
            }
            return iInput;
        }
    }
}
