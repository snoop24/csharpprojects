﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                PrintBorders(40,20,ConsoleColor.DarkCyan, 20,0);


                
                Console.WriteLine("\n\n");
            } while (OnceAgain(ConsoleKey.X, ConsoleKey.A));
        }


        #region:utility
        static void PrintBorders(
            int borderWidth = 20,
            int borderHeight = 15,
            ConsoleColor borderColor = ConsoleColor.Blue,
            int anchorLeft = 0,
            int anchorTop = 0)
        {
            Console.BackgroundColor = borderColor;
            // print top and bottom
            for (int i = 0; i < borderWidth; i++)
            {
                Console.SetCursorPosition(anchorLeft+i, anchorTop);
                Console.Write(" ");
                Console.SetCursorPosition(anchorLeft+i, anchorTop+borderHeight-1);
                Console.Write(" ");
            }
            // print left and right
            for (int i = 1 ; i < borderHeight-1; i++)
            {
                Console.SetCursorPosition( anchorLeft,anchorTop+i);
                Console.Write(" ");
                Console.SetCursorPosition(anchorLeft+borderWidth-1, anchorTop+i);
                Console.Write(" ");
            }
            Console.ResetColor();
        }

        
        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }
        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }
        #endregion
    }
}
