﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeTestEtc
{
    class Program
    {
        static void Main(string[] args)
        {
            // ***** FIELD ******
            // declare field
            int boxHeight = 60;
            int boxWidth = 100;
            int offsetLeft = 0;
            int offsetTop = 0;
            ConsoleColor borderColor = ConsoleColor.Yellow;
            Console.CursorVisible = true;


            do
            {
                // ***** GAME *****
                bool gameOver = false;

                // ***** CURSOR ******
                // color
                ConsoleColor backGroundColor = Console.BackgroundColor;
                ConsoleColor foreGroundColor = Console.ForegroundColor;
                ConsoleColor cursorForegroundColor = ConsoleColor.Green;
                ConsoleColor cursorBackgroundColor = ConsoleColor.DarkGreen;
                ConsoleColor cursorHeadForegroundColor = ConsoleColor.DarkGreen;
                ConsoleColor cursorHeadBackgroundColor = ConsoleColor.Green;
                int cursorLength = 10;
                int cursorLengthMax = 110;
                // head, body, tail
                string cursorHead = "O";
                string cursorBody = "x";
                string cursorTail = "*";
                string[] cursor = new string[cursorLengthMax];
                int[] cursorTop = new int[cursorLengthMax];
                int[] cursorLeft = new int[cursorLengthMax];
                // set startposition
                for (int i = 0; i < cursorLength + 1; i++)
                {
                    cursorLeft[i] = boxWidth / 2 - i;
                    cursorTop[i] = boxHeight / 2;
                }
                // fill cursor
                cursor[0] = cursorHead;
                for (int i = 1; i < cursorLength - 2; i++)
                {
                    cursor[i] = cursorBody;
                }
                for (int i = cursorLength - 2; i < cursorLengthMax; i++)
                {
                    cursor[i] = cursorTail;
                }

                // ***** MOVEMENT *****
                int moveLeft = 1;
                int moveTop = 0;
                int speed = 20;
                ConsoleKey direction;

                // ***** LVLUP *****
                int lvlUp = 65;
                int speedLvlUp = 1;
                int cursorLengthLvlUp = 5;
                Random rnd = new Random();
                int lvlUpLeft = offsetLeft + 1;
                int lvlUpTop = offsetTop + 1;
                bool lvlUpPositionOk = false;
                ConsoleColor lvlUpForegroundColor = ConsoleColor.Red;
                ConsoleColor lvlUpBackgroundColor = backGroundColor;

                // set console size and wipe screen
                Console.SetWindowSize(boxWidth + offsetLeft * 2, boxHeight + offsetTop * 2);
                Console.Clear();
                Console.CursorVisible = false;

                // print field
                PrintBoxBorders(boxWidth, boxHeight, borderColor, offsetLeft, offsetTop);
                //reset cursor
                Console.SetCursorPosition(0, 0);
                Console.SetCursorPosition(offsetLeft, offsetTop);

                // print initial snake
                Console.ForegroundColor = cursorForegroundColor;
                Console.BackgroundColor = cursorBackgroundColor;
                for (int i = 0; i < cursorLength; i++)
                {
                    Console.SetCursorPosition(cursorLeft[i], cursorTop[i]);
                    Console.Write(cursor[i]);
                }


                while (!gameOver)
                {
                    // end or direction change
                    if (Console.KeyAvailable)
                    {
                        direction = Console.ReadKey(true).Key;

                        if (direction == ConsoleKey.A)
                        {
                            if (moveTop != 0)
                            {
                                moveLeft = -1;
                                moveTop = 0;
                            }
                            else if (moveLeft != 0)
                            {
                                moveLeft = 0;
                                moveTop = -1;
                            }
                        }else if (direction == ConsoleKey.Z || direction == ConsoleKey.Y)
                        {
                            if (moveTop != 0)
                            {
                                moveLeft = 1;
                                moveTop = 0;
                            }
                            else if (moveLeft != 0)
                            {
                                moveLeft = 0;
                                moveTop = 1;
                            }
                        }else if (direction == ConsoleKey.UpArrow)
                        {
                            if (moveTop == 0)
                            {
                                moveLeft = 0;
                                moveTop = -1;
                            }
                        }
                        else if (direction == ConsoleKey.DownArrow)
                        {
                            if (moveTop == 0)
                            {
                                moveLeft = 0;
                                moveTop = 1;
                            }
                        }
                        else if (direction == ConsoleKey.LeftArrow)
                        {
                            if (moveLeft == 0)
                            {
                                moveLeft = -1;
                                moveTop = 0;
                            }
                        }
                        else if (direction == ConsoleKey.RightArrow)
                        {
                            if (moveLeft == 0)
                            {
                                moveLeft = 1;
                                moveTop = 0;
                            }
                        }
                        else if (direction == ConsoleKey.F10)
                        {
                            lvlUpLeft = cursorLeft[0] + moveLeft;
                            lvlUpTop = cursorTop[0] + moveTop;
                        }
                        else if (direction == ConsoleKey.Escape)
                        {
                            gameOver = true;
                        }
                    }

                    // set lvlUp position
                    while (!lvlUpPositionOk)
                    {
                        lvlUpTop = rnd.Next(1, boxHeight - 2);
                        lvlUpLeft = rnd.Next(1, boxWidth - 2);
                        lvlUpPositionOk = true;
                        for (int i = 0; i < cursorLength; i++)
                        {
                            if (cursorTop[i] == lvlUpTop && cursorLeft[i] == lvlUpLeft)
                            {
                                lvlUpPositionOk = false;
                            }
                        }
                        if (lvlUpPositionOk)  // print lvlup
                        {
                            Console.BackgroundColor = lvlUpBackgroundColor;
                            Console.ForegroundColor = lvlUpForegroundColor;
                            Console.SetCursorPosition(lvlUpLeft, lvlUpTop);
                            Console.Write((char)lvlUp);
                        }
                    }

                    // check if next move hits border then end
                    if (cursorLeft[0] + moveLeft == 0 + offsetLeft || cursorLeft[0] + moveLeft == boxWidth - 1 + offsetLeft || cursorTop[0] + moveTop == 0 + offsetTop || cursorTop[0] + moveTop == boxHeight - 1 + offsetTop)
                    {
                        gameOver = true;
                    }
                    // check if next move hits snake then end
                    for (int i = 1; i < cursorLength - 2; i++)
                    {
                        if (cursorLeft[0] + moveLeft == cursorLeft[i] && cursorTop[0] + moveTop == cursorTop[i])
                        {
                            gameOver = true;
                        }
                    }

                    // check if next move hits lvlup
                    if (cursorLeft[0] + moveLeft == lvlUpLeft && cursorTop[0] + moveTop == lvlUpTop)
                    {
                        if (cursorLength + cursorLengthLvlUp > cursorLengthMax)
                        {
                            Console.SetCursorPosition(boxWidth / 2 - 5, boxHeight / 2);
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.BackgroundColor = ConsoleColor.Blue;
                            Console.Write("YOU WIN!");
                            Console.SetCursorPosition(boxWidth / 2 - 10, boxHeight / 2 + 3);
                            gameOver = true;
                        }
                        speed += speedLvlUp;
                        for (int i = 0; i < cursorLengthLvlUp; i++)
                        {
                            cursorLeft[cursorLength + i] = cursorLeft[cursorLength - 1];
                            cursorTop[cursorLength + i] = cursorTop[cursorLength - 1];
                        }
                        cursorLength += cursorLengthLvlUp;
                        lvlUpPositionOk = false;
                        lvlUp++;

                    }

                    // move snake
                    Console.ForegroundColor = cursorHeadForegroundColor;
                    Console.BackgroundColor = cursorHeadBackgroundColor;
                    Console.SetCursorPosition(cursorLeft[0], cursorTop[0]);
                    Console.Write(cursor[0]);
                    Console.ForegroundColor = cursorForegroundColor;
                    Console.BackgroundColor = cursorBackgroundColor;
                    Console.SetCursorPosition(cursorLeft[1], cursorTop[1]);
                    Console.Write(cursor[1]);
                    Console.SetCursorPosition(cursorLeft[cursorLength - 2], cursorTop[cursorLength - 2]);
                    Console.Write(cursor[cursorLength - 2]);
                    Console.BackgroundColor = backGroundColor;
                    Console.SetCursorPosition(cursorLeft[cursorLength - 1], cursorTop[cursorLength - 1]);
                    Console.Write(" ");

                    // switch cursorpositions back
                    for (int i = cursorLength + 1; i > 0; i--)
                    {
                        cursorLeft[i] = cursorLeft[i - 1];
                        cursorTop[i] = cursorTop[i - 1];
                    }
                    // next cursorposition
                    cursorLeft[0] = cursorLeft[0] + moveLeft;
                    cursorTop[0] = cursorTop[0] + moveTop;

                    // wait aka movementspeed
                    System.Threading.Thread.Sleep(1000 / speed);




                }
                System.Threading.Thread.Sleep(500);
                Console.SetCursorPosition(boxWidth / 2 - 5 + offsetLeft, boxHeight / 2 + offsetTop);
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Magenta;
                Console.Write("GAME OVER!");
                Console.SetCursorPosition(0, 0);

                Console.ResetColor();

            } while (OnceAgain(ConsoleKey.X, offsetLeft + 20, boxHeight + offsetTop * 2 - 20));

            Console.CursorVisible = true;
        }


        #region:utility

        #region:lineBoxPrinting

        static void PrintLineH(
            int lineLength,
            bool horizontal = true,
            ConsoleColor lineColor = ConsoleColor.Blue,
            int fromPointLeft = 0,
            int fromPointTop = 0)
        {
            Console.BackgroundColor = lineColor;
            // print top and bottom
            if (horizontal)
            {
                for (int i = 0; i < lineLength; i++)
                {
                    Console.SetCursorPosition(fromPointLeft + i, fromPointTop);
                    Console.Write(" ");
                }
            }
            else
            {
                for (int i = 0; i < lineLength; i++)
                {
                    Console.SetCursorPosition(fromPointLeft, fromPointTop + i);
                    Console.Write(" ");
                }
            }
            Console.ResetColor();
        }

        static void PrintBoxBorders(
            int borderWidth = 20,
            int borderHeight = 15,
            ConsoleColor borderColor = ConsoleColor.Blue,
            int anchorLeft = 0,
            int anchorTop = 0)
        {
            // top line
            PrintLineH(borderWidth, true, borderColor, anchorLeft, anchorTop);
            // bottom line
            PrintLineH(borderWidth, true, borderColor, anchorLeft, anchorTop + borderHeight - 1);
            // left line
            PrintLineH(borderHeight, false, borderColor, anchorLeft, anchorTop);
            // right line
            PrintLineH(borderHeight, false, borderColor, anchorLeft + borderWidth - 1, anchorTop);
        }
        #endregion

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion

        #endregion
    }
}
