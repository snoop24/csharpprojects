﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A06_b_ZahlenSystemWandler
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // declare 
                int victim, priest;
                string convert;

                // wipe screen and print title
                Console.Clear();
                Console.WriteLine("\n***** Whaalalalaaa-Whaalalalaaa *****\n");


                // read victim
                victim = GetInputInt("\n positive dezimale Ganzzahl: ", "\n   Ganzahl? Dezimal? Positiv? \n",0);

                // read priest
                int[] valid = new int[3] { 2, 8, 16 };
                priest = GetInputInt("\n in ( 2) Binär\n    ( 8) Oktal\n    (16) Hexadezimal: ", "\n   Korrekte Auswahl? \n", valid);

                // convert victim by priest
                convert = Pray(victim, priest);

                // output
                Console.WriteLine("\n\n   {0}\n\n     ist im {2}-System \n\n   {1}\n\n\n\n", victim, convert, priest == 2? "Binär": priest == 8? "Oktal" : "Hexadezimal" );


                // loop prompt 
                Console.Write("... (Esc) zum Beenden.");
            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }

        /// <summary>
        /// converts number to numeral system of factor
        /// </summary>
        /// <param name="number"></param>
        /// <param name="factor"></param>
        /// <returns>converted number as string</returns>
        private static string Pray(int number, int factor)
        {
            // temp buffer
            StringBuilder convert = new StringBuilder(); 
            
            // extend standard digits
            char[] digits = new char[16] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

            do 
            {
                // calculate and add new digit on the left
                convert.Insert(0, digits[number % factor]);

                // reduce number 
                number = number / factor;

            } while (number != 0); // until fully converted

            // return converted number as string
            return convert.ToString();
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">int array of valid values</param>
        /// <returns>integer value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int[] range)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>integer value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>integer value element of [min,max]</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min, int max)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min|| value > max);
            return value;
        }
        
        /// <summary>
        /// prints a string with a given backgroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for background</param>
        static void BackColoredOutput(string message, ConsoleColor background)
        {
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

    }
}
