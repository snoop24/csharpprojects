﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A02_01_KreditRatenZinsen
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen
                Console.Clear();

                // declare
                decimal rate, kredit, zinssatz;
                decimal monatsZins;
                int laufzeit;

                // declare for GetInput...
                string prompt, error;
                decimal minVal;

                minVal = 0;
                error = string.Format("   Zahl? > {0} ? \n", minVal);

                // read kredit
                prompt = " Bitte geben Sie die Höhe des Kredits ein: ";
                kredit = GetInputDecimal(prompt, error, minVal);

                // read laufzeit
                prompt = " Bitte geben Sie die Laufzeit in Monaten ein: ";
                laufzeit = GetInputInt(prompt, error, (int)minVal);

                // read zinssatz
                prompt = " Bitte geben Sie den Zinssatz in % ein: ";
                zinssatz = GetInputDecimal(prompt, error, minVal);

                // calc monatsZins
                monatsZins = MonatsZins(zinssatz);
                // calc rate
                rate = Rate(kredit, monatsZins, laufzeit);

                // output
                Console.Clear();
                Console.WriteLine("\t*** Kreditberechnung ***\n\n\n Höhe: {0}\n\n Laufzeit: {1}\n\n Zinssatz: {2}\n\n monatliche Rate: {3:f2}\n\n\n", kredit,laufzeit,zinssatz,rate);
            } while (OnceAgain());
        }

        /// <summary>
        /// returns the monthly rate based on amount, monthly interest rate and period in month of a loan
        /// </summary>
        /// <param name="kredit">loan amount</param>
        /// <param name="monatsZins">interest rate per month</param>
        /// <param name="laufzeit">period in months</param>
        /// <returns></returns>
        private static decimal Rate(decimal kredit, decimal monatsZins, int laufzeit)
        {
            return (kredit * (decimal)Math.Pow((double)monatsZins, laufzeit) * (monatsZins - 1) / ((decimal)Math.Pow((double)monatsZins, laufzeit) - 1));
        }

        /// <summary>
        /// returns the monthly interest rate based on the yearly interest rate
        /// </summary>
        /// <param name="zinssatz"></param>
        /// <returns>yearly interest rate</returns>
        static decimal MonatsZins(decimal zinssatz)
        {
            return (1 + (zinssatz / 1200));
        }

        /// <summary>
        /// prompts for/returns a decimal and gives error when not in range or no decimal
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns></returns>
        static decimal GetInputDecimal(string promptMessage, string errorMessage, decimal min)
        {
            string eMess = "";
            decimal value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!decimal.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns></returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
