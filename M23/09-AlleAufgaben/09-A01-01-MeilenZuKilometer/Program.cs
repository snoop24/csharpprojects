﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A01_01_MeilenZuKilometer
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen
                Console.Clear();

                // declare
                double distanceKm, distanceM, minVal;
                string promptMess, errorMess;

                // read miles
                promptMess = "Bitte Meilen eingeben ( > 0 ): ";
                errorMess = "   Zahl? > 0 ? \n";
                minVal = 0;
                distanceM = GetInputDouble(promptMess, errorMess, minVal);

                // convert miles to km
                distanceKm = ConvertMilesToKm(distanceM);

                // output
                Console.WriteLine("{0} Meilen entsprechen {1:F1} Kilometern.", distanceM, distanceKm);

            } while (OnceAgain());
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns></returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min)
        {
            string eMess = "";
            double value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }


        /// <summary>
        /// converts miles to kilometer
        /// </summary>
        /// <param name="miles"></param>
        /// <returns>miles input</returns>
        static double ConvertMilesToKm(double miles)
        {
            return (miles * 1.609344);
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
