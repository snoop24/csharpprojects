﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A01_02_MantelflaecheVolumenZylinder
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen
                Console.Clear();

                // declare
                double durchmesser, hoehe, mantel, volumen, minVal;
                string promptMess, errorMess;

                // read diameter
                promptMess = "Bitte Durchmesser eingeben ( > 0 ): ";
                errorMess = "   Zahl? > 0 ? \n";
                minVal = 0;
                durchmesser = GetInputDouble(promptMess, errorMess, minVal);

                // read height
                promptMess = "Bitte Höhe eingeben ( > 0 ): ";
                minVal = 0;
                hoehe = GetInputDouble(promptMess, errorMess, minVal);

                // calc lateral surface
                mantel = Mantel(durchmesser, hoehe);

                // calc volume
                volumen = Volume(durchmesser, hoehe);

                // output
                Console.WriteLine("Ein Zylinder mit dem Durchmesser {0} und der Höhe {1} hat eine Mantelfläche von {2:f2} und ein Volumen von {3:f3}.", durchmesser, hoehe, mantel, volumen);

            } while (OnceAgain());
        }

        /// <summary>
        /// calculates lateral surface of a zylinder from its' diameter and height 
        /// </summary>
        /// <param name="durchmesser">zylinder diameter</param>
        /// <param name="hoehe">zylinder height</param>
        /// <returns></returns>
        static double Mantel(double durchmesser, double hoehe)
        {
            return (Math.PI * durchmesser* hoehe);
        }

        /// <summary>
        /// calculates volume of a zylinder from its' diameter and height 
        /// </summary>
        /// <param name="durchmesser">zylinder diameter</param>
        /// <param name="hoehe">zylinder height</param>
        /// <returns></returns>
        static double Volume(double durchmesser, double hoehe)
        {
            return (Math.PI * durchmesser * durchmesser /4 * hoehe);
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns></returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min)
        {
            string eMess = "";
            double value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }



        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
