﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A02_02_SekundenZuTageStundenMinutenSekunden
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen
                Console.Clear();
                Console.WriteLine("\t*** Sekunden-In-Tage-Stunden-Minuten-Sekunden-Rechner ***");


                // declare
                int seconds;
                int[] tageStunMinSek = new int[4];

                // read seconds
                seconds = GetInputInt("\n Bitte geben Sie die Sekundenzahl ein: ", "   Zahl? > 0 ? ", 0);

                // calc 
                tageStunMinSek = SekundenZuTageStundenMinutenSekunden(seconds);

                // output
                Console.WriteLine("\n\n {0,10} Tag(e)\n\n {1,10} Stunde(n)\n\n {2,10} Minute(n)\n\n {3,10} Sekunde(n)\n\n\n\n", tageStunMinSek[0], tageStunMinSek[1], tageStunMinSek[2], tageStunMinSek[3]);


            } while (OnceAgain());
            
            Main(args);
        }


        /// <summary>
        /// returns an array with days, hours, minutes, seconds calculated from a time in seconds
        /// </summary>
        /// <param name="seconds">time in seconds</param>
        /// <returns>returns an array of days, hours, mins, secs</returns>
        private static int[] SekundenZuTageStundenMinutenSekunden(int seconds)
        {
            // declare
            int[] tageStunMinSek = new int[4];
            
            // days
            tageStunMinSek[0] = seconds / (60 * 60 * 24);
            seconds = seconds % (60 * 60 * 24);

            // hours
            tageStunMinSek[1] = seconds / (60 * 60 );
            seconds = seconds % (60 * 60 );

            // minutes
            tageStunMinSek[2] = seconds / 60 ;
            seconds = seconds % 60;

            // days
            tageStunMinSek[3] = seconds;

            return tageStunMinSek ;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>integer value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
