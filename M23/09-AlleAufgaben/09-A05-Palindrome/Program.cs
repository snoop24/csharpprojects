﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A05_Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen and print title
                Console.Clear();
                Console.WriteLine("***** PalindromChecker *****\n\n");

                // read palindrom candidate
                Console.Write(" Bitte den zu testenden Teil eingeben: \n\n    ");
                string palindromeCandidate = Console.ReadLine();

                // check for palindrome and result output
                Console.WriteLine("\n Es handelt sich bei \n\n   {0} \n\n {1}um ein Palindrom!", palindromeCandidate, IsPalindrom(palindromeCandidate) ? "" : "nicht ");


                // loop if not Escape
                Console.WriteLine("\n\n\n Press Escape to Exit!");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

        }

        /// <summary>
        /// leaves only upper case letters and numbers in a string
        /// </summary>
        /// <param name="removeFrom">string to be purged</param>
        /// <returns> purged string</returns>
        static string LeaveOnlyLettersAndNumbers(string removeFrom)
        {
            StringBuilder temp = new StringBuilder();
            removeFrom = removeFrom.ToUpper();
            for (int i = 0; i < removeFrom.Length; i++)
            {
                if ((47 < (int)removeFrom[i] && (int)removeFrom[i] < 58) || (64 < (int)removeFrom[i] && (int)removeFrom[i] < 88))
                {
                    temp.Append(removeFrom[i]);
                }
            }
            return temp.ToString();
        }
        /// <summary>
        /// checks if string is palindrome
        /// </summary>
        /// <param name="palindromCandidate"></param>
        /// <returns>bool if it is a palindrom</returns>
        static bool IsPalindrom(string palindromCandidate)
        {
            // declare return value
            bool isPalindrom = true;

            // remove blanks and letters and put in upper case
            palindromCandidate = LeaveOnlyLettersAndNumbers(palindromCandidate);

            // check if characters are mirrored
            for (int i = 0; i < palindromCandidate.Length; i++)
            {
                if (palindromCandidate[i] != palindromCandidate[palindromCandidate.Length-1-i])
                {
                    isPalindrom = false;
                    break;
                }
            }
            
            // return value
            return isPalindrom;
        }

    }
}
