﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A04_01_TauschVerschluesselung
{
    class Program
    {
        static void Main(string[] args)
        {

            do
            {
                // wipe screen and print title
                Console.Clear();
                Console.WriteLine("***** Verschluesselung durch Tausch etc. *****\n\n");

                // declare
                string inputString;
                string outputString;

                // read string
                Console.Write(" Bitte Zeichenkette eingeben:\n\n   :");
                inputString = Console.ReadLine();

                // encrypt
                outputString = TauschVerschluesselung(inputString);

                // output
                Console.WriteLine("\n   :{0}", outputString);

                // decrypt
                outputString = TauschEntschluesselung(outputString);

                // output
                Console.WriteLine("\n   :{0}\n\n\n", outputString);

            } while (OnceAgain());
           
        }
        /// <summary>
        /// encrypts a string very weak
        /// </summary>
        /// <param name="inputString">string to encrypt</param>
        /// <returns>encrypted string</returns>
        private static string TauschVerschluesselung(string inputString)
        {
            StringBuilder encryptedString = new StringBuilder();
            for (int i = 0; i < inputString.Length; i+=2)
            {
                encryptedString.Append(inputString[i]);
            }
            for (int i = 1; i < inputString.Length; i += 2)
            {
                encryptedString.Append(inputString[i]);
            }

            return encryptedString.ToString();
        }

        /// <summary>
        /// decrypts a string from TauschVerschluesselung
        /// </summary>
        /// <param name="inputString">string to decrypt</param>
        /// <returns>decrypted string</returns>
        private static string TauschEntschluesselung(string inputString)
        {
            StringBuilder decryptedString = new StringBuilder();
            for (int i = 0; i < (inputString.Length+1)/2; i++)
            {
                decryptedString.Append(inputString[i]);
                if (i*2+1 < inputString.Length) { decryptedString.Append(inputString[i+ (inputString.Length +1) / 2]); }
            }
            return decryptedString.ToString();
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}

