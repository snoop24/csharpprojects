﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A04_02_NameWertPaare
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen and print title
                Console.Clear();
                Console.WriteLine("***** \"Name=Wert\" - Paare *****");

                // declare
                int count;
                string inputString;

                // read count
                count = GetInputInt("\n Bitte geben Sie die Anzahl an Paaren ein: ", "   Ganzzahl > 0 ? ", 1);

                // declare array
                string[,] values = new string[2, count];

                // read values
                Console.WriteLine("\n Bitte geben Sie die {0}. Wertepaare einzeln ein (\"Name=Wert\")\n      ");
                for (int i = 0; i < count; i++)
                {
                    string[] temp = new string[2];
                    temp = GetInputValueArray(string.Format("   {0}: ",  i + 1), "   Eingabe entspricht nicht dem Schema? \n", '=');
                    values[0, i] = temp[0];
                    values[1, i] = temp[1];
                }

                // output
                OutputValues(values, '=');


            } while (OnceAgain());
        }

        /// <summary>
        /// prints a 2,n array on the console with a delimiter
        /// </summary>
        /// <param name="values">input array</param>
        /// <param name="delimiter">delimiter</param>
        private static void OutputValues(string[,] values, char delimiter)
        {
            Console.WriteLine("\n\n");
            for (int i = 0; i < values.GetLength(1); i++)
            {
                Console.Write("   {3}: {0,30} {2} {1,-30}\n",values[0,i], values [1,i],delimiter,i+1);
            }
            Console.WriteLine("\n\n");
        }

        /// <summary>
        /// reads input and calls a check for correct input
        /// </summary>
        /// <param name="promptMessage">user prompt message</param>
        /// <param name="errorMessage">user message on bad input</param>
        /// <param name="delimiter">delimits the values in the string</param>
        /// <returns>string array of arraySize</returns>
        private static string[] GetInputValueArray(string promptMessage, string errorMessage, char delimiter)
        {
            string eMess = "";
            string value;
            string[] temp = new string[2];
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
                value = Console.ReadLine();
            } while (!EvaluateValues(value, delimiter, out temp));
            return temp;
        }

        /// <summary>
        /// splits up the string and validates if conditions are met
        /// </summary>
        /// <param name="value">string to split</param>
        /// <param name="delimiter">split by</param>
        /// <param name="temp">return value</param>
        /// <returns>string array</returns>
        private static bool EvaluateValues(string value, char delimiter, out string[] temp)
        {
            bool valid = true;
            temp = value.Split(delimiter);
            if (temp.Length != 2) { valid = false; }
            if (temp[0].Length == 0) { valid = false; }
            return valid;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>integer value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
