﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A03_02_Dazwischen
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen and print title
                Console.Clear();
                Console.WriteLine("\n *** Dazwischen ***\n");

                // declare
                int uGrenze, oGrenze, inGrenze;

                // promt/error messages and minimal value
                string prompt = "\n Untergrenze A = ";
                string error = "\n   Zahl ? \n";
                double minVal = 0;

                // read uGrenze
                uGrenze = GetInputInt(prompt, error);

                // new prompt message
                prompt = "\n Obergrenze B = ";
                error = "\n   Zahl ? größer Untergrenze ? \n";
                // read y
                oGrenze = GetInputInt(prompt, error, uGrenze);

                // new prompt message
                prompt = "\n Prüfzahl X = ";
                error = "\n   Zahl ? \n";
                // read inGrenze
                inGrenze = GetInputInt(prompt, error);

                // check if between and output
                Console.WriteLine("\n\n {0} liegt {3}zwischen {1} und {2}\n\n\n", inGrenze, uGrenze, oGrenze, Dazwischen(inGrenze, uGrenze, oGrenze) ? "" : "nicht ");

            } while (OnceAgain());
        }

        /// <summary>
        /// returns true if X is between (with equal) A and B
        /// </summary>
        /// <param name="X">value to check</param>
        /// <param name="A">lower limit</param>
        /// <param name="B">upper limit</param>
        /// <returns>bool (true/false)</returns>
        private static bool Dazwischen(int inGrenze, int uGrenze, int oGrenze)
        {
            if (uGrenze <= inGrenze && inGrenze <= oGrenze) { return true; }
            else { return false; }
        }

        /// <summary>
        /// prompts for/returns a int and gives error if no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns></returns>
        static int GetInputInt(string promptMessage, string errorMessage)
        {
            string eMess = "";
            int value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns></returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
