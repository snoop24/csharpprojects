﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_A03_01_AGHMittel
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                // wipe screen and print title
                Console.Clear();
                Console.WriteLine("\n *** Mittel-Berechnung ***\n");
                // declare
                double arithMittel, geoMittel, harmMittel;

                // promt/error messages and minimal value
                string prompt = "\n 1. positive Zahl: ";
                string error = "\n   Positive Zahl ? \n";
                double minVal = 0;

                // read x
                double x = GetInputDouble(prompt, error, minVal);

                // new prompt message
                prompt = "\n 2. positive Zahl: ";
                // read y
                double y = GetInputDouble(prompt, error, minVal);

                // calc stuff
                arithMittel = ArithMittel(x, y);
                geoMittel = GeoMittel(x, y);
                harmMittel = HarmMittel(x, y);

                // output

                Console.WriteLine("\n\n Die Zahlen {0} und {1} haben\n\n {2,7:f2} als arithmetisches Mittel\n\n {3,7:f2} als geometrisches Mittel\n\n {4,7:f2} als harmonisches Mittel\n\n\n",x,y,arithMittel,geoMittel,harmMittel );
            } while (OnceAgain());

        }

        private static double HarmMittel(double x, double y)
        {
            return (2 * x * y / (x + y));
        }

        private static double GeoMittel(double x, double y)
        {
            return (Math.Pow(x * y, 0.5));
        }

        private static double ArithMittel(double x, double y)
        {
            return ((x + y) / 2);
        }



        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns></returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min)
        {
            string eMess = "";
            double value;
            do
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.Write(eMess);
                Console.ResetColor();
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
