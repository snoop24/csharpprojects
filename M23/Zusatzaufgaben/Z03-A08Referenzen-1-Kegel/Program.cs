﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z03_A08Referenzen_1_Kegel
{
    class Program
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                double radius, height;
                double volume, lateral, surface;
                
                // wipe screen and print title
                TitleOutput("Berechnungen am Kegel");

                // get input
                string prompt, error = "   Zahl? Positiv? ";
                double minVal = 0;

                // read radius
                prompt = "\n Radius: ";
                radius = GetInputDouble(prompt, error, minVal);

                // read height
                prompt = "\n Höhe: ";
                height = GetInputDouble(prompt, error, minVal);

                // calc
                ConeCalcs(radius, height, out volume, out lateral, out surface);

                // output
                ForeColoredOutput(string.Format(
                    "\n\n " +
                    " Ein Kegel mit \n\n" +
                    "   Radius: {0,5}\n" +
                    "   Höhe:   {1,5}\n\n" +
                    " hat folgende Größen\n\n" +
                    "  Volumen:      {2,7:f2}\n" +
                    "  Mantelfläche: {3,7:f2}\n" +
                    "  Oberfläche:   {4,7:f2}\n"
                    ,radius, height, volume, lateral, surface));
            
                // loop prompt
                ForeColoredOutput("\n\n\n... (ESC) zum Beenden.", ConsoleColor.DarkRed);
            } while (Console.ReadKey(true).Key!= ConsoleKey.Escape);
        }

        private static bool ConeCalcs(double radius, double height, out double volume, out double lateral, out double surface)
        {
            // help calc
            double m = Math.Sqrt(radius * radius + height * height);

            // main calc
            volume = Math.PI * radius * radius * height / 3;
            lateral = Math.PI * radius * m;
            surface = Math.PI * radius * (radius + m);

            //return
            return height > 0 ? radius > 0 ? true: false :false ;
        }

        #region:Tools

        #region:GeTinput

        #region:int
        /// <summary>
        /// prompts for/returns a int and gives error when no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>int value </returns>
        static int GetInputInt(string promptMessage, string errorMessage)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>int value greater or equal "min"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>int value element of [min,max]</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int min, int max)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a int and gives error when not in range or no int
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">int array of valid values</param>
        /// <returns>int value element range"</returns>
        static int GetInputInt(string promptMessage, string errorMessage, int[] range)
        {
            string eMess = "";
            int value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!int.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #region:double
        /// <summary>
        /// prompts for/returns a double and gives error when no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <returns>double value </returns>
        static double GetInputDouble(string promptMessage, string errorMessage)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value));
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <returns>double value greater or equal "min"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="min">minimal value of range</param>
        /// <param name="max">maximal value of range</param>
        /// <returns>double value element of [min,max]</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double min, double max)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || value < min || value > max);
            return value;
        }

        /// <summary>
        /// prompts for/returns a double and gives error when not in range or no double
        /// </summary>
        /// <param name="promptMessage">prompt message text</param>
        /// <param name="errorMessage">error message text</param>
        /// <param name="range">double array of valid values</param>
        /// <returns>double value element range"</returns>
        static double GetInputDouble(string promptMessage, string errorMessage, double[] range)
        {
            string eMess = "";
            double value;
            do
            {
                BackColoredOutput(eMess, ConsoleColor.Red);
                Console.Write(promptMessage);
                eMess = errorMessage;
            } while (!double.TryParse(Console.ReadLine(), out value) || !range.Contains(value));
            return value;
        }
        #endregion

        #endregion

        #region:Output

        /// <summary>
        /// clears console and prints title with leading and trailing ****** and newline before and after 
        /// </summary>
        /// <param name="title">string to print</param>
        static void TitleOutput(string title)
        {
            Console.Clear();
            Console.Write("\n***** {0} *****\n\n", title);
        }

        /// <summary>
        /// prints a string with a given backgroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for background</param>
        static void BackColoredOutput(string message, ConsoleColor background = ConsoleColor.Red)
        {
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given foregroundcolor
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ForeColoredOutput(string message, ConsoleColor foreground = ConsoleColor.Cyan)
        {
            Console.ForegroundColor = foreground;
            Console.Write(message);
            Console.ResetColor();
        }

        /// <summary>
        /// prints a string with a given colors
        /// </summary>
        /// <param name="message">string to print</param>
        /// <param name="background">color for foreground</param>
        static void ColoredOutput(string message, ConsoleColor foreground = ConsoleColor.White, ConsoleColor background = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        #endregion

        #endregion
    }
}
