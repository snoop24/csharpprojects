﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            axAcroPDF1.LoadFile(@"E:\test.pdf");
            axAcroPDF1.setViewRect(100f, 100f, 100f, 100f);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            axAcroPDF1.Select();
            SendKeys.SendWait("+^{SUBTRACT}");
            axAcroPDF1.Select();
            SendKeys.SendWait("+^{SUBTRACT}");
        }
    }
}
