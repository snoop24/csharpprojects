﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitelMenue
{
    class Program
    {

        static void Main(string[] args)
        {
            bool endTM = false; // end menu?
            bool validTMO = true; // input error?
            string menuPrompt; // menu text
            string menuOption = ""; // selected option

            // main menu loop
            while (!endTM)
            {
                // set TM menu via prompt
                menuPrompt = " ******  Meine Musiktitel  ******\n\n" +
                         "  N  =  NeuenEintrag hinzufügen\n" +
                         "  L  =  Eintrag löschen\n" +
                         "  F  =  Titel finden\n" +
                         "  A  =  Alle Einträge anzeigen\n" +
                         "  B  =  Programm beenden\n\n" +
                         " Ihre Wahl: ";

                // print message and menu, get input
                Console.Clear();
                if (!validTMO) // print error message if needed
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("\n Ihre Eingabe \"{0}\" war ungültig!\n", menuOption);
                    Console.ResetColor();

                    validTMO = true; // reset trigger
                }
                Console.Write(menuPrompt);
                menuOption = Console.ReadLine().ToUpper();
                Console.Clear();


                // do selected option
                switch (menuOption)
                {
                    case "A": // Alle Einträge anzeigen

                        //do your stuff
                        Console.WriteLine("\n Alle Einträge anzeigen");
                        Console.ReadKey();

                        break;

                    case "F": // Titel Finden

                        //do your stuff
                        Console.WriteLine("\n Titel Finden");
                        Console.ReadKey();

                        break;

                    case "L": // Eintrag Löschen

                        //do your stuff
                        Console.WriteLine("\n Eintrag Löschen");
                        Console.ReadKey();

                        break;

                    case "N": // Neuen Eintrag hinzufügen

                        //do your stuff
                        Console.WriteLine("\n Neuen Eintrag Hinzufügen");
                        Console.ReadKey();

                        break;

                    case "B": // Programm Beenden
                        endTM = true;
                        break;

                    default:
                        validTMO = false; // trigger error message on menu print
                        break;
                }

            }

            Console.WriteLine("\n Auf Wiedersehen!\n");
            Console.ReadKey();
        }
    }
}
