﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A5_FormatierungenConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // Aufgabenteile a-c)
            DateTime dtTest;
            dtTest = DateTime.Now;

            Console.WriteLine("a) Die aktuelle Stunde der Uhrzeit (DateTime.Now):\n");
            Console.WriteLine(" {0:HH}\n",dtTest);

            Console.WriteLine("b) Das aktuelle Jahr, einmal zweistellig und einmal vierstellig:\n");
            Console.WriteLine(" {0:yy}\t {0:yyyy}\n", dtTest);

            Console.WriteLine("c) Einen Satz, der wie folgt aussieht:\n \"Heute ist Freitag der 13. September 2017.\"\nDie Informationen \"Freitag\",\"September\" und \"2017\" sollen sie ebenfalls aus der DateTime.Now ermitteln.\n");
            Console.WriteLine(" Heute ist {0:dddd}, der {0:dd}.{0:MMMM} {0:yyyy}.\n", dtTest);

            // Aufgabenteil d)
            string vorname1="Gerwin", nachname1="Schnittjer", vorname2="Andreas", nachname2="Golonko";
            string rb = "rechtsbündig:", lb = "linksbündig:";
            Console.WriteLine("d)");
            Console.WriteLine("{0,20}|{1,20}| - |{2,20}",rb,vorname1,nachname1);
            Console.WriteLine("{0,20}|{1,20}| - |{2,20}", rb, vorname2, nachname2);
            Console.WriteLine("{0,-20}|{1,-20}| - |{2,-20}", lb, vorname1, nachname1);
            Console.WriteLine("{0,-20}|{1,-20}| - |{2,-20}", lb, vorname2, nachname2);

            Console.WriteLine();

            // Aufgabenteil e)
            Console.BackgroundColor = ConsoleColor.White;
            Console.Write("\"");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("Blau, ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Rot, ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Gelb, ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Grün, ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("Grau, ");
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("Schwarz");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("\"");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();

        }
    }
}
