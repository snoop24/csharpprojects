﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Zusatz_A10_1_Turmhoehe
{
    class Program
    {
        static void Main(string[] args)
        {            
            double test,schattenLaenge, winkelSpitze, hoeheTurm;

            // get schattenlänge and test if in range
            Console.Write("Schattenlänge in m: ");
            string testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0 )
            {
                Console.Write("Schattenlänge \"{0}\" ungültig. Bitte wiederholen: ", testInput);
                testInput = Console.ReadLine();
            }
            schattenLaenge = test;

            // get winkel and test if in range
            Console.Write("Winkel zur Turmspitze in Grad: ");
            testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0 || test >=180)
            {
                Console.Write("Winkel zur Turmspitze \"{0}\" ungültig. Bitte wiederholen: ", testInput);
                testInput = Console.ReadLine();
            }
            winkelSpitze = test;

            // calculate and convert degrees to radians
            hoeheTurm = schattenLaenge * Math.Tan(winkelSpitze * Math.PI / 180);


            // output
            Console.WriteLine("Der Turm ist {0} m hoch.", hoeheTurm );
            Console.ReadKey();
        }
    }
}
