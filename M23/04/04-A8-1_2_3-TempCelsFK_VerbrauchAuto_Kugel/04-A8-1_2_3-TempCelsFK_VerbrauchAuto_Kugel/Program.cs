﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A8_1_2_3_TempCelsFK_VerbrauchAuto_Kugel
{
    class Program
    {
        static void Main(string[] args)
        {
            TempCFK();
            VerbrauchAuto();
            KugelVolOberfl();
        }
        static void TempCFK()
        {
            Console.Clear();
            double tC, tF, tK, test;
            // get temperature in degrees Celsius and test if in range
            Console.Write("Temperatur in ° C: ");
            string testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false )
            {
                Console.Write("Temperatur \"{0}\" ungültig. Bitte wiederholen: ", testInput);
                testInput = Console.ReadLine();
            }
            tC = test;

            // calculations
            tK = tC + 273.15;
            tF = tC * 9 / 5 + 32;

            Console.Write("\n {0,10:0.0} ° Celsius\n\n {1,10:0.0} ° Fahrenheit\n\n {2,10:0.0} ° Kelvin", tC, tF, tK);

            Console.ReadKey();  

        }
        static void VerbrauchAuto()
        {
            Console.Clear();
            double liter, strecke, verbrauch, test;
            // get distance and test if valid
            Console.Write("Strecke in km: ");
            string testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("Strecke \"{0}\" km ungültig. Bitte wiederholen: ", testInput);
                testInput = Console.ReadLine();
            }
            strecke = test;

            // get liters and test if valid
            Console.Write("Verbrauch in l: ");
            testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("Verbrauch \"{0}\" l ungültig. Bitte wiederholen: ", testInput);
                testInput = Console.ReadLine();
            }
            liter = test;

            // calculations

            verbrauch = liter / strecke * 100;

            Console.Write(" {0} l auf {1} km ergibt einen Durchschnittsverbrauch von {2:0.00} l/100km", liter, strecke, verbrauch);

            Console.ReadKey();
        }
        static void KugelVolOberfl()
        {
            Console.Clear();
            double durchm, volumen, oberfl, test;
            // get durchmesser and test if in range
            Console.Write("Kugeldurchmesser: ");
            string testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("Kugeldurchmesser \"{0}\" ungültig. Bitte wiederholen: ", testInput);
                testInput = Console.ReadLine();
            }
            durchm = test;

            // calculations
            volumen = Math.PI * Math.Pow(durchm, 3) / 6;
            oberfl = Math.PI * Math.Pow(durchm, 2);

            Console.Clear();
            Console.Write("\n Kugel\n\n Durchmesser: {0,10:0.00}\n\n Oberfläche:  {1,10:0.00}\n\n Volumen:     {2,10:0.00}",durchm, oberfl, volumen);

            Console.ReadKey();
        }
    }
}
