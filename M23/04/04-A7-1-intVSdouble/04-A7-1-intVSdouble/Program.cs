﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A7_1_intVSdouble
{
    class Program
    {
        static void Main(string[] args)
        {
            int iA, iB, iC, iD, iS, iTest;
            
            // Ausgabe vor Einlesen
            Console.WriteLine("\n Bitte geben Sie nacheinander vier ganze Zahlen ein.");

            // Einlesen
            // prompt für Zahl1
            string prompt = "Zahl1";
            Console.Write(" {0}:", prompt);
            string testInput = Console.ReadLine();
            while (int.TryParse(testInput, out iTest) == false || iTest < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            iA = iTest;
            // prompt für Zahl2
            prompt = "Zahl2";
            Console.Write(" {0}:", prompt);
            testInput = Console.ReadLine();
            while (int.TryParse(testInput, out iTest) == false || iTest < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            iB = iTest;
            // prompt für Zahl3
            prompt = "Zahl3";
            Console.Write(" {0}:", prompt);
            testInput = Console.ReadLine();
            while (int.TryParse(testInput, out iTest) == false || iTest < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            iC = iTest;
            // prompt für Zahl4
            prompt = "Zahl4";
            Console.Write(" {0}:", prompt);
            testInput = Console.ReadLine();
            while (int.TryParse(testInput, out iTest) == false || iTest < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            iD = iTest;
            
            // berechnung
            iS = iA + iB + iC + iD;

            // ausgabe
            Console.WriteLine();
            Console.WriteLine("Die Summe der Ganzzahlen ist {0}.", iS);

            Console.ReadKey();

            // jetzt die double Variante

            double dA, dB, dC, dD, dS, dTest;

            // Ausgabe vor Einlesen
            Console.WriteLine("\n Bitte geben Sie nacheinander vier rationale Zahlen ein.");

            // Einlesen
            // prompt für Zahl1
            prompt = "Zahl1";
            Console.Write(" {0}:", prompt);
            testInput = Console.ReadLine();
            while (double.TryParse(testInput, out dTest) == false || dTest < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            dA = dTest;
            // prompt für Zahl2
            prompt = "Zahl2";
            Console.Write(" {0}:", prompt);
            testInput = Console.ReadLine();
            while (double.TryParse(testInput, out dTest) == false || dTest < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            dB = dTest;
            // prompt für Zahl3
            prompt = "Zahl3";
            Console.Write(" {0}:", prompt);
            testInput = Console.ReadLine();
            while (double.TryParse(testInput, out dTest) == false || dTest < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            dC = dTest;
            // prompt für Zahl4
            prompt = "Zahl4";
            Console.Write(" {0}:", prompt);
            testInput = Console.ReadLine();
            while (double.TryParse(testInput, out dTest) == false || dTest < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            dD = dTest;

            // berechnung
            dS = dA + dB + dC + dD;

            // ausgabe
            Console.WriteLine();
            Console.WriteLine("Die Summe der rationalen Zahlen ist {0}.", dS);

            Console.ReadKey();

        }
    }
}
