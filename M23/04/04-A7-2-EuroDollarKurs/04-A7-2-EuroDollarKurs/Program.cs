﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A7_2_EuroDollarKurs
{
    class Program
    {
        static void Main(string[] args)
        {
            double euro, dollar, kurs, test;
            Random newRnd = new Random();

            // kurs variieren
            kurs = 1.1933 + ( newRnd.Next(-1000, 1000)/10000.00);


            // eingabe abfrage
            Console.Write("\n Bitte geben Sie den Eurobetrag zur Umrechnung an:\n ");
            string testInput = Console.ReadLine();

            //abfrage wiederholen falls ungültig
            while (double.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("Eurobetrag \"{0}\" ungültig. Bitte wiederholen:\n ", testInput);
                testInput = Console.ReadLine();
            }
            euro = test;

            dollar = euro * kurs;

            Console.WriteLine(" Bei einem Kurs von {0} \n ist der Dollarbetrag {1}.",kurs ,dollar );

            Console.ReadKey();

        }
    }
}
