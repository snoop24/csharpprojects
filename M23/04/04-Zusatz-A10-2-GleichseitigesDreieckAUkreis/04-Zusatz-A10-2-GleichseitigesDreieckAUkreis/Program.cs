﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Zusatz_A10_2_GleichseitigesDreieckAUkreis
{
    class Program
    {
        static void Main(string[] args)
        {
            double test, seitenlänge, umkreisRadius, flächeninhalt;

            // get schattenlänge and test if in range
            Console.Write("Seitenlänge in cm: ");
            string testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("Seitenlänge \"{0}\" ungültig. Bitte wiederholen: ", testInput);
                testInput = Console.ReadLine();
            }
            seitenlänge = test;


            // calculate
            flächeninhalt = seitenlänge * seitenlänge / 4 * Math.Sqrt(3);
            umkreisRadius = seitenlänge / Math.Sqrt(3);


            // output
            Console.WriteLine("Die Fläche des Dreiecks ist {0} cm² und der Umkreisradius {1} cm.", Math.Round( flächeninhalt,2), Math.Round( umkreisRadius,2));
            Console.ReadKey();
        }
    }
}
