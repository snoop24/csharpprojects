﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A9_KinderGebUeEi_SpuleWiderstand
{
    class Program
    {
        static void Main(string[] args)
        {
            //KinderGebUei();
            //SpuleWiderstand();
        }
        static void KinderGebUei()
        {
            int anzKin, anzUei, anzUK, anzUeber, test;

            // get Anzahl Kinder and test if in range
            string prompt = ("\n Anzahl Kinder: ");
            Console.Clear();
            Console.Write(prompt);
            string testInput = Console.ReadLine();
            while (int.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            anzKin = test;

            // get Menge Ü-Eier and test if in range
            prompt = (" Menge Ü-Eier: ");
            Console.Write(prompt);
            testInput = Console.ReadLine();
            while (int.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            anzUei = test;

            //calc Ü-Eier pro Kind und überzählige
            anzUeber = anzUei % anzKin;
            anzUK = anzUei / anzKin;

            // output
            Console.WriteLine(" Jedes Kind bekommt {0} Ü-Eier und es bleiben {1} Ü-Eier übrig.", anzUK, anzUeber);

            Console.ReadKey();
        }
        static void SpuleWiderstand()
        {
            double R, L, f, Z, test;
            Console.Clear();
            Console.WriteLine("\n\tSpule\n");

            // get ohmscher widerstand and test if in range
            string prompt = (" Ohmscher Widerstand: ");
            Console.Write(prompt);
            string testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            R = test;

            // get induktivität and test if in range
            prompt = (" Induktivität: ");
            Console.Write(prompt);
            testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            L = test;

            // get frequenz and test if in range
            prompt = (" Frequenz: ");
            Console.Write(prompt);
            testInput = Console.ReadLine();
            while (double.TryParse(testInput, out test) == false || test < 0)
            {
                Console.Write("{1} \"{0}\" ungültig. Bitte wiederholen: ", testInput, prompt);
                testInput = Console.ReadLine();
            }
            f = test;

            //calc Z der spule
            Z = Math.Sqrt( R*R + Math.Pow( 2 * Math.PI * f * L ,2));

            // output
            Console.Clear();
            Console.WriteLine("\n \t Spule:\n\n Widerstand:         {0,10:0.0} Ohm\n Induktivität:       {1,10:0.0} Henry\n Frequenz:           {2,10:0.0} Hz\n Widerstand(gesamt): {3,10:0.0} Ohm", R, L, f, Z);

            Console.ReadKey();
        }
    }
}
