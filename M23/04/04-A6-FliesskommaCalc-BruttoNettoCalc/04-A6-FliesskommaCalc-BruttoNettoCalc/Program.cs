﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_A6_FliesskommaCalc_BruttoNettoCalc
{
    class Program
    {
        static void Main(string[] args)
        {

            // fliesskommacalc
            double d1, d2, sum12, dif12, dif21, prod12, quot12,quot21;
            Random newRnd = new Random();
            d1 = newRnd.Next(0, 1000);
            d2 = newRnd.Next(0, 1000);
            sum12 = d1 + d2;
            dif12 = d1 - d2;
            dif21 = d2 - d1;
            prod12 = d1 * d2;
            quot12 = d1 / d2;
            quot21 = d2 / d1;

            Console.WriteLine("\n Für die Zahlen a={0} und b={1}:\n\n a+b={2}\n\n a-b={3}\tb-a={4}\n\n a*b={5}\n\n a/b={6}\t b/a={7}\n",d1,d2,sum12,dif12,dif21,prod12, Math.Round(quot12,4),Math.Round(quot21,4));

            //Console.ReadKey();
            //Console.Clear();
            Console.WriteLine("\n");

            // BruttoNettoCalc
            double brutto, netto, mwst, mwstSatz=0.19;
            brutto = newRnd.Next(10000, 100000);
            netto = brutto / (1 + mwstSatz);
            mwst = brutto * (1- 1 / (1 + mwstSatz));
            Console.WriteLine("\n Bruttowert:    {0,10:0.00}\n MwStSatz:   {1,10}%\n MwSt:          {2,10:0.00}\n Nettobetrag:   {3,10:0.00}\n", brutto,  mwstSatz * 100, mwst,netto);
        }
    }
}
