﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A09_SucheArrayHalbieren
{
    class SucheArrayHalbieren
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // wipe screen 
                Console.Clear();

                // declare
                int anzahl = 20;
                int position=0;
                int sucheZahl;
                int minPos = 0;
                int maxPos = anzahl - 1;
                int durchlaeufe = 0;

                int[] array = new int[anzahl];

                Random rnd = new Random();

                bool zahlGefunden = false;


                // fill array
                for (int i = 0; i < anzahl; i++)
                {
                    array[i] = rnd.Next(0, 100);
                }

                // sort array
                Array.Sort(array);

                // output array for testing
                Console.WriteLine("\n TESTAUSGABE:");
                Console.WriteLine(" {0}\n", string.Join(" ", array));

                //read suche Zahl
                Console.WriteLine(" *** Array halbieren zur Zahlen Suche ***" );
                string errMsg, inpErrMsg, inpPrompt;
                errMsg = "";
                inpErrMsg = "\n   Zahl? Im Intervall?\n";
                inpPrompt = string.Format("\n   Zahl (0-99): ");
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out sucheZahl) || sucheZahl < 0 || sucheZahl > 99);

                // loop until zahl gefunden oder limit erreicht
                while (!(zahlGefunden || minPos>maxPos))
                {
                    position = (maxPos - minPos) / 2 + minPos;
                    if (array[position] == sucheZahl)
                    {
                        zahlGefunden = true;
                    }
                    else if (array[position] < sucheZahl)
                    {
                        minPos = position + 1;
                    }
                    else
                    {
                        maxPos = position - 1;
                    }
                    durchlaeufe++;
                }

                // get first position
                if (position > 0 && array[position] == array[position - 1]){ position--; }

                // output array 
                Console.WriteLine("\n\n {0}\n", string.Join(" ", array));

                // output ergebnis
                Console.WriteLine("\n {1} {2}: Die Zahl {0} wurde {3} gefunden.\n\n\n\n",
                    sucheZahl, durchlaeufe,
                    durchlaeufe==1 ? "Durchlauf":"Durchläufen",
                    zahlGefunden ? ("an Position "+(position+1)):"nicht");


            } while (OnceAgain());


        }
        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write("(again), ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write("(end)!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
