﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07b_A__SelectionSort
{
    class SelectionSort
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                int anzahl = 15;
                int position;
                int temp;

                int[] array = new int[anzahl];

                Random rnd = new Random();

                // fill array
                for (int i = 0; i < anzahl; i++)
                {
                    array[i] = rnd.Next(0, 100);
                }

                // wipe screen, print title
                Console.Clear();
                Console.WriteLine("\n\n *** Selection Sort ***\n");

                // output unsorted array
                Console.WriteLine("\n   {0}\n", string.Join(" | ", array));

                // sort array with selection sort

                // select first to second last postion
                for (int i = 0; i < anzahl-1; i++)
                {
                    // set init minimum
                    position = i; 
                    // find minimum from next to last
                    for (int j = i+1; j < anzahl; j++)
                    {
                        // if new minimum set new position
                        if (array[position] > array[j]) { position = j; }
                    }
                    // if minimum is not initial position do switch both
                    if (i != position)
                    {
                        temp = array[i];
                        array[i] = array[position];
                        array[position] = temp;
                    }
                }

                // output sorted array
                Console.WriteLine("\n   {0}\n\n", string.Join(" | ", array));


            } while (OnceAgain());

        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
