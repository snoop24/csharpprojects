﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A07_PasswortGenerator
{
    class Passwortgenerator
    {
        static void Main(string[] args)
        {
            do
            {
                // PAP von Dittrich umsetzen Start
                
                // 1
                Random zufall = new Random();
                
                // 2
                char[] arrayPw = new char[8];

                // 5a
                int countSz;
                int countGross;
                int countZahl;

                // 3
                do
                {
                    // 5b
                    countSz = 0;
                    countGross = 0;
                    countZahl = 0;

                    // 4 , 6 , 16
                    for (int i = 0; i < arrayPw.Length; i++)
                    {
                        // 7
                        int temp = zufall.Next(33, 127);
                        if (temp > 32 && temp < 47)
                        {
                            arrayPw[i] = (char)temp;
                            countSz++;
                        }
                        else if (temp >=48 && temp <=57)
                        {
                            arrayPw[i] = (char)temp;
                            countZahl++;
                        }
                        else if (temp >= 65 && temp <= 90)
                        {
                            arrayPw[i] = (char)temp;
                            countGross++;
                        }
                        else if (temp >= 97 && temp <=122)
                        {
                            arrayPw[i] = (char)temp;
                        }
                    }

                } while (!(countGross >=1 && countZahl ==2 && countSz ==1));

                Console.Clear();
                Console.WriteLine("\n\n *** PasswortGenerator ***\n\n {0}\n\n\n", string.Join("",arrayPw));

                // PAP umsetzen Ende
            } while (OnceAgain());
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
