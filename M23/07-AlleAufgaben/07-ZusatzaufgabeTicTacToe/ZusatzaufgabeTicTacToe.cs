﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_ZusatzaufgabeTicTacToe
{
    class ZusatzaufgabeTicTacToe
    {
        static void Main(string[] args)
        {
            #region:setup
            // general variables
            bool gameEnd;
            bool checkAfterMove = false;

            byte[] offset = new byte[2] { 1, 1 };



            int sizeT = 11 +6; // odd 11+
            int sizeL = 18 + 8;// even 18+
            int bottomFieldSize = 3;
            int messageLeft = offset[0] + 2;
            int messageTop = (sizeT + 1) * 3 + offset[1] - 1 + bottomFieldSize;

            int player;
            int winner;
            int moves = 0;

            int cursorField;





            ConsoleKeyInfo pressedKey;

            // COLORS
            ConsoleColor fieldBorders = ConsoleColor.DarkYellow;
            ConsoleColor foregroundColor = Console.ForegroundColor;
            ConsoleColor backgroundColor = Console.BackgroundColor;
            ConsoleColor highLbackColor = ConsoleColor.DarkGray;


            // CURSOR
            #region:cursor
            // cursor color type 012 , fore 0 and back 1 / type 3, back not highlighted 0 ,back  highlighted 1
            ConsoleColor[,] cursorColorType = new ConsoleColor[4, 2];
            cursorColorType[0, 0] = ConsoleColor.Black;
            cursorColorType[0, 1] = ConsoleColor.Black;
            cursorColorType[1, 0] = ConsoleColor.Blue;
            cursorColorType[1, 1] = ConsoleColor.Black;
            cursorColorType[2, 0] = ConsoleColor.Green;
            cursorColorType[2, 1] = ConsoleColor.Black;
            cursorColorType[3, 0] = ConsoleColor.Black;
            cursorColorType[3, 1] = ConsoleColor.DarkGray;


            // cursorPosField
            int[,] cursorPosField = new int[9, 2];
            for (int i = 0; i < 9; i++)
            {
                cursorPosField[i, 0] = (sizeL + 1) * (i % 3) + offset[0] + 1;
                cursorPosField[i, 1] = (sizeT + 1) * (i / 3) + offset[1] + 1;
            }
            // cursorType empty field, cursorType 0
            string[,] cursorType = new string[3, sizeT];
            for (int i = 0; i < sizeT; i++)
            {
                for (int j = 0; j < sizeL; j++)
                {
                    cursorType[0, i] += " ";
                }
            }
            // cursorType X field, cursorfieldtype 1
            for (int i = 0; i < sizeT; i++)
            {
                if (i < sizeT / 2 + 1)
                {
                    for (int j = 0; j < sizeL; j++)
                    {
                        if (j < sizeL / 2 + 1)
                        {
                            if (i < 2)
                            {
                                cursorType[1, i] += " ";
                            }
                            else
                            {
                                cursorType[1, i] += sizeT / 2 - i == sizeL / 2 - j ? "X" : " ";
                            }
                        }
                        else // mirror top left to top right
                        {
                            cursorType[1, i] += cursorType[1, i][sizeL - j];
                        }
                    }
                }
                else // mirror Top to Bottom
                {
                    cursorType[1, i] = cursorType[1, sizeT - i - 1];
                }
            }

            // cursorType O field, cursorfieldtype 2
            for (int i = 0; i < sizeT; i++)
            {
                if (i < sizeT / 2 + 1)
                {
                    for (int j = 0; j < sizeL; j++)
                    {
                        if (j < sizeL / 2 + 1)
                        {
                            if (i < 2)
                            {
                                cursorType[2, i] += " ";
                            }
                            else
                            {
                                if (j + i == sizeL / 2 - 1 && j >= sizeL / 4) { cursorType[2, i] += "O"; }
                                else if (i == 2 && i + j >= sizeL / 2 - 1) { cursorType[2, i] += "O"; }
                                else if (i >= sizeT / 2 - 1 && j == sizeL / 4) { cursorType[2, i] += "O"; }
                                else { cursorType[2, i] += " "; }
                            }
                        }
                        else // mirror top left to top right
                        {
                            cursorType[2, i] += cursorType[2, i][sizeL - j];
                        }
                    }
                }
                else // mirror Top to Bottom
                {
                    cursorType[2, i] = cursorType[2, sizeT - i - 1];
                }
            }
            #endregion

            #endregion

            Console.CursorVisible = false;
            do
            {
                #region:restart
                // set variables if necessary for each start
                cursorField = 0;
                gameEnd = false;
                player = 1;
                winner = 0;
                moves = 0;
                // cursorTypeField set to 0 for all fields
                int[] cursorTypeField = new int[9]; // 0:empty, 1: X, 2:O
                for (int i = 0; i < 9; i++)
                {
                    cursorTypeField[i] = 0;
                }
                #endregion

                #region:printfield
                // SET CONSOLE SIZE
                Console.SetWindowSize(sizeL * 3 + 4 + offset[0] * 2, sizeT * 3 + 5 + bottomFieldSize + offset[1] * 2);
                Console.BufferHeight = sizeT * 3 + 5 + bottomFieldSize + offset[1] * 2;
                Console.BufferWidth = sizeL * 3 + 4 + offset[0] * 2;

                // PRINT FIELD
                Console.Clear();
                // horizontal and vertical lines
                for (int i = 0; i < 4; i++)
                {
                    PrintLineH(sizeL * 3 + 4, true, fieldBorders, offset[0], (sizeT + 1) * i + offset[1]);
                    PrintLineH(sizeT * 3 + 4, false, fieldBorders, (sizeL + 1) * i + offset[0], offset[1]);
                }
                // print bottom field box
                PrintBoxBorders(sizeL * 3 + 4, bottomFieldSize + 2, fieldBorders, offset[0], sizeT * 3 + 4 - 1 + offset[1]);

                //cursor to top left
                Console.SetCursorPosition(1 + offset[0], (sizeT + 1) * 3 + offset[1] - 1 + bottomFieldSize);

                // highlight first field
                PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 1);

                // print who is next message
                PrintMessage(Console.WindowWidth / 2 - 24, offset[1] / 2, "ESC(exit) ARROWKEYS/SPACEBAR(move) ENTER(select)");
                PrintMessage(messageLeft, messageTop, "Next move: PlayerX");

                #endregion

                #region:gameloop
                // START GAME LOOP
                while (!gameEnd)
                {
                    // check key pressed
                    #region:control
                    if (Console.KeyAvailable)
                    {
                        pressedKey = Console.ReadKey(true);

                        if (pressedKey.Key == ConsoleKey.Escape) { gameEnd = true; }
                        else if ((pressedKey.Key == ConsoleKey.Spacebar))
                        {
                            // remove highlight from field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 0);
                            do // move to next empty field if moves less than 9
                            {
                                cursorField += cursorField == 8 ? -8 : 1;
                            } while (moves < 9 && cursorTypeField[cursorField] != 0);
                            // highlight new field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 1);
                        }
                        else if ((pressedKey.Key == ConsoleKey.UpArrow))
                        {
                            // remove highlight from field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 0);
                            // move to next field
                            cursorField += cursorField - 3 < 0 ? +6 : -3;
                            // highlight new field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 1);
                        }
                        else if ((pressedKey.Key == ConsoleKey.DownArrow))
                        {
                            // remove highlight from field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 0);
                            // move to next field
                            cursorField += cursorField + 3 > 8 ? -6 : +3;
                            // highlight new field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 1);
                        }
                        else if ((pressedKey.Key == ConsoleKey.LeftArrow))
                        {
                            // remove highlight from field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 0);
                            // move to next field
                            cursorField += cursorField % 3 > 0 ? -1 : +2;
                            // highlight new field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 1);
                        }
                        else if ((pressedKey.Key == ConsoleKey.RightArrow))
                        {
                            // remove highlight from field
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 0);
                            // move to next field
                            cursorField += cursorField % 3 < 2 ? +1 : -2;
                            PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 1);
                        }
                        else if ((pressedKey.Key == ConsoleKey.Enter))
                        {
                            if (cursorTypeField[cursorField] == 0) // field empty?
                            {
                                cursorTypeField[cursorField] = player; // assign player to field
                                player = player == 1 ? 2 : 1; // next player
                                // highlight and print field
                                PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 1);
                                ++moves;
                                // initiate check after move ( win condition or draw with moves = 9 );
                                checkAfterMove = true;
                                // next player message
                                PrintMessage(messageLeft, messageTop, "Next move: Player" + (player == 1 ? "X" : "O"), sizeL * 3 - "Next move: Player".Length);
                            }
                        }
                    }
                    #endregion

                    // check after move has been made
                    #region:checkaftermove
                    if (checkAfterMove)
                    {

                        // is there a matching line
                        #region:matchingline
                        bool match = false;
                        // to highlight matched line
                        int[] field = new int[3];

                        // can a player have won
                        if (moves > 4)
                        {
                            // did a player win
                            int h1 = cursorField + (cursorField % 3 > 0 ? -1 : +2);
                            int h2 = h1 + (h1 % 3 > 0 ? -1 : +2);
                            int v1 = cursorField + (cursorField - 3 < 0 ? +6 : -3);
                            int v2 = v1 + (v1 - 3 < 0 ? +6 : -3);
                            // horizontal match ?
                            if (cursorTypeField[cursorField] == cursorTypeField[h1] && cursorTypeField[h1] == cursorTypeField[h2])
                            {
                                match = true;
                                field[0] = cursorField;
                                field[1] = h1;
                                field[2] = h2;
                            }
                            // vertical match?
                            else if (cursorTypeField[cursorField] == cursorTypeField[v1] && cursorTypeField[v1] == cursorTypeField[v2])
                            {
                                match = true;
                                field[0] = cursorField;
                                field[1] = v1;
                                field[2] = v2;
                            }
                            // diagonal match if cursorField on diagonal topleft bottomright
                            else if ("048".Contains(cursorField.ToString()))
                            {
                                if (cursorTypeField[0] == cursorTypeField[4] && cursorTypeField[4] == cursorTypeField[8])
                                {
                                    match = true;
                                    field[0] = 0;
                                    field[1] = 4;
                                    field[2] = 8;
                                }
                            }
                            // diagonal match if cursorField on diagonal bottomleft topright
                            else if ("246".Contains(cursorField.ToString()))
                            {
                                if (cursorTypeField[2] == cursorTypeField[4] && cursorTypeField[4] == cursorTypeField[6])
                                {
                                    match = true;
                                    field[0] = 2;
                                    field[1] = 4;
                                    field[2] = 6;
                                }
                            }
                        }

                        if (match)
                        {
                            // set winner as player with last move (correct move switched players)
                            winner = player == 1 ? 2 : 1;
                            // highlight fields ...
                            foreach (int i in field)
                            {
                                PrintCursor(cursorPosField, i, cursorType, cursorTypeField, cursorColorType, 1);
                            }

                        }
                        #endregion
                        // if winner or out of moves
                        #region:gameabouttoend 
                        string message = "";
                        if (winner != 0)
                        {
                            // set draw message
                            message = string.Format(" Player{0} WINS! ", winner == 1 ? "X" : "O");
                            // set game end
                            gameEnd = true;
                        }
                        else if (moves == 9)
                        {
                            // set draw message
                            message = string.Format(" You played DRAW! ");
                            // set game end
                            gameEnd = true;
                        }
                        if (gameEnd == true)
                        {
                            // unhighlight cursor if no winner
                            if (winner == 0)
                            {
                                PrintCursor(cursorPosField, cursorField, cursorType, cursorTypeField, cursorColorType, 0);
                            }
                            // print message
                            PrintMessage(sizeL * 3 / 2 + offset[0] + 2 - message.Length / 2, sizeT * 3 / 2 + offset[1] + 2, message, 0, ConsoleColor.White, ConsoleColor.Red);
                        }
                        #endregion
                    }
                    // disable check until next move made
                    checkAfterMove = false;
                }
                #endregion

                #endregion

                // clear bottom field
                PrintMessage(messageLeft, messageTop, "", sizeL * 3);
            } while (OnceAgain(ConsoleKey.Escape, messageLeft, messageTop));
            Console.CursorVisible = true;
        }

        #region:utility

        static void PrintMessage(
            int cursorLeft,
            int cursorTop,
            string message,
            int messageLength = 0,
            ConsoleColor foregroundColor = ConsoleColor.Gray,
            ConsoleColor backgroundColor = ConsoleColor.Black)
        {
            Console.ForegroundColor = foregroundColor;
            Console.BackgroundColor = backgroundColor;
            Console.SetCursorPosition(cursorLeft, cursorTop);
            Console.Write(message);
            for (int i = 0; i < messageLength; i++)
            {
                Console.Write(" ");
            }
            Console.ResetColor();
        }

        static void PrintCursor(
            int[,] cursorPosField,
            int cursorField,
            string[,] cursorType,
            int[] cursorFieldType,
            ConsoleColor[,] cursorColorType,
            int highlight)
        {
            Console.ForegroundColor = cursorColorType[cursorFieldType[cursorField], 0];
            Console.BackgroundColor = cursorColorType[3, highlight];
            for (int i = 0; i < cursorType.GetLength(1); i++)
            {
                Console.SetCursorPosition(cursorPosField[cursorField, 0], cursorPosField[cursorField, 1] + i);
                Console.Write(cursorType[cursorFieldType[cursorField], i]);
            }
            Console.ResetColor();
        }




        #region:lineBoxPrinting

        static void PrintLineH(
            int lineLength,
            bool horizontal = true,
            ConsoleColor lineColor = ConsoleColor.Blue,
            int fromPointLeft = 0,
            int fromPointTop = 0)
        {
            Console.BackgroundColor = lineColor;
            // print top and bottom
            if (horizontal)
            {
                for (int i = 0; i < lineLength; i++)
                {
                    Console.SetCursorPosition(fromPointLeft + i, fromPointTop);
                    Console.Write(" ");
                }
            }
            else
            {
                for (int i = 0; i < lineLength; i++)
                {
                    Console.SetCursorPosition(fromPointLeft, fromPointTop + i);
                    Console.Write(" ");
                }
            }
            Console.ResetColor();
        }

        static void PrintBoxBorders(
            int borderWidth = 20,
            int borderHeight = 15,
            ConsoleColor borderColor = ConsoleColor.Blue,
            int anchorLeft = 0,
            int anchorTop = 0)
        {
            // top line
            PrintLineH(borderWidth, true, borderColor, anchorLeft, anchorTop);
            // bottom line
            PrintLineH(borderWidth, true, borderColor, anchorLeft, anchorTop + borderHeight - 1);
            // left line
            PrintLineH(borderHeight, false, borderColor, anchorLeft, anchorTop);
            // right line
            PrintLineH(borderHeight, false, borderColor, anchorLeft + borderWidth - 1, anchorTop);
        }
        #endregion

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion

        #endregion
    }
}
