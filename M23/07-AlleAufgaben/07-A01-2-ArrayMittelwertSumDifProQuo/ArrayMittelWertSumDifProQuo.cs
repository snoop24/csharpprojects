﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A01_2_ArrayMittelwertSumDifProQuo
{
    class ArrayMittelwertSumDifProQuo
    {
        static void Main(string[] args)
        {
            do
            {

                // declare
                int[] array = new int[10];
                decimal mittelW = 0;
                decimal ergebnis = 1;
                char rechenart;

                // wipe screen
                Console.Clear();
                Console.WriteLine();

                // fill array with random
                Random rnd = new Random();
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = rnd.Next(1, 100);
                }

                // get average
                for (int i = 0; i < array.Length; i++)
                {
                    mittelW += array[i];
                    Console.Write("{0,3}", array[i]);
                }
                mittelW /= array.Length;
                Console.WriteLine("\tMittelwert: {0}\n", mittelW);

                //get input
                string errMsg, inpErrMsg, inpPrompt;
                //read rechenart
                errMsg = "";
                inpErrMsg = "\n\nFehlerhafte Eingabe!\n";
                inpPrompt = " \"+\" \"-\" \"*\" \"/\" ? ";
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine(errMsg);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write(inpPrompt);
                    Console.ResetColor();
                    errMsg = inpErrMsg;
                    rechenart = Console.ReadKey(true).KeyChar;
                } while (!"+-*/".ToString().Contains(rechenart)|| rechenart.ToString().Length>1);
                Console.Write(rechenart);
                Console.WriteLine("\n\n");

                switch (rechenart)
                {
                    case'+':
                        ergebnis = mittelW * array.Length;
                        break;
                    case '-': 
                        ergebnis = (-1)*mittelW * array.Length;
                        break;
                    default: // * and /                        
                        for (int i = 0; i < array.Length; i++)
                        {
                            ergebnis *= array[i];
                        }
                        if (rechenart != '*') // switch to quotient
                        {
                            ergebnis = 1 / ergebnis;
                        }
                        
                        break;
                }
                
                // output
                Console.WriteLine("{0} aller Felder ergibt {1}.\n\n", rechenart == '+'? " Addition": rechenart == '-' ? " Subtraktion": rechenart == '*' ? " Multiplikation": "Division" , ergebnis);

            } while (OnceAgain());
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
