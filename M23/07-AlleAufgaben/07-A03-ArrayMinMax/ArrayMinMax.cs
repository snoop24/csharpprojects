﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A03_ArrayMinMax
{
    class ArrayMinMax
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Clear();
                Console.WriteLine();
                // declare
                int size = 40;
                int temp;
                int minPos = 0;
                int maxPos = 0;
                int minCount;
                int maxCount;

                int[] array = new int[size];

                Random rnd = new Random();


                // fill array
                for (int i = 0; i < size; i++)
                {
                    array[i] = rnd.Next(1, 100);
                }

                // get min max from array
                for (int i = 0; i < size; i++)
                {
                    if (array[maxPos] < array[i]) { maxPos = i; }
                    if (array[minPos] > array[i]) { minPos = i; }
                }

                // output array
                //Console.WriteLine(string.Join(" | ", array));
                maxCount = 0;
                minCount = 0;
                for (int i = 0; i < size; i++)
                {
                    Console.Write(" ");
                    if (array[i] == array[maxPos])
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        ++maxCount;
                    }
                    if (array[i] == array[minPos])
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        ++minCount;
                    }
                    Console.Write("{0,2}", array[i]);
                    Console.ResetColor();
                    Console.Write(" ");
                }
                Console.WriteLine("\n");


                // switch in array positions
                for (int i = 0; i < size / 2; i++)
                {
                    temp = array[i];
                    array[i] = array[size - 1 - i];
                    array[size - 1 - i] = temp;
                }
                minPos = size - 1 - minPos;
                maxPos = size - 1 - maxPos;


                // output array
                //Console.WriteLine(string.Join(" | ", array));
                for (int i = 0; i < size; i++)
                {
                    Console.Write(" ");
                    if (array[i] == array[maxPos]) { Console.BackgroundColor = ConsoleColor.DarkGreen; }
                    if (array[i] == array[minPos]) { Console.BackgroundColor = ConsoleColor.DarkRed; }
                    Console.Write("{0,2}", array[i]);
                    Console.ResetColor();
                    Console.Write(" ");
                }
                Console.WriteLine("\n");
                // output min max
                Console.WriteLine(" {2} mal Minimum : {0}\n {3} mal Maximum : {1}\n\n\n", array[minPos], array[maxPos],minCount,maxCount);


            } while (OnceAgain());
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
