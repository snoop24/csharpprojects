﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A10_SiebDesEratosthenes
{
    class SiebDesEratosthenes
    {
        static void Main(string[] args)
        {
            // general loop
            do
            {
                // declare
                int maxPlus1;
                bool[] primNein;

                //read maxPlus1
                Console.Clear();
                Console.WriteLine(" *** Sieb des Eratosthenes ***");
                string errMsg, inpErrMsg, inpPrompt;
                errMsg = "";
                inpErrMsg = "\n   Zahl? Im Intervall?\n";
                inpPrompt = string.Format("\n   Primzahlen von 0 bis (2-9999): ");
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out maxPlus1) || maxPlus1 < 2 || maxPlus1 > 9999);
                maxPlus1++;

                // init
                primNein = new bool[maxPlus1];
                // set 0 and 1 to no prime
                primNein[0] = true;
                primNein[1] = true;

                // loop all prime numbers and set primeNein[multiples]=false start with prime 2
                for (int i = 2; i * i < maxPlus1;)
                {
                    // loop all numbers after prime*prime 
                    for (int j = i * i; j < maxPlus1; j+=i)
                    {
                        primNein[j] = true; 
                    }

                    // check if next i is prime, else next i
                    do { i++; } while (primNein[i]);
                }

                // output
                Console.WriteLine("\n\n");
                int count = 0;
                for (int i = 0; i < maxPlus1; i++)
                {

                    if (primNein[i] == false)
                    {
                        // newline with count
                        if (count % 10 == 0)
                        { Console.Write("\n{0,4} -{1,4} :  ", count + 1, count + 10); }
                        //output primNumber
                        Console.Write("{0,5}", i);
                        count++;
                    }

                }
                Console.WriteLine("\n\n\n\n");

            } while (OnceAgain(ConsoleKey.Escape, ConsoleKey.Enter));
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("   again(");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(")   end(");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(")");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
