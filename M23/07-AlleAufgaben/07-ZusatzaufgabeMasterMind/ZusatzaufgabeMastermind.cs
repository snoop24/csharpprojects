﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_ZusatzaufgabeMasterMind
{
    class ZusatzaufgabeMastermind
    {
        static void Main(string[] args)
        {
            // declare help
            int leftPosOutSideDo;
            int topPosOutSideDo;

            do
            {
                Console.CursorVisible = false;
                // to allow reprinting without causing trouble
                Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight + 1);

                // DECLARE
                // user
                // default
                int[] triesDifficulty = new int[3] { 8, 10, 12 };
                int[] fieldSize = new int[2] { 5, 3 };
                int titleBoxWidth = 30;
                ConsoleColor[] borderColor = new ConsoleColor[2] { ConsoleColor.Gray, ConsoleColor.DarkGray };
                ConsoleColor[] colors = new ConsoleColor[11] { ConsoleColor.Black, ConsoleColor.Yellow, ConsoleColor.DarkYellow, ConsoleColor.Red, ConsoleColor.DarkRed, ConsoleColor.Magenta, ConsoleColor.DarkMagenta, ConsoleColor.Cyan, ConsoleColor.DarkCyan, ConsoleColor.Green, ConsoleColor.DarkGreen };
                bool gameEnd = false;
                bool youWon = false;
                ConsoleKeyInfo pressedKey;
                int[] cursor = new int[2] { 0, 1 };
                int[] correctPosCol = new int[2];

                // get user settings
                int fieldCount = 4; // 4 to 6
                int difficulty = 2; // 0 gives 8 tries, 1:10, 2:12

                // calc some stuff
                int maxTries = triesDifficulty[difficulty];
                int[] helpBoxSize = new int[2] { titleBoxWidth - 4, maxTries * (fieldSize[1] + 1) + 2 };

                // help for outer loop
                leftPosOutSideDo = (1 + fieldSize[0]) * fieldCount + 3;
                topPosOutSideDo = fieldSize[1];

                // set fieldColor array
                int[,] fieldsColor = new int[fieldCount, maxTries + 1];
                // set field coords
                int[,,] fieldsLeftTopCoord = new int[fieldCount, maxTries + 1, 2];
                for (int i = 0; i < fieldsLeftTopCoord.GetLength(0); i++)
                {
                    for (int j = 0; j < fieldsLeftTopCoord.GetLength(1); j++)
                    {
                        fieldsLeftTopCoord[i, j, 0] = 1 + i * (fieldSize[0] + 1);
                        fieldsLeftTopCoord[i, j, 1] = 1 + j * (fieldSize[1] + 1);
                    }
                }
                

                // PREPARE
                #region:prepare

                // wipe screen
                Console.Clear();

                // windows size
                Console.SetWindowSize((fieldSize[0] + 1) * fieldCount + titleBoxWidth, (fieldSize[1] + 1) * (maxTries + 1) + 1);

                // print fields
                for (int i = 0; i < fieldCount; i++)
                {
                    for (int j = 0; j < maxTries; j++)
                    {
                        PrintBoxBorders(fieldSize[0] + 2, fieldSize[1] + 2, borderColor[1], (1 + fieldSize[0]) * i, 1 + fieldSize[1] + j * (1 + fieldSize[1]));
                    }

                }
                for (int i = 0; i < fieldCount; i++)
                {
                    PrintBoxBorders(fieldSize[0] + 2, fieldSize[1] + 2, borderColor[0], (1 + fieldSize[0]) * i);
                }

                // print response fields
                PrintBoxBorders(2, (fieldSize[1] + 1) * maxTries, borderColor[1], (1 + fieldSize[0]) * fieldCount + 1, fieldSize[1] + 2);
                for (int i = 0; i < maxTries; i++)
                {
                    PrintBoxBorders(4, fieldSize[1] + 2, borderColor[0], (1 + fieldSize[0]) * fieldCount, fieldSize[1] + 1 + (1 + fieldSize[1]) * (i));
                }

                // print outer box
                PrintBoxBorders((fieldSize[0] + 1) * fieldCount + titleBoxWidth, (fieldSize[1] + 1) * (maxTries + 1) + 1, borderColor[0]);

                // print title box
                PrintBoxBorders(titleBoxWidth, fieldSize[1] + 2, borderColor[0], (1 + fieldSize[0]) * fieldCount, 0);
                PrintMessage((1 + fieldSize[0]) * fieldCount + 1, fieldSize[1] / 2 + 1, "   Are you a MasterMIND?!", 0, ConsoleColor.Yellow);

                // buffer size
                Console.SetCursorPosition(0, 0);
                Console.SetBufferSize((fieldSize[0] + 1) * fieldCount + titleBoxWidth, (fieldSize[1] + 1) * (maxTries + 1) + 1);


                // print ? master fields output and give random color value to master fields
                Random rnd = new Random();
                for (int i = 0; i < fieldCount; i++)
                {
                    PrintMessage(fieldsLeftTopCoord[i, 0, 0] + fieldSize[0] / 2, fieldsLeftTopCoord[i, 0, 1] + fieldSize[1] / 2, "?", 0, ConsoleColor.White, ConsoleColor.Black);
                    fieldsColor[i, 0] = rnd.Next(1, 11);
                }

                #endregion

                //// test show master fields
                //for (int i = 0; i < fieldCount; i++)
                //{
                //    PrintCursor(fieldsLeftTopCoord[i, 0, 0], fieldsLeftTopCoord[i, 0, 1], fieldSize[0], fieldSize[1], colors[0], colors[fieldsColor[i, 0]]);
                //}

                // start cursor
                PrintCursorMidChar(fieldsLeftTopCoord[cursor[0], cursor[1], 0], fieldsLeftTopCoord[cursor[0], cursor[1], 1], fieldSize[0], fieldSize[1]);

                #region:gameloop
                // START GAME LOOP
                while (!gameEnd)
                {
                    // check key pressed
                    #region:control
                    if (Console.KeyAvailable)
                    {
                        pressedKey = Console.ReadKey(true);

                        if (pressedKey.Key == ConsoleKey.Escape) { gameEnd = true; }
                        else if (pressedKey.Key == ConsoleKey.UpArrow)
                        {
                            ChangeFieldColor(cursor, fieldsColor, 1);
                            RePrintFieldCursor(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                        }
                        else if (pressedKey.Key == ConsoleKey.DownArrow)
                        {
                            ChangeFieldColor(cursor, fieldsColor, -1);
                            RePrintFieldCursor(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                        }
                        else if ("0123456789".Contains(pressedKey.KeyChar.ToString()))
                        {
                            fieldsColor[cursor[0], cursor[1]] = int.Parse(pressedKey.KeyChar.ToString()) + 1;
                            RePrintFieldCursor(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                        }
                        else if (pressedKey.Key == ConsoleKey.LeftArrow)
                        {
                            PrintField(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                            MoveCursor(cursor, -1, fieldCount);
                            RePrintFieldCursor(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                        }
                        else if (pressedKey.Key == ConsoleKey.RightArrow)
                        {
                            PrintField(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                            MoveCursor(cursor, 1, fieldCount);
                            RePrintFieldCursor(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                        }
                        else if (pressedKey.Key == ConsoleKey.Enter)
                        {
                            PrintField(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                            bool noblack = true;
                            for (int i = fieldCount - 1; i >= 0; i--)
                            {
                                if (fieldsColor[i, cursor[1]] == 0)
                                {
                                    noblack = false;
                                    cursor[0] = i;
                                }
                            }
                            if (noblack)
                            {

                                CorrectPosCol(cursor, fieldsColor, correctPosCol, fieldCount);
                                PrintCorrectPosCol(cursor, correctPosCol, fieldSize, fieldsLeftTopCoord, fieldCount, borderColor);
                                if (correctPosCol[0] == fieldCount)
                                {
                                    youWon = true;
                                    gameEnd = true;
                                }

                                if (cursor[1] < maxTries)
                                {

                                    cursor[0] = 0;
                                    cursor[1]++;
                                    if (!youWon)
                                    {
                                        RePrintFieldCursor(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                                    }
                                }
                                else
                                {
                                    gameEnd = true;
                                }



                            }
                            else
                            {
                                RePrintFieldCursor(cursor, fieldsLeftTopCoord, fieldSize, colors, fieldsColor);
                            }
                        }
                    }
                    #endregion

                }
                #endregion

                // reveal master fields
                for (int i = 0; i < fieldCount; i++)
                {
                    PrintCursor(fieldsLeftTopCoord[i, 0, 0], fieldsLeftTopCoord[i, 0, 1], fieldSize[0], fieldSize[1], colors[0], colors[fieldsColor[i, 0]]);
                }

                // win/lose message
                PrintMessage(fieldSize[0]*2, Console.WindowHeight / 2, youWon ? "YOU WIN!" : "YOU LOSE!", 0, ConsoleColor.Red, ConsoleColor.White);


            } while (OnceAgain(ConsoleKey.Escape, leftPosOutSideDo, topPosOutSideDo));

        }

        private static void PrintCorrectPosCol(
            int[] cursor,
            int[] correctPosCol,
            int[] fieldSize,
            int[,,] fieldsLeftTopCoord,
            int fieldCount, ConsoleColor[] borderColor)
        {
            PrintMessage(fieldsLeftTopCoord[fieldCount - 1, cursor[1], 0] + fieldSize[0] + 1, fieldsLeftTopCoord[0, cursor[1], 1] + fieldSize[1] / 2, correctPosCol[0].ToString(), 0, ConsoleColor.Red, ConsoleColor.Black);
            PrintMessage(fieldsLeftTopCoord[fieldCount - 1, cursor[1], 0] + fieldSize[0] + 2, fieldsLeftTopCoord[0, cursor[1], 1] + +fieldSize[1] / 2, correctPosCol[1].ToString(), 0, ConsoleColor.Blue, ConsoleColor.White);
        }

        private static void CorrectPosCol(int[] cursor, int[,] fieldsColor, int[] correctPosCol, int fieldCount)
        {
            bool[,] inValid = new bool[fieldsColor.GetLength(0), 2];
            correctPosCol[0] = 0;
            correctPosCol[1] = 0;
            for (int i = 0; i < fieldsColor.GetLength(0); i++)
            {
                if (fieldsColor[i, 0] == fieldsColor[i, cursor[1]])
                {
                    correctPosCol[0]++;
                    inValid[i, 0] = true;
                    inValid[i, 1] = true;
                }
            }
            for (int i = 0; i < fieldCount; i++)
            {
                for (int j = 0; j < fieldCount; j++)
                {
                    if (fieldsColor[i, cursor[1]] == fieldsColor[j, 0] && !inValid[j, 1] && !inValid[i, 0])
                    {
                        correctPosCol[1]++;
                        inValid[j, 1] = true;
                        inValid[i, 0] = true;
                    }
                }
            }
        }

        private static void MoveCursor(int[] cursor, int leftRight, int fieldCount)
        {
            cursor[0] += leftRight;
            if (cursor[0] < 0) { cursor[0] = fieldCount; }
            if (cursor[0] >= fieldCount) { cursor[0] = 0; }
        }

        #region:utility

        static void PrintField(int[] cursor, int[,,] fieldsLeftTopCoord, int[] fieldSize, ConsoleColor[] colors, int[,] fieldsColor)
        {
            // print new field color
            PrintCursor(fieldsLeftTopCoord[cursor[0], cursor[1], 0], fieldsLeftTopCoord[cursor[0], cursor[1], 1], fieldSize[0], fieldSize[1], colors[0], colors[fieldsColor[cursor[0], cursor[1]]]);
        }

        static void RePrintFieldCursor(int[] cursor, int[,,] fieldsLeftTopCoord, int[] fieldSize, ConsoleColor[] colors, int[,] fieldsColor)
        {
            // print new field color
            PrintCursor(fieldsLeftTopCoord[cursor[0], cursor[1], 0], fieldsLeftTopCoord[cursor[0], cursor[1], 1], fieldSize[0], fieldSize[1], colors[0], colors[fieldsColor[cursor[0], cursor[1]]]);
            // reprint cursor
            PrintCursorMidChar(fieldsLeftTopCoord[cursor[0], cursor[1], 0], fieldsLeftTopCoord[cursor[0], cursor[1], 1], fieldSize[0], fieldSize[1]);
        }

        static void ChangeFieldColor(int[] cursor, int[,] fieldsColor, int upDown)
        {
            fieldsColor[cursor[0], cursor[1]] += upDown;
            if (fieldsColor[cursor[0], cursor[1]] <= 0) { fieldsColor[cursor[0], cursor[1]] = 10; }
            if (fieldsColor[cursor[0], cursor[1]] == 11) { fieldsColor[cursor[0], cursor[1]] = 1; }
        }

        static void PrintCursor(
            int cursorLeft,
            int cursorTop,
            int cursorWidth,
            int cursorHeight,
            ConsoleColor foregroundColor = ConsoleColor.Gray,
            ConsoleColor backgroundColor = ConsoleColor.Black)
        {
            Console.ForegroundColor = foregroundColor;
            Console.BackgroundColor = backgroundColor;

            for (int i = 0; i < cursorHeight; i++)
            {
                Console.SetCursorPosition(cursorLeft, cursorTop + i);
                for (int j = 0; j < cursorWidth; j++)
                {
                    Console.Write(" ");
                }
            }
            Console.ResetColor();
        }

        static void PrintCursorMidChar(
            int cursorLeft,
            int cursorTop,
            int cursorWidth,
            int cursorHeight,
            ConsoleColor foregroundColor = ConsoleColor.White,
            ConsoleColor backgroundColor = ConsoleColor.White)
        {
            Console.ForegroundColor = foregroundColor;
            Console.BackgroundColor = backgroundColor;

            Console.SetCursorPosition(cursorLeft + cursorWidth / 2, cursorTop + cursorHeight / 2);

            Console.Write(" ");

            Console.ResetColor();
        }


        static void PrintMessage(
        int cursorLeft,
        int cursorTop,
        string message,
        int messageLength = 0,
        ConsoleColor foregroundColor = ConsoleColor.Gray,
        ConsoleColor backgroundColor = ConsoleColor.Black)
        {
            Console.ForegroundColor = foregroundColor;
            Console.BackgroundColor = backgroundColor;
            Console.SetCursorPosition(cursorLeft, cursorTop);
            Console.Write(message);
            for (int i = 0; i < messageLength; i++)
            {
                Console.Write(" ");
            }
            Console.ResetColor();
        }

        #region:lineBoxPrinting

        static void PrintLineH(
            int lineLength,
            bool horizontal = true,
            ConsoleColor lineColor = ConsoleColor.Blue,
            int fromPointLeft = 0,
            int fromPointTop = 0)
        {
            Console.BackgroundColor = lineColor;
            // print top and bottom
            if (horizontal)
            {
                for (int i = 0; i < lineLength; i++)
                {
                    Console.SetCursorPosition(fromPointLeft + i, fromPointTop);
                    Console.Write(" ");
                }
            }
            else
            {
                for (int i = 0; i < lineLength; i++)
                {
                    Console.SetCursorPosition(fromPointLeft, fromPointTop + i);
                    Console.Write(" ");
                }
            }
            Console.ResetColor();
        }

        static void PrintBoxBorders(
            int borderWidth = 20,
            int borderHeight = 15,
            ConsoleColor borderColor = ConsoleColor.Blue,
            int anchorLeft = 0,
            int anchorTop = 0)
        {
            // top line
            PrintLineH(borderWidth, true, borderColor, anchorLeft, anchorTop);
            // bottom line
            PrintLineH(borderWidth, true, borderColor, anchorLeft, anchorTop + borderHeight - 1);
            // left line
            PrintLineH(borderHeight, false, borderColor, anchorLeft, anchorTop);
            // right line
            PrintLineH(borderHeight, false, borderColor, anchorLeft + borderWidth - 1, anchorTop);
        }
        #endregion

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write("(again), ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write("(end)!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion

        #endregion
    }
}
