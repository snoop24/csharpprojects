﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A05_LottoZahlen
{
    class LottoZahlen
    {
        static void Main(string[] args)
        {
            do
            {
                // declare
                int anzahl = 6;
                int min = 1;
                int max = 49;
                int[] array = new int[anzahl];
                int versuche = 0;
                Random rnd = new Random();

                // fill array
                for (int i = 0; i < anzahl; i++)
                {
                    array[i] = rnd.Next(min, max+1);
                    for (int j = 0; j < i; j++)
                    {
                        if (array[j] == array[i])
                        {
                            ++versuche; // count matches
                            --i; // reset i to fill field again
                            break; // break check loop
                        }
                    }
                }
                
                // output
                Array.Sort(array); // sort array
                Console.Clear();
                Console.WriteLine("\n\n *** Lotto Zahlen ***\n\n  {1} Fehlversuch{2}\n\n  {0}\n\n\n", String.Join(" | ",array),versuche,versuche == 1 ? "": "e");


            } while (OnceAgain());
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }

}
