﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A04_ZufallsZahlenArraySuche
{
    class ZufallsZahlenArraySuche
    {
        static void Main(string[] args)
        {
            do
            {
                // declare
                Random rnd = new Random();

                int anzahl = 100000;
                int min = 0;
                int max = 50000;
                int vorkommen = 0;
                int sucheZahl;
                int sucheZahlbak;

                int[] array = new int[anzahl];

                // fill array
                for (int i = 0; i < anzahl; i++)
                {
                    array[i] = rnd.Next(min, max + 1);
                }

                //read anzahlSchueler
                Console.Clear();
                string errMsg, inpErrMsg, inpPrompt;
                errMsg = "";
                inpErrMsg = "\n   Zahl? Im Intervall?\n";
                inpPrompt = string.Format("\n   Zahl ({0}-{1}): ", min, max);
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out sucheZahl) || sucheZahl < min || sucheZahl > max);

                // search array
                Console.WriteLine("\n   {0,9} : {1,9}", "Vorkommen", "Stelle");
                for (int i = 0; i < anzahl; i++)
                {
                    if (array[i] == sucheZahl)
                    {
                        ++vorkommen;
                        Console.WriteLine("   {0,9} : {1,9}", vorkommen, i);
                    }
                }
                Console.WriteLine("\n   Die gesuchte Zahl {0} wurde {1} mal gefunden.\n", sucheZahl, vorkommen);


                //sucheZahlbak = 50001;
                //int anzahlNicht = 0;
                //do
                //{
                //    vorkommen = 0;
                //    //search array
                //    for (int i = 0; i < anzahl; i++)
                //    {
                //        if (array[i] == sucheZahl)
                //        {
                //            ++vorkommen;
                //        }
                //    }
                //    if (vorkommen == 0)
                //    {
                //        //Console.BackgroundColor = ConsoleColor.DarkRed;
                //        //Console.Write("{0,5}:{1,1} ", sucheZahl, vorkommen);
                //        ++anzahlNicht;
                //    }
                //    Console.ResetColor();
                //    ++sucheZahl;
                //} while (sucheZahl < sucheZahlbak);
                //Console.WriteLine("\n Anzahl ohne Vorkommen: {0}", anzahlNicht);

            } while (OnceAgain());
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorLeft = -1
            , int cursorTop = -1
            )
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorLeft = -1
            , int cursorTop = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
