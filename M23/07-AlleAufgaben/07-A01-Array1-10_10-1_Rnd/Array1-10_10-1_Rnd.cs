﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_A02_Array1_10_10_1_Rnd
{
    class Array1_10_10_1_Rnd
    {
        static void Main(string[] args)
        {
            // declare 
            int[] array = new int[10];

            // once again loop
            do
            {
                // wipe screen
                Console.Clear();
                Console.WriteLine();

                // fill array with 1 - 10
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = 1+i;
                }

                // print array
                for (int i = 0; i < array.Length; i++)
                {
                    Console.Write("{0,3} ", array[i]);
                }

                Console.WriteLine("\n\n");

                // fill array with 10 - 1
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = array.Length-i;
                }

                // print array
                for (int i = 0; i < array.Length; i++)
                {
                    Console.Write("{0,3} ", array[i]);
                }

                Console.WriteLine("\n\n");


                // fill array with random
                Random rnd = new Random();
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = rnd.Next(1,100);
                }

                // print array
                for (int i = 0; i < array.Length; i++)
                {
                    Console.Write("{0,3} ", array[i]);
                }

                Console.WriteLine("\n\n");


            } while (OnceAgain());
        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }

}
