﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace _07_A02_b_ArraySchuelerNoten
{
    class ArraySchuelerNoten
    {
        static void Main(string[] args)
        {
            do
            {
                // declare
                int anzahlSchueler;
                int anzahlNoten = 0;
                int summeNoten = 0;
                float schnittNoten;
                Console.Clear();
                //read anzahlSchueler
                string errMsg, inpErrMsg, inpPrompt;
                errMsg = "";
                inpErrMsg = "\n   Zahl? Im Intervall?\n";
                inpPrompt = string.Format("\n   Anzahl Schüler (0-40): ");
                do
                {
                    //Console.Clear();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write(errMsg);
                    Console.ResetColor();
                    Console.Write(inpPrompt);
                    errMsg = inpErrMsg;
                } while (!int.TryParse(Console.ReadLine(), out anzahlSchueler) || anzahlSchueler < 0 || anzahlSchueler > 40);

                //declare arrays
                string[] namenSchueler = new string[anzahlSchueler];
                byte[] notenSchueler = new byte[anzahlSchueler];

                //loop names and grades
                for (int i = 0; i < anzahlSchueler; i++)
                {
                    Console.Clear();
                    //read namenSchueler
                    errMsg = "";
                    inpErrMsg = "\n   Nur Buchstaben und Leerzeichen? [a-zA-Z äöü,]? Zu lang? \n";
                    inpPrompt = string.Format("\n   Name(max. 40Zeichen/leer=Ende) Schüler{0:00} : ", i+1);
                    do
                    {
                        //Console.Clear();
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.Write(errMsg);
                        Console.ResetColor();
                        Console.Write(inpPrompt);
                        errMsg = inpErrMsg;
                        namenSchueler[i] = Console.ReadLine();
                        if (namenSchueler[i] == "") { anzahlSchueler=i;}
                    } while ((!Regex.IsMatch(namenSchueler[i], @"^[a-zA-Z äöü,]+$") || namenSchueler[i].Length > 40) && i < anzahlSchueler);

                    if (i < anzahlSchueler)
                    {
                        Console.Clear();
                        //read notenSchueler
                        errMsg = "";
                        inpErrMsg = "\n   Zahl? Im Intervall?\n";
                        inpPrompt = string.Format("\n   Note(0=keine;1-6) {0} : ", namenSchueler[i]);
                        do
                        {
                            //Console.Clear();
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.Write(errMsg);
                            Console.ResetColor();
                            Console.Write(inpPrompt);
                            errMsg = inpErrMsg;
                        } while (!byte.TryParse(Console.ReadLine(), out notenSchueler[i]) || notenSchueler[i] < 0 || notenSchueler[i] > 6);
                    }

                }

                Console.Clear();
                Console.WriteLine("\n");

                // calc schnittNoten output namen noten schnitt
                Console.WriteLine("\n   {1,4} : {0,-40}", "Name", "Note");
                for (int i = 0; i < anzahlSchueler; i++)
                {
                    Console.WriteLine("\n    {1,3} : {0,-40}", namenSchueler[i], notenSchueler[i]);
                    summeNoten += notenSchueler[i];
                    anzahlNoten += notenSchueler[i] == 0 ? 0 : 1;
                }
                schnittNoten = (float)summeNoten / anzahlNoten;
                Console.WriteLine("\n   Der Notendurchschnitt ist: {0:F1}\n   {1} Schüler {2} noch keine Note.\n\n",schnittNoten,anzahlSchueler-anzahlNoten, anzahlSchueler - anzahlNoten==1 ? "hat": "haben");

            } while (OnceAgain());

        }

        #region: OnceAgain

        static void OnceAgainMenu(
            int cursorLeft
            , int cursorTop
            , string end
            , string repeat = "ANY"
            )
        {
            Console.SetCursorPosition(cursorLeft == -1 ? Console.CursorLeft : cursorLeft, cursorTop == -1 ? Console.CursorTop : cursorTop);
            Console.Write("press (");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(repeat);
            Console.ResetColor();
            Console.Write(") to start again, (");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(end);
            Console.ResetColor();
            Console.Write(") to end!");
        }

        static bool OnceAgain(
            ConsoleKey end = ConsoleKey.X
            , int cursorTop = -1
            , int cursorLeft = -1)
        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString());
            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed != end)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        static bool OnceAgain(
            ConsoleKey end
            , ConsoleKey repeat
            , int cursorTop = -1
            , int cursorLeft = -1
            )

        {
            ConsoleKey keyPressed;
            // print menu
            OnceAgainMenu(cursorLeft, cursorTop, end.ToString(), repeat.ToString());

            while (true)
            {
                keyPressed = Console.ReadKey(true).Key;
                if (keyPressed == repeat)
                {
                    return true;
                }
                else if (keyPressed == end)
                {
                    return false;
                }

            }

        }

        #endregion
    }
}
