﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _12_A01_Explorer
{
    class Menu
    {
        static Explorer woDenn;
        static bool ende;
        static string titel = "\n* * *  * * *  Explorer NischMitMKommander  * * *  * * *\n";

        public static void HauptMenue()
        {
            // general loop
            ende = false;
            do
            {
                int auswahlFehler = 0;
                bool auswahlGueltig ;
                // putzen
                Console.Clear();

                PrintMenu();
                do
                {
                    int auswahl;
                    auswahlGueltig = true;

                    // prompt und eingabe
                    Console.Write(" -> ");
                    string eingabe = Console.ReadLine().ToLower();
                    string pfad;

                    // laufwerksbuchstaben fuer eingabeableich
                    string laufwerksBuchstaben = "";
                    foreach (DriveInfo item in woDenn.GetLaufwerke())
                    { laufwerksBuchstaben += item.Name[0].ToString().ToLower(); }

                    // vorabgleich der eingabe
                    if (int.TryParse(eingabe, out auswahl) && -1 <= auswahl && auswahl <= woDenn.GetUnterOrdner().Length)
                    {
                        eingabe = (auswahl == -1 ? "laufwerk" : (auswahl == 0 ? "hoch" : "ordner"));
                    }
                    else if (laufwerksBuchstaben.Contains(eingabe) && eingabe.Length==1)
                    {
                        auswahl = -((int) char.Parse(eingabe));
                        eingabe = "ordner";
                    }


                    // endabgleich der eingabe
                    switch (eingabe)
                    {
                        case "laufwerk":
                            woDenn.SetOrdner(woDenn.GetOrdner().Root.ToString());
                            break;

                        case "hoch":
                        case "z":
                            if (woDenn.GetOrdner().Parent != null)
                            { woDenn.SetOrdner(woDenn.GetOrdner().Parent.FullName); }
                            else
                            {
                                Console.WriteLine("   Es gibt keine höhere Ebene \n");
                                auswahlGueltig = false;
                            }
                            break;

                        case "ordner":
                            if (auswahl < 0)
                            {
                                woDenn.SetOrdner(((char)-auswahl).ToString() + @":\\");
                            }
                            else
                            {
                                woDenn.SetOrdner(woDenn.GetUnterOrdner()[auswahl - 1]);
                            }
                                break;

                        case "q":
                        case "quit":
                            ende = true;
                            break;

                        default:
                            PrintColored(auswahlFehler > 4 ? "   Doof? \n" : "   Ungültige Auswahl/Eingabe. \n\n", ConsoleColor.White, ConsoleColor.Red);
                            auswahlFehler++;
                            auswahlGueltig = false;
                            break;
                    }
                } while (!auswahlGueltig);

            } while (!ende);
            PrintColored("\n\n   Auf Wiedersehen! ", ConsoleColor.Magenta, ConsoleColor.Gray);
        }

        private static void PrintMenu()
        {
            DriveInfo[] laufwerke = woDenn.GetLaufwerke();
            DirectoryInfo ordner;
            string[] unterOrdner = woDenn.GetUnterOrdner();
            string[] dateien = woDenn.GetDateien();
            string temp;

            // print titel
            PrintColored(titel + "\n", ConsoleColor.Red);

            // print unterpunkt navigation mit laufwerken
            PrintColored("Eingabe:\n\n", ConsoleColor.Yellow);
            PrintColored(
                "       n -> Ordner \"n\" öffnen\n" +
                "z /    0 -> einen Ordner hoch\n" +
                "      -1 -> zur Laufwerksebene\n" +
                "q / quit -> schliessen\n\n   ", ConsoleColor.White);

            for (int i = 0; i < laufwerke.Length; i++)
            {
                temp = string.Format("{0,12}",string.Format("{0} -> {1}", laufwerke[i].Name[0].ToString().ToLower(), laufwerke[i].Name));
                PrintColored(temp, ConsoleColor.White);
            }
            Console.WriteLine();

            // print aktueller ort
            PrintColored("\nAktueller Pfad:\n\n", ConsoleColor.Yellow);
            PrintColored("   " + woDenn.GetOrdner().FullName + "\n\n", ConsoleColor.Cyan);
            PrintColored(string.Format(" {0,6} Ordner {1,6} Datei(en)\n\n", unterOrdner.Length, dateien.Length), ConsoleColor.Gray);

            // PRINT INHALT

            // ordner
            for (int i = 0; i < unterOrdner.Length; i++)
            {
                ordner = new DirectoryInfo(unterOrdner[i]);
                temp = string.Format("{0,7} -> {1}\n", i + 1, ordner.Name);

                PrintColored(temp, ConsoleColor.Green);
            }
            Console.WriteLine();

            // dateien
            for (int i = 0; i < dateien.Length; i++)
            {
                temp = string.Format("{0,7} -> {1}\n", " ", Path.GetFileName(dateien[i]));

                PrintColored(temp, ConsoleColor.DarkGreen);
            }

        }

        private static void PrintColored(string message, ConsoleColor foreground, ConsoleColor background = ConsoleColor.Black)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

        #region:Get
        static public Explorer GetWoDenn()
        {
            return woDenn;
        }

        #endregion

        #region:Set

        static public void SetWoDenn(Explorer woDenn)
        {
            Menu.woDenn = woDenn;
        }

        #endregion
    }
}
