﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _12_A01_Explorer
{
    class Program
    {
        static void Main(string[] args)
        {
            // 
            Console.SetWindowSize(80, 60);
            // general loop
            do
            {
                Explorer nischMitMKommander = new Explorer();

                Menu.SetWoDenn(nischMitMKommander);

                Menu.HauptMenue();

                // loop prompt
                Console.Clear();
                Console.WriteLine("\n\n\n ... raus hier? Escape!");
            } while (Console.ReadKey(true).Key!=ConsoleKey.Escape);
        }
    }
}
