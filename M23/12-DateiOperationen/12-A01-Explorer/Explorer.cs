﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _12_A01_Explorer
{
    class Explorer
    {
        DriveInfo[] laufwerke;
        DirectoryInfo ordner;
        string[] unterOrdner;
        string[] dateien;

        #region:Constructor

        public Explorer()
        {
            laufwerke = DriveInfo.GetDrives();
            SetOrdner(laufwerke[0].Name);
        }

        #endregion

        #region:Get

        public DriveInfo[] GetLaufwerke()
        {
            return laufwerke;
        }

        public DirectoryInfo GetOrdner()
        {
            return ordner;
        }

        public string[] GetUnterOrdner()
        {
            return unterOrdner;
        }

        public string[] GetDateien()
        {
            return dateien;
        }

        #endregion

        #region:Set

        public void SetLaufwerke()
        {
            laufwerke = DriveInfo.GetDrives();
        }

        public void SetOrdner(string path)
        {
            DirectoryInfo temp = ordner;
            try
            {
                ordner = new DirectoryInfo(path);
                SetUnterOrdner(ordner);
                SetDateien(ordner);
            }
            catch (Exception error)
            {
                PrintColored(error.Message+"\n", ConsoleColor.Red,ConsoleColor.White);
                PrintColored("Spring auf letzte Position!\n", ConsoleColor.Red, ConsoleColor.Green);
                System.Threading.Thread.Sleep(1000);
                SetOrdner(temp.FullName);
            }
        }
        
        private void SetUnterOrdner(DirectoryInfo ordner)
        {
            unterOrdner = Directory.GetDirectories(ordner.FullName);
        }

        private void SetDateien(DirectoryInfo ordner)
        {
            dateien = Directory.GetFiles(ordner.FullName);
        }

        #endregion

        private static void PrintColored(string message, ConsoleColor foreground, ConsoleColor background = ConsoleColor.Black)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
            Console.Write(message);
            Console.ResetColor();
        }

    }
}
