﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
	class Program
	{
		static void Main(string[] args)
		{

			HumanPlayer human1 = new HumanPlayer ("Adam");
			HumanPlayer human2 = new HumanPlayer ("Bob");

			BotPlayer bot1 = new BotPlayer ("DBfirst");
			BotPlayer bot2 = new BotPlayer ("DBsecond");

			Game gameOne = new Game(human1,human2,1);
            Player winner = gameOne.Start();
            Console.SetCursorPosition(0, 0);


            // win message
            string winMessage = string.Empty;
            if (winner != null)
            {
                winMessage = $"the winner is {winner.Name}";
            }
            else
            {
                winMessage = "boring draw";
            }
            Console.WriteLine(winMessage);


            Console.WriteLine("the end is near ...");          
			Console.ReadKey(true);
		}
        
	}
}