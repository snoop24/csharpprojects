﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

namespace ConnectFour
{
    //moves boardStates
    //0 1
    //1 7
    //2 49
    //3 238
    //4 1120
    //5 4263
    //6 16422
    //7 54859
    //8 184275
    //9 558186
    //10 1662623
    //11 4568683
    //12 12236101
    //13 30929111
    //14 75437595
    //15 176541259
    //16 394591391
    //17 858218743
    //18 1763883894
    //19 3568259802
    //20 6746155945
    //21 12673345045
    //22 22010823988
    //23 38263228189
    //24 60830813459
    //25 97266114959
    //26 140728569039
    //27 205289508055
    //28 268057611944
    //29 352626845666
    //30 410378505447
    //31 479206477733
    //32 488906447183
    //33 496636890702
    //34 433471730336
    //35 370947887723
    //36 266313901222
    //37 183615682381
    //38 104004465349
    //39 55156010773
    //40 22695896495
    //41 7811825938
    //42 1459332899


    [DataContract]
    class BotPlayer : Player
    {
        string dataFolder = Directory.GetCurrentDirectory() + @"\\data\\";
        string DataFile { get { return dataFolder + Name + ".dat"; } }
        string DataFileBak { get { return dataFolder + Name + DateTime.Now.ToString("s") + ".dat"; } }

        [DataMember]
        Dictionary<string, int[]> boardStateToMoveValue;

        public BotPlayer(string name)
            : base(name)
        {
        }

        public override int NextMove(Board board)
        {
            return DiceMoveForCurrentState(board);
        }

        private void InitDataList()
        {
            try
            {
                DataContractSerializer js = new DataContractSerializer(typeof(Dictionary<string, int[]>));

                using (FileStream fs = File.OpenRead(DataFile))
                {
                    boardStateToMoveValue = (Dictionary<string, int[]>)js.ReadObject(fs);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void SaveDataList()
        {
            try
            {
                // backup old data
                File.Move(DataFile, DataFileBak);

                DataContractSerializer js = new DataContractSerializer(typeof(Dictionary<string, int[]>));

                using (FileStream fs = File.Create(DataFile))
                {
                    js.WriteObject(fs, boardStateToMoveValue);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private int DiceMoveForCurrentState(Board board)
        {
            int bestMove = 0;


            return bestMove;
        }
    }
}
