﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    abstract class Player
    {
        public string Name { get; }

		public Player(string name)
        {
            Name = name;
        }
        
		public abstract int NextMove (Board board);
    }

    class HumanPlayer : Player
    {
        public HumanPlayer(string name)
            :base(name)
        {
        }

		public override int NextMove(Board board)
        {
            int result = 0;
            do
            {
                Console.SetCursorPosition(0, 2);
				Console.WriteLine(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(1, 2);
                Console.Write($"Next Move >{Name}<: ");
            } while (!int.TryParse(Console.ReadLine(),out result));
            return result-1;
        }
    }
}
