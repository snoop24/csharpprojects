﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    #region: board with console output functionality
    class OutBoard : Board
    {
        int width;
        int height;

        int borderWidth = 2;
        int borderHeight = 1;

        int offLeft = 1;
        int offTop = 5;

        ConsoleColor color = ConsoleColor.Blue;
        ConsoleColor[] playerColor = { ConsoleColor.Yellow, ConsoleColor.Red };

        #region: constructor
        public OutBoard() : base()
        {
            width = 7 + 8 * borderWidth;
            height = 6 + 7 * borderHeight;
        }
        #endregion

        #region: InitialDisplay
        internal override void PrintInitBoard()
        {
            Console.CursorVisible = false;
            PrintBoardBackground();
            PrintEmptyFields();
        }

        protected override void PrintBoardBackground()
        {
            Console.BackgroundColor = color;
            string back = new string(' ', width);
            for (int i = 0; i < height; i++)
            {
                Console.SetCursorPosition(offLeft, offTop + i);
                Console.WriteLine(back);
            }
            Console.ResetColor();
        }

        protected override void PrintEmptyFields()
        {
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    PrintField(i, j);
                }
            }
        }
        #endregion

        protected override void PrintField(int left, int top, ConsoleColor coinColor = ConsoleColor.Black)
        {
            Console.ForegroundColor = coinColor;
            Console.BackgroundColor = color;
            top = 5 - top;
            Console.SetCursorPosition(offLeft + borderWidth + left * (1 + borderWidth), offTop + borderHeight + top * (1 + borderHeight));
            Console.Write('█');
            Console.ResetColor();
        }

        public override bool Move(int row)
        {
            bool validMove = false;

            validMove = base.Move(row);

            if (validMove)
            {
                PrintField(row, RowCount[row] - 1, playerColor[nextPlayer]);
            }

            return validMove;
        }

    }
    #endregion

    class Board
    {
        char[] rows;
        public char[] Rows { get { return rows; } }

        int[] rowCount;
        public int[] RowCount { get { return rowCount; } }

        protected int nextPlayer;

        int winPlayer;
        public int WinPlayer { get { return winPlayer; } }


        public bool End
        {
            get
            {
                return ((winPlayer != -1) || (42 == RowCount.Sum()));
            }

        }

        public Board()
        {
            rows = new char[42] {
                  ' ',' ',' ',' ',' ',' '
                , ' ',' ',' ',' ',' ',' '
                , ' ',' ',' ',' ',' ',' '
                , ' ',' ',' ',' ',' ',' '
                , ' ',' ',' ',' ',' ',' '
                , ' ',' ',' ',' ',' ',' '
                , ' ',' ',' ',' ',' ',' '
            };
            rowCount = new int[7] { 0, 0, 0, 0, 0, 0, 0 };

            nextPlayer = 0;
            winPlayer = -1;
        }

        public override string ToString()
        {
            StringBuilder boardToString = new StringBuilder();

            boardToString.Append(rowCount.Sum().ToString() + ';');

            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i] != ' ')
                {
                    boardToString.Append(rows[i]);
                }
                if (i % 6 == 5 && i < 41)
                {
                    boardToString.Append('|');
                }
            }
            return boardToString.ToString();

            //return new string(rows);
        }

        public virtual bool Move(int row)
        {
            bool validMove = false;

            if (-1 < row && row < 7 && rowCount[row] < 6)
            {
                validMove = true;
                rows[row * 6 + rowCount[row]] = (char)(nextPlayer + 48);
                rowCount[row]++;
                nextPlayer = (nextPlayer + 1) % 2;

                WinConditionMetForPlayer();
            }


            return validMove;
        }

        public void WinConditionMetForPlayer()
        {
            if (winPlayer == -1)
            {
                for (int i = 0; i < rows.Length; i++)
                {
                    if (rows[i] != ' ')
                    {
                        if (
                        // horizontal
                        (i < 24 && rows[i] == rows[i + 6] && rows[i] == rows[i + 12] && rows[i] == rows[i + 18])
                        ||
                        // vertical
                        ((i % 6) < 3 && rows[i] == rows[i + 1] && rows[i] == rows[i + 2] && rows[i] == rows[i + 3])
                        ||
                        // diag up
                        ((i % 6) < 3 && i < 24 && rows[i] == rows[i + 7] && rows[i] == rows[i + 14] && rows[i] == rows[i + 21])
                        ||
                        // diag down
                        ((i % 6) > 2 && i < 24 && rows[i] == rows[i + 5] && rows[i] == rows[i + 10] && rows[i] == rows[i + 15])
                        )
                        {
                            winPlayer = (int)rows[i] - 48;
                            i = 42;
                        }
                    }
                }
            }
        }

        #region: out methods

        #region: InitialDisplay
        internal virtual void PrintInitBoard() { }

        protected virtual void PrintBoardBackground() { }

        protected virtual void PrintEmptyFields() { }
        #endregion

        protected virtual void PrintField(int left, int top, ConsoleColor coinColor = ConsoleColor.Black) { }
        #endregion
    }
}