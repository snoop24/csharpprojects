﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    class Game {
        Board board;
        Player p1;
        Player p2;
        int startPlayer;
        Player nextPlayer;

        bool slowed = false;

        public Game(Player p1, Player p2, int startPlayer =0 , bool visible=true, bool slowed=false) {
			
			board = (visible ? new OutBoard(): new Board());
			this.p1 = p1;
			this.p2 = p2;

            this.startPlayer = startPlayer;
            nextPlayer = (startPlayer ==0? p1: p2);


            this.slowed = slowed;
        }

        public Player Start() {
            board.PrintInitBoard();
            while (!board.End) {
                int move = nextPlayer.NextMove(board);

                if (board.Move(move)) {
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine(move);
					if (slowed) {
						System.Threading.Thread.Sleep (200);
					}
                    nextPlayer = (nextPlayer == p1 ? p2 : p1);

                    Console.SetCursorPosition(0,20);
                    Console.WriteLine(board.ToString());
                }
            }
            // startPlayer + board.WinPlayer mod 2 = 0 if first player wins 
            Player winner = null;
            if (board.WinPlayer != -1)
            {
                winner = ((startPlayer + board.WinPlayer) % 2 == 0 ? p1 : p2);
            }
            return winner;
        }
    }
}
